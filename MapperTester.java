import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MapperTester {
    public static void main(String[] args) {
	try {
	    byte[] buf = new String("Mapper:MAP:w:Name:Desc:[n,e,s,w]").getBytes(); //$NON-NLS-1$
	    DatagramSocket socket = new DatagramSocket();
	    InetAddress address = InetAddress.getByName("127.0.0.1"); //$NON-NLS-1$
	    DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 4444);
	    socket.send(packet);
	} catch (Exception e) {
	    System.out.println("Ooops! " + e.toString()); //$NON-NLS-1$
	    e.printStackTrace();
	}

    }

}
