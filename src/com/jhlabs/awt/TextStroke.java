/**
Copyright 2006 Jerry Huxtable (heavily modified by Ecthelion)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.jhlabs.awt;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

public class TextStroke implements Stroke {
	private String					text;
	private final Font				font;
	private boolean					stretchToFit	= false;
	private boolean					repeat			= false;
	private final AffineTransform	t				= new AffineTransform();
	private final boolean			center;
	private final int				numberOfRepeats;

	private static final float		FLATNESS		= 1;

	public TextStroke(String text, Font font) {
		this(text, font, true, false, false, 1);
	}

	public TextStroke(String text, Font font, boolean stretchToFit, boolean repeat, boolean center, int numberOfRepeats) {
		this.text = text;
		this.font = font;
		this.stretchToFit = stretchToFit;
		this.repeat = repeat;
		this.center = center;
		this.numberOfRepeats = numberOfRepeats;
	}

	@Override
	public Shape createStrokedShape(Shape shape) {
		FontRenderContext frc = new FontRenderContext(null, true, true);
		boolean reversed = shouldReversed(shape);
		if (reversed) {
			char[] newText = new char[text.length()];
			int i = text.length() - 1;
			for (char c : text.toCharArray()) {
				newText[i--] = c;
			}
			text = new String(newText);
		}
		GlyphVector glyphVector = font.createGlyphVector(frc, text);
		GeneralPath result = new GeneralPath();
		PathIterator it = new FlatteningPathIterator(shape.getPathIterator(null), FLATNESS);
		float points[] = new float[6];
		float moveX = 0, moveY = 0;
		float lastX = 0, lastY = 0;
		float thisX = 0, thisY = 0;
		int type = 0;
		float next = 0;
		int currentChar = 0;
		int length = glyphVector.getNumGlyphs();
		if (length == 0) return result;

		float factor = stretchToFit ? measurePathLength(shape) / (float) glyphVector.getLogicalBounds().getWidth() : 1.0f;
		float nextAdvance = 0;
		float lengthOfSegment = (Math.abs(measurePathLength(shape)) / numberOfRepeats);
		int repetitions = numberOfRepeats;
		while (currentChar < length && !it.isDone()) {
			type = it.currentSegment(points);
			switch (type) {
				case PathIterator.SEG_MOVETO:
					moveX = lastX = points[0];
					moveY = lastY = points[1];
					result.moveTo(moveX, moveY);
					nextAdvance = glyphVector.getGlyphMetrics(currentChar).getAdvance() * 0.5f;
					next = (center ? lengthOfSegment / 2 - ((nextAdvance * (length + 1)) / 2) : nextAdvance);
					break;

				case PathIterator.SEG_CLOSE:
					points[0] = moveX;
					points[1] = moveY;
					// Fall into....

				case PathIterator.SEG_LINETO:
					thisX = points[0];
					thisY = points[1];
					float dx = thisX - lastX;
					float dy = thisY - lastY;
					float distance = (float) Math.sqrt(dx * dx + dy * dy);
					if (distance >= next) {
						float r = 1.0f / distance;
						float angle = (float) Math.atan2(dy, dx);
						if (reversed) {
							angle -= Math.PI;
						}
						while (currentChar < length && distance >= next) {
							Shape glyph = glyphVector.getGlyphOutline(currentChar);
							Point2D p = glyphVector.getGlyphPosition(currentChar);
							float px = (float) p.getX();
							float py = (float) p.getY();
							float x = lastX + next * dx * r;
							float y = lastY + next * dy * r;
							float advance = nextAdvance;
							nextAdvance = currentChar < length - 1 ? glyphVector.getGlyphMetrics(currentChar + 1).getAdvance() * 0.5f : glyphVector.getGlyphMetrics(currentChar
																																											% length)
																																					.getAdvance() * 0.5f;
							t.setToTranslation(x, y);
							t.rotate(angle);
							t.translate(-px - advance, -py);
							result.append(t.createTransformedShape(glyph), false);
							next += (advance + nextAdvance) * factor;
							currentChar++;

							if ((currentChar % length) == 0) {
								repetitions--;
								if (repetitions > 0) {
									currentChar %= length;
									next += (lengthOfSegment - glyphVector.getLogicalBounds().getWidth());
								}
							}
						}
					}

					next -= distance;
					lastX = thisX;
					lastY = thisY;
					if (repeat) currentChar %= length;
					break;
			}
			it.next();
		}
		return result;
	}

	private static boolean shouldReversed(Shape shape) {
		PathIterator it = new FlatteningPathIterator(shape.getPathIterator(null), FLATNESS);
		float points[] = new float[6];
		float moveX = 0, moveY = 0;
		float lastX = 0;
		float thisX = 0;
		float firstX = 0;
		int type = 0;

		while (!it.isDone()) {
			type = it.currentSegment(points);
			switch (type) {
				case PathIterator.SEG_MOVETO:
					moveX = lastX = firstX = points[0];
					moveY = points[1];
					break;

				case PathIterator.SEG_CLOSE:
					points[0] = moveX;
					points[1] = moveY;
					// Fall into....

				case PathIterator.SEG_LINETO:
					thisX = points[0];
					lastX = thisX;
					break;
			}
			it.next();
		}

		if ((firstX - lastX) > 0) return true;

		return false;
	}

	public static float measurePathLength(Shape shape) {
		PathIterator it = new FlatteningPathIterator(shape.getPathIterator(null), FLATNESS);
		float points[] = new float[6];
		float moveX = 0, moveY = 0;
		float lastX = 0, lastY = 0;
		float thisX = 0, thisY = 0;
		int type = 0;
		float total = 0;

		while (!it.isDone()) {
			type = it.currentSegment(points);
			switch (type) {
				case PathIterator.SEG_MOVETO:
					moveX = lastX = points[0];
					moveY = lastY = points[1];
					break;

				case PathIterator.SEG_CLOSE:
					points[0] = moveX;
					points[1] = moveY;
					// Fall into....

				case PathIterator.SEG_LINETO:
					thisX = points[0];
					thisY = points[1];
					float dx = thisX - lastX;
					float dy = thisY - lastY;
					total += (float) Math.sqrt(dx * dx + dy * dy);
					lastX = thisX;
					lastY = thisY;
					break;
			}
			it.next();
		}

		return total;
	}

	public Font getFont() {
		return font;
	}

	public static void drawLabel(Graphics2D g, Shape shape, boolean repeat, Font font, String text, int numberOfRepeats, boolean stretchToFit, boolean center) {
		FontRenderContext frc = new FontRenderContext(null, true, true);
		AffineTransform t = new AffineTransform();
		boolean reversed = shouldReversed(shape);
		if (reversed) {
			char[] newText = new char[text.length()];
			int i = text.length() - 1;
			for (char c : text.toCharArray()) {
				newText[i--] = c;
			}
			text = new String(newText);
		}
		GlyphVector glyphVector = font.createGlyphVector(frc, text);
		GeneralPath result = new GeneralPath();
		PathIterator it = new FlatteningPathIterator(shape.getPathIterator(null), FLATNESS);
		float points[] = new float[6];
		float moveX = 0, moveY = 0;
		float lastX = 0, lastY = 0;
		float thisX = 0, thisY = 0;
		int type = 0;
		float next = 0;
		int currentChar = 0;
		int length = glyphVector.getNumGlyphs();
		if (length == 0) return;

		float factor = stretchToFit ? measurePathLength(shape) / (float) glyphVector.getLogicalBounds().getWidth() : 1.0f;
		float nextAdvance = 0;
		float lengthOfSegment = (Math.abs(measurePathLength(shape)) / numberOfRepeats);
		int repetitions = numberOfRepeats;
		while (currentChar < length && !it.isDone()) {
			type = it.currentSegment(points);
			switch (type) {
				case PathIterator.SEG_MOVETO:
					moveX = lastX = points[0];
					moveY = lastY = points[1];
					result.moveTo(moveX, moveY);
					nextAdvance = glyphVector.getGlyphMetrics(currentChar).getAdvance() * 0.5f;
					next = (center ? lengthOfSegment / 2 - ((nextAdvance * (length + 1)) / 2) : nextAdvance);
					break;

				case PathIterator.SEG_CLOSE:
					points[0] = moveX;
					points[1] = moveY;
					// Fall into....

				case PathIterator.SEG_LINETO:
					thisX = points[0];
					thisY = points[1];
					float dx = thisX - lastX;
					float dy = thisY - lastY;
					float distance = (float) Math.sqrt(dx * dx + dy * dy);
					if (distance >= next) {
						float r = 1.0f / distance;
						float angle = (float) Math.atan2(dy, dx);
						if (reversed) {
							angle -= Math.PI;
						}
						while (currentChar < length && distance >= next) {
							Shape glyph = glyphVector.getGlyphOutline(currentChar);
							Point2D p = glyphVector.getGlyphPosition(currentChar);
							float px = (float) p.getX();
							float py = (float) p.getY();
							float x = lastX + next * dx * r;
							float y = lastY + next * dy * r;
							float advance = nextAdvance;
							nextAdvance = currentChar < length - 1 ? glyphVector.getGlyphMetrics(currentChar + 1).getAdvance() * 0.5f : glyphVector.getGlyphMetrics(currentChar
																																											% length)
																																					.getAdvance() * 0.5f;
							t.setToTranslation(x, y);
							t.rotate(angle);
							t.translate(-px - advance, -py);
							g.setTransform(t);
							g.draw(glyph);
							g.fill(glyph);
							// result.append(t.createTransformedShape(glyph),
							// false);
							next += (advance + nextAdvance) * factor;
							currentChar++;

							if ((currentChar % length) == 0) {
								repetitions--;
								if (repetitions > 0) {
									currentChar %= length;
									next += (lengthOfSegment - glyphVector.getLogicalBounds().getWidth());
								}
							}
						}
					}

					next -= distance;
					lastX = thisX;
					lastY = thisY;
					if (repeat) currentChar %= length;
					break;
			}
			it.next();
		}

	}

}
