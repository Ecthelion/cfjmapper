/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Room;

/**
 * 
 * @author Ecthelion
 * 
 */
class ColourChooser extends JDialog {
	/**
	 * 
	 */
	private static final long			serialVersionUID	= 8129462389682431192L;
	private JPanel						buttonPanel, colourChooserPanel;
	private ArrayList<RoomColourHandle>	changedHandles;
	private JLabel						colourLabel, colourSquare, textLabel, typeLabel;
	private RoomColourHandle			currentHandle;
	private Database					database;
	private JButton						deleteButton, closeButton, addButton, updateWholeMapButton, applyButton;
	private final Mapper				mapper;
	private JList<RoomColourHandle>		nameList;

	private JTextField					textField;
	private JComboBox<String>			typeComboBox;

	public ColourChooser(Mapper mapper) {
		super(mapper, true);
		this.mapper = mapper;
		initiateComponents();
	}

	public ColourChooser(Mapper mapper, String name) {
		super(mapper, false);
		this.mapper = mapper;
		initiateComponents();
		ColourEdit ce = new ColourEdit(mapper, name);
		RoomColourHandle tmpHandle = ce.getResult();
		System.out.println(tmpHandle);
		if (tmpHandle != null) {
			RoomColour.addColour(tmpHandle);
		}
		RoomColourHandle[] handles = new RoomColourHandle[database.getRoomColourHandles().size()];
		handles = database.getRoomColourHandles().toArray(handles);
		nameList.setListData(handles);
	}

	private void deleteButtonActionPerformed() {
		if (JOptionPane.showConfirmDialog(this, Messages.getString("RoomColour.15"), Messages.getString("RoomColour.16"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) //$NON-NLS-1$ //$NON-NLS-2$
																																												database.deleteRoomColour(nameList.getSelectedValue());

		RoomColourHandle[] handles = new RoomColourHandle[database.getRoomColourHandles().size()];
		handles = database.getRoomColourHandles().toArray(handles);
		nameList.setListData(handles);
	}

	private void initiateComponents() {
		setIconImage(Mapper.getIcon().getImage());
		setTitle(Messages.getString("RoomColour.5")); //$NON-NLS-1$
		setLayout(new BorderLayout());
		database = Database.getInstance();
		RoomColourHandle[] handles = new RoomColourHandle[database.getRoomColourHandles().size()];
		changedHandles = new ArrayList<RoomColourHandle>();
		handles = database.getRoomColourHandles().toArray(handles);
		nameList = new JList<RoomColourHandle>(handles);
		nameList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				currentHandle = nameList.getSelectedValue();
				updateColourChooserPanel();
				applyButton.setEnabled(false);
			}
		});
		nameList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(nameList);
		scrollPane.setPreferredSize(new Dimension(200, 100));
		scrollPane.setSize(new Dimension(200, 100));
		deleteButton = new JButton(Messages.getString("RoomColour.6")); //$NON-NLS-1$
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				deleteButtonActionPerformed();
			}
		});
		closeButton = new JButton(Messages.getString("RoomColour.7")); //$NON-NLS-1$
		updateWholeMapButton = new JButton(Messages.getString("RoomColour.8")); //$NON-NLS-1$
		updateWholeMapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				for (RoomColourHandle handle : changedHandles) {
					database.updateColoursForRoomName(handle);
				}
				changedHandles.clear();
				// database.updateAllRoomColours();
				mapper.updateMainMap();
			}
		});
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setVisible(false);
				dispose();
			}
		});
		addButton = new JButton(Messages.getString("RoomColour.9")); //$NON-NLS-1$
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				ColourEdit ce = new ColourEdit(mapper);
				RoomColourHandle tmpHandle = ce.getResult();
				System.out.println(tmpHandle);
				if (tmpHandle != null) {
					RoomColour.addColour(tmpHandle);
				}
				RoomColourHandle[] handles = new RoomColourHandle[database.getRoomColourHandles().size()];
				handles = database.getRoomColourHandles().toArray(handles);
				nameList.setListData(handles);
			}
		});
		applyButton = new JButton(Messages.getString("RoomColour.10")); //$NON-NLS-1$
		applyButton.setEnabled(false);
		applyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				updateHandle();
				applyButton.setEnabled(false);
			}
		});
		colourChooserPanel = new JPanel();
		colourLabel = new JLabel(Messages.getString("RoomColour.11")); //$NON-NLS-1$
		colourSquare = new JLabel();
		colourSquare.setOpaque(true);
		colourSquare.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				if (nameList.getSelectedIndex() >= 0) {
					mapper.getColourChooser(colourSquare.getBackground(), null);

					Mapper.oldColor = mapper.getCurrentColour();
					//Color c = JColorChooser.showDialog(ColourChooser.this, Messages.getString("RoomColour.12"), currentHandle.getColour()); //$NON-NLS-1$
					if (mapper.getCurrentColour() != null) {
						colourSquare.setBackground(mapper.getCurrentColour());
						applyButton.setEnabled(true);
					}
				}
			}
		});
		colourSquare.setBackground(Color.LIGHT_GRAY);
		textLabel = new JLabel(Messages.getString("RoomColour.13")); //$NON-NLS-1$
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent ke) {
				applyButton.setEnabled(true);
			}
		});
		typeLabel = new JLabel(Messages.getString("RoomColour.14")); //$NON-NLS-1$
		typeComboBox = new JComboBox<String>(RoomColour.choices);
		typeComboBox.setSelectedIndex(3);
		typeComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				applyButton.setEnabled(true);
			}
		});
		colourChooserPanel.setLayout(new GridLayout(0, 2));
		colourChooserPanel.add(textLabel);
		colourChooserPanel.add(textField);
		colourChooserPanel.add(colourLabel);
		colourChooserPanel.add(colourSquare);
		colourChooserPanel.add(typeLabel);
		colourChooserPanel.add(typeComboBox);
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		buttonPanel.add(addButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(applyButton);
		buttonPanel.add(updateWholeMapButton);
		buttonPanel.add(closeButton);
		add(scrollPane, BorderLayout.WEST);
		add(colourChooserPanel, BorderLayout.EAST);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
		setLocationRelativeTo(mapper);

	}

	protected void updateColourChooserPanel() {
		if (currentHandle != null) {
			textField.setText(currentHandle.getText());
			colourSquare.setBackground(currentHandle.getColour());
			typeComboBox.setSelectedIndex(currentHandle.getType());
		}
	}

	protected void updateHandle() {
		currentHandle.setText(textField.getText());
		currentHandle.setColour(colourSquare.getBackground());
		currentHandle.setType(typeComboBox.getSelectedIndex());
		changedHandles.add(currentHandle);
		database.updateRoomColourHandle(currentHandle);
	}
}

class ColourEdit extends JDialog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 189325191366129938L;
	private JPanel				buttonPanel, editPanel;
	private JButton				okButton, cancelButton;
	private final Mapper		parent;
	private JTextField			textField;
	private JLabel				textLabel, colourLabel, typeLabel, colourBoxLabel;
	private RoomColourHandle	theHandleToReturn	= null;
	private String				triggerLine			= Messages.getString("RoomColour.17");	//$NON-NLS-1$
	private JComboBox<String>	typeComboBox;

	public ColourEdit(Mapper parent) {
		super(parent);
		this.parent = parent;
		initiateComponents();
	}

	public ColourEdit(Mapper parent, String triggerLine) {
		super(parent);
		this.triggerLine = triggerLine;
		this.parent = parent;
		initiateComponents();
	}

	public RoomColourHandle getResult() {
		return theHandleToReturn;
	}

	private void initiateComponents() {
		setLocationRelativeTo(parent);
		setModal(true);
		setIconImage(Mapper.getIcon().getImage());
		buttonPanel = new JPanel();
		editPanel = new JPanel();
		okButton = new JButton(Messages.getString("RoomColour.18")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				theHandleToReturn = new RoomColourHandle(textField.getText(), colourBoxLabel.getBackground(), typeComboBox.getSelectedIndex());
				setVisible(false);
			}
		});
		cancelButton = new JButton(Messages.getString("RoomColour.19")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				theHandleToReturn = null;
				setVisible(false);
			}
		});
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		textLabel = new JLabel(Messages.getString("RoomColour.20")); //$NON-NLS-1$
		textField = new JTextField(triggerLine);
		colourLabel = new JLabel(Messages.getString("RoomColour.21")); //$NON-NLS-1$
		typeLabel = new JLabel(Messages.getString("RoomColour.22")); //$NON-NLS-1$
		typeComboBox = new JComboBox<String>(RoomColour.choices);
		typeComboBox.setSelectedIndex(3);
		colourBoxLabel = new JLabel(Messages.getString("RoomColour.23")); //$NON-NLS-1$
		colourBoxLabel.setOpaque(true);
		colourBoxLabel.setBackground(Color.LIGHT_GRAY);
		colourBoxLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				parent.getColourChooser(colourBoxLabel.getBackground(), null);
				Color c = parent.getCurrentColour();
				if (c != null) {
					colourBoxLabel.setBackground(c);
				}
			}
		});
		editPanel.setLayout(new GridLayout(0, 2));
		editPanel.add(textLabel);
		editPanel.add(textField);
		editPanel.add(colourLabel);
		editPanel.add(colourBoxLabel);
		editPanel.add(typeLabel);
		editPanel.add(typeComboBox);
		setLayout(new BorderLayout());
		add(editPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}

	public void mouseClicked(MouseEvent me) {}
}

public class RoomColour {

	protected static String[]					choices			= {
			Messages.getString("RoomColour.0"), Messages.getString("RoomColour.1"), Messages.getString("RoomColour.2"), Messages.getString("RoomColour.3"), Messages.getString("RoomColour.4") };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	private static ColourChooser				colourChooser;
	private static ArrayList<RoomColourHandle>	colours			= null;
	// private static Color toReturn;
	private static Database						database;
	protected static Color						defaultColour	= Color.LIGHT_GRAY;

	public static void addColour(RoomColourHandle colourHandle) {
		if (database == null) database = Database.getInstance();
		if (colours == null) colours = database.getRoomColourHandles();
		database.addColourForRoomType(colourHandle);
		colours = database.getRoomColourHandles();
	}

	public static void addColour(String name, Color colour, int type) {
		if (database == null) database = Database.getInstance();
		if (colours == null) colours = database.getRoomColourHandles();

		RoomColourHandle colourHandle = new RoomColourHandle(name, colour, type);
		database.addColourForRoomType(colourHandle);
		colours = database.getRoomColourHandles();
	}

	/**
	 * public final static int STARTS_WITH = 0; public final static int
	 * ENDS_WITH = 1; public final static int CONTAINS = 2; public final static
	 * int EXACTLY = 3; public final static int MATCHES = 4;
	 */
	public static Color getColorForName(String name) {
		if (database == null) database = Database.getInstance();
		if (colours == null) colours = database.getRoomColourHandles();
		Color toReturn = null;
		for (RoomColourHandle handle : colours) {
			switch (handle.getType()) {
				case 0:
					if (name.trim().startsWith(handle.getText().trim())) toReturn = handle.getColour();
					break;
				case 1:
					if (name.trim().endsWith(handle.getText().trim())) toReturn = handle.getColour();
					break;
				case 2:
					if (name.trim().contains(handle.getText().trim())) toReturn = handle.getColour();
					break;
				case 3:
					if (name.trim().equals(handle.getText().trim())) toReturn = handle.getColour();
					break;
				case 4:
					if (name.trim().matches(handle.getText().trim())) toReturn = handle.getColour();
					break;
			}
		}
		if (toReturn != null) {
			return toReturn;
		} else {
			return Room.getDefaultRoomColour();
		}
	}

	public static RoomColourHandle showAddColourDialog(Mapper window, String name) {
		ColourEdit colourEdit = new ColourEdit(window, name);
		return colourEdit.getResult();
	}

	public static void showColourChooser(Mapper mapper) {
		colourChooser = new ColourChooser(mapper);
		colourChooser.setVisible(true);
	}

	public static void updateRoomColours() {
		if (database == null) database = Database.getInstance();
		colours = database.getRoomColourHandles();
	}

}
