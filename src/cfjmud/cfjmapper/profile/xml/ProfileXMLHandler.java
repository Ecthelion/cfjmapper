/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.profile.xml;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import cfjmud.cfjmapper.profile.User;

/**
 * @author Ecthelion
 * 
 */
public class ProfileXMLHandler extends DefaultHandler {

	StringBuffer			textBuffer;
	private final boolean	debugga				= false;
	String					currentElement		= "";		//$NON-NLS-1$
	String					currentMainElement	= "";		//$NON-NLS-1$
	User					currentUser;
	ArrayList<User>			userList;

	public ProfileXMLHandler() {
		userList = new ArrayList<User>();
	}

	@Override
	public void characters(char buf[], int offset, int len) throws SAXException {
		String s = new String(buf, offset, len);
		if (textBuffer == null) {
			textBuffer = new StringBuffer(s);
		} else {
			textBuffer.append(s);
		}
	}

	private void debug(String s) throws SAXException {
		if (debugga) {
			System.out.print(s);
			System.out.flush();
		}
	}

	private void echoText() throws SAXException {
		if (textBuffer == null) return;
		nl();
		debug("CHARS:   "); //$NON-NLS-1$

		String s = "" + textBuffer; //$NON-NLS-1$
		debug("The Current Element: " + currentElement + " - " + s + " "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (!s.trim().equals("")) { //$NON-NLS-1$
			debug("The Current Element: " + currentElement + s); //$NON-NLS-1$
			/* ********************* USER ********************* */
			if (currentElement.equalsIgnoreCase("username")) currentUser.setUserName(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomid")) currentUser.setCurrentRoomId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaid")) currentUser.setCurrentAreaId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("username")) currentUser.setUserName(s); //$NON-NLS-1$
			/* ************************************************ */

			// debug("|");
		}

		textBuffer = null;
	}

	@Override
	public void endDocument() throws SAXException {
		nl();
		debug("END DOCUMENT"); //$NON-NLS-1$

	}

	@Override
	public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
		echoText();
		nl();
		debug("END_ELM: "); //$NON-NLS-1$

		String eName = sName; // element name
		if ("".equals(eName)) eName = qName; // not namespace-aware //$NON-NLS-1$

		if (eName.equalsIgnoreCase("user")) { //$NON-NLS-1$
			debug("Adding area to Vector"); //$NON-NLS-1$
			userList.add(currentUser);
		}
		debug("</" + eName + ">"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public void startDocument() throws SAXException {
		debug("START DOCUMENT"); //$NON-NLS-1$
		debug("<?xml version='1.0' encoding='UTF-8'?>"); //$NON-NLS-1$
	}

	private void nl() throws SAXException {
		String lineEnd = System.getProperty("line.separator"); //$NON-NLS-1$
		debug(lineEnd);
	}

	@Override
	public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
		echoText();
		nl();
		debug("ELEMENT: "); //$NON-NLS-1$
		String eName = sName; // element name
		if ("".equals(eName)) //$NON-NLS-1$
								eName = qName; // not namespace-aware

		if (eName.equalsIgnoreCase("user")) { //$NON-NLS-1$
			debug("Creating New Room."); //$NON-NLS-1$
			currentUser = new User();
		}
		currentElement = eName;

		debug("<" + eName); //$NON-NLS-1$
		debug(">"); //$NON-NLS-1$
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

}
