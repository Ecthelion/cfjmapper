/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.profile;

import java.util.Comparator;

/**
 * 
 * @author Ecthelion
 * 
 */
public class User implements Comparable<User>, Comparator<User> {
	private String			userName;
	private int				currentAreaId;
	private int				currentRoomId;
	private final String	dtab	= "\t\t";	//$NON-NLS-1$
	private final String	eol		= "\n";	//$NON-NLS-1$
	private final String	tab		= "\t";	//$NON-NLS-1$

	public User() {}

	public User(String userName, int currentAreaId, int currentRoomId) {
		this.userName = userName;
		this.currentAreaId = currentAreaId;
		this.currentRoomId = currentRoomId;
	}

	public String toXml() {
		String toReturn = tab + "<user>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<username>" + getUserName() + "</username>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaid>" + getCurrentAreaId() + "</areaid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomid>" + getCurrentRoomId() + "</roomid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "</user>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the currentAreaId
	 */
	public int getCurrentAreaId() {
		return currentAreaId;
	}

	/**
	 * @param currentAreaId
	 *            the currentAreaId to set
	 */
	public void setCurrentAreaId(int currentAreaId) {
		this.currentAreaId = currentAreaId;
	}

	/**
	 * @return the currentRoomId
	 */
	public int getCurrentRoomId() {
		return currentRoomId;
	}

	/**
	 * @param currentRoomId
	 *            the currentRoomId to set
	 */
	public void setCurrentRoomId(int currentRoomId) {
		this.currentRoomId = currentRoomId;
	}

	@Override
	public String toString() {
		return userName;
	}

	@Override
	public int compareTo(User user) {
		return getUserName().compareTo(user.getUserName());
	}

	@Override
	public int compare(User o1, User o2) {
		return o1.compareTo(o2);
	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof User) {
			User user = (User) obj;
			return getUserName().equalsIgnoreCase(user.getUserName());
		} else {
			return false;
		}
	}
}
