/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.profile;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cfjmud.cfjmapper.Mapper;

/**
 * 
 * @author Ecthelion
 * 
 */
class ProfileManagerDialog extends JDialog implements PropertyChangeListener, ListSelectionListener {

	private JComboBox<User>			userList;
	private final ProfileManager	profileManager;
	private JButton					addButton, cancelButton, okButton, applyButton;
	private JFormattedTextField		roomIdTF, areaIdTF, nameTF;
	private JLabel					roomIdLabel, areaIdLabel, nameLabel;
	private final Mapper			mapper;
	/**
     * 
     */
	private static final long		serialVersionUID	= -3881301549551698950L;

	public ProfileManagerDialog(Mapper mapper, ProfileManager profileManager) {
		super(mapper);
		setModal(true);
		setTitle("Profile Manager"); //$NON-NLS-1$
		this.mapper = mapper;
		this.profileManager = profileManager;
		initiateComponents();
		setLocationRelativeTo(mapper);
	}

	private void initiateComponents() {
		ArrayList<User> userArrayList = profileManager.getUsers();
		System.out.println(userArrayList);
		User[] users = new User[userArrayList.size()];
		users = userArrayList.toArray(users);

		userList = new JComboBox<User>(users);
		userList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showUser();
			}

		});

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(gbl);

		addButton = new JButton("Add Character"); //$NON-NLS-1$
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String newName = JOptionPane.showInputDialog(ProfileManagerDialog.this, "Choose A Name:"); //$NON-NLS-1$
				if (newName != null) {
					profileManager.addUser(newName);
					ArrayList<User> list = profileManager.getUsers();
					User[] items = new User[list.size()];
					items = list.toArray(items);
					DefaultComboBoxModel<User> u = new DefaultComboBoxModel<User>(items);
					userList.setModel(u);
				}
			}
		});
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		okButton = new JButton("Set Profile"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				User toSend = ((User) userList.getSelectedItem());
				mapper.setCurrentProfile(userList.getSelectedIndex() == 0 ? null : toSend);
			}
		});
		applyButton = new JButton("Apply"); //$NON-NLS-1$
		applyButton.setEnabled(false);
		nameLabel = new JLabel("Name:"); //$NON-NLS-1$
		nameTF = new JFormattedTextField(""); //$NON-NLS-1$
		nameTF.setColumns(15);
		areaIdLabel = new JLabel("Current Area Id:"); //$NON-NLS-1$
		areaIdTF = new JFormattedTextField(new Integer(-1));
		areaIdTF.setColumns(15);
		roomIdLabel = new JLabel("Current Room Id:"); //$NON-NLS-1$
		roomIdTF = new JFormattedTextField(new Integer(-1));
		roomIdTF.setColumns(15);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(1, 1, 1, 1);
		add(userList, c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		add(nameLabel, c);
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 2;
		add(nameTF, c);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		add(areaIdLabel, c);
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 2;
		add(areaIdTF, c);
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		add(roomIdLabel, c);
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 2;
		add(roomIdTF, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 4;
		add(addButton, c);
		c.gridx = 1;
		add(applyButton, c);
		c.gridx = 2;
		add(okButton, c);
		c.gridx = 3;
		add(cancelButton, c);
		pack();
		userList.setSelectedIndex(profileManager.getCurrentUserIndex());
	}

	private void showUser() {
		User selected = (User) userList.getSelectedItem();
		if (selected == null) return;
		nameTF.setValue(selected.getUserName());
		areaIdTF.setValue(new Integer(selected.getCurrentAreaId()));
		roomIdTF.setValue(new Integer(selected.getCurrentRoomId()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		applyButton.setEnabled(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {}
}
