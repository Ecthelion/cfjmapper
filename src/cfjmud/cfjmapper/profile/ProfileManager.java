/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.profile;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.profile.xml.ProfileXMLHandler;

/**
 * @author Ecthelion
 * 
 */
public class ProfileManager {
	private final Mapper			mapper;
	private ProfileManagerDialog	dialog;
	private final String			fileName			= "profiles.xml";	//$NON-NLS-1$
	private ArrayList<User>			users;
	private int						currentUserIndex	= 0;

	public ProfileManager(Mapper mapper) {
		this.mapper = mapper;
		users = new ArrayList<User>();
		users.add(0, new User("<none>", -1, -1)); //$NON-NLS-1$
		users = loadUsers();
	}

	private ArrayList<User> loadUsers() {
		try {
			File file = new File(fileName);
			if (file.exists()) {
				ProfileXMLHandler handler = new ProfileXMLHandler();
				SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
				saxParser.parse(file, handler);
				users = handler.getUserList();
				users.add(0, new User("<none>", -1, -1)); //$NON-NLS-1$
				setCurrentUser(MapperProperties.getInstance().getProperty(MapperProperties.PROFILE));
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println("Failed to read profiles.xml: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return users;
	}

	public ProfileManagerDialog showDialog() {
		if (dialog == null) {
			dialog = new ProfileManagerDialog(mapper, this);
		}
		dialog.setVisible(true);
		return dialog;
	}

	public boolean hasProfile() {
		return !(getCurrentAreaId() != -1 || getCurrentRoomId() != -1 || users.isEmpty());
	}

	public ArrayList<User> getUsers() {
		return loadUsers();
	}

	public void setCurrentUser(String userName) {
		if (userName == null) {
			currentUserIndex = 0;
		}
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getUserName().equalsIgnoreCase(userName)) {
				currentUserIndex = i;
				break;
			}
		}
	}

	public void addUser(String userName) {
		users.add(new User(userName, mapper.getCurrentAreaId(), mapper.getCurrentRoomId()));
		saveProfilesAsXml();
	}

	public void updateUser(String userName, int currentAreaId, int currentRoomId) {
		for (User user : users) {
			if (user.getUserName().equalsIgnoreCase(userName)) {
				user.setCurrentAreaId(currentAreaId);
				user.setCurrentRoomId(currentRoomId);
			}
		}
		saveProfilesAsXml();
	}

	public void updateCurrentUser(int currentAreaId, int currentRoomId) {
		if (users.isEmpty()) return;
		if (currentUserIndex == 0) return;
		users.get(currentUserIndex).setCurrentAreaId(currentAreaId);
		users.get(currentUserIndex).setCurrentRoomId(currentRoomId);
	}

	public int getCurrentAreaId() {
		if (users.isEmpty()) return -1;
		if (currentUserIndex == 0) return -1;
		return users.get(currentUserIndex).getCurrentAreaId();
	}

	public int getCurrentRoomId() {
		if (users.isEmpty()) return -1;
		if (currentUserIndex == 0) return -1;
		return users.get(currentUserIndex).getCurrentRoomId();
	}

	public void setCurrentAreaId(int currentAreaId) {
		if (users.isEmpty()) return;
		if (currentUserIndex == 0) return;
		users.get(currentUserIndex).setCurrentAreaId(currentAreaId);
		saveProfilesAsXml();
	}

	public void setCurrentRoomId(int currentRoomId) {
		if (users.isEmpty()) return;
		if (currentUserIndex == 0) return;
		users.get(currentRoomId).setCurrentRoomId(currentRoomId);
		saveProfilesAsXml();
	}

	public void saveProfilesAsXml() {
		File file = new File(fileName);
		try {

			if (!file.exists()) file.createNewFile();
			PrintStream ps = new PrintStream(file);
			ps.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			ps.write("<root>\n".getBytes()); //$NON-NLS-1$
			for (User user : users) {
				if (user.getUserName().equalsIgnoreCase("<none>")) continue; //$NON-NLS-1$
				ps.write(user.toXml().getBytes());
			}
			ps.write("</root>\n".getBytes()); //$NON-NLS-1$
			ps.flush();
			ps.close();
		} catch (IOException e) {
			System.out.println("Failed to save profiles: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	public void setCurrentUser(User user) {
		if (user == null) {
			currentUserIndex = 0;
		} else {
			currentUserIndex = users.indexOf(user);
		}
		saveProfilesAsXml();
	}

	/**
	 * @return the currentUserIndex
	 */
	public int getCurrentUserIndex() {
		if (currentUserIndex < 1) {
			setCurrentUser(MapperProperties.getInstance().getProperty(MapperProperties.PROFILE));
		}
		return currentUserIndex;
	}

	/**
	 * @param currentUserIndex
	 *            the currentUserIndex to set
	 */
	public void setCurrentUserIndex(int currentUserIndex) {
		this.currentUserIndex = currentUserIndex;
		saveProfilesAsXml();
	}

	public User getCurrentUser() {
		if (currentUserIndex == 0) {
			return null;
		} else {
			return users.get(currentUserIndex);
		}

	}

	public boolean hasUser(String profileName) {
		if (profileName == null) return false;
		ArrayList<User> users = getUsers();
		for (User user : users) {
			if (user.getUserName().equalsIgnoreCase(profileName)) return true;
		}
		return false;
	}

	String	startUpDialogUserToReturn	= null;

	public String showStartUpDialog() {
		final JDialog dialog = new JDialog(mapper, true);
		dialog.setTitle("Select Profile..."); //$NON-NLS-1$
		User[] userArray = new User[getUsers().size()];
		userArray = getUsers().toArray(userArray);
		final JComboBox<User> users = new JComboBox<User>(userArray);
		JButton okButton = new JButton("Select..."); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				startUpDialogUserToReturn = ((User) users.getSelectedItem()).getUserName();
				dialog.setVisible(false);
			}
		});
		JButton cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				startUpDialogUserToReturn = null;
				dialog.setVisible(false);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		dialog.setLayout(new BorderLayout());
		dialog.add(users, BorderLayout.CENTER);
		dialog.add(buttonPanel, BorderLayout.SOUTH);
		dialog.pack();
		dialog.setLocationRelativeTo(mapper);
		dialog.setVisible(true);
		return startUpDialogUserToReturn;
	}
}
