/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.awt.Color;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Ecthelion
 * 
 */
public class RoomColourHandle {
	public final static int	CONTAINS	= 2;
	public final static int	ENDS_WITH	= 1;
	public final static int	EXACTLY		= 3;
	public final static int	MATCHES		= 4;
	public final static int	STARTS_WITH	= 0;

	private Color			colour;
	private final int		id;
	private String			text;
	private int				type;

	public RoomColourHandle(int id, String text, Color colour, int type) {
		this.text = text;
		this.colour = colour;
		this.type = type;
		this.id = id;
	}

	/* , , , */
	public RoomColourHandle(ResultSet rs) throws SQLException {
		this.id = rs.getInt(1);
		this.text = rs.getString(2);
		this.colour = new Color(rs.getInt(3));
		this.type = rs.getInt(4);
	}

	public RoomColourHandle(String text, Color colour, int type) {
		this.text = text;
		this.colour = colour;
		this.type = type;
		id = 0;
	}

	/**
	 * For xml uses only...
	 */
	public RoomColourHandle() {
		id = 0;
		text = ""; //$NON-NLS-1$
		colour = Color.GRAY;
		type = 0;
	}

	public Color getColour() {
		return colour;
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public int getType() {
		return type;
	}

	public PreparedStatement prepareInsertColourHandleStatement(PreparedStatement ps) throws SQLException {
		ps.setString(1, getText());
		if (getColour() == null) setColour(Color.GREEN);
		ps.setInt(2, getColour().getRGB());
		ps.setInt(3, getType());
		return ps;
	}

	public PreparedStatement prepareUpdateColourHandle(PreparedStatement ps) throws SQLException {
		ps.setString(1, getText());
		ps.setInt(2, getColour().getRGB());
		ps.setInt(3, getType());
		ps.setInt(4, getId());
		return ps;
	}

	public void setColour(Color colour) {
		this.colour = colour;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return id + " - " + text; //$NON-NLS-1$
	}

	public String toXml() {
		String toReturn = ""; //$NON-NLS-1$
		toReturn += "<roomcolour>"; //$NON-NLS-1$
		toReturn += "<roomcolourid>" + getId() + "</roomcolourid>"; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += "<roomcolourtext>" + getText() + "</roomcolourtext>"; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += "<roomcolourtype>" + getType() + "</roomcolourtype>"; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += "<roomcolourcolour>" + getColour().getRGB() + "</roomcolourcolour>"; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += "</roomcolour>"; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * rs_id, Name,Colour, Type
	 * 
	 * @param ps
	 * @return ps
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatementForRestore(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setString(2, getText());
		ps.setInt(3, getColour().getRGB());
		ps.setInt(4, getType());
		return ps;
	}
}
