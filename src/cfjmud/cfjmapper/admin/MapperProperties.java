/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.dialogs.PropertyDialog;
import cfjmud.cfjmapper.interfaces.PropertiesConstants;
import cfjmud.cfjmapper.interfaces.PropertiesErrorMessages;
import cfjmud.cfjmapper.profile.User;

/**
 * 
 * @author Ecthelion
 * 
 */
public class MapperProperties extends Properties implements PropertiesConstants, PropertiesErrorMessages {

	/** 
     * 
     */
	private static final long		serialVersionUID	= 6920886391757553731L;

	private static MapperProperties	instance			= null;

	/**
	 * Singleton unit...
	 * 
	 * @return static instance of MapperProperties.
	 */
	public static MapperProperties getInstance() {

		if (instance == null) {
			instance = new MapperProperties();
		}

		return instance;
	}

	/**
	 * Show the dialog for preferences
	 * 
	 * @param mapper
	 */
	public static void showPreferenceDialog(Mapper mapper) {
		new PropertyDialog(mapper, instance);
	}

	private File	theFileToStoreTo	= null;

	private MapperProperties() {
		super();
		try {
			theFileToStoreTo = new File(propertyFile);
			if (theFileToStoreTo.exists()) {
				loadFromXML(new FileInputStream(theFileToStoreTo));
			} else {
				theFileToStoreTo.createNewFile();
			}
		} catch (Exception e) {
			System.out.println(COULDNOTINITIATEPROPERTIES + e.toString());
			e.printStackTrace();
		}
	}

	public void createDefaultValues() {
		super.setProperty(PROPERTYFILENAME, PROPERTYFILENAME_DEFAULT);
		super.setProperty(STARTINGXPOS, STARTINGXPOS_DEFAULT);
		super.setProperty(STARTINGYPOS, STARTINGYPOS_DEFAULT);
		super.setProperty(STARTINGLEVEL, STARTINGLEVEL_DEFAULT);
		super.setProperty(XPOS, XPOS_DEFAULT);
		super.setProperty(YPOS, YPOS_DEFAULT);
		super.setProperty(SPACEBETWEENROOMS, SPACEBETWEENROOMS_DEFAULT);
		super.setProperty(STARTINGROOM, STARTINGROOM_DEFAULT);
		super.setProperty(STARTINGAREA, STARTINGAREA_DEFAULT);
		super.setProperty(STARTINGAREAID, STARTINGAREAID_DEFAULT);
		super.setProperty(WINDOWSTARTX, WINDOWSTARTX_DEFAULT);
		super.setProperty(WINDOWSTARTY, WINDOWSTARTY_DEFAULT);
		super.setProperty(WINDOWWIDTH, WINDOWWIDTH_DEFAULT);
		super.setProperty(WINDOWHEIGHT, WINDOWHEIGHT_DEFAULT);
		super.setProperty(CONNECTIONSTRING, CONNECTIONSTRING_DEFAULT);
		super.setProperty(PREFERREDDBNAME, PREFERREDDBNAME_DEFAULT);
		super.setProperty(EXITSPOLICY, EXITSPOLICY_DEFAULT);
		super.setProperty(USERNAME, USERNAME_DEFAULT);
		super.setProperty(PASSWORD, PASSWORD_DEFAULT);
		super.setProperty(PORTALALIAS, PORTALALIAS_DEFAULT);
		super.setProperty(CENTREONCURRENTROOM, CENTREONCURRENTROOM_DEFAULT);
		super.setProperty(SHOWSPECIALEXITS, SHOWSPECIALEXITS_DEFAULT);
		super.setProperty(SUBTISTUTESPECIALEXITS, SUBTISTUTESPECIALEXITS_DEFAULT);
		super.setProperty(AUTOJOURNEY, AUTOJOURNEY_DEFAULT);
		super.setProperty(RADIUS, RADIUS_DEFAULT);
		super.setProperty(ROOMREPRESENTATION, ROOMREPRESENTATION_DEFAULT);
	}

	public void save() {
		if (theFileToStoreTo != null) {
			try {
				storeToXML(new FileOutputStream(theFileToStoreTo), PROPERTIESFORMAPPER);
			} catch (Exception e) {
				System.out.println(e.toString());
				e.printStackTrace();
			}
		}
	}

	@Override
	public Object setProperty(String key, String value) {
		Object objectToReturn = super.setProperty(key, value);
		save();
		return objectToReturn;
	}

	protected Object setPropertyWithOutSave(String key, String value) {
		return super.setProperty(key, value);

	}

	public void putAll(Properties properties) {
		super.putAll(properties);
		save();
	}

	@Override
	public Object remove(Object obj) {
		Object toReturn = super.remove(obj);
		save();
		return toReturn;
	}

	public void save(Mapper mapper) {
		setProperty(STARTINGLEVEL, Integer.toString(mapper.getCurrentLevel()));
		setProperty(WINDOWSTARTX, Integer.toString(mapper.getX()));
		setProperty(WINDOWSTARTY, Integer.toString(mapper.getY()));
		setProperty(WINDOWWIDTH, Integer.toString(mapper.getWidth()));
		setProperty(WINDOWHEIGHT, Integer.toString(mapper.getHeight()));
		setProperty(DIVIDERLOCATION, Integer.toString(mapper.getDividerLocation()));
		if (mapper.getProfileManager().getCurrentUser() == null) {
			setProperty(STARTINGAREAID,
						Integer.toString(((mapper.getMapView().getCurrentRoom() == null ? 1 : mapper.getMapView().getCurrentRoom().getAreaId()))));
			setProperty(STARTINGROOM, Integer.toString(mapper.getMapView().getCurrentRoomId()));
			remove(PROFILE);
		} else {
			User user = mapper.getProfileManager().getCurrentUser();
			setProperty(PROFILE, user.getUserName());
			mapper.getProfileManager().saveProfilesAsXml();
		}
	}
}
