/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.admin;

import java.util.ArrayList;

import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;

/**
 * @author Ecthelion
 * 
 */
public class CommonVariables {

	private static CommonVariables	instance	= null;
	private Room					currentRoom, currentShadowRoom;
	private ArrayList<Zone>			currentZones;

	/**
	 * 
	 */
	private CommonVariables() {
		// TODO Auto-generated constructor stub
	}

	public static CommonVariables getInstance() {
		if (instance == null) {
			instance = new CommonVariables();
		}
		return instance;
	}

	/**
	 * @return the currentRoom
	 */
	public Room getCurrentRoom() {
		return currentRoom;
	}

	/**
	 * @param currentRoom
	 *            the currentRoom to set
	 */
	public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
	}

	/**
	 * @return the currentShadowRoom
	 */
	public Room getCurrentShadowRoom() {
		return currentShadowRoom;
	}

	/**
	 * @param currentShadowRoom
	 *            the currentShadowRoom to set
	 */
	public void setCurrentShadowRoom(Room currentShadowRoom) {
		this.currentShadowRoom = currentShadowRoom;
	}

	/**
	 * @return the currentZones
	 */
	public ArrayList<Zone> getCurrentZones() {
		return currentZones;
	}

}
