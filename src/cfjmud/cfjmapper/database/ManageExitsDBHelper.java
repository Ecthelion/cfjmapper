/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.awt.Rectangle;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipOutputStream;

import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.objects.Exit;

/**
 * @author Ecthelion
 * 
 */
class ManageExitsDBHelper implements SQLConstants {

	static boolean addExit(Exit exit, boolean commit, boolean initiated, HashMap<String, PreparedStatement> statements,
			Hashtable<String, PreparedStatement> exitsInsertStatements, Database database) {
		if (!initiated) return false;
		if (exit.getTargetArea() == exit.getSourceArea()) {
			try {
				PreparedStatement ps = exitsInsertStatements.get(Integer.toString(exit.getTargetArea()));
				ps = exit.createInsertStatement(ps);
				ps.execute();
				if (commit) database.commit();
			} catch (Exception e) {
				System.out.println(failedToAddExit + e.toString());
				e.printStackTrace();
				return false;
			}
		} else {
			try {
				PreparedStatement ps = statements.get(exitInsert[0]);
				ps = exit.createInsertStatement(ps);
				ps.execute();
			} catch (Exception e) {
				System.out.println(failedToAddExit + e.toString());
				e.printStackTrace();
				return false;
			}

		}
		return true;
	}

	static boolean addExits(ArrayList<Exit> exits, HashMap<String, PreparedStatement> statements, Hashtable<String, PreparedStatement> exitsInsertStatements,
			Connection conn, Database database) {
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			System.out.println("Failed to setAutoCommit(false)" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		PreparedStatement ps = null;
		statements.get(exitInsert[0]);
		for (Exit exit : exits) {
			if (exit.getSourceArea() == exit.getTargetArea()) {
				ps = exitsInsertStatements.get(Integer.toString(exit.getSourceArea()));
			} else {
				ps = statements.get(exitInsert[0]);
			}
			try {
				ps = exit.createInsertStatement(ps);
				ps.execute();
			} catch (SQLException e) {
				System.out.println(failedToAddExit + e.toString());
				e.printStackTrace();
			}

		}
		database.commit();
		try {
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("Failed to setAutoCommit(true)" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return true;
	}

	static void deleteExit(Exit exit, boolean initiated, Hashtable<String, PreparedStatement> exitsDeleteStatements,
			HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		if (exit == null) return;
		try {
			PreparedStatement ps = null;
			if (exit.getSourceArea() == exit.getTargetArea()) {
				ps = exitsDeleteStatements.get(Integer.toString(exit.getSourceArea()));
			} else {
				ps = statements.get(deleteExit[0]);
			}
			ps.setInt(1, exit.getId());
			ps.execute();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToDeleteExit + e.toString());
			e.printStackTrace();
		}
	}

	/*
	 * String selectExit =
	 * "SELECT e_id, startX, startY, endX, endY, oneWay, Level, Type FROM " +
	 * areaName +
	 * "_Exits WHERE Level=? AND ((startX>=? AND startY>=? AND startX<=? AND  startY<=?) OR (endX>=? AND endY>=? AND endX<=? AND endY<=?))"
	 * ;
	 * 
	 * public Exit(int id, int startX, int startY, int endX, int endY, int
	 * areaName, int level, int type, int sourceRoom, int targetRoom, int
	 * sourceLevel, int targetLevel, int sourceArea, int targetArea) SELECT
	 * e_id, startX, startY, endX, endY, oneWay, Level, Type, sourceRoom,
	 * targetRoom, sourceArea, targetArea, sourceLevel, targetLevel, ShowStub
	 * FROM Exits WHERE (sourceLevel=? OR targetLevel=?) AND ((startX>=? AND
	 * startY>=? AND startX<=? AND startY<=?) OR (endX>=? AND endY>=? AND
	 * endX<=? AND endY<=?)) AND (sourceArea=? OR targetArea=?)
	 */
	static ArrayList<Exit> getExits(int areaId, Rectangle theAreaToFetchFor, int level, boolean withBorders, boolean initiated,
			Hashtable<String, PreparedStatement> exitsSelectStatements, Database database) {
		if (!initiated) return null;
		ArrayList<Exit> vectorToReturn = new ArrayList<Exit>(2000);
		String id = new Integer(areaId).toString();
		int startX = (int) theAreaToFetchFor.getX();
		int startY = (int) theAreaToFetchFor.getY();
		int endX = (int) theAreaToFetchFor.getX() + (int) theAreaToFetchFor.getWidth();
		int endY = (int) theAreaToFetchFor.getY() + (int) theAreaToFetchFor.getHeight();
		int border = (withBorders ? 15 : 0);
		try {
			PreparedStatement ps = exitsSelectStatements.get(id);
			if (ps == null) return vectorToReturn;
			ps.setInt(1, level);
			ps.setInt(2, level);
			ps.setInt(3, startX - border);
			ps.setInt(4, startY - border);
			ps.setInt(5, endX + border);
			ps.setInt(6, endY + border);
			ps.setInt(7, startX - border);
			ps.setInt(8, startY - border);
			ps.setInt(9, endX + border);
			ps.setInt(10, endY + border);
			ps.setInt(11, level);
			ps.setInt(12, level);
			ps.setInt(13, startX - border);
			ps.setInt(14, startY - border);
			ps.setInt(15, endX + border);
			ps.setInt(16, endY + border);
			ps.setInt(17, startX - border);
			ps.setInt(18, startY - border);
			ps.setInt(19, endX + border);
			ps.setInt(20, endY + border);
			ps.setInt(21, areaId);
			ps.setInt(22, areaId); // sourceLock, targetLock,
			// sourceWalkCommand,
			// targetWalkCommand
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				vectorToReturn.add(new Exit(rs));
			}
			rs.close();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToFetchExits + e.toString());
			e.printStackTrace();
		}
		return vectorToReturn;
	}

	static ArrayList<Exit> getExistsInsideArea(int areaId, Hashtable<String, PreparedStatement> getAllExitsForAreaStatement) {
		ArrayList<Exit> toReturn = new ArrayList<Exit>();
		try {

			PreparedStatement ps = getAllExitsForAreaStatement.get(Integer.toString(areaId));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new Exit(rs));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println(failedToGetAllExitsForArea + e.toString());
			e.printStackTrace();
		}

		return toReturn;
	}

	/**
	 * UPDATE Exits SET startX=?, startY=?, endX=?, endY=?, oneWay=?, Level=?,
	 * Type=?, sourceRoom=?, targetRoom=?, sourceArea=?, targetArea=?,
	 * ShowStub=? WHERE e_id=? String updateExit =
	 * "UPDATE Exits SET startX=?, startY=?, endX=?, endY=?, oneWay=?, Level=?, Type=?, sourceRoom=?, targetRoom=?,  sourceArea=?, targetArea=? WHERE e_id=?"
	 * ; PreparedStatement tmpPs4_1 = conn.prepareStatement(updateExit);
	 * statements.put("ExitUpdate", tmpPs4_1);
	 */
	static void updateExit(Exit old_exit, Exit new_exit, boolean initiated, Hashtable<String, PreparedStatement> exitsUpdateStatements,
			HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		PreparedStatement ps = null;
		try {
			if (old_exit != null) {
				database.deleteExit(old_exit);
				database.addExit(new_exit);
			} else {
				if (new_exit.getSourceArea() == new_exit.getTargetArea()) {
					ps = exitsUpdateStatements.get(Integer.toString(new_exit.getSourceArea()));
				} else {
					ps = statements.get(exitUpdate[0]);
				}
				ps = new_exit.createUpdateExitPrepareStatement(ps);
				ps.execute();
				database.commit();

			}
		} catch (Exception e) {
			System.out.println(failedToUpdateExit + e.toString());
			e.printStackTrace();
		}
	}

	static ArrayList<Exit> getExitsForBetweenArea(int areaId, HashMap<String, PreparedStatement> statements) {
		ArrayList<Exit> toReturn = new ArrayList<Exit>();
		try {
			PreparedStatement ps = statements.get(allExitsForArea[0]);
			ps.setInt(1, areaId);
			ps.setInt(2, areaId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new Exit(rs));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Exits Between Failure: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	static void writeAllExits(ZipOutputStream stream, Connection conn) throws IOException {
		try {
			String statement = "SELECT * FROM exits"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stream.write(new Exit(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to get exits in writeAllExits: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}
}
