/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import cfjmud.cfjmapper.admin.MapperProperties;

/**
 * @author Ecthelion
 * 
 */
public class ConnectionManager {

	private static ConnectionManager	instance	= null;
	private static String				connectionString;
	private static Database				database;
	private Connection					conn, zoneConn;

	private final MapperProperties		properties;

	/**
	 * 
	 */
	private ConnectionManager() {
		properties = MapperProperties.getInstance();
		// TODO Auto-generated constructor stub
	}

	public Connection getConnection() throws SQLException {
		if (conn == null) {
			conn = initiateConnection();
			prepareStatements();
		} else if (conn.isClosed()) {
			conn = initiateConnection();
			prepareStatements();
		}
		return conn;
	}

	public Connection getZoneConnection() throws SQLException {
		if (zoneConn == null) {
			zoneConn = initiateConnection();
			prepareStatements();
		} else if (conn.isClosed()) {
			zoneConn = initiateConnection();
			prepareStatements();
		}
		return zoneConn;
	}

	private void prepareStatements() {
		try {
			database.prepareStatesments(getConnection(), getZoneConnection());
		} catch (SQLException e) {
			System.out.println("Failed to prepare statements for connection in prepareStatements\nin ConnectionManager when creating new connection.\n" //$NON-NLS-1$
								+ e.toString());
			e.printStackTrace();
		}
	}

	private Connection initiateConnection() throws SQLException {
		return DriverManager.getConnection(	connectionString, properties.getProperty(MapperProperties.USERNAME),
											properties.getProperty(MapperProperties.PASSWORD));

	}

	public static ConnectionManager getInstance() {
		if (instance == null) instance = new ConnectionManager();
		return instance;
	}

	/**
	 * Sets the variable needed to connect to the db.
	 * 
	 * @param connectionString
	 * @return
	 */
	public static ConnectionManager initiateRoomConnectionManager(String connectionString, Database database) {
		ConnectionManager.connectionString = connectionString;
		ConnectionManager.database = database;
		return getInstance();
	}

}
