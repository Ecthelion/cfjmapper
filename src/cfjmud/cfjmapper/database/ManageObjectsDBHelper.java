/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.awt.Rectangle;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.ZipOutputStream;

import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.objects.MapObject;

/**
 * @author Ecthelion
 * 
 */
class ManageObjectsDBHelper implements SQLConstants {

	/*
	 * con, Description, xPos, yPos, width, height, labelposition, background,
	 * area, level , textsize , font , textstyle s.execute(
	 * "CREATE TABLE Objects(o_id int NOT NULL GENERATED ALWAYS AS IDENTITY, icon int, Description varchar(200), xPos int, yPos int)"
	 * );
	 * "INSERT INTO Objects(Icon, Description, xPos, yPos, width, height, labelposition, background) VALUES (?,?,?,?,?,?,?)"
	 */
	static void addObject(MapObject object, HashMap<String, PreparedStatement> statements, boolean initiated, Database database) {
		if (!initiated || object == null) return;
		try {
			PreparedStatement ps = statements.get(addObject[0]);
			ps = object.prepareInsertStatement(ps);
			ps.execute();
			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToAddObject + e.toString());
			e.printStackTrace();
		}
	}

	static void deleteObject(MapObject object, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		try {
			PreparedStatement ps = statements.get(deleteObject[0]);
			ps = object.prepareDeleteObject(ps);
			ps.execute();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToDeleteObject + e.toString());
			e.printStackTrace();
		}
	}

	/*
	 * statements.put("addObject", conn.prepareStatement(
	 * "INSERT INTO Objects(Icon, Description, xPos, yPos) VALUES (?,?,?,?)"));
	 * statements.put("selectObjectById", conn.prepareStatement(
	 * "SELECT c_id, Icon, Description, xPos, yPos FROM Objects WHERE c_id=?"));
	 * statements.put("selectObjectsByRectangle", conn.prepareStatement(
	 * "SELECT c_id, Icon, Description, xPos, yPos FROM Objects WHERE xPos>? AND xPos<? AND yPos>? AND yPos<?"
	 * )); statements.put("updateObject", conn.prepareStatement(
	 * "UPDATE Objects SET Icon=?, Description=?, xPos=?, yPos=? WHERE c_id=?"
	 * ));
	 */
	static MapObject getObject(int id, HashMap<String, PreparedStatement> statements, Database database) {
		MapObject toReturn = null;
		try {
			PreparedStatement ps = statements.get(selectObjectById[0]);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn = new MapObject(rs);
			}
			rs.close();
			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToGetObject + e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	static ArrayList<MapObject> getObjectsForArea(int areaId, HashMap<String, PreparedStatement> statements, Database database) {
		ArrayList<MapObject> toReturn = new ArrayList<MapObject>();
		try {
			PreparedStatement ps = statements.get(allObjectsForArea[0]);
			ps.setInt(1, areaId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new MapObject(rs));
			}
			rs.close();

			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToGetAllObjectsForArea + e.toString());
			e.printStackTrace();
		}

		return toReturn;
	}

	static ArrayList<MapObject> getObjectsForArea(int areaId, int level, HashMap<String, PreparedStatement> statements, Database database) {
		ArrayList<MapObject> toReturn = new ArrayList<MapObject>();
		try {
			PreparedStatement ps = statements.get(selectObjectsByArea[0]);
			ps.setInt(1, areaId);
			ps.setInt(2, level);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new MapObject(rs));
			}
			database.commit();
			rs.close();
		} catch (SQLException e) {
			System.out.println(failedToGetObjectsForRectangle + e.toString());
			e.printStackTrace();
		}

		return toReturn;
	}

	/*
	 * SELECT o_id, Icon, Description, xPos, yPos, width, height, labelposition
	 * FROM Objects WHERE (xPos>? AND xPos<? AND yPos>? AND yPos<?)
	 * "));// OR (xPos>?+width AND xPos<?+width AND yPos>?+height AND yPos<?+height)"
	 * )); public MapObject(int id, int xPos, int yPos, int iconId, String
	 * label, int width, int height, int labelPosition) {
	 */
	static ArrayList<MapObject> getObjectsForRectangle(Rectangle rect, int currentArea, int currentLevel, HashMap<String, PreparedStatement> statements,
			Database database) {
		ArrayList<MapObject> toReturn = new ArrayList<MapObject>();
		try {
			PreparedStatement ps = statements.get(selectObjectsByArea[0]);
			ps.setInt(1, currentArea);
			ps.setInt(2, currentLevel);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MapObject tmpObject = new MapObject(rs);
				Rectangle bounds = new Rectangle(tmpObject.getxPos(), tmpObject.getyPos(), tmpObject.getWidth(), tmpObject.getHeight());
				if (bounds.intersects(rect)) toReturn.add(new MapObject(rs));
			}
			rs.close();
			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToGetObjectsForRectangle + rect.toString());
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	/*
	 * UPDATE Objects SET Icon=?, Description=?, xPos=?, yPos=?, width=?,
	 * height=?, labelposition=?, background=?, area=?, level =?, textsize=?,
	 * font=?, textstyle=? WHERE o_id=?
	 */
	static void updateObject(MapObject object, HashMap<String, PreparedStatement> statements, Database database) {
		if (object == null) return;
		try {
			PreparedStatement ps = statements.get(updateObject[0]);
			ps = object.prepareUpdateStatement(ps);
			ps.execute();
			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToUpdateObject + e.toString());
			e.printStackTrace();
		}
	}

	static void writeAllObjects(ZipOutputStream stream, Connection conn) throws IOException {
		try {
			String statement = "SELECT * FROM objects"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stream.write(new MapObject(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to get objects in writeAllObjects: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

}
