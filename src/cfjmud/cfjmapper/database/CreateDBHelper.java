/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.sql.SQLException;
import java.sql.Statement;

import cfjmud.cfjmapper.interfaces.SQLConstants;

/**
 * @author Ecthelion
 * 
 */
class CreateDBHelper implements SQLConstants {

	static void createTablesForJavaDB(Statement s) throws SQLException {
		s.execute(javaDBcreateAreas);
		s.execute(javaDBcreateExits);
		s.execute(javaDBcreateRoomColours);
		s.execute(javaDBcreateTerrainTypes);
		s.execute(javaDBcreateIcons);
		s.execute(javaDBcreateObjects);
		s.execute(javaDBcreateZones);
		s.execute(javaDBcreateZoneRooms);
	}

	static void createTablesForMySQL(Statement s) throws SQLException {
		s.execute(mySQLcreateAreas);
		s.execute(mySQLcreateExits);
		s.execute(mySQLcreateRoomColours);
		s.execute(mySQLcreateTerrainTypes);
		s.execute(mySQLcreateIcons);
		s.execute(mySQLcreateObjects);
		s.execute(mySQLcreateZones);
		s.execute(mySQLcreateZoneRooms);
	}

	static void createTablesForpostgreSQL(Statement s) throws SQLException {
		s.execute(postgreSQLcreateAreas);
		s.execute(postgreSQLcreateExits);
		s.execute(postgreSQLcreateRoomColours);
		s.execute(postgreSQLcreateTerrainTypes);
		s.execute(postgreSQLcreateIcons);
		s.execute(postgreSQLcreateObjects);
		s.execute(postgreSQLcreateZones);
		s.execute(postgreSQLcreateZoneRooms);
	}
}
