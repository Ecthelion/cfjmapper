/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.objects.MapperIcon;

/**
 * @author Ecthelion
 * 
 */
class ManageIconsDBHelper implements SQLConstants, MapperConstants {

	static int addIcon(File icon, String name, boolean initiated, HashMap<String, PreparedStatement> statements, int dbType, Database database) {
		if (!initiated) return -1;

		try {
			return database.addIcon(new FileInputStream(icon), name, (int) icon.length());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return -1;
	}

	static int addIcon(InputStream inputStream, String name, int length, boolean initiated, HashMap<String, PreparedStatement> statements, int dbType,
			Database database) {
		if (!initiated) return -1;

		try {
			PreparedStatement ps = statements.get(addIcon[0]);
			if (dbType == POSTG) {
				ps.setBinaryStream(1, inputStream, length);
			} else {
				ps.setBlob(1, inputStream);
			}
			ps.setString(2, name);
			ps.execute();
			database.commit();

		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		return database.getLastIconIndex();
	}

	static int addIcon(String fileName, String name, boolean initiated, Database database) {
		if (!initiated) return -1;

		try {
			File f = new File(fileName);
			return database.addIcon(new FileInputStream(f), name, (int) f.length());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return -1;
	}

	static MapperIcon getIcon(int id, boolean initiated, Hashtable<Integer, MapperIcon> icons, HashMap<String, PreparedStatement> statements, int dbType,
			Database database) {
		if (!initiated) return null;

		if (id == 0) return null;
		if (icons == null) icons = new Hashtable<Integer, MapperIcon>();
		MapperIcon iconToReturn = icons.get(new Integer(id));
		if (iconToReturn == null) {
			try {
				PreparedStatement ps = statements.get(selectIcon[0]);
				ps.setInt(1, id);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					InputStream inputStream;
					if (dbType == POSTG) {
						inputStream = rs.getBinaryStream(1);
						BufferedInputStream bis = new BufferedInputStream(inputStream);
						ArrayList<Integer> tmpV = new ArrayList<Integer>();
						int val = bis.read();
						while (val != -1) {
							tmpV.add(new Integer(val));
							val = bis.read();
						}
						Object[] arr = tmpV.toArray();
						byte[] img = new byte[arr.length];
						int c = 0;
						for (Object i : arr) {
							img[c++] = ((Integer) i).byteValue();
						}
						String name = rs.getString(2);
						int ic_id = rs.getInt(3);
						iconToReturn = new MapperIcon(ic_id, name, img);
					} else {
						Blob img = null;
						img = rs.getBlob(1);
						String name = rs.getString(2);
						int ic_id = rs.getInt(3);
						iconToReturn = new MapperIcon(ic_id, name, img.getBytes(1, (int) img.length()));
					}
				}
				rs.close();
				database.commit();
				if (iconToReturn != null) icons.put(new Integer(id), iconToReturn);
			} catch (Exception e) {
				System.out.println(failedToGetIcon + e.toString());
				e.printStackTrace();
			}
		}
		return iconToReturn;
	}

	static ArrayList<MapperIcon> getIcons(boolean initiated, Hashtable<Integer, MapperIcon> icons, HashMap<String, PreparedStatement> statements,
			Database database) {
		if (!initiated) return null;

		if (icons == null) icons = new Hashtable<Integer, MapperIcon>();
		icons.clear();
		try {
			PreparedStatement ps = statements.get(selectIcons[0]);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				MapperIcon icon = new MapperIcon(rs);
				icons.put(new Integer(icon.getId()), icon);
			}
			rs.close();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToGetIcon + e.toString());
			e.printStackTrace();
		}
		return new ArrayList<MapperIcon>(icons.values());
	}

	static int getLastIconIndex(boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return -1;

		int toReturn = -1;
		try {
			PreparedStatement ps = statements.get(maxIconIndex[0]);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				toReturn = rs.getInt(1);
			}
			rs.close();
			database.commit();
		} catch (Exception e) {
			System.out.println("Failed to get Last generated Room Id: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	static void updateIcon(int icon, File tmpIcon, boolean initiated, HashMap<String, PreparedStatement> statements, int dbType, Database database) {
		if (!initiated) return;

		try {
			FileInputStream fis = new FileInputStream(tmpIcon);
			PreparedStatement ps = statements.get(updateIcon2[0]);
			if (dbType == POSTG) {
				ps.setBinaryStream(1, fis, (int) tmpIcon.length());
			} else {
				ps.setBlob(1, fis);
			}
			ps.setInt(2, icon);
			ps.execute();
			database.commit();

		} catch (SQLException e) {
			System.out.println("Failed to Execute SQL in updateIcon: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("No bloody file in updateIcon: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void writeAllIconsAsXml(ZipOutputStream stream, Connection conn, Database database) throws IOException {
		try {
			String statement = "SELECT * FROM icons"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);
			database.debug("Prepared Statement"); //$NON-NLS-1$
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stream.write(new MapperIcon(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Wasn't able to get all icons in writeAllIconsAsXml:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void writeAllIcons(ZipOutputStream stream, Connection conn, Database database) throws IOException {
		try {
			String statement = "SELECT * FROM icons"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);
			database.debug("Prepared Statement"); //$NON-NLS-1$
			ResultSet rs = ps.executeQuery();
			database.debug("Executed Query"); //$NON-NLS-1$
			ArrayList<String> names = new ArrayList<String>();
			while (rs.next()) {
				database.debug("Start of Loop"); //$NON-NLS-1$
				MapperIcon icon = new MapperIcon(rs);
				database.debug("An Icon;" + icon.getName()); //$NON-NLS-1$
				boolean changed = false;
				while (names.contains(icon.toFileName())) {
					changed = true;
					database.debug("Oh Dear! Name already Exists! " + icon.toFileName()); //$NON-NLS-1$
					String[] parts = icon.toFileName().split("\\."); //$NON-NLS-1$
					if (parts.length > 1) {
						parts[0] = parts[0] + "_1."; //$NON-NLS-1$
					}
					String name = ""; //$NON-NLS-1$
					for (String part : parts) {
						name += part;
					}
					icon.setFileName(name);
				}
				if (changed) {
					database.updateIconName(icon);
				}
				names.add(icon.toFileName());
				database.debug("Added iconname to array:" + icon.toFileName()); //$NON-NLS-1$

				stream.putNextEntry(new ZipEntry(icon.toFileName()));
				database.debug("Putted the next entry"); //$NON-NLS-1$
				Image image = icon.getImage();
				database.debug("Got Image"); //$NON-NLS-1$
				BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TRANSLUCENT);
				database.debug("Got BufferedImage"); //$NON-NLS-1$
				Graphics2D g2 = bi.createGraphics();
				database.debug("Got Graphics"); //$NON-NLS-1$
				g2.drawImage(image, null, null);
				database.debug("Drawn Image"); //$NON-NLS-1$
				ImageIO.write(bi, "png", stream); //$NON-NLS-1$
				database.debug("Wrote Image"); //$NON-NLS-1$
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Couldn't write all Icons: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void updateIconName(MapperIcon icon, HashMap<String, PreparedStatement> statements) {
		try {
			PreparedStatement ps = statements.get(updateIconName[0]);
			ps.setString(1, icon.getName());
			ps.setInt(2, icon.getId());
			ps.execute();
		} catch (SQLException e) {
			System.out.println("Failed to update icon name: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void deleteIcon(MapperIcon selectedIcon, HashMap<String, PreparedStatement> statements) {
		try {
			PreparedStatement ps = statements.get(deleteIcon[0]);
			PreparedStatement psA = statements.get(updateIconForAreasOnIcon[0]);
			PreparedStatement psO = statements.get(updateIconForObjectsOnIcon[0]);
			psA.setInt(1, 0);
			psA.setInt(2, selectedIcon.getId());
			psO.setInt(1, 0);
			psO.setInt(2, selectedIcon.getId());
			ps.setInt(1, selectedIcon.getId());
			ps.execute();
			psA.execute();
			psO.execute();
		} catch (SQLException e) {
			System.out.println("Failed to delete Icon: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

}
