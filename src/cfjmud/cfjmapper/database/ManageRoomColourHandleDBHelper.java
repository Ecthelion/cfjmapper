/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.ZipOutputStream;

import cfjmud.cfjmapper.RoomColour;
import cfjmud.cfjmapper.RoomColourHandle;
import cfjmud.cfjmapper.interfaces.SQLConstants;

/**
 * @author Ecthelion
 * 
 */
class ManageRoomColourHandleDBHelper implements SQLConstants {

	static void deleteRoomColour(RoomColourHandle handle, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		try {
			PreparedStatement ps = statements.get(roomColourDelete[0]);
			ps.setInt(1, handle.getId());
			ps.execute();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToDeleteRoomColourHandle + e.toString());
			e.printStackTrace();
		}
	}

	static void addColourForRoomType(RoomColourHandle handle, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		if (handle == null) return;
		try {
			PreparedStatement ps = statements.get(roomColourInsert[0]);
			ps = handle.prepareInsertColourHandleStatement(ps);
			ps.execute();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToAddRoomColourHandle + e.toString());
			e.printStackTrace();
		}
	}

	static ArrayList<RoomColourHandle> getRoomColourHandles(HashMap<String, PreparedStatement> statements, boolean initiated, Database database) {
		if (!initiated) return null;
		ArrayList<RoomColourHandle> toReturn = new ArrayList<RoomColourHandle>();

		try {
			PreparedStatement ps = statements.get(roomColourSelect[0]);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				toReturn.add(new RoomColourHandle(rs));
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToGetRoomColourHandle + e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	static void updateAllRoomColours(boolean initiated, Connection conn, ArrayList<PreparedStatement> roomUpdateColourByNameArray, Database database) {
		if (!initiated) return;
		ArrayList<RoomColourHandle> handles = database.getRoomColourHandles();
		try {
			conn.setAutoCommit(false);
			for (RoomColourHandle handle : handles) {
				for (PreparedStatement statement : roomUpdateColourByNameArray) {
					statement.setInt(1, handle.getColour().getRGB());
					switch (handle.getType()) {
						case RoomColourHandle.STARTS_WITH:
							statement.setString(2, handle.getText() + "%"); //$NON-NLS-1$
							break;
						case RoomColourHandle.ENDS_WITH:
							statement.setString(2, "%" + handle.getText()); //$NON-NLS-1$
							break;
						case RoomColourHandle.CONTAINS:
							statement.setString(2, "%" + handle.getText() + "%"); //$NON-NLS-1$ //$NON-NLS-2$
							break;
						case RoomColourHandle.EXACTLY:
							statement.setString(2, handle.getText());
							break;
						case RoomColourHandle.MATCHES:
							break;
					}
					statement.execute();
				}
			}
			database.commit();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("Failed to update Rooms: \n" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void updateColoursForRoomName(RoomColourHandle handle, Connection conn, ArrayList<PreparedStatement> roomUpdateColourByNameArray) {
		try {
			conn.setAutoCommit(false);
			for (PreparedStatement statement : roomUpdateColourByNameArray) {
				statement.setInt(1, handle.getColour().getRGB());
				switch (handle.getType()) {
					case RoomColourHandle.STARTS_WITH:
						statement.setString(2, handle.getText() + "%"); //$NON-NLS-1$
						break;
					case RoomColourHandle.ENDS_WITH:
						statement.setString(2, "%" + handle.getText()); //$NON-NLS-1$
						break;
					case RoomColourHandle.CONTAINS:
						statement.setString(2, "%" + handle.getText() + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						break;
					case RoomColourHandle.EXACTLY:
						statement.setString(2, handle.getText());
						break;
					case RoomColourHandle.MATCHES:
						break;
				}
				statement.execute();
			}
			conn.commit();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("Failed to update Colour RoomHandle: \n" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void updateRoomColourHandle(RoomColourHandle handle, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return;
		try {
			PreparedStatement ps = statements.get(roomColourUpdate[0]);
			ps = handle.prepareUpdateColourHandle(ps);
			ps.execute();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToUpdateRoomColourHandle + e.toString());
			e.printStackTrace();
		}
		RoomColour.updateRoomColours();
	}

	static void writeAllColourTriggers(ZipOutputStream zipFile, Connection conn) throws IOException {
		try {
			String statement = "SELECT * FROM roomcolours"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				zipFile.write(new RoomColourHandle(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Couldn't writeAllColourTriggers to file: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}
}
