/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipOutputStream;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.RoomColourHandle;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.objects.ZoneRoom;

/**
 * 
 * @author Ecthelion
 * 
 */
public class Database implements SQLConstants, MapperConstants {
	private static Database	instance	= null;

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	private ConcurrentHashMap<Integer, Area>			areas					= null;

	// private Connection conn = null;
	private String										connectionString		= "";		//jdbc:derby:CFJMapperDB;create=true"; //$NON-NLS-1$

	boolean												createDefaultValues		= false;
	// private int currentAreaId = 1;
	public static int									dbType					= 0;
	private final Hashtable<Integer, MapperIcon>		icons					= null;
	private boolean										initiated				= false;

	private final boolean								areaCreated				= false;
	private final int									lastGeneratedRoomId		= 1;
	String												preferredDBName			= "";		//$NON-NLS-1$
	MapperProperties									properties;

	private Statement									s						= null;

	private final HashMap<String, PreparedStatement>	statements;						// list
																							// of
	// Statements,
	// PreparedStatements

	private final Hashtable<String, PreparedStatement>	exitsSelectStatements;
	private final Hashtable<String, PreparedStatement>	exitsSelectForRoomStatements;
	private final Hashtable<String, PreparedStatement>	exitsInsertStatements;
	private final Hashtable<String, PreparedStatement>	exitsUpdateStatements;
	private final Hashtable<String, PreparedStatement>	exitsDeleteStatements;
	private final Hashtable<String, PreparedStatement>	dropExitsTableForAreaStatements;

	private final Hashtable<String, PreparedStatement>	roomsSelectStatements;
	private final Hashtable<String, PreparedStatement>	allRoomsForAreaSelectStatements;
	private final Hashtable<String, PreparedStatement>	maxRoomIdStatements;
	private final Hashtable<String, PreparedStatement>	getRoomAtPointStatements;

	private final Hashtable<String, PreparedStatement>	totalRoomsInAreaStatements;

	private final Hashtable<String, PreparedStatement>	roomFindStatements;
	private final Hashtable<String, PreparedStatement>	oldestRoomFindStatements;
	private final Hashtable<String, PreparedStatement>	roomsInsertStatements;
	private final Hashtable<String, PreparedStatement>	roomsUpdateStatements;
	private final Hashtable<String, PreparedStatement>	roomsDeleteStatements;
	private final Hashtable<String, PreparedStatement>	dropRoomTableForAreaStatements;
	private final Hashtable<String, PreparedStatement>	areaDimenensionStatements;
	private final Hashtable<String, PreparedStatement>	getAllExitsForAreaStatement;
	private final ArrayList<PreparedStatement>			roomUpdateColourByNameArray;
	private final Hashtable<String, PreparedStatement>	zoneRoomsSelectStatements;

	private final boolean								debug					= true;

	private PreparedStatement							getRoomByAliasStatement	= null;

	private final Hashtable<String, PreparedStatement>	deleteDescriptionInAreaStatements;

	private ConnectionManager							cmgr;

	private Database() {
		properties = MapperProperties.getInstance();
		statements = new HashMap<String, PreparedStatement>();
		exitsSelectStatements = new Hashtable<String, PreparedStatement>();
		roomsSelectStatements = new Hashtable<String, PreparedStatement>();
		roomFindStatements = new Hashtable<String, PreparedStatement>();
		oldestRoomFindStatements = new Hashtable<String, PreparedStatement>();
		exitsInsertStatements = new Hashtable<String, PreparedStatement>();
		roomsInsertStatements = new Hashtable<String, PreparedStatement>();
		exitsUpdateStatements = new Hashtable<String, PreparedStatement>();
		roomsUpdateStatements = new Hashtable<String, PreparedStatement>();
		roomsDeleteStatements = new Hashtable<String, PreparedStatement>();
		areaDimenensionStatements = new Hashtable<String, PreparedStatement>();
		exitsSelectForRoomStatements = new Hashtable<String, PreparedStatement>();
		exitsDeleteStatements = new Hashtable<String, PreparedStatement>();
		totalRoomsInAreaStatements = new Hashtable<String, PreparedStatement>();
		maxRoomIdStatements = new Hashtable<String, PreparedStatement>();
		getRoomAtPointStatements = new Hashtable<String, PreparedStatement>();
		dropRoomTableForAreaStatements = new Hashtable<String, PreparedStatement>();
		dropExitsTableForAreaStatements = new Hashtable<String, PreparedStatement>();
		allRoomsForAreaSelectStatements = new Hashtable<String, PreparedStatement>();
		getAllExitsForAreaStatement = new Hashtable<String, PreparedStatement>();
		roomUpdateColourByNameArray = new ArrayList<PreparedStatement>();
		zoneRoomsSelectStatements = new Hashtable<String, PreparedStatement>();
		deleteDescriptionInAreaStatements = new Hashtable<String, PreparedStatement>();

		connectionString = properties.getProperty(MapperProperties.CONNECTIONSTRING);
		preferredDBName = properties.getProperty(MapperProperties.PREFERREDDBNAME);
		dbType = analyseConnectionString(connectionString);
		try {
			switch (dbType) {
				case MYSQL:
					createConnectionAndSoForthForMySQL();
					break;
				case POSTG:
					createConnectionAndSoForthForPostgreSQL();
					break;
				case DERBY:
				default:
					createConnectionAndSoForthForDerby();
					break;
			}

		} catch (Exception e) {
			System.out.println("Failed to create connection to DB...\n" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
			System.out.println(exitingProgram);
			System.exit(-1);
		}

		try {

			System.out.print("Preparing Statements..."); //$NON-NLS-1$
			getRoomByAliasStatement = ManageRoomsDBHelper.createGetRoomByAliasStateMent(this, cmgr.getConnection());
			if (createDefaultValues) {
				createDefaultValues();
			}
			System.out.println("...Finished"); //$NON-NLS-1$
		} catch (Exception e) {
			System.out.println(failedPrepareStatements + e.toString());
		}

		System.out.println("Database Initiated..."); //$NON-NLS-1$
		initiated = true;
	}

	public int addArea(Area area) {
		return addArea(	area.getName(), area.getParent(), area.getType(), area.getXPos(), area.getYPos(), area.getHeight(), area.getWidth(), area.getIcon(),
						area.getColour(), area.getBorderType(), area.getBorderColour(), area.getBorderThickness(), area.getShadowColour(),
						area.getBorderRaised(), area.getBorderTitle(), area.getLabel(), area.getLabelPosition(), area.getLabelFontSize(),
						area.getLabelColour(), area.getFont(), area.getLevel(), area.getPreferredRoomDistance());
	}

	public int addArea(String areaName, int parent) {
		int spaceBR = 35;
		String pdbr = properties.getProperty(MapperProperties.SPACEBETWEENROOMS, MapperProperties.SPACEBETWEENROOMS_DEFAULT);
		spaceBR = Integer.parseInt(pdbr);
		return addArea(areaName, parent, 0, 0, 0, 0, 0, 0, null, 0, null, 0, null, false, false, empty, 0, 10, null, null, 0, spaceBR);
	}

	/**
	 * 
	 * public Area(int id, String areaName, int parent, int type, int xPos, int
	 * yPos, int height, int width, int theIcon, Color colour, int borderType,
	 * Color borderColour, int borderThickness, Color shadowColour, boolean
	 * borderRaised) { INSERT INTO Areas(Name, Parent, Type, xPos, yPos, Icon,
	 * Width, Height, Colour, BorderType, BorderColour, BorderThickness,
	 * BorderShadow, Raised, BorderTitle, LabelText, LabelPosition,
	 * LabelFontSize) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,?,?,?)"));
	 * 
	 * @param areaName
	 * @param parent
	 * @param type
	 * @param xPos
	 * @param yPos
	 * @param height
	 * @param width
	 * @param theIconId
	 * @param colour
	 * @param borderType
	 * @param borderColour
	 * @param borderThickness
	 * @param shadowColour
	 * @param borderRaised
	 * @param borderTitle
	 * @param label
	 * @param labelPosition
	 * @param labelFontSize
	 * @param labelColour
	 */
	public int addArea(String areaName, int parent, int type, int xPos, int yPos, int height, int width, int theIconId, Color colour, int borderType,
			Color borderColour, int borderThickness, Color shadowColour, boolean borderRaised, boolean borderTitle, String label, int labelPosition,
			float labelFontSize, Color labelColour, Font theFont, int level, int preferredRoomDistance) {
		if (theFont == null) theFont = new Font("Dialog", Font.PLAIN, 12); //$NON-NLS-1$
		int toReturn = -1;
		try {
			PreparedStatement ps = statements.get(addArea[0]);
			ps.setString(1, areaName);
			ps.setInt(2, parent);
			ps.setInt(3, type);
			ps.setInt(4, xPos);
			ps.setInt(5, yPos);
			ps.setInt(6, theIconId);
			ps.setInt(7, width);
			ps.setInt(8, height);
			ps.setInt(9, (colour != null ? colour.getRGB() : 0));
			ps.setInt(10, borderType);
			ps.setInt(11, (borderColour != null ? borderColour.getRGB() : 0));
			ps.setInt(12, borderThickness);
			ps.setInt(13, (shadowColour != null ? shadowColour.getRGB() : 0));
			ps.setInt(14, (borderRaised ? 1 : 0));
			ps.setInt(15, (borderTitle ? 1 : 0));
			ps.setString(16, label);
			ps.setInt(17, labelPosition);
			ps.setFloat(18, theFont.getSize());
			ps.setInt(19, (labelColour != null ? labelColour.getRGB() : 0));
			ps.setString(20, theFont.getName());
			ps.setInt(21, theFont.getStyle());
			ps.setInt(22, level);
			ps.setInt(23, preferredRoomDistance);
			ps.setInt(24, -1);
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null) {
				if (rs.next()) {
					toReturn = rs.getInt(1);
				}
			}
			commit();
		} catch (Exception e) {
			System.out.println(failedToAddArea + e.toString());
			e.printStackTrace();
		}
		if (toReturn == -1) toReturn = getNewestArea();
		createTablesForArea(toReturn);
		try {
			getRoomByAliasStatement = ManageRoomsDBHelper.createGetRoomByAliasStateMent(this, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to update multiselect statements: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	void createTablesForArea(int area_id) {
		Area area = getArea(area_id);
		if (area == null) {
			area = new Area();
			area.setId(area_id);
		}
		try {
			s.execute(area.getCreateExitsTableStringForArea());
			s.execute(area.getCreateRoomsTableStringForArea());
			prepareStatementsForArea(area, cmgr.getConnection());
			if (dbType == POSTG) {
				s.execute(area.getCreateIndexOnExitsString());
				s.execute(area.getCreateIndexOnExitsOnRoomString());
				s.execute(area.getCreateIndexOnRoomsString());
				s.execute(area.getCreateIndexOnRoomNamesString());
			}
		} catch (SQLException e) {
			System.out.println("Failed to create tables for Area: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	private void prepareStatementsForArea(Area area, Connection conn) {
		if (area == null) return;
		try {
			String id = Integer.toString(area.getId());
			areaDimenensionStatements.put(id, conn.prepareStatement(area.createAreaDimensionString()));
			exitsSelectStatements.put(id, conn.prepareStatement(area.createSelectExitsForAreaString()));
			exitsSelectForRoomStatements.put(id, conn.prepareStatement(area.createSelectExitsForRoomString()));
			exitsInsertStatements.put(id, conn.prepareStatement(area.createInsertExitsString()));
			exitsUpdateStatements.put(id, conn.prepareStatement(area.createUpdateExitsString()));
			exitsDeleteStatements.put(id, conn.prepareStatement(area.createDeleteExitsString()));
			getAllExitsForAreaStatement.put(id, conn.prepareStatement(area.createGetAllExitsForAreaString()));
			dropExitsTableForAreaStatements.put(id, conn.prepareStatement(area.createDropExitsTableForAreaString()));
			roomUpdateColourByNameArray.add(conn.prepareStatement(area.createRoomUpdateColourByNameString()));
			roomsSelectStatements.put(id, conn.prepareStatement(area.createSelectRoomsForAreaString()));
			roomFindStatements.put(id, conn.prepareStatement(area.createRoomFindForAreaString()));
			oldestRoomFindStatements.put(id, conn.prepareStatement(area.createOldestRoomFindString()));
			totalRoomsInAreaStatements.put(id, conn.prepareStatement(area.createTotalRoomsInAreaString()));
			allRoomsForAreaSelectStatements.put(id, conn.prepareStatement(area.createGetAllRoomsInAreaString()));
			maxRoomIdStatements.put(id, conn.prepareStatement(area.createMaxRoomIdInAreaString()));
			getRoomAtPointStatements.put(id, conn.prepareStatement(area.createGetRoomAtPointString()));
			dropRoomTableForAreaStatements.put(id, conn.prepareStatement(area.createDropRoomTableForAreaString()));
			roomsInsertStatements.put(id, conn.prepareStatement(area.createInsertRoomsForAreaString()));
			roomsUpdateStatements.put(id, conn.prepareStatement(area.createUpdateRoomForAreaString()));
			roomsDeleteStatements.put(id, conn.prepareStatement(area.createDeleteRoomForAreaString()));
			zoneRoomsSelectStatements.put(id, conn.prepareStatement(area.createZoneRoomsSelect()));
			deleteDescriptionInAreaStatements.put(id, conn.prepareStatement(area.createDeleteDescriptionsInArea()));

		} catch (SQLException e) {
			System.out.println("Bugger! Failed to prepare statements for Area: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * String insertRoomColour =
	 * "INSERT INTO RoomColours(Name, Colour, Type) VALUES (?,?,?)";
	 * PreparedStatement tmpPsRC = conn.prepareStatement(insertRoomColour);
	 * statements.put("RoomColourInsert", tmpPsRC);
	 */
	public void addColourForRoomType(RoomColourHandle handle) {
		ManageRoomColourHandleDBHelper.addColourForRoomType(handle, initiated, statements, this);
	}

	/**
	 * 
	 * INSERT INTO Exits(startX, startY, endX, endY, oneWay, Level, Type,
	 * sourceRoom, targetRoom, sourceArea, targetArea, sourceLevel, targetLevel)
	 * 
	 */
	public boolean addExit(Exit exit) {
		return addExit(exit, true);
	}

	public boolean addExitFromXml(Exit exit) {
		return addExit(exit, false);
	}

	public int getTotalNumberOfRooms() {
		try {
			return ManageRoomsDBHelper.getTotalNumberOfRooms(cmgr.getConnection(), this);
		} catch (SQLException e) {
			System.out.println("Failed to get the Total Number of Rooms in Database.getTotalNumberOfRooms: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return -1;
	}

	private boolean addExit(Exit exit, boolean commit) {
		return ManageExitsDBHelper.addExit(exit, commit, initiated, statements, exitsInsertStatements, this);
	}

	public boolean addExits(ArrayList<Exit> exits) {
		try {
			return ManageExitsDBHelper.addExits(exits, statements, exitsInsertStatements, cmgr.getConnection(), this);
		} catch (SQLException e) {
			System.out.println("Failed to addExits Database.addExits: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return false;
	}

	public int addIcon(File icon, String name) {
		return ManageIconsDBHelper.addIcon(icon, name, initiated, statements, dbType, this);
	}

	public int addIcon(InputStream inputStream, String name, int length) {
		return ManageIconsDBHelper.addIcon(inputStream, name, length, initiated, statements, dbType, this);
	}

	public int addIcon(String fileName, String name) {
		return ManageIconsDBHelper.addIcon(fileName, name, initiated, this);
	}

	/**
	 * add an object to the database.
	 * 
	 * @param object
	 */
	public void addObject(MapObject object) {
		ManageObjectsDBHelper.addObject(object, statements, initiated, this);
	}

	/**
	 * Add a Room to the database.
	 * 
	 * @param room
	 * @return newest room id.
	 */
	public int addRoom(Room room) {
		return addRoom(room, true);
	}

	/**
	 * Adding room from importer. No Commit, the only difference really.
	 * 
	 * @param room
	 * @return newest room id.
	 */
	public int addXmlRoom(Room room) {
		return addRoom(room, false);
	}

	private int addRoom(Room room, boolean commit) {
		return ManageRoomsDBHelper.addRoom(room, commit, initiated, roomsInsertStatements, maxRoomIdStatements, this);
	}

	private int analyseConnectionString(String str) {
		if (str.contains("derby")) return DERBY; //$NON-NLS-1$
		if (str.contains("mysql")) return MYSQL; //$NON-NLS-1$
		if (str.contains("postgresql")) return POSTG; //$NON-NLS-1$
		return DERBY; // Assumes derby... even tho' it fucks up everything. :P
	}

	public boolean areasHasPreferredRoomDistance() {
		try {
			s.execute(tryToGetPreferredDistanceTest);
			commit();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void cleanOutExits() {
		try {
			PreparedStatement ps = statements.get(cleanOutExits[0]);
			ps.execute();
			commit();
		} catch (Exception e) {
			System.out.println(failedToCleanOutExits + e.toString());
			e.printStackTrace();
		}
	}

	void commit() {

		try {
			if (cmgr.getConnection().getAutoCommit()) {
				return;
			} else {
				cmgr.getConnection().commit();
			}
		} catch (SQLException e) {
			System.out.println(failedToCommit + e.toString());
			e.printStackTrace();
		}

	}

	private void createConnectionAndSoForthForDerby() throws SQLException {
		connectionString = connectionString.concat(preferredDBName);
		cmgr = ConnectionManager.initiateRoomConnectionManager(connectionString, this);// conn
																						// =
																						// DriverManager.getConnection(connectionString);
		// conn.setAutoCommit(true);
		s = cmgr.getConnection().createStatement();
		createDefaultValues = false;

		try {
			s.execute(genericSelect);
		} catch (Exception e) {
			try {
				System.out.println(creatingTables);
				createTablesForJavaDB();
				createDefaultValues = true;
			} catch (Exception e2) {
				boolean d = false;
				if (d) {
					System.out.println(e2.toString());
					e2.printStackTrace();
					System.exit(-1);
				} else {
					System.out.println(tablesExists);
				}
			}
		}
	}

	private void createConnectionAndSoForthForMySQL() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance(); //$NON-NLS-1$
		} catch (Exception ex) {
			System.out.println("No MySQL Driver in the Class-Path! (In /lib/ that is)\n" + ex.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			ex.printStackTrace();
			System.exit(-1);
		}

		cmgr = ConnectionManager.initiateRoomConnectionManager(connectionString, this);
		// = DriverManager.getConnection( connectionString,
		// properties.getProperty(MapperProperties.USERNAME),
		// properties.getProperty(MapperProperties.PASSWORD));
		cmgr.getConnection().setAutoCommit(true);
		s = cmgr.getConnection().createStatement();
		createDefaultValues = false;

		System.out.println("mySQL, trying to use \"USE " + preferredDBName + "\""); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			s.execute("USE " + preferredDBName + ";"); //$NON-NLS-1$ //$NON-NLS-2$
			s.execute(genericSelect);
		} catch (Exception e) {
			try {
				System.out.println(creatingTables + " for mySQL."); //$NON-NLS-1$
				s.executeUpdate(mySQLDatabaseCreation + preferredDBName + ";"); //$NON-NLS-1$
				s.execute("USE " + preferredDBName + ";"); //$NON-NLS-1$ //$NON-NLS-2$
				createTablesForMySQL();
				createDefaultValues = true;
			} catch (Exception e2) {
				boolean d = false;
				if (d) {
					System.out.println(e2.toString());
					e2.printStackTrace();
					System.exit(-1);
				} else {
					System.out.println(tablesExists);
				}
			}
		}
	}

	private void createConnectionAndSoForthForPostgreSQL() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver"); //$NON-NLS-1$
		} catch (Exception ex) {
			System.out.println("No postgreSQL Driver in the Class-Path! (In /lib/ that is)\n" + ex.toString() + "\n"); //$NON-NLS-1$ //$NON-NLS-2$
			ex.printStackTrace();
			System.exit(-1);
		}

		// connectionString = connectionString.concat(preferredDBName);
		cmgr = ConnectionManager.initiateRoomConnectionManager(connectionString, this);
		// DriverManager.getConnection( connectionString,
		// properties.getProperty(MapperProperties.USERNAME),
		// properties.getProperty(MapperProperties.PASSWORD));
		cmgr.getConnection().setAutoCommit(true);
		s = cmgr.getConnection().createStatement();
		createDefaultValues = false;
		try {
			// s.execute(useString);
			s.execute(genericSelect);
		} catch (SQLException e) {
			try {
				System.out.println("Creating DB for postgreSQL."); //$NON-NLS-1$
				s.executeUpdate(postgreSQLDatabaseCreation + preferredDBName + ";"); //$NON-NLS-1$
				cmgr.getConnection().close();
				connectionString = connectionString.replaceAll("/postgres", "/" + preferredDBName.toLowerCase()); //$NON-NLS-1$ //$NON-NLS-2$
				System.out.println("********** " + connectionString); //$NON-NLS-1$
				properties.setProperty(MapperProperties.CONNECTIONSTRING, connectionString);

				cmgr = ConnectionManager.initiateRoomConnectionManager(connectionString, this);
				// conn = DriverManager.getConnection( connectionString,
				// properties.getProperty(MapperProperties.USERNAME),
				// properties.getProperty(MapperProperties.PASSWORD));
				s = cmgr.getConnection().createStatement();
				System.out.println(creatingTables + " for postgreSQL."); //$NON-NLS-1$
				createTablesForpostgreSQL();
				createDefaultValues = true;
			} catch (SQLException e2) {
				boolean d = false;
				if (d) {
					System.out.println(e2.toString());
					e2.printStackTrace();
					System.exit(-1);
				} else {
					System.out.println(tablesExists);
				}
			}
		}

	}

	public void createDefaultValues() throws SQLException {
		addArea(defaultAreaName, 0);
	}

	private void createTablesForJavaDB() throws SQLException {
		CreateDBHelper.createTablesForJavaDB(s);
	}

	private void createTablesForMySQL() throws SQLException {
		CreateDBHelper.createTablesForMySQL(s);
	}

	private void createTablesForpostgreSQL() throws SQLException {
		CreateDBHelper.createTablesForpostgreSQL(s);
	}

	public void deleteArea(Area daArea) {
		Hashtable<Integer, Area> subAreas = getSubAreas(daArea);
		try {
			PreparedStatement da = statements.get(deleteArea[0]);
			for (Area area : subAreas.values()) {
				ArrayList<Zone> zones = getZonesForAreaForDeletionAndSave(area.getId());
				int areaId = area.getId();
				da.setInt(1, areaId);
				da.execute();
				dropTablesForArea(area.getId());
				for (Zone zone : zones) {
					deleteZone(zone);
				}
			}
		} catch (SQLException e) {
			System.out.println(failedToDeleteArea + e.toString());
			e.printStackTrace();
		}
	}

	private void dropTablesForArea(int area_id) {
		try {
			PreparedStatement dropRoomTable = dropRoomTableForAreaStatements.get(Integer.toString(area_id));
			PreparedStatement dropExitTable = dropExitsTableForAreaStatements.get(Integer.toString(area_id));
			dropRoomTable.execute();
			dropExitTable.execute();

		} catch (SQLException e) {
			System.out.println("Failed to delete sub tables: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	public void deleteExit(Exit exit) {
		ManageExitsDBHelper.deleteExit(exit, initiated, exitsDeleteStatements, statements, this);
	}

	public void deleteObject(MapObject object) {
		ManageObjectsDBHelper.deleteObject(object, initiated, statements, this);
	}

	public void deleteRoom(int old_area, Room theRoom) {
		ManageRoomsDBHelper.deleteRoom(old_area, theRoom, roomsDeleteStatements, initiated, this);
	}

	public void deleteRoomColour(RoomColourHandle handle) {
		ManageRoomColourHandleDBHelper.deleteRoomColour(handle, initiated, statements, this);
	}

	@Override
	protected void finalize() throws Throwable {
		for (PreparedStatement ps : statements.values()) {
			ps.close();
		}
		commit();
		s.close();
		cmgr.getConnection().close();
		super.finalize();
	}

	public boolean fixExitsInMapThatsBorked() {
		try {
			System.out.println("Fixing Exits..."); //$NON-NLS-1$
			s.executeUpdate(fixEnterStallExits);
			commit();
		} catch (SQLException e) {
			System.out.println("Ehh: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

		return true;
	}

	public int getAnyAreaId() {
		if (getAreas().isEmpty()) {
			try {
				createDefaultValues();
			} catch (SQLException e) {
				System.out.println("Couldn't create Default Values in getAnyAreaId():" + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
		return getAreas().iterator().next().getId();
	}

	public Area getAnyArea() {
		if (getAreas().isEmpty()) {
			try {
				createDefaultValues();
			} catch (SQLException e) {
				System.out.println("Couldn't find any area! " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
		return getAreas().iterator().next();
	}

	/**
	 * statements.put("addArea", conn.prepareStatement(
	 * "INSERT INTO Areas(Name, Parent, Type, xPos, yPos, Icon, Width, Height, Colour, BorderType, BorderColour, BorderThickness, BorderShadow, Raised) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"
	 * )); public Area(int id, String areaName, int parent, int type, int xPos,
	 * int yPos, int height, int width, int theIcon, Color colour, int
	 * borderType, Color borderColour, int borderThickness, Color shadowColour,
	 * boolean borderRaised) {
	 * 
	 * @param areaId
	 * @return
	 */
	public Area getArea(int areaId) {
		if (!initiated) return null;
		if (areas == null) areas = new ConcurrentHashMap<Integer, Area>();
		Area area = areas.get(new Integer(areaId));
		if (area == null) {
			try {
				PreparedStatement ps = statements.get(selectArea[0]);
				ps.setInt(1, areaId);
				ResultSet rs = ps.executeQuery();

				while (rs.next()) {
					area = new Area(rs);
					areas.put(new Integer(area.getId()), area);
				}
				rs.close();
				commit();
			} catch (Exception e) {
				System.out.println(failedToGetArea + e.toString());
				e.printStackTrace();
			}
		}
		return area;
	}

	synchronized public Rectangle getAreaDimension(Area area) {
		if (!initiated) return null;
		Rectangle sizeToReturn = new Rectangle();
		try {
			PreparedStatement ps = areaDimenensionStatements.get(Integer.toString(area.getId()));
			PreparedStatement psO = statements.get(areaObjectsDimension[0]);
			PreparedStatement psA = statements.get(areaAreaDimension[0]);
			psO.setInt(1, area.getId());
			psA.setInt(1, area.getId());
			ResultSet rs = ps.executeQuery();
			ResultSet rsO = psO.executeQuery();
			ResultSet rsA = psA.executeQuery();
			int xSECorner = 0;
			int xNWCorner = 0;
			int ySECorner = 0;
			int yNWCorner = 0;
			while (rs.next()) {
				xSECorner = rs.getInt(1);
				xNWCorner = rs.getInt(2);
				ySECorner = rs.getInt(3);
				yNWCorner = rs.getInt(4);
			}
			while (rsO.next()) {
				int minX = rsO.getInt(1);
				int minY = rsO.getInt(2);
				int maxX = rsO.getInt(3);
				int maxY = rsO.getInt(4);
				if (minX == 0 || minY == 0 || maxX == 0 || maxY == 0) continue;
				if (minX < xNWCorner) xNWCorner = minX;
				if (minY < yNWCorner) yNWCorner = minY;
				if (maxX > xSECorner) xSECorner = maxX;
				if (maxY > ySECorner) ySECorner = maxY;
			}

			while (rsA.next()) {
				int minX = rsA.getInt(1);
				int minY = rsA.getInt(2);
				int maxX = rsA.getInt(3);
				int maxY = rsA.getInt(4);
				if (minX == 0 || minY == 0 || maxX == 0 || maxY == 0) continue;
				if (minX < xNWCorner) xNWCorner = minX;
				if (minY < yNWCorner) yNWCorner = minY;
				if (maxX > xSECorner) xSECorner = maxX;
				if (maxY > ySECorner) ySECorner = maxY;
			}

			xSECorner += area.getPreferredRoomDistance();
			ySECorner += area.getPreferredRoomDistance();
			sizeToReturn.setLocation(xNWCorner, yNWCorner);
			sizeToReturn.setSize(xSECorner - xNWCorner, ySECorner - yNWCorner);
			ArrayList<Zone> zones = getZonesForArea(area.getId());
			java.awt.geom.Area a = new java.awt.geom.Area();
			for (Zone zone : zones) {
				Rectangle2D rect = zone.getShapeBoundariesForZone();
				if (rect != null) a.add(new java.awt.geom.Area(rect));
			}
			a.add(new java.awt.geom.Area(sizeToReturn));
			sizeToReturn = a.getBounds();
			rsO.close();
			rsA.close();
			rs.close();
			commit();
		} catch (Exception e) {
			System.out.println(failedToGetSizeOfArea + e.toString());
			e.printStackTrace();
		}
		return sizeToReturn;
	}

	/**
	 * Returns All areas... statements.put("selectAreas", conn.prepareStatement(
	 * "SELECT a_id, Name, Parent, Type, xPos, yPos, Icon, Width, Height, Colour, BorderType, BorderColour, BorderThickness, BorderShadow, Raised, BorderTitle, LabelText, LabelPosition, LabelFontSize FROM Areas"
	 * )); public Area(int id, String areaName, int parent, int type, int xPos,
	 * int yPos, int height, int width, int theIcon, Color colour, int
	 * borderType, Color borderColour, int borderThickness, Color shadowColour,
	 * boolean borderRaised, boolean borderTitle, String label, int
	 * labelPosition, int labelFontSize) {
	 * 
	 * @return ArrayList<Area> All Areas
	 */
	public ArrayList<Area> getAreas() {
		ArrayList<Area> vectorToReturn = new ArrayList<Area>();
		if (areas == null) areas = new ConcurrentHashMap<Integer, Area>();
		areas.clear();
		try {
			PreparedStatement ps = statements.get(selectAreas[0]);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Area tmpArea = new Area(rs);
				areas.put(new Integer(tmpArea.getId()), tmpArea);
				vectorToReturn.add(tmpArea);
			}
			rs.close();
			commit();
		} catch (Exception e) {
			System.out.println(failedToGetAllAreas + e.toString());
			e.printStackTrace();
		}

		return vectorToReturn;
	}

	public List<Area> getAreasForRectangle(int currentAreaId, Rectangle rectangle, int currentLevel) {
		if (!initiated) return null;
		ArrayList<Area> existingAreas = getAreas();
		List<Area> areasToReturn = Collections.synchronizedList(new ArrayList<Area>());
		for (Area area : existingAreas) {
			if (area.intersects(rectangle) && area.getParent() == currentAreaId && currentLevel == area.getLevel()) {
				areasToReturn.add(area);
			}
		}
		return areasToReturn;
	}

	public int getDatabaseType() {
		return dbType;
	}

	public ArrayList<Exit> getExistsInsideArea(int areaId) {
		return ManageExitsDBHelper.getExistsInsideArea(areaId, getAllExitsForAreaStatement);
	}

	/**
	 * Fetch exits for current rectangle to show.
	 * 
	 * @param areaId
	 * @param theAreaToFetchFor
	 * @param level
	 * @param withBorders
	 * @return
	 */
	public ArrayList<Exit> getExits(int areaId, Rectangle theAreaToFetchFor, int level, boolean withBorders) {
		return ManageExitsDBHelper.getExits(areaId, theAreaToFetchFor, level, withBorders, initiated, exitsSelectStatements, this);
	}

	/**
	 * // SELECT e_id, startX, startY, endX, endY, oneWay, Level, Type,
	 * sourceRoom, // targetRoom, sourceArea, targetArea FROM
	 * 
	 * @param room
	 * @return
	 */
	synchronized public ArrayList<Exit> getExitsForRoom(Room room) {
		return ManageRoomsDBHelper.getExitsForRoom(room, initiated, exitsSelectForRoomStatements, this);
	}

	public MapperIcon getIcon(int id) {
		return ManageIconsDBHelper.getIcon(id, initiated, icons, statements, dbType, this);
	}

	/**
	 * statements.put("selectIcons",
	 * conn.prepareStatement("SELECT ti_id, Icon, Name FROM Icons"));
	 * 
	 * @return
	 */
	public ArrayList<MapperIcon> getIcons() {
		return ManageIconsDBHelper.getIcons(initiated, icons, statements, this);
	}

	public int getLastGeneratedRoomId() {
		return lastGeneratedRoomId;
	}

	public int getLastIconIndex() {
		return ManageIconsDBHelper.getLastIconIndex(initiated, statements, this);
	}

	public int getMaxRoomId(int area_id) {
		return ManageRoomsDBHelper.getMaxRoomId(area_id, initiated, maxRoomIdStatements, this);
	}

	int getNewestArea() {
		int toReturn = 0;
		try {
			PreparedStatement ps = statements.get(newestArea[0]);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) toReturn = rs.getInt(1);
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println(failedToFetchAllObjectForArea + e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	/**
     * 
     */
	public MapObject getObject(int id) {
		return ManageObjectsDBHelper.getObject(id, statements, this);
	}

	public ArrayList<MapObject> getObjectsForArea(int areaId) {
		return ManageObjectsDBHelper.getObjectsForArea(areaId, statements, this);
	}

	public ArrayList<MapObject> getObjectsForArea(int areaId, int level) {
		return ManageObjectsDBHelper.getObjectsForArea(areaId, level, statements, this);
	}

	public ArrayList<MapObject> getObjectsForRectangle(Rectangle rect, int currentArea, int currentLevel) {
		return ManageObjectsDBHelper.getObjectsForRectangle(rect, currentArea, currentLevel, statements, this);
	}

	public Room getOldestRoomInArea(Area area) {
		return getOldestRoomInArea(area.getId());
	}

	public Room getOldestRoomInArea(int areaId) {
		return ManageRoomsDBHelper.getOldestRoomInArea(areaId, oldestRoomFindStatements, this);
	}

	/**
	 * Fetch a room
	 * 
	 * @param currentArea
	 * @param r_id
	 * @return the room.
	 */
	synchronized public Room getRoom(int currentArea, int r_id) {
		return ManageRoomsDBHelper.getRoom(currentArea, r_id, roomFindStatements, initiated, this);
	}

	/**
	 * SELECT r_id, Name, Description, Colour, Level, xPos, yPos, Area FROM
	 * " + areaName + "_Rooms WHERE xPos=? AND yPos=? AND Level=? AND Area=?"
	 * 
	 * @param tmpPoint
	 * @param currentLevel
	 * @return
	 */
	public Room getRoomAtPoint(int currentAreaId, Point tmpPoint, int level) {
		return ManageRoomsDBHelper.getRoomAtPoint(currentAreaId, tmpPoint, level, getRoomAtPointStatements, initiated, this);
	}

	public Room getRoomByAlias(String alias) {
		try {
			return ManageRoomsDBHelper.getRoomByAlias(alias, areaCreated, initiated, getRoomByAliasStatement, this, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to get room by alias in Database.getRoomByAlias:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

		return null;
	}

	public ArrayList<Room> getRoomByName(String name) {
		return ManageRoomsDBHelper.getRoomByName(name, initiated, statements, this);
	}

	/**
	 * SELECT rc_id, Name, Colour, Type FROM RoomColours
	 * 
	 * @return Vector with ColourHandles...
	 */
	public ArrayList<RoomColourHandle> getRoomColourHandles() {
		return ManageRoomColourHandleDBHelper.getRoomColourHandles(statements, initiated, this);
	}

	/**
	 * Fetching ALL rooms for an area, all levels... SELECT * FROM Rooms WHERE
	 * AREA=?;
	 * 
	 * @param areaId
	 *            the id of the area, what did you think?
	 * @return A Vector with all the Room objects.
	 */
	public ArrayList<Room> getRoomsForArea(int areaId) {
		return ManageRoomsDBHelper.getRoomsForArea(areaId, allRoomsForAreaSelectStatements, this);
	}

	/**
	 * Fetching all the rooms for an area
	 * 
	 * @param areaId
	 * @param level
	 * @return Vector with the rooms of the Area
	 */
	public ArrayList<Room> getRoomsForArea(int areaId, int level) {
		return ManageRoomsDBHelper.getRoomsForArea(areaId, level, initiated, statements, this);
	}

	/**
	 * 
	 * SELECT r_id, Name, Description, Colour, Level, xPos, yPos, Area FROM
	 * " + areaName + "_Rooms WHERE Level=? AND xPos<? AND yPos<? AND xPos>? AND
	 * yPos>? AND Area=? public Room(Point location, int level, String name,
	 * String description) {
	 * 
	 * @param currentArea
	 * @param level
	 * @param rectangle
	 * @param currentRoomId
	 * @return
	 */
	public ArrayList<Room> getRoomsForArea(Mapper mapper, Area currentArea, int level, Rectangle rectangle, Room currentRoom, int currentShadowRoom) {
		return ManageRoomsDBHelper.getRoomsForArea(	mapper, currentArea, level, rectangle, currentRoom, currentShadowRoom, initiated, roomsSelectStatements,
													this);
	}

	// "SELECT r_id, Name, Description, Colour, Level, xPos, yPos, Area FROM Rooms WHERE Level=? AND xPos<? AND yPos<? AND xPos>? AND yPos>? AND Area=?"
	public ArrayList<Room> getRoomsForMerge(Room room) {
		return ManageRoomsDBHelper.getRoomsForMerge(room, initiated, roomsSelectStatements, this);
	}

	private Hashtable<Integer, Area> getSubAreas(Area daArea) {
		Hashtable<Integer, Area> toReturn = new Hashtable<Integer, Area>();

		for (Area area : areas.values()) {
			if (area.getParent() == daArea.getId()) {
				toReturn.put(new Integer(area.getId()), area);
				toReturn.putAll(getSubAreas(area));
			}
		}
		toReturn.put(new Integer(daArea.getId()), daArea);
		return toReturn;
	}

	public ArrayList<Area> getSubAreasForArea(int id, int level) {
		ArrayList<Area> toReturn = new ArrayList<Area>();
		if (areas == null) return toReturn;

		for (Area area : areas.values()) {
			if (area.getParent() == id && area.getLevel() == level) toReturn.add(area);
		}
		return toReturn;
	}

	public int getTotalRoomsInArea(int areaId) {
		return ManageRoomsDBHelper.getTotalRoomsInArea(areaId, totalRoomsInAreaStatements);
	}

	public boolean isReady() {
		return initiated;
	}

	void prepareStatesments(Connection conn, Connection zoneConn) throws SQLException {
		putStatement(addArea, conn);
		putStatement(updateArea, conn);
		putStatement(selectArea, conn);
		putStatement(selectAreas, conn);
		putStatement(selectSubAreas, conn);
		putStatement(areaSelect, conn);
		putStatement(deleteArea, conn);
		putStatement(roomColourSelect, conn);
		putStatement(roomColourInsert, conn);
		putStatement(roomColourUpdate, conn);
		putStatement(roomColourDelete, conn);
		putStatement(selectIcon, conn);
		putStatement(selectIcons, conn);
		putStatement(addIcon, conn);
		putStatement(updateIcon, conn);
		putStatement(updateIcon2, conn);
		putStatement(maxIconIndex, conn);
		putStatement(deleteIcon, conn);
		putStatement(addObject, conn);
		putStatement(deleteObject, conn);
		putStatement(selectObjectById, conn);
		putStatement(selectObjectsByRectangle, conn);
		putStatement(selectObjectsByArea, conn);
		putStatement(updateObject, conn);
		putStatement(allObjectsForArea, conn);
		putStatement(allExitsForArea, conn);
		putStatement(newestArea, conn);
		putStatement(updateIconForAreasOnIcon, conn);
		putStatement(updateIconForObjectsOnIcon, conn);
		putStatement(updateIconName, conn);
		putStatement(exitInsert, conn);
		putStatement(deleteExit, conn);
		putStatement(exitUpdate, conn);
		putStatement(areaObjectsDimension, conn);
		putStatement(areaAreaDimension, conn);
		putStatement(zoneSelect, zoneConn);
		putStatement(zoneSelectForArea, zoneConn);
		putStatement(addZone, zoneConn);
		putStatement(addZoneRoom, zoneConn);
		putStatement(deleteZone, zoneConn);
		putStatement(deleteZoneRoomsForZone, zoneConn);
		putStatement(updateZone, zoneConn);
		putStatement(updateZoneRooms, zoneConn);
		putStatement(getZoneSelect, zoneConn);
		putStatement(getZoneRoomsForZone, zoneConn);
		putStatement(getMaxIndexForZoneRoom, zoneConn);
		putStatement(updateZoneRoomIndex, zoneConn);
		putStatement(deleteZoneRoom, zoneConn);
		putStatement(getZoneRoom, zoneConn);
		putStatement(getMaxZoneId, zoneConn);
		for (Area area : getAreas()) {
			prepareStatementsForArea(area, conn);
		}
	}

	private void putStatement(String[] arg, Connection conn) throws SQLException {
		boolean debug = false;
		if (debug) System.out.println("Trying to put:" + arg[0] + " with: " + arg[1]); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			statements.put(arg[0], conn.prepareStatement(arg[1]));
		} catch (SQLException e) {
			System.out.println("Failed to prepare statement: " + arg[0] + " : " + arg[1]); //$NON-NLS-1$ //$NON-NLS-2$
			System.out.println("Exception: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	private PreparedStatement getStatement(String[] arg) {
		return statements.get(arg[0]);
	}

	public ArrayList<Room> searchRooms(String text, int arg) {
		try {
			return ManageRoomsDBHelper.searchRooms(text, arg, this, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to search rooms Database.searchRooms:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return new ArrayList<Room>();
	}

	public void updateAllRoomColours() {
		try {
			ManageRoomColourHandleDBHelper.updateAllRoomColours(initiated, cmgr.getConnection(), roomUpdateColourByNameArray, this);
		} catch (SQLException e) {
			System.out.println("Failed update all room colours in Database.updateAllRoomColours:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * UPDATE Areas SET Name = ?, Parent = ?, Type = ?, xPos = ?, yPos = ?, Icon
	 * =?, Width = ?, Height =?, Colour=?, BorderType=?, BorderColour=?,
	 * BorderThickness=?, BorderShadow=?, Raised=? , BorderTitle=?, LabelText=?,
	 * LabelPosition=?, LabelFontSize=?, LabelColour=? WHERE a_id = ?
	 * 
	 * @param area
	 */
	public void updateArea(Area area) {
		if (!initiated) return;
		areas.put(new Integer(area.getId()), area);
		try {
			PreparedStatement ps = getStatement(updateArea);
			ps = area.prepareStatementForUpdate(ps);
			ps.execute();
			commit();
		} catch (Exception e) {
			System.out.println(failedToUpdateArea + e.toString());
			e.printStackTrace();
		}
	}

	public void updateColoursForRoomName(RoomColourHandle handle) {
		try {
			ManageRoomColourHandleDBHelper.updateColoursForRoomName(handle, cmgr.getConnection(), roomUpdateColourByNameArray);
		} catch (SQLException e) {
			System.out.println("Failed update all room colours for the name of a room in Database.updateColoursForRoomName:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * Update exit
	 * 
	 * @param old_exit
	 *            the old exit
	 * @param new_exit
	 *            the new exit
	 */
	public void updateExit(Exit old_exit, Exit new_exit) {
		ManageExitsDBHelper.updateExit(old_exit, new_exit, initiated, exitsUpdateStatements, statements, this);
	}

	/**
	 * updateIcon
	 * 
	 * @param icon
	 * @param tmpIcon
	 */
	public void updateIcon(int icon, File tmpIcon) {
		ManageIconsDBHelper.updateIcon(icon, tmpIcon, initiated, statements, dbType, this);
	}

	/**
	 * Update an MapObject
	 * 
	 * @param object
	 *            the object to update...
	 */
	public void updateObject(MapObject object) {
		ManageObjectsDBHelper.updateObject(object, statements, this);
	}

	/**
	 * 
	 * UPDATE Rooms SET Name=?, Description=?, Colour=?, Level=?, xPos =?,
	 * yPos=?, Area=? , type=? WHERE r_id=?
	 * 
	 * @param room
	 */
	public int updateRoom(int area_id, Room room) {
		return ManageRoomsDBHelper.updateRoom(area_id, room, initiated, roomsUpdateStatements, this);
	}

	public void updateRoomColourHandle(RoomColourHandle handle) {
		ManageRoomColourHandleDBHelper.updateRoomColourHandle(handle, initiated, statements, this);
	}

	/**
	 * Removes descriptions from selected Area.
	 */
	public void removeDescriptionsFromArea(int areaId) {
		ManageRoomsDBHelper.removeDescriptionsFromArea(areaId, deleteDescriptionInAreaStatements);
	}

	public void removeDescriptionsFromArea(Area area) {
		removeDescriptionsFromArea(area.getId());
	}

	public void writeAllAreas(ZipOutputStream stream) throws IOException {
		try {
			PreparedStatement ps = cmgr.getConnection().prepareStatement("SELECT * FROM areas"); //$NON-NLS-1$
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stream.write(new Area(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to get All areaas in writeAreas: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	public void writeAllExits(ZipOutputStream stream) throws IOException {
		try {
			ManageExitsDBHelper.writeAllExits(stream, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to write all Exits in Database.writeAllExits: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void writeAllIconsAsXml(ZipOutputStream stream) throws IOException {
		try {
			ManageIconsDBHelper.writeAllIconsAsXml(stream, cmgr.getConnection(), this);
		} catch (SQLException e) {
			System.out.println("Failed to write all Icons in Database.writeAllIconsAsXml: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void writeAllIcons(ZipOutputStream stream) throws IOException {
		try {
			ManageIconsDBHelper.writeAllIcons(stream, cmgr.getConnection(), this);
		} catch (SQLException e) {
			System.out.println("Failed to write all Icons in Database.writeAllIcons: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	void updateIconName(MapperIcon icon) {
		ManageIconsDBHelper.updateIconName(icon, statements);
	}

	void debug(String string) {
		if (debug) System.out.println(string);
	}

	public void writeAllObjects(ZipOutputStream stream) throws IOException {
		try {
			ManageObjectsDBHelper.writeAllObjects(stream, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to write all MapObjects in Database.writeAllObjects: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void writeAllRooms(ZipOutputStream stream) throws IOException {
		try {
			ManageRoomsDBHelper.writeAllRooms(stream, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to write all Rooms in Database.writeAllRooms: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void writeAllColourTriggers(ZipOutputStream zipFile) throws IOException {
		try {
			ManageRoomColourHandleDBHelper.writeAllColourTriggers(zipFile, cmgr.getConnection());
		} catch (SQLException e) {
			System.out.println("Failed to write all ColourTriggers in Database.writeAllColourTriggers: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void truncateDB() {
		try {
			switch (dbType) {
				case MYSQL:
					s.execute(postgreSQLtruncateTables);
					break;
				case POSTG:
					for (Area area : getAreas()) {
						dropTablesForArea(area.getId());
					}
					s.execute(postgreSQLtruncateTables);
					break;
				case DERBY:
					for (String str : javaDBtruncateTables) {
						s.execute(str);
					}
				default:
					for (String str : javaDBtruncateTables) {
						s.execute(str);
					}
					break;
			}
		} catch (SQLException e) {
			System.out.println("Oh dear, couldn't truncate the tables: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void startRestore() {
		try {
			cmgr.getConnection().setAutoCommit(false);
		} catch (SQLException e) {
			System.out.println("Couldn't set Autocommit to false: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void endRestore() {
		commit();
		try {
			cmgr.getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("Couldn't set Autocommit to true: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public ArrayList<Exit> getExitsForBetweenArea(int areaId) {
		return ManageExitsDBHelper.getExitsForBetweenArea(areaId, statements);
	}

	public ArrayList<Area> getAreasInArea(Area area) {
		ArrayList<Area> toReturn = new ArrayList<Area>();
		for (Area a : areas.values()) {
			if (a.getParent() == area.getId()) {
				toReturn.add(a);
			}
		}
		return toReturn;
	}

	public void deleteIcon(MapperIcon selectedIcon) {
		ManageIconsDBHelper.deleteIcon(selectedIcon, statements);
	}

	public int addZone(Zone zone) {
		try {
			PreparedStatement ps = statements.get(addZone[0]);
			ps = zone.prepareInsertForZoneInsert(ps);
			ps.execute();
			zoneRoomsSelectStatements.put(Integer.toString(zone.getArea_id()), cmgr.getConnection()
																					.prepareStatement(getArea(zone.getArea_id()).createZoneRoomsSelect()));
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to insert a effing Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return getMaxZoneId();
	}

	private int getMaxZoneId() {
		int toReturn = 0;
		try {
			PreparedStatement ps = getStatement(getMaxZoneId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn = rs.getInt(1);
			}

		} catch (SQLException e) {
			System.out.println("Failed to get the highest ZoneId: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	public void updateZone(Zone zone) {
		try {
			PreparedStatement ps = statements.get(updateZone[0]);
			ps = zone.prepareUpdateForZone(ps);
			ps.execute();
			commit();
		} catch (SQLException e) {
			System.out.println("Fauiled to update Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void deleteZone(int z_id) {
		deleteZone(getZone(z_id));
	}

	public Zone getZone(int zone_id) {
		Zone toReturn = null;
		try {
			PreparedStatement ps = statements.get(getZoneSelect[0]);
			ps.setInt(1, zone_id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn = new Zone(rs);
			}
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println("Fauled to get Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;

	}

	public ArrayList<Room> getRoomsForZone(int z_id, int a_id) {
		ArrayList<Room> toReturn = new ArrayList<Room>();
		try {
			PreparedStatement ps = zoneRoomsSelectStatements.get(Integer.toString(a_id));
			ps.setInt(1, z_id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new Room(rs));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to get Rooms for Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

		return toReturn;
	}

	public void deleteZone(Zone zone) {
		try {
			PreparedStatement ps1 = statements.get(deleteZone[0]);
			PreparedStatement ps2 = statements.get(deleteZoneRoomsForZone[0]);
			ps1 = zone.prepareDeleteZone(ps1);
			ps2.setInt(1, zone.getZone_id());
			ps1.execute();
			ps2.execute();

		} catch (SQLException e) {
			System.out.println("Failed to delete Zone:" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * Adds a room to the zone.
	 * 
	 * @param zone
	 * @param room
	 */
	public void addRoomToZone(Zone zone, Room room) {
		try {
			PreparedStatement ps = statements.get(addZoneRoom[0]);
			ps = zone.prepareInsertForZoneRoomInsert(ps, room);
			ps.execute();
			commit();
		} catch (SQLException e) {
			System.out.println("Couldn't Insert in zoneRooms: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	public void deleteRoomsInZone(Zone zone, Room room) {
		try {
			PreparedStatement ps = statements.get(deleteZoneRoomsForZone[0]);
			zone.prepareDeleteZoneRooms(ps, room);
			ps.execute();
			manageIndexesInZone(zone);
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to delete room in Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	@Deprecated
	public void updateRoomInZone(Zone zone, Room room) {

	}

	public ArrayList<Zone> getZones() {
		ArrayList<Zone> toReturn = new ArrayList<Zone>();
		try {
			PreparedStatement ps = statements.get(zoneSelect[0]);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new Zone(rs));
			}
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println("Couldn't get Zones: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	ArrayList<Zone>	zones						= null;
	boolean			getZonesForAreaExecuting	= false;
	private int		oldArea						= -1;

	synchronized public ArrayList<Zone> getZonesForArea(int a_id) {
		if (!getZonesForAreaExecuting) {
			getZonesForAreaExecuting = true;
			boolean debug = false;
			if (zones == null) zones = new ArrayList<Zone>();

			if (oldArea != a_id) {
				oldArea = a_id;
				zones = new ArrayList<Zone>();

				if (debug) System.out.println("Updating Zones: " + new Date(System.currentTimeMillis()).toString() + " - " + a_id); //$NON-NLS-1$ //$NON-NLS-2$
				try {
					PreparedStatement ps = getStatement(zoneSelectForArea);
					ps.setInt(1, a_id);
					ResultSet rs = ps.executeQuery();
					while (rs.next()) {
						zones.add(new Zone(rs));
					}
					rs.close();
					commit();
				} catch (SQLException e) {
					System.out.println("Couldn't get Zones for Area: " + e.toString()); //$NON-NLS-1$
					e.printStackTrace();
				}
			}
			getZonesForAreaExecuting = false;
		}
		return zones;
	}

	synchronized public ArrayList<Zone> getZonesForAreaForDeletionAndSave(int a_id) {
		boolean debug = false;
		ArrayList<Zone> zones = new ArrayList<Zone>();

		zones = new ArrayList<Zone>();

		if (debug) System.out.println("Updating Zones: " + new Date(System.currentTimeMillis()).toString() + " - " + a_id); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			PreparedStatement ps = getStatement(zoneSelectForArea);
			ps.setInt(1, a_id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				zones.add(new Zone(rs));
			}
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println("Couldn't get Zones for Area: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

		return zones;
	}

	@Deprecated
	public Zone[] getZonesForJList() {
		ArrayList<Zone> zones = getZones();
		Zone[] zoneArray = new Zone[zones.size()];
		zoneArray = zones.toArray(zoneArray);
		return zoneArray;
	}

	public Zone[] getZonesForAreaForJList(int a_id) {
		ArrayList<Zone> zones = getZonesForArea(a_id);
		Zone[] zoneArray = new Zone[zones.size()];
		zoneArray = zones.toArray(zoneArray);
		return zoneArray;

	}

	public void deleteRoomsInZone(Room room, int a_id) {
		for (Zone zone : getZonesForAreaForDeletionAndSave(a_id)) {
			deleteRoomsInZone(zone, room);
		}
	}

	public ArrayList<Zone> getZonesForRoom(int roomId, int areaId) {
		ArrayList<Zone> toReturn = new ArrayList<Zone>();
		for (Zone zone : getZonesForArea(areaId)) {
			for (Room room : zone.getRoomsInZone()) {
				if (room.getId() == roomId) {
					toReturn.add(zone);
					continue;
				}
			}
		}
		return toReturn;
	}

	public void deleteZoneReferencesForRoom(int roomId, int areaId) {
		ArrayList<Zone> zones = getZonesForRoom(roomId, areaId);
		for (Zone zone : zones) {
			ArrayList<ZoneRoom> zrList = getZoneRoom(roomId, zone, areaId);
			for (ZoneRoom zr : zrList) {
				deleteZoneRoom(zr, zone);
				manageIndexesInZone(zone);
			}
		}
	}

	public ArrayList<ZoneRoom> getZoneRoom(int roomId, Zone zone, int areaId) {
		ArrayList<ZoneRoom> toReturn = new ArrayList<ZoneRoom>();
		try {
			PreparedStatement ps = getStatement(getZoneRoom);
			ps.setInt(1, roomId);
			ps.setInt(2, zone.getZone_id());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new ZoneRoom(rs, areaId));
			}
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to get ZoneRoom: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	public ZoneRoom[] getZoneRoomsForZone(Zone zone) {
		ArrayList<ZoneRoom> zoneRooms = new ArrayList<ZoneRoom>();

		try {
			PreparedStatement ps = statements.get(getZoneRoomsForZone[0]);
			ps.setInt(1, zone.getZone_id());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				zoneRooms.add(new ZoneRoom(rs, zone.getArea_id()));
			}
			rs.close();
			commit();
		} catch (SQLException e) {
			System.out.println("Failed fetching ZoneRooms for Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		ZoneRoom[] toReturn = new ZoneRoom[zoneRooms.size()];
		toReturn = zoneRooms.toArray(toReturn);
		return toReturn;
	}

	public TreeSet<ZoneRoom> getSortedZoneRoomsForZone(Zone zone) {
		TreeSet<ZoneRoom> toReturn = new TreeSet<ZoneRoom>();

		try {
			PreparedStatement ps = statements.get(getZoneRoomsForZone[0]);
			ps.setInt(1, zone.getZone_id());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new ZoneRoom(rs, zone.getArea_id()));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed fetching ZoneRooms for Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	public int getMaxIndexForZoneRoomsInZone(Zone zone) {
		int toReturn = 0;
		try {
			PreparedStatement ps = statements.get(getMaxIndexForZoneRoom[0]);
			ps.setInt(1, zone.getZone_id());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn = rs.getInt(1);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to get Max Index for Zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	public void updateZoneRoomIndex(ZoneRoom zr) {
		try {
			PreparedStatement ps = statements.get(updateZoneRoomIndex[0]);
			ps.setInt(1, zr.getIndex());
			ps.setInt(2, zr.getId());
			ps.execute();
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to update ZoneRoomIndex: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	public void deleteZoneRoom(ZoneRoom zr, Zone zone) {
		try {
			PreparedStatement ps = statements.get(deleteZoneRoom[0]);
			ps.setInt(1, zr.getId());
			ps.execute();
			manageIndexesInZone(zone);
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to remove room from zone: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	private void manageIndexesInZone(Zone zone) {
		ZoneRoom[] zrs = getZoneRoomsForZone(zone);
		Arrays.sort(zrs);
		for (int i = 0; i < zrs.length; i++) {
			int index = i + 1;
			if (zrs[i].getIndex() != index) {
				zrs[i].setIndex(index);
				updateZoneRoomIndex(zrs[i]);
			}
		}
	}

	public void addOldZoneRoom(ZoneRoom zoneRoom) {
		try {
			PreparedStatement ps = getStatement(addZoneRoom);
			ps = zoneRoom.prepareInsertForZoneRoom(ps);
			ps.execute();
			commit();
		} catch (SQLException e) {
			System.out.println("Failed to add old ZoneRoom: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

}
