/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.database;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipOutputStream;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.Room;

/**
 * @author Ecthelion
 * 
 */
class ManageRoomsDBHelper implements SQLConstants {
	static int	lastGeneratedRoomId	= 0;

	static int addRoom(Room room, boolean commit, boolean initiated, Hashtable<String, PreparedStatement> roomsInsertStatements,
			Hashtable<String, PreparedStatement> maxRoomIdStatements, Database database) {
		if (!initiated) return -1;
		String id = Integer.toString(room.getAreaId());
		try {
			PreparedStatement ps = roomsInsertStatements.get(id);// statements.get(roomInsert[0]);
			ps = room.prepareInsertStatements(ps);
			ps.execute();
			if (commit) database.commit();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs != null) {
				if (rs.next()) {
					lastGeneratedRoomId = rs.getInt(1);
				} else {
					lastGeneratedRoomId = getMaxRoomId(room.getAreaId(), initiated, maxRoomIdStatements, database);
				}
			} else {
				lastGeneratedRoomId = getMaxRoomId(room.getAreaId(), initiated, maxRoomIdStatements, database);
			}
		} catch (SQLException e) {
			System.out.println(failedToAddRoom + e.toString());
			e.printStackTrace();
		}

		return lastGeneratedRoomId;

	}

	static void deleteRoom(int old_area, Room theRoom, Hashtable<String, PreparedStatement> roomsDeleteStatements, boolean initiated, Database database) {
		if (!initiated) return;
		try {
			PreparedStatement ps = roomsDeleteStatements.get(Integer.toString(old_area));
			ps.setInt(1, theRoom.getId());
			ps.execute();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToDeleteRoom + e.toString());
			e.printStackTrace();
		}
	}

	static ArrayList<Exit> getExitsForRoom(Room room, boolean initiated, Hashtable<String, PreparedStatement> exitsSelectForRoomStatements, Database database) {
		if (!initiated) return null;

		ArrayList<Exit> theVectorToReturn = new ArrayList<Exit>();

		try {
			PreparedStatement ps = exitsSelectForRoomStatements.get(Integer.toString(room.getAreaId()));
			ps = room.prepareGetExitsForRoom(ps);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Exit tmpExit = new Exit(rs);
				theVectorToReturn.add(tmpExit);
			}
			rs.close();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToFetchExits + e.toString());
			e.printStackTrace();
			return null;
		}
		return theVectorToReturn;
	}

	static Room getRoom(int currentArea, int r_id, Hashtable<String, PreparedStatement> roomFindStatements, boolean initiated, Database database) {
		if (!initiated) return null;

		String id = Integer.toString(currentArea);
		Room roomToReturn = null;

		try {
			PreparedStatement ps = roomFindStatements.get(id);
			if (ps == null) return null;
			ps.setInt(1, r_id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				roomToReturn = new Room(rs);
				if (roomToReturn != null) roomToReturn.setCurrentRoom(false);
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRoom + e.toString());
			e.printStackTrace();
			return null;
		}

		return roomToReturn;
	}

	static Room getRoomAtPoint(int currentAreaId, Point tmpPoint, int level, Hashtable<String, PreparedStatement> getRoomAtPointStatements, boolean initiated,
			Database database) {
		if (!initiated) return null;

		Room roomToReturn = null;

		try {
			PreparedStatement ps = getRoomAtPointStatements.get(Integer.toString(currentAreaId));
			ps.setInt(1, (int) tmpPoint.getX());
			ps.setInt(2, (int) tmpPoint.getY());
			ps.setInt(3, level);
			ps.setInt(4, currentAreaId);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				roomToReturn = new Room(rs);
				if (roomToReturn != null) roomToReturn.setCurrentRoom(false);
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRoom + e.toString());
			e.printStackTrace();
			return null;
		}

		return roomToReturn;
	}

	static int getMaxRoomId(int area_id, boolean initiated, Hashtable<String, PreparedStatement> maxRoomIdStatements, Database database) {
		if (!initiated) return -1;

		int id = 1;
		try {
			PreparedStatement ps = maxRoomIdStatements.get(Integer.toString(area_id));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) id = rs.getInt(1);
			rs.close();
			database.commit();
		} catch (Exception e) {
			System.out.println(failedToGetLastRoomId + e.toString());
			e.printStackTrace();
		}
		return id;

	}

	static Room getOldestRoomInArea(int areaId, Hashtable<String, PreparedStatement> oldestRoomFindStatements, Database database) {
		Room toReturn = null;
		String id = new Integer(areaId).toString();
		try {
			PreparedStatement ps = oldestRoomFindStatements.get(id);
			if (ps == null) return null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) toReturn = new Room(rs);
			rs.close();

			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToGetOldestRoomForArea + e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	static Room getRoomByAlias(String alias, boolean areaCreated, boolean initiated, PreparedStatement getRoomByAliasStatement, Database database,
			Connection conn) {
		if (!initiated) return null;
		boolean debug = false;

		Room roomToReturn = null;
		if (debug) return null;
		try {

			if (areaCreated || getRoomByAliasStatement == null) {
				getRoomByAliasStatement = createGetRoomByAliasStateMent(database, conn);
			}
			for (int i = 0; i < database.getAreas().size(); i++)
				getRoomByAliasStatement.setString(i + 1, alias);
			ResultSet rs = getRoomByAliasStatement.executeQuery();

			while (rs.next()) {
				roomToReturn = new Room(rs);
				if (roomToReturn != null) roomToReturn.setCurrentRoom(false);
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRoom + e.toString());
			e.printStackTrace();
			return null;
		}
		return roomToReturn;
	}

	static ArrayList<Room> getRoomByName(String name, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return null;
		ArrayList<Room> toReturn = new ArrayList<Room>();
		try {
			PreparedStatement ps = statements.get(roomSelectByName[0]);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Room roomToReturn = new Room(rs);
				if (roomToReturn != null) roomToReturn.setCurrentRoom(false);
				toReturn.add(roomToReturn);
			}
			rs.close();
			database.commit();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(failedToGetRoom + e.toString());
		}
		return toReturn;
	}

	static ArrayList<Room> getRoomsForArea(int areaId, Hashtable<String, PreparedStatement> allRoomsForAreaSelectStatements, Database database) {
		ArrayList<Room> toReturn = new ArrayList<Room>();
		try {
			PreparedStatement ps = allRoomsForAreaSelectStatements.get(Integer.toString(areaId));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn.add(new Room(rs));
			}
			database.commit();

		} catch (SQLException e) {
			System.out.println(failedToGetAllRoomsForArea + e.toString());
			e.printStackTrace();
		}

		return toReturn;
	}

	static ArrayList<Room> getRoomsForArea(int areaId, int level, boolean initiated, HashMap<String, PreparedStatement> statements, Database database) {
		if (!initiated) return null;
		ArrayList<Room> toReturn = new ArrayList<Room>();

		try {
			PreparedStatement ps = statements.get(roomSelectByArea[0]);
			ps.setInt(1, level);
			ps.setInt(2, areaId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Room tmpRoom = new Room(rs);
				toReturn.add(tmpRoom);
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRooms + e.toString());
			e.printStackTrace();
		}
		return toReturn;
	}

	static ArrayList<Room> getRoomsForArea(Mapper mapper, Area currentArea, int level, Rectangle rectangle, Room currentRoom, int currentShadowRoom,
			boolean initiated, Hashtable<String, PreparedStatement> roomsSelectStatements, Database database) {
		if (!initiated) return null;
		ArrayList<Room> vectorToReturn = new ArrayList<Room>();
		if (currentArea == null) return vectorToReturn;
		if (currentArea.getId() <= 0) return vectorToReturn;
		String id = Integer.toString(currentArea.getId());
		try {
			PreparedStatement ps = roomsSelectStatements.get(id);
			ps.setInt(1, level);
			ps.setInt(2, level - 1);
			ps.setInt(3, (int) rectangle.getX() + (int) rectangle.getWidth() + currentArea.getPreferredRoomDistance());
			ps.setInt(4, (int) rectangle.getY() + (int) rectangle.getHeight() + currentArea.getPreferredRoomDistance());
			ps.setInt(5, (int) rectangle.getX() - currentArea.getPreferredRoomDistance());
			ps.setInt(6, (int) rectangle.getY() - currentArea.getPreferredRoomDistance());
			// ps.setInt(6, currentArea.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Room tmpRoom = new Room(rs);
				boolean sameRoom = false, sameArea = false;
				if (currentRoom != null) {
					sameRoom = currentRoom.getId() == tmpRoom.getId();
					if (currentArea != null) {
						sameArea = currentArea.getId() == currentRoom.getAreaId();
					}
				}
				boolean isCurrentRoom = (sameRoom && sameArea);
				tmpRoom.setCurrentRoom(isCurrentRoom);

				tmpRoom.setShadowRoom((currentShadowRoom == tmpRoom.getId() && (currentArea != null ? currentArea.getId() : -1) == (currentRoom != null ? currentRoom.getAreaId() : -2)));
				tmpRoom.setNotification(mapper.roomNotification(tmpRoom.getId()));
				vectorToReturn.add(tmpRoom);
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRooms + e.toString());
			e.printStackTrace();
		}
		return vectorToReturn;
	}

	static ArrayList<Room> getRoomsForMerge(Room room, boolean initiated, Hashtable<String, PreparedStatement> roomsSelectStatements, Database database) {
		if (!initiated) return null;

		ArrayList<Room> vectorToReturn = new ArrayList<Room>();
		try {

			PreparedStatement ps = roomsSelectStatements.get(Integer.toString(room.getAreaId()));
			ps.setInt(1, room.getLevel());
			ps.setInt(2, (int) room.getPosition().getX() + room.getWidth() + 5);
			ps.setInt(3, (int) room.getPosition().getY() + room.getHeight() + 5);
			ps.setInt(4, (int) room.getPosition().getX() - 5);
			ps.setInt(5, (int) room.getPosition().getY() - 5);
			// ps.setInt(6, room.getAreaId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				vectorToReturn.add(new Room(rs));
			}
			rs.close();
			database.commit();

		} catch (Exception e) {
			System.out.println(failedToFetchRoomsForMerge + e.toString());
			e.printStackTrace();
		}
		return vectorToReturn;
	}

	static int getTotalRoomsInArea(int areaId, Hashtable<String, PreparedStatement> totalRoomsInAreaStatements) {
		int toReturn = -1;
		try {
			PreparedStatement ps = totalRoomsInAreaStatements.get(Integer.toString(areaId));
			if (ps == null) return 0;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) toReturn = rs.getInt(1);
			rs.close();
		} catch (SQLException e) {
			System.out.println("Couldn't execute getTotalRoomsInArea: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	public static ArrayList<Room> searchRooms(String text, int arg, Database database, Connection conn) {
		ArrayList<Room> toReturn = new ArrayList<Room>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Area> areas = database.getAreas();
		try {
			switch (arg) {
				case 1:
					ps = createStatementForSearchRooms(roomSelectByName[1], conn, database);
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(i + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;
				case 2:
					ps = createStatementForSearchRooms(roomSelectByDesc[1], conn, database);
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(i + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}

					database.commit();

					break;
				case 3:
					ps = createStatementForSearchRooms(roomSelectByNameOrDesc[1], conn, database);
					int q = 1;
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(q, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						ps.setString(q + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						q += 2;
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;
				case 4:
					ps = createStatementForSearchRooms(roomSelectByAlias[1], conn, database);
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(i + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;
				case 5:
					ps = createStatementForSearchRooms(roomSelectByNameOrAlias[1], conn, database);
					q = 1;
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(q, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						ps.setString(q + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						q += 2;
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;
				case 6:
					ps = createStatementForSearchRooms(roomSelectByDescOrAlias[1], conn, database);
					q = 1;
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(q, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						ps.setString(q + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						q += 2;
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;
				case 7:
					ps = createStatementForSearchRooms(roomSelectByNameOrDescOrAlias[1], conn, database);
					q = 1;
					for (int i = 0; i < areas.size(); i += 1) {
						ps.setString(q, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						ps.setString(q + 1, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						ps.setString(q + 2, "%" + text + "%"); //$NON-NLS-1$ //$NON-NLS-2$
						q += 3;
					}
					rs = ps.executeQuery();
					while (rs.next()) {
						toReturn.add(new Room(rs));
					}
					database.commit();

					break;

				default:
					break;
			}
			rs.close();

		} catch (SQLException e) {
			System.out.println("Couldn't Search For Rooms: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	private static PreparedStatement createStatementForSearchRooms(String original, Connection conn, Database database) throws SQLException {
		String toReturn = ""; //$NON-NLS-1$
		ArrayList<Area> areas = database.getAreas();
		for (int i = 0; i < areas.size(); i++) {
			toReturn += original.replaceAll("Rooms", "Rooms_" + areas.get(i).getId()) + (i != areas.size() - 1 ? " UNION ALL " : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		return conn.prepareStatement(toReturn);
	}

	static int updateRoom(int area_id, Room room, boolean initiated, Hashtable<String, PreparedStatement> roomsUpdateStatements, Database database) {
		if (!initiated) return -1;
		int toReturn = -1;
		if (area_id == room.getAreaId()) {
			String id = new Integer(room.getAreaId()).toString();
			try {
				PreparedStatement ps = roomsUpdateStatements.get(id);
				ps = room.prepareUpdateStatement(ps);
				ps.execute();
				database.commit();
			} catch (Exception e) {
				System.out.println(failedToUpdateRoom + e.toString());
				e.printStackTrace();
			}
		} else {
			database.deleteRoom(area_id, room);
			toReturn = database.addRoom(room);
		}
		return toReturn;
	}

	static int getTotalNumberOfRooms(Connection conn, Database database) {
		int toReturn = 0;

		try {
			PreparedStatement ps = conn.prepareStatement(createGetTotalNumberOfRoomsStatementString(database));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				toReturn += rs.getInt(1);
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Failed to getTotalNumberOfRooms: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return toReturn;
	}

	private static String createGetTotalNumberOfRoomsStatementString(Database database) {
		String toReturn = ""; //$NON-NLS-1$
		ArrayList<Area> areas = database.getAreas();

		for (int i = 0; i < areas.size(); i++) {
			toReturn += "SELECT count(r_id) FROM Rooms_" + areas.get(i).getId() + (i < areas.size() - 1 ? " UNION ALL " : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return toReturn;
	}

	static PreparedStatement createGetRoomByAliasStateMent(Database database, Connection conn) throws SQLException {
		String start = "SELECT * FROM "; //$NON-NLS-1$
		String middle = " WHERE (alias) LIKE LOWER(?)"; //$NON-NLS-1$
		String end = ""; //$NON-NLS-1$
		ArrayList<Area> areas = database.getAreas();
		for (int i = 0; i < areas.size(); i++) {
			end += start + "Rooms_" + areas.get(i).getId() + middle + (i != areas.size() - 1 ? " UNION ALL " : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return conn.prepareStatement(end);
	}

	static void writeAllRooms(ZipOutputStream stream, Connection conn) throws IOException {
		try {
			String statement = "SELECT * FROM rooms"; //$NON-NLS-1$
			PreparedStatement ps = conn.prepareStatement(statement);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				stream.write(new Room(rs).toXml().getBytes());
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("Couldn't Write All Rooms to File: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	static void removeDescriptionsFromArea(int areaId, Hashtable<String, PreparedStatement> deleteDescriptionInAreaStatements) {
		try {
			PreparedStatement ps = deleteDescriptionInAreaStatements.get(deleteDescriptionsInArea[0]);
			ps.setString(1, ""); //$NON-NLS-1$
			ps.setInt(2, areaId);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("Failed to delete descriptions in removeDescriptionsFromArea(): " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}
}
