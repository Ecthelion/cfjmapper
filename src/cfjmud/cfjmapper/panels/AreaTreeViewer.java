/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.panels;

import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.menus.PopupMenuForAreaTree;
import cfjmud.cfjmapper.objects.Area;

/**
 * 
 * @author Ecthelion
 * 
 */
class AreaNode extends DefaultMutableTreeNode {
	/**
     * 
     */
	private static final long						serialVersionUID	= -269589680319063713L;
	private final Area								area;

	protected Comparator<DefaultMutableTreeNode>	nodeComparator		= new Comparator<DefaultMutableTreeNode>() {
																			@Override
																			public int compare(DefaultMutableTreeNode o1, DefaultMutableTreeNode o2) {
																				int toReturn = o1.toString().compareToIgnoreCase(o2.toString());
																				return toReturn;
																			}

																			@Override
																			public boolean equals(Object obj) {
																				return false;
																			}

																			@Override
																			public int hashCode() {
																				int hash = 7;
																				return hash;
																			}

																		};

	public AreaNode(Area area) {
		super(area.getName());
		this.area = area;
	}

	/**
	 * @return the area
	 */
	public Area getArea() {
		return area;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void insert(MutableTreeNode newChild, int childIndex) {
		super.insert(newChild, childIndex);
		Collections.sort(this.children, nodeComparator);
	}

	@Override
	public String toString() {
		return (area == null ? "Areas" : area.getName()); //$NON-NLS-1$
	}
}

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaTreeViewer extends JTree implements TreeModelListener, MouseListener {
	/**
     * 
     */
	private static final long				serialVersionUID	= -1784203783485873661L;
	private Hashtable<Integer, AreaNode>	areas				= null;
	AreaNode								root;
	Mapper									mapper;
	private final PopupMenuForAreaTree		popupForAreaTree;
	private final boolean					debug				= false;

	public AreaTreeViewer(ArrayList<Area> areas, Mapper mapper) {
		super();
		this.mapper = mapper;
		popupForAreaTree = new PopupMenuForAreaTree(mapper);
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();

		URL url = this.getClass().getResource("/" + "Treasure map.png"); //$NON-NLS-1$ //$NON-NLS-2$
		ImageIcon iconToReturn = null;
		if (url != null) {
			iconToReturn = new ImageIcon(url);
		} else {
			iconToReturn = new ImageIcon("./icons/" + "Treasure map.png", "Treasure map.png"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		if (iconToReturn != null) {
			Image img = iconToReturn.getImage();
			Image newimg = img.getScaledInstance(16, 16, java.awt.Image.SCALE_SMOOTH);
			iconToReturn = new ImageIcon(newimg);
			renderer.setLeafIcon(iconToReturn);
			renderer.setOpenIcon(iconToReturn);
			renderer.setClosedIcon(iconToReturn);
			renderer.updateUI();
			renderer.setOpaque(false);
			setCellRenderer(renderer);
		}
		addMouseListener(this);
		getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		setAreas(areas);
	}

	public void addArea(Area area) {
		AreaNode areaNode = new AreaNode(area);
		AreaNode parent = areas.get(new Integer(area.getParent()));
		((DefaultTreeModel) getModel()).insertNodeInto(areaNode, parent, parent.getChildCount());
		((DefaultTreeModel) getModel()).reload(parent);
		areas.put(new Integer(area.getId()), areaNode);
		revalidate();
		repaint();
	}

	public void deleteArea(Area areaToDelete) {
		if (areaToDelete == null) return;
		AreaNode areaNode = areas.get(new Integer(areaToDelete.getId()));
		areas.remove(areaToDelete);
		((DefaultTreeModel) getModel()).removeNodeFromParent(areaNode);
		validate();
		repaint();
	}

	private void getChildren(AreaNode parent, int id) {

		for (AreaNode areaNode : areas.values()) {
			Area area = (areaNode).getArea();
			if (area.getParent() == id) {
				getChildren(areaNode, area.getId());
				parent.add(areaNode);
				areaNode.setParent(parent);
			}
		}
	}

	public Area getSelectedArea() {
		if (((DefaultMutableTreeNode) getLastSelectedPathComponent()) instanceof AreaNode) {
			AreaNode selectedNode = (AreaNode) getLastSelectedPathComponent();
			if (selectedNode.getArea().getId() == -1) {
				return null;
			} else {
				return selectedNode.getArea();
			}
		} else {
			return null;
		}

	}

	public void moveArea(Area newParent, Area toMove) {
		deleteArea(toMove);
		addArea(toMove);
	}

	public void setAreas(ArrayList<Area> newAreas) {
		Area rootArea = new Area();
		rootArea.setName("Areas"); //$NON-NLS-1$
		rootArea.setId(0);
		root = new AreaNode(rootArea);
		areas = new Hashtable<Integer, AreaNode>();
		for (Area area : newAreas) {
			areas.put(new Integer(area.getId()), new AreaNode(area));
		}
		getChildren(root, 0);
		areas.put(new Integer(0), root);
		setModel(new DefaultTreeModel(root));
		validate();
		repaint();
	}

	public void setSelectedArea(Area currentArea) {
		if (currentArea == null) return;
		AreaNode areaNode = areas.get(new Integer(currentArea.getId()));
		TreePath tp = new TreePath(((DefaultTreeModel) getModel()).getPathToRoot(areaNode));
		// addSelectionPath(tp);
		setSelectionPath(tp);
		scrollPathToVisible(tp);
		revalidate();
		repaint();
	}

	@Override
	public void treeNodesChanged(TreeModelEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void treeNodesInserted(TreeModelEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void treeNodesRemoved(TreeModelEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void treeStructureChanged(TreeModelEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void updateArea(Area currentSelectedArea) {
		AreaNode an = new AreaNode(currentSelectedArea);
		areas.put(new Integer(currentSelectedArea.getId()), an);
		validate();
	}

	public Area getArea(int id) {
		return areas.get(new Integer(id)).getArea();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getSource() instanceof JTree) mousePressedOnTreeViewer(e);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getSource() instanceof JTree) {
			maybeShowPopup(e, e.getSource());
		}
	}

	private void maybeShowPopup(MouseEvent e, Object o) {
		if (e.isPopupTrigger()) {
			if (o instanceof JTree) {
				if (getSelectedArea() == null) return;
				if (getSelectedArea().getId() == 0) return;
				popupForAreaTree.show(e.getComponent(), e.getX(), e.getY());
				mapper.requestFocusInWindowForMapView();
			}
		}
	}

	public void mousePressedOnTreeViewer(MouseEvent e) {
		if (getSelectedArea() != null) {
			maybeShowPopup(e, e.getSource());
			int selRow = getRowForLocation(e.getX(), e.getY());
			if (selRow != -1) {
				if (e.getClickCount() == 1) {
					if (mapper.getCurrentArea().getId() == getSelectedArea().getParent()) {
						if (debug) System.out.println("Area Type: " + getSelectedArea().getType()); //$NON-NLS-1$
						if (getSelectedArea().getType() != 0) mapper.getMapView().moveViewTo(getSelectedArea().getPosition());
					}
				} else if (e.getClickCount() == 2) {
					if (getSelectedArea().getId() == root.getArea().getId()) return;
					mapper.getMapView().moveViewTo(getSelectedArea());
					mapper.requestFocusInWindowForMapView();
				}
			}
		}
	}

}
