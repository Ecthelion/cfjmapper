/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.panels;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.VolatileImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JComponent;

import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.RoomRep;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.AreaPanel;
import cfjmud.cfjmapper.objects.AreaRepresentation;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSShape;

import com.jhlabs.awt.TextStroke;

/**
 * 
 * @author Ecthelion
 * 
 */
class Arrow extends Polygon implements MapperConstants {

	/**
     * 
     */
	private static final long	serialVersionUID	= 5398020041221263774L;

	public Arrow(int startX, int startY) {
		addPoint(startX, startY);
		addPoint(startX + 4, startY - 4);
		addPoint(startX + 4, startY + 4);
		addPoint(startX, startY);
	}
}

/**
 * 
 * @author Ecthelion
 * 
 */
class DownArrow extends Polygon {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3705831203976628850L;

	public DownArrow(int startX, int startY) {
		addPoint(startX, startY);
		addPoint(startX + 2, startY + 3);
		addPoint(startX + 4, startY);
		addPoint(startX, startY);
	}
}

/**
 * 
 * @author Ecthelion
 * 
 */
public class MapPanel extends JComponent implements MapperConstants, ComponentListener, FocusListener {

	private static Color		defaultBackgroundColour	= null;
	/**
	 * 
	 */
	private static final long	serialVersionUID		= 8899110076084962921L;

	public static Color getDefaultBackgroundColour() {

		if (defaultBackgroundColour == null) {
			String colour = MapperProperties.getInstance().getProperty(MapperProperties.DEFAULTBACKGROUNDCOLOUR);
			if (colour == null) {
				defaultBackgroundColour = new Color(225, 225, 225);
				MapperProperties.getInstance().setProperty(MapperProperties.DEFAULTBACKGROUNDCOLOUR, Integer.toString(defaultBackgroundColour.getRGB()));
			} else {
				defaultBackgroundColour = new Color(Integer.parseInt(colour));
			}
		}
		return defaultBackgroundColour;
	}

	private ArrayList<AreaRepresentation>	areaReps;
	private List<AreaPanel>					areaVector;
	private Area							currentArea		= null;
	// int currentAreaId = 0;
	private int								currentLevel	= 0;
	private Point							currentPoint;
	private Room							currentRoom;
	private final Database					database;
	private Point							diffPoint;
	private boolean							dragging		= false;
	private final Rectangle					dragRect		= new Rectangle(0, 0, 16, 16);
	private final ArrayList<Rectangle>		dragRectangles;
	private ArrayList<Exit>					exits;
	private final Color						highlighColor	= Color.CYAN;
	// private final Mapper mapper;
	private final MapView					mapView;
	private FontMetrics						metrics;
	private ArrayList<MapObject>			objectVector;
	private ArrayList<Room>					rooms;
	private ArrayList<Zone>					zones;
	private double							scale			= 1.0;
	private AffineTransform					scaleTransform;
	private Point							startDragPoint;
	Point									startMarkerRectPoint, endMarkerRectPoint, start, end;
	private final Rectangle					theAreaToShow;
	private Font							theFont;
	private boolean							updatingView;
	private VolatileImage					volatileImage;

	private int								xPos, yPos, mXPos, mYPos;
	private Point							centerPoint;

	public MapPanel(MapView mapView) {
		super();
		this.mapView = mapView;
		start = new Point();
		end = new Point();
		startMarkerRectPoint = null;
		endMarkerRectPoint = null;
		centerPoint = new Point();
		setDoubleBuffered(false);
		setBackground(defaultBackgroundColour);
		setOpaque(true);
		addComponentListener(this);
		addFocusListener(this);
		theFont = new Font("Dialog", Font.PLAIN, 9); //$NON-NLS-1$
		database = Database.getInstance();
		theAreaToShow = new Rectangle();
		exits = new ArrayList<Exit>();
		dragRectangles = new ArrayList<Rectangle>();
		areaReps = new ArrayList<AreaRepresentation>();
		zones = new ArrayList<Zone>();
		setLayout(null);
		setToolTipText(""); //$NON-NLS-1$
		reCalculateRectangle();
	}

	public void addExit(Exit exit) {
		exits.add(exit);
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentResized(ComponentEvent e) {
		reCalculateRectangle();
	}

	@Override
	public void componentShown(ComponentEvent e) {}

	private void createBuffer() {
		if (theAreaToShow.getBounds().isEmpty()) return;
		GraphicsConfiguration graphicsConfiguration = getGraphicsConfiguration();
		if (graphicsConfiguration != null) {
			volatileImage = graphicsConfiguration.createCompatibleVolatileImage((int) theAreaToShow.getWidth(), (int) theAreaToShow.getHeight());
		} else {
			volatileImage = createVolatileImage((int) theAreaToShow.getWidth(), (int) theAreaToShow.getHeight());
		}
	}

	private void drawAreaName(Graphics2D g, String name, int startX, int startY, int type) {
		if (metrics == null) metrics = g.getFontMetrics(theFont);

		Color oldColor = g.getColor();
		g.setColor(Color.BLACK);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		double theta = 0 * java.lang.Math.PI / 180;
		switch (type) {
			case NORTH:
				startY -= 4;
				startX += metrics.getHeight() / 2;
				theta = 270 * java.lang.Math.PI / 180;
				break;
			case NORTHEAST:
				startX += 4;
				startY -= 4;
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case EAST:
				startX += 5;
				break;
			case SOUTHEAST:
				startX += 4;
				startY += 4;
				// startY += metrics.getHeight();
				theta = 45 * java.lang.Math.PI / 180;
				break;
			case SOUTH:
				startY += 4;
				startX -= metrics.getHeight() / 2;
				theta = 90 * java.lang.Math.PI / 180;
				break;
			case SOUTHWEST:
				// startX -=4;
				startX -= metrics.stringWidth(name);
				// startY +=4;
				startY += metrics.stringWidth(name);
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case WEST:
				startX -= 5;
				startX -= metrics.stringWidth(name);
				break;
			case NORTHWEST:
				// startX -=5;
				startX -= metrics.stringWidth(name);
				// startY -=5;
				startY -= metrics.stringWidth(name);
				theta = 45 * java.lang.Math.PI / 180;
				break;
		}

		AffineTransform fontAT = g.getTransform();// new AffineTransform();
		fontAT.rotate(theta);
		Font theDerivedFont = theFont.deriveFont(fontAT);
		g.setFont(theDerivedFont);

		g.drawString(name, startX, startY);

		g.setFont(theFont);
		g.setColor(oldColor);
	}

	/**
	 * public static final String[] areaRepresentationTypes = { "Label", "None",
	 * "Icon (House)", "Icon and Label" };
	 * 
	 * @param dir
	 * @param areaRep
	 * @param x
	 * @param y
	 */

	private void drawAreaRep(Graphics2D graphics, int dir, int areaRep, int x, int y, Area area, int targetRoom, Exit exit, boolean source) {
		int oldX = x;
		int oldY = y;
		int xe = 5;
		int yn = 19;
		int ys = 5;
		int xw = 19;
		int yew = 7;
		int xns = 7;
		AreaRepresentation areaG;

		switch (dir) {
			case NORTH:
				x -= xns;
				y -= yn;
				break;
			case NORTHEAST:
				x += xe;
				y -= yn;
				break;
			case NORTHWEST:
				x -= xw;
				y -= yn;
				break;
			case EAST:
				x += xe;
				y -= yew;
				break;
			case WEST:
				x -= xw;
				y -= yew;
				break;
			case SOUTHEAST:
				x += xe;
				y += ys;
				break;
			case SOUTH:
				x -= xns;
				y += ys;
				break;
			case SOUTHWEST:
				x -= xw;
				y += ys;
				break;
		}
		Color oldColor = graphics.getColor();
		x -= 1;
		y -= 1;
		switch (areaRep) {
			case 0:
				graphics.setColor(Color.BLACK);
				drawAreaName(graphics, area.getName(), oldX, oldY, dir);
				break;
			case 2:
			case 4:
				graphics.setColor(area.getColour());
				Room tmpRoom = new Room(new Point(x, y), 0, "", "", 0, ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				tmpRoom.setType(source ? exit.getSourceGraphicsIconId() : exit.getTargetGraphicsIconId());
				graphics.translate(x, y);
				RoomRep.paintRoomRep(tmpRoom, graphics);
				RoomRep.paintRoomGraphics(tmpRoom, graphics);
				graphics.translate(-x, -y);
				areaG = new AreaRepresentation(x, y, 15, 15, area.getName());
				areaG.setAreaId(area.getId());
				areaG.setRoomId(targetRoom);
				registerAreaRepresentation(areaG);
				break;
			case 3:
				graphics.setColor(area.getColour());
				Room tmpRoom2 = new Room(new Point(x, y), 0, "", "", 0, ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				tmpRoom2.setType(source ? exit.getSourceGraphicsIconId() : exit.getTargetGraphicsIconId());
				graphics.translate(x, y);
				RoomRep.paintRoomRep(tmpRoom2, graphics);
				RoomRep.paintRoomGraphics(tmpRoom2, graphics);
				graphics.translate(-x, -y);
				areaG = new AreaRepresentation(x, y, 15, 15, area.getName());
				areaG.setAreaId(area.getId());
				areaG.setRoomId(targetRoom);
				registerAreaRepresentation(areaG);
				graphics.setColor(Color.BLACK);
				drawAreaName(graphics, area.getName(), oldX, oldY, dir);
				break;
			case 5:
				MapperIcon mi = database.getIcon(source ? exit.getSourceGraphicsIconId() : exit.getTargetGraphicsIconId());
				if (mi != null) {
					drawIcon(graphics, mi, x, y, dir);
					areaG = new AreaRepresentation(x, y, mi.getImage().getWidth(null), mi.getImage().getHeight(null));
					registerAreaRepresentation(areaG);
				}
				break;
		}
		graphics.setColor(oldColor);

	}

	private void drawIcon(Graphics2D graphics, MapperIcon mi, int x, int y, int dir) {
		// int oldX = x;
		// int oldY = y;
		// int xe = 5;
		int yn = 18;
		// int ys = 5;
		int xw = 18;
		int yew = 8;
		int xns = 8;
		int iconHeight = mi.getImage().getHeight(null);
		int iconWidth = mi.getImage().getWidth(null);
		switch (dir) {
			case NORTH:
				x = (x - iconWidth / 2) + xns;
				y = (y - iconHeight) + yn;
				break;
			case NORTHEAST:
				y = (y - iconHeight) + yn;
				x++;
				break;
			case NORTHWEST:
				x = (x - iconWidth) + xw;
				y = (y - iconHeight) + yn;
				break;
			case EAST:
				y -= iconHeight / 2 - yew;
				break;
			case WEST:
				y -= iconHeight / 2 - yew;
				x = (x - iconWidth) + xw;
				break;
			case SOUTHEAST:
				x++;
				y++;
				break;
			case SOUTH:
				x = (x - iconWidth / 2) + xns;
				y++;
				break;
			case SOUTHWEST:
				x = (x - iconWidth) + xw;
				y++;
				break;
		}
		x -= 1;
		y -= 1;
		graphics.drawImage(mi.getImage(), x, y, Color.black, null);

	}

	private void drawAreas(Graphics2D graphics) {
		if (areaVector == null) return;
		for (AreaPanel panel : areaVector) {
			Point tmpPoint = panel.getArea().getPosition();
			if (panel.getArea().intersects(theAreaToShow)) {
				Point correctedPoint = getCorrectedPoint(tmpPoint);
				panel.setLocation(correctedPoint);

				graphics.translate(panel.getX(), panel.getY());
				panel.paint(graphics);
				graphics.translate(-panel.getX(), -panel.getY());
			}
		}
	}

	private void drawZone(Zone zone, Graphics2D g) {
		zone.paint(g, theAreaToShow, currentLevel, this);
	}

	public void drawZones(Graphics2D g) {
		boolean debug = false;
		if (zones == null) return;
		if (debug) return;
		Zone highlighted = null;
		for (Zone zone : zones) {
			if (zone.isHighlighted()) {
				highlighted = zone;
				continue;
			}
			if (zone.intersects(theAreaToShow)) drawZone(zone, g);
		}

		if (highlighted != null) drawZone(highlighted, g);
	}

	private void drawArrow(Graphics2D g, int startX, int startY, boolean isHighlighted, int direction) {
		AffineTransform orig = g.getTransform();
		AffineTransform at = new AffineTransform(orig);
		Arrow arrow = new Arrow(startX, startY);
		at.rotate(getRotationForDir(direction), startX, startY);
		g.setTransform(at);
		g.fillPolygon(arrow);
		g.setTransform(orig);
	}

	private void drawDownArrow(Graphics2D g, int startX, int startY, boolean highlighted) {
		if (highlighted) g.setColor(highlighColor);
		g.drawLine(startX, startY, startX + 4, startY);
		g.drawLine(startX + 1, startY + 1, startX + 3, startY + 1);
		g.drawLine(startX + 2, startY + 2, startX + 2, startY + 2);
		g.setColor(Color.BLACK);
	}

	private void drawDragRects(Graphics2D graphics) {
		for (Rectangle dragRect : dragRectangles) {
			dragRect.translate((int) -diffPoint.getX(), (int) -diffPoint.getY());
			graphics.draw(dragRect);
			dragRect.translate((int) diffPoint.getX(), (int) diffPoint.getY());
		}
	}

	private void drawExit(Graphics2D graphics, Exit cme) {
		boolean debug = false;
		int startX = cme.getStartX() - mXPos;
		int startY = cme.getStartY() - mYPos;
		int endX = cme.getEndX() - mXPos;
		int endY = cme.getEndY() - mYPos;
		int type = cme.getType();
		int eType = type / BASE;
		int sType = type % BASE;
		int sourceType = cme.getSourceType();
		int targetType = cme.getTargetType();
		int sArea = cme.getSourceArea();
		int tArea = cme.getTargetArea();
		int tRoom = cme.getTargetRoomId();
		int sRoom = cme.getSourceRoomId();
		boolean highlight = cme.isHighlighted();
		int sLvl = cme.getSourceLevel();
		boolean oneWay = cme.isOneWay();
		Area targetArea = database.getArea(tArea);
		Area sourceArea = database.getArea(sArea);
		int currentAreaId = currentArea.getId();
		String targetWalkCommand = cme.getTargetWalkCommand();
		String sourceWalkCommand = cme.getSourceWalkCommand();
		boolean showStub = cme.getShowStub();
		boolean shortLine = (Math.abs(startX - endX) < 6 || Math.abs(startY - endY) < 6);
		start.setLocation(startX, startY);
		end.setLocation(endX, endY);
		switch (cme.getType()) {
			case ENTERSTALL_LEAVE:
			case ENTERSTALL:
			case ENTERSTALL * BASE:
				if (debug) System.out.println("Enter Stall Exit..."); //$NON-NLS-1$
				if (sLvl == currentLevel && sArea == currentAreaId) {
					drawFigure(graphics, 2, startX, startY);
				} else {
					drawFigure(graphics, 3, startX, startY);
				}

				break;
			case ENTERTENT_LEAVE:
			case ENTERTENT:
			case ENTERTENT * BASE:
				if (debug) System.out.println("Enter Tent Exit..."); //$NON-NLS-1$
				// System.out.println("Trying to Draw a Tent");
				if (sLvl == currentLevel && sArea == currentAreaId) {
					drawFigure(graphics, 1, startX, startY);
				} else {
					drawFigure(graphics, 3, startX, startY);
				}
				break;
			case UP_DOWN:
			case UP:
			case UP * BASE:
				if (sLvl == currentLevel && sArea == currentAreaId) {
					if (debug) System.out.println("Should paint exit UP"); //$NON-NLS-1$
					drawUpArrow(graphics, startX, startY, highlight);
				} else if (tArea == currentAreaId && cme.getTargetLevel() == currentLevel) {
					if (debug) System.out.println("Should paint exit DOWN"); //$NON-NLS-1$
					if (!cme.isOneWay()) drawDownArrow(graphics, endX, endY, highlight);
				}
				break;
			case DOWN_UP:
			case DOWN:
			case DOWN * BASE:
				if (sLvl == currentLevel && sArea == currentAreaId) {
					if (debug) System.out.println("Should paint exit DOWN"); //$NON-NLS-1$
					drawDownArrow(graphics, startX, startY, highlight);
				} else {
					if (debug) System.out.println("Should paint exit UP"); //$NON-NLS-1$
					if (!cme.isOneWay()) drawUpArrow(graphics, endX, endY, highlight);
				}
				break;
			default:
				if (sArea == tArea) {
					if (tRoom == 0) {
						start.setLocation(startX, startY);
						drawShortLine(graphics, start, highlight, eType, sourceType, shortLine);
					} else if (sRoom == 0) {
						end.setLocation(endX, endY);
						drawShortLine(graphics, end, highlight, sType, targetType, shortLine);
					} else {
						drawLine(	graphics, startX, startY, endX, endY, highlight, type, sourceType, targetType, oneWay, targetWalkCommand,
									sourceWalkCommand,
									showStub);
					}
				} else if (tArea == currentAreaId) {
					if (sourceArea != null) {
						if (cme.getTargetLevel() == currentLevel) {
							drawAreaRep(graphics, sType, cme.getTargetAreaRep(), endX, endY, sourceArea, sRoom, cme, false);
						}
					}
					if (cme.getTargetLevel() == currentLevel) {
						drawShortLine(graphics, end, highlight, sType, targetType, shortLine);
					}
				} else if (sArea == currentAreaId) {
					if (targetArea != null) {
						if (cme.getSourceLevel() == currentLevel) {
							drawAreaRep(graphics, eType, cme.getSourceAreaRep(), startX, startY, targetArea, tRoom, cme, true);
						}
					}
					if (cme.getSourceLevel() == currentLevel) drawShortLine(graphics, start, highlight, eType, sourceType, shortLine);
				}
				break;
		}
	}

	private void drawExits(Graphics2D graphics) {
		areaReps.clear();
		if (theFont == null) theFont = new Font("Dialog", Font.PLAIN, 10); //$NON-NLS-1$
		graphics.setFont(theFont);
		metrics = graphics.getFontMetrics(theFont);
		for (Exit cme : exits) {
			if (!cme.isInvisible()) drawExit(graphics, cme);
		}

	}

	private void drawFigure(Graphics2D g, int type, int startX, int startY) {
		boolean isSelected = false;
		MapperProperties properties = MapperProperties.getInstance();
		if (currentRoom != null) {
			isSelected = currentRoom.getBounds().contains(startX + 1, startY + 1);
		}
		g.setStroke(getStroke(0, false));
		switch (type) {
			case 1: // TENT
				String prop = properties.getProperty(MapperProperties.ENTERTENTREPRESENTATION, MapperProperties.ENTERTENTREPRESENTATIONDEFAULT);
				int val = Integer.parseInt(prop);
				if (val == 0) {
					g.setColor(Color.BLACK);
					g.drawLine(startX, startY + 15, startX + 8, startY);
					g.drawLine(startX + 8, startY, startX + 15, startY + 16);
					g.fillRect(startX + 13, startY + 11, 3, 5);
				} else {
					RoomGraphic rg = RoomGraphicsManager.getInstance().getShape(val);
					Color org = g.getColor();
					g.translate(startX, startY);
					for (MSShape shape : rg.getShapes()) {
						g.setColor(shape.getColour());
						if (shape.isFilled()) {
							g.fill(shape);
						} else {
							g.draw(shape);
						}
					}
					g.setColor(org);
					g.translate(-startX, -startY);
				}
				break;
			case 2: // STALL
				String esprop = properties.getProperty(MapperProperties.ENTERSTALLREPRESENTATION, MapperProperties.ENTERSTALLREPRESENTATIONDEFAULT);
				int esval = Integer.parseInt(esprop);
				if (esval == 0) {
					g.setColor(Color.ORANGE);
					for (int i = 0; i < 3; i++)
						g.drawLine(startX + 7 - i, startY + 8 + i, startX + 10 - i, startY + 8 + i);
					g.drawLine(startX + 6, startY + 11, startX + 6, startY + 14);
					g.drawLine(startX + 10, startY + 8, startX + 10, startY + 14);
					g.drawLine(startX + 8, startY + 11, startX + 8, startY + 12);
					g.drawLine(startX + 12, startY + 8, startX + 12, startY + 12);
					if (isSelected) {
						g.setColor(Color.blue);
						g.fillOval(currentRoom.getX() + 3, currentRoom.getY() + 3, currentRoom.getWidth() - 5, currentRoom.getHeight() - 5);
					}
					g.setColor(Color.BLACK);
					for (int i = 0; i < 5; i++)
						g.drawLine(startX + 3 - i, startY + 3 + i, startX + 15 - i, startY + 3 + i);
					g.drawLine(startX + 1, startY + 7, startX + 1, startY + 14);
					g.drawLine(startX + 11, startY + 5, startX + 11, startY + 14);
					g.drawLine(startX + 3, startY + 6, startX + 3, startY + 10);
					g.drawLine(startX + 14, startY + 3, startX + 14, startY + 10);
					// End Stall
					// Table
					g.setColor(Color.BLACK);
				} else {
					g.translate(startX, startY);
					Color orgc = g.getColor();
					RoomGraphic rg = RoomGraphicsManager.getInstance().getShape(esval);
					for (MSShape shape : rg.getShapes()) {
						g.setColor(shape.getColour());
						if (shape.isFilled()) {
							g.fill(shape);
						} else {
							g.draw(shape);
						}
					}
					g.setColor(orgc);
					g.translate(-startX, -startY);
				}
				// End Table
				break;
			case 3: // LEAVE/OUT == Ladder of some sort.

				break;
		}
	}

	private void drawLine(Graphics2D graphics, int startX, int startY, int endX, int endY, boolean isHighlighted, int type, int sourceType, int targetType,
			boolean oneWay, String targetWalkCommand, String sourceWalkCommand, boolean showStub) {
		int base = BASE;
		int lineType = NORMAL_TYPE;
		if (sourceType == targetType) {
			lineType = sourceType;
		} else {
			lineType = NORMAL_TYPE;
		}
		Stroke normalStroke = getStroke(lineType, false);
		Stroke thickStroke = getStroke(lineType, true);
		graphics.setColor(Exit.getColour(lineType));
		start.setLocation(startX, startY);
		end.setLocation(endX, endY);
		int dx = startX - endX;
		int dy = startY - endY;
		float distance = (float) Math.sqrt(dx * dx + dy * dy);
		boolean shortLine = distance < 6;
		start = drawShortLine(graphics, start, isHighlighted, type / base, sourceType, shortLine);
		end = drawShortLine(graphics, end, isHighlighted, type % base, targetType, shortLine);
		if (oneWay) drawArrow(graphics, endX, endY, isHighlighted, type % BASE);
		if (mapView.getMapper().isShowSpecialExits()) {
			if (!targetWalkCommand.trim().isEmpty()) {
				drawSpecialExit(graphics, targetWalkCommand, type % BASE, endX, endY);
			}
			if (!sourceWalkCommand.trim().isEmpty()) {
				drawSpecialExit(graphics, sourceWalkCommand, type / BASE, startX, startY);
			}
		}
		if (isHighlighted) {
			graphics.setStroke(thickStroke);
			graphics.setColor(highlighColor);
			if (!showStub) graphics.drawLine(start.x, start.y, end.x, end.y);
			graphics.setColor(Exit.getColour(lineType));
		}
		graphics.setStroke(normalStroke);
		if (!showStub) {
			if (!shortLine) graphics.drawLine(start.x, start.y, end.x, end.y);
		}

	}

	private void drawMarkingRect(Graphics2D graphics) {
		if (startMarkerRectPoint == null || endMarkerRectPoint == null) return;
		graphics.draw(getRectangle());
	}

	private void drawObjects(Graphics2D graphics) {
		if (objectVector == null) return;
		for (MapObject object : objectVector) {
			Point tmpPoint = object.getPosition();
			Point correctedPoint = getCorrectedPoint(tmpPoint);
			object.setLocation(correctedPoint);
			if (object.intersects(theAreaToShow)) {
				graphics.translate(object.getX(), object.getY());
				object.paint(graphics);
				graphics.translate(-object.getX(), -object.getY());
			}
		}
	}

	private void drawRoom(Graphics2D graphics, Room room) {
		if (room == null) return;
		Composite composite = graphics.getComposite();
		if (room.getLevel() == currentLevel || room.getLevel() == (currentLevel - 1)) {
			if (room.getLevel() == currentLevel - 1) {
				float alphaValue = Float.parseFloat(MapperProperties.getInstance().getProperty(MapperProperties.ALPAVALUE, MapperProperties.ALPAVALUEDEFAULT));
				graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alphaValue));
				graphics.translate(-10, 10);
			}
			room.setLocation(getCorrectedPoint(room.getPosition()));
			graphics.translate(room.getX(), room.getY());
			room.paint(graphics);
			graphics.translate(-room.getX(), -room.getY());
			if (room.getLevel() == currentLevel - 1) {
				graphics.setComposite(composite);
				graphics.translate(10, -10);
			}
		}
	}

	private void drawRooms(Graphics2D graphics) {
		if (rooms == null) return;
		for (Room room : rooms) {
			if (room.isCurrentRoom()) currentRoom = room;
			drawRoom(graphics, room);
		}
	}

	private Point drawShortLine(Graphics2D g, Point pointToReturn, boolean isHighlighted, int type, int lineType, boolean shortLine) {
		Stroke normalStroke = getStroke(lineType, false);
		Stroke thickStroke = getStroke(lineType, true);
		int length = (shortLine ? 3 : 5);
		Point p = new Point();
		switch (type) {
			case NORTH:
				p.setLocation(0, -length);
				break;
			case NORTHEAST:
				p.setLocation(length, -length);
				break;
			case EAST:
				p.setLocation(length, 0);
				break;
			case SOUTHEAST:
				p.setLocation(length, length);
				break;
			case SOUTH:
				p.setLocation(0, length);
				break;
			case SOUTHWEST:
				p.setLocation(-length, length);
				break;
			case WEST:
				p.setLocation(-length, 0);
				break;
			case NORTHWEST:
				p.setLocation(-length, -length);
				break;
		}
		g.setColor(Exit.getColour(lineType));
		if (isHighlighted) {
			g.setStroke(thickStroke);
			g.setColor(highlighColor);
			g.drawLine(pointToReturn.x, pointToReturn.y, pointToReturn.x + p.x, pointToReturn.y + p.y);
			g.setColor(Color.BLACK);
		}
		g.setStroke(normalStroke);
		g.drawLine(pointToReturn.x, pointToReturn.y, pointToReturn.x + p.x, pointToReturn.y + p.y);
		pointToReturn.translate(p.x, p.y);
		return pointToReturn;
	}

	private void drawSpecialExit(Graphics2D g, String walkCommand, int direction, int startX, int startY) {
		Color oldColor = g.getColor();
		g.setColor(Color.BLACK);

		double theta = 0 * java.lang.Math.PI / 180;
		switch (direction) {
			case NORTH:
				startY -= 2;
				startX -= metrics.getHeight() / 4;
				theta = 270 * java.lang.Math.PI / 180;
				break;
			case NORTHEAST:
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case EAST:
				break;
			case SOUTHEAST:
				startX += 2;
				theta = 45 * java.lang.Math.PI / 180;
				break;
			case SOUTH:
				startY += 2;
				startX += metrics.getHeight() / 4;
				theta = 90 * java.lang.Math.PI / 180;
				break;
			case SOUTHWEST:
				startX -= metrics.stringWidth(walkCommand) / Math.sqrt(2);
				startY += metrics.stringWidth(walkCommand) / Math.sqrt(2);
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case WEST:
				startX -= metrics.stringWidth(walkCommand);
				// startX -= metrics.stringWidth(walkCommand);
				break;
			case NORTHWEST:
				startX -= (metrics.stringWidth(walkCommand) / Math.sqrt(2));
				startY -= (metrics.stringWidth(walkCommand) / Math.sqrt(2));
				theta = 45 * java.lang.Math.PI / 180;
				break;
		}

		AffineTransform orig = g.getTransform();
		AffineTransform at = new AffineTransform(orig);
		at.rotate(theta, startX, startY);
		// Shape arrow = at.createTransformedShape(new Arrow(startX, startY));
		g.setTransform(at);
		g.drawString(walkCommand, startX, startY);
		g.setTransform(orig);
		g.setColor(oldColor);
	}

	private void drawUpArrow(Graphics2D g, int startX, int startY, boolean highlighted) {
		if (highlighted) g.setColor(highlighColor);
		g.drawLine(startX, startY, startX + 4, startY);
		g.drawLine(startX + 1, startY - 1, startX + 3, startY - 1);
		g.drawLine(startX + 2, startY - 2, startX + 2, startY - 2);
		g.setColor(Color.BLACK);
	}

	@Override
	public void focusGained(FocusEvent e) {
		repaint();
	}

	@Override
	public void focusLost(FocusEvent e) {}

	@Override
	public Component getComponentAt(int x, int y) {
		revalidate();
		while (updatingView) {
			System.out.println("Waiting for view to update."); //$NON-NLS-1$
		}
		reCalculateRectangle();
		Point p = getScaledPoint(x, y);
		Component c = super.getComponentAt(p.x, p.y);
		if (c instanceof MapPanel) {
			if (rooms != null) {
				for (Room room : rooms) {
					if (room.getLevel() != currentLevel) continue;
					if (room.getBounds().contains(p)) {
						c = room;
						return c;
					}
				}
			}
			if (areaVector != null) {
				for (AreaPanel area : areaVector) {
					if (area.getBounds().contains(p)) {
						c = area;
						return c;
					}
				}
			}
			if (objectVector != null) {
				for (MapObject obj : objectVector) {
					if (obj.getBounds().contains(p)) {
						c = obj;
						return c;
					}
				}
			}

			if (areaReps != null) {
				for (AreaRepresentation areaRep : areaReps) {
					if (areaRep.getBounds().contains(p)) {
						c = areaRep;
						return c;
					}
				}
			}

		}
		return c;
	}

	@Override
	public Component getComponentAt(Point p) {
		return getComponentAt((int) p.getX(), (int) p.getY());
	}

	protected Point getCorrectedPoint(Point point) {
		Point correctedPoint = new Point();

		mXPos = xPos;
		mYPos = yPos;

		correctedPoint.setLocation(point.getX() - mXPos, point.getY() - mYPos);
		return correctedPoint;
	}

	public int getCurrentLevel() {
		return currentLevel;
	}

	public ArrayList<Exit> getExitVector() {
		return exits;
	}

	@Override
	public Point getLocation() {
		return theAreaToShow.getLocation();
	}

	public Point getPanelLocation() {
		return super.getLocation();
	}

	private Rectangle getRectangle() {
		reCalculateRectangle();
		Rectangle toReturn = null;
		int startX = (int) startMarkerRectPoint.getX();
		int startY = (int) startMarkerRectPoint.getY();
		int endX = (int) endMarkerRectPoint.getX();
		int endY = (int) endMarkerRectPoint.getY();
		if ((startX - endX) <= 0 && (startY - endY) <= 0) { // SE
			toReturn = new Rectangle(startX, startY, endX - startX, endY - startY);
		} else if ((startX - endX) >= 0 && (startY - endY) >= 0) { // NW
			toReturn = new Rectangle(endX, endY, startX - endX, startY - endY);
		} else if ((startX - endX) >= 0 && (startY - endY) <= 0) {// SW
			toReturn = new Rectangle(endX, startY, startX - endX, endY - startY);
		} else if ((startX - endX) <= 0 && (startY - endY) >= 0) { // NE
			toReturn = new Rectangle(startX, endY, endX - startX, startY - endY);
		}
		return toReturn;
	}

	public Rectangle getRectToShow() {
		reCalculateRectangle();
		return theAreaToShow;
	}

	private double getRotationForDir(int direction) {
		double theta = 0;
		switch (direction) {
			case NORTH:
				theta = 270 * java.lang.Math.PI / 180;
				break;
			case NORTHEAST:
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case EAST:
				break;
			case SOUTHEAST:
				theta = 45 * java.lang.Math.PI / 180;
				break;
			case SOUTH:
				theta = 90 * java.lang.Math.PI / 180;
				break;
			case SOUTHWEST:
				theta = 135 * java.lang.Math.PI / 180;
				break;
			case WEST:
				theta = 180 * java.lang.Math.PI / 180;
				break;
			case NORTHWEST:
				theta = 225 * java.lang.Math.PI / 180;
				break;
		}
		return theta;
	}

	/**
	 * @return the scale
	 */
	public double getScale() {
		return scale;
	}

	public Point getScaledPoint(int x, int y) {
		return getScaledPoint(new Point(x, y));
	}

	public Point getScaledPoint(Point p) {
		reCalculateRectangle();
		if (scale == 1.0) return p;
		Point2D.Double tmpPoint = new Point2D.Double(p.getX(), p.getY());
		try {
			Point2D p2 = scaleTransform.inverseTransform(p, null);
			tmpPoint = new Point2D.Double(p2.getX(), p2.getY());
		} catch (NoninvertibleTransformException e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return null;
		}

		return new Point((int) (tmpPoint.x), (int) (tmpPoint.y));
	}

	public ArrayList<Room> getSelectedRooms() {
		ArrayList<Room> toReturn = new ArrayList<Room>();
		if (startMarkerRectPoint == null || endMarkerRectPoint == null) return toReturn;
		Rectangle rect = getRectangle();

		for (Room room : rooms) {
			if (room.getLevel() != currentLevel) continue;
			if (rect.intersects(room.getBounds())) {
				toReturn.add(room);
			}
		}
		startMarkerRectPoint = null;
		endMarkerRectPoint = null;
		return toReturn;
	}

	public ArrayList<Exit> getExitsForRoom(Room room) {
		ArrayList<Exit> toReturn = new ArrayList<Exit>();
		for (Exit exit : exits) {
			if (exit.getTargetRoomId() == room.getId() || exit.getSourceRoomId() == room.getId()) {
				toReturn.add(exit);
			}
		}
		return toReturn;
	}

	private Stroke getStroke(int type, boolean thick) {
		Stroke normalStroke;
		Stroke thickStroke;
		if (type == NORMAL_TYPE) {
			normalStroke = new BasicStroke(0.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
			thickStroke = new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
		} else {
			normalStroke = new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
			thickStroke = new BasicStroke(5.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
		}
		return (thick ? thickStroke : normalStroke);
	}

	@Override
	public String getToolTipText(MouseEvent me) {
		JComponent comp = (JComponent) getComponentAt(me.getPoint());
		if (comp != null) {
			return comp.getToolTipText();
		} else {
			return ""; //$NON-NLS-1$
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		createBuffer();
		if (volatileImage == null) return;
		do {
			GraphicsConfiguration graphicsConfiguration = this.getGraphicsConfiguration();
			int valCode = volatileImage.validate(graphicsConfiguration);

			if (valCode == VolatileImage.IMAGE_INCOMPATIBLE) {
				createBuffer();
			}
			Graphics2D graphics = (Graphics2D) volatileImage.getGraphics();
			graphics.setPaint(getDefaultBackgroundColour());
			graphics.fillRect(0, 0, volatileImage.getWidth(), volatileImage.getHeight());
			graphics.setPaint(Color.BLACK);
			graphics.setStroke(new BasicStroke(1.0f));
			drawZones(graphics);
			drawStuff(graphics);
			drawZoneLabels(graphics);

			/* For getComponentAt function... It isn't used here at all. */
			scaleTransform = g2.getTransform();
			scaleTransform.scale(scale, scale);
			/* ********************************************************* */
			Image image = null;
			if (scale == 1.0d) {
				image = volatileImage;
			} else {
				image = volatileImage.getScaledInstance(getWidth(), getHeight(), VolatileImage.SCALE_AREA_AVERAGING);
			}
			g2.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		} while (volatileImage.contentsLost());
	}

	public void drawStuff(Graphics2D graphics) {
		drawObjects(graphics);
		drawAreas(graphics);
		drawRooms(graphics);
		drawExits(graphics);
		drawMarkingRect(graphics);
		if (dragging) {
			drawDragRects(graphics);
		}
	}

	private void reCalculateRectangle() {
		Dimension oldSize = theAreaToShow.getSize();
		theAreaToShow.setSize((int) (getWidth() / scale), (int) (getHeight() / scale));
		int widthDiff = theAreaToShow.width - oldSize.width;
		int heightDiff = theAreaToShow.height - oldSize.height;
		theAreaToShow.getLocation().translate(-widthDiff, -heightDiff);
	}

	private void registerAreaRepresentation(AreaRepresentation areaRepresentation) {
		if (areaReps == null) areaReps = new ArrayList<AreaRepresentation>();
		areaReps.add(areaRepresentation);
	}

	public void replaceExits(ArrayList<Exit> replaceExits) {
		for (int i = 0; i < exits.size(); i++) {
			for (Exit exit : replaceExits) {
				if (exits.get(i).getId() == exit.getId()) {
					exits.set(i, exit);
				}
			}
		}
	}

	public void setAreas(List<Area> areas) {
		this.areaVector = Collections.synchronizedList(new ArrayList<AreaPanel>());
		for (Area area : areas) {
			if (area != null) {
				AreaPanel panel = area.getPanel(mapView.getMapper());
				areaVector.add(panel);
			}
		}
	}

	public void setCurrentLevel(int currentLevel) {
		this.currentLevel = currentLevel;
		reCalculateRectangle();
	}

	public void setDragging(boolean dragging) {
		reCalculateRectangle();
		this.dragging = dragging;
	}

	public void setDraggingAreaSizes(ArrayList<AreaPanel> panels) {
		dragRectangles.clear();
		for (AreaPanel areaPanel : panels) {
			dragRectangles.add(areaPanel.getBounds());
		}
	}

	public void setDraggingObjectSizes(ArrayList<MapObject> objects) {
		dragRectangles.clear();
		for (MapObject object : objects) {
			dragRectangles.add(object.getBounds());
		}

	}

	public void setDraggingPoint(Point dragPoint, Point dragModPoint) {
		reCalculateRectangle();
		currentPoint = dragPoint;
		int xDiff = (int) (startDragPoint.getX() - currentPoint.getX());
		int yDiff = (int) (startDragPoint.getY() - currentPoint.getY());
		diffPoint = getScaledPoint(xDiff, yDiff);
		dragRect.setLocation(getScaledPoint(dragPoint.x - dragModPoint.x, dragPoint.y - dragModPoint.y));
	}

	public void setDraggingSize(Dimension size) {
		reCalculateRectangle();
		dragRect.setSize(size);
	}

	public void setDraggingSizes(List<Room> highlightedRooms) {
		dragRectangles.clear();
		for (Room room : highlightedRooms) {
			dragRectangles.add(room.getBounds());
		}
	}

	public void setEndMarkerRectPoint(Point endMarkerRectPoint) {
		reCalculateRectangle();
		this.endMarkerRectPoint = getScaledPoint(endMarkerRectPoint);
	}

	public void setCenterLocation(Point point) {
		centerPoint = point;
		xPos = (int) (point.getX() - (theAreaToShow.getWidth() / 2));
		yPos = (int) (point.getY() - (theAreaToShow.getHeight() / 2));
		theAreaToShow.setLocation(xPos, yPos);
		updateObjects();
		repaint();
		reCalculateRectangle();
	}

	private void setObjects(ArrayList<MapObject> objectsForRectangle) {
		objectVector = objectsForRectangle;
		if (objectVector == null) objectVector = new ArrayList<MapObject>();
	}

	/**
	 * @param scale
	 *            the scale to set
	 */
	public void setScale(double scale) {
		this.scale = scale;
		reCalculateRectangle();
	}

	@Override
	public void setSize(Dimension dimension) {
		super.setSize(dimension);
		theAreaToShow.setSize(dimension);
		reCalculateRectangle();
	}

	@Override
	public void setPreferredSize(Dimension dimension) {
		super.setPreferredSize(dimension);
		theAreaToShow.setSize(dimension);
		reCalculateRectangle();
	}

	public void setStartDragPoint(Point point) {
		reCalculateRectangle();
		startDragPoint = point;
	}

	public void setStartMarkerRectPoint(Point startMarkerRectPoint) {
		reCalculateRectangle();
		this.startMarkerRectPoint = getScaledPoint(startMarkerRectPoint);
	}

	public void updateExit(ArrayList<Exit> exitsArray) {
		for (int o = 0; o < exits.size(); o++) {
			for (int i = 0; i < exitsArray.size(); i++) {
				if (exits.get(o).getId() == exitsArray.get(i).getId()) {
					exits.set(o, exitsArray.get(i));
				}
			}

		}

	}

	public void updateExit(Exit exit) {
		for (int i = 0; i < exits.size(); i++) {
			if (exits.get(i).getId() == exit.getId()) exits.set(i, exit);
		}
	}

	public void updateView() {
		updatingView = true;
		reCalculateRectangle();
		if (currentArea == null) {
			currentArea = mapView.getCurrentArea();
		}
		updateObjects();
		repaint();
		updatingView = false;
	}

	public void updateObjects() {
		exits = database.getExits(currentArea.getId(), getRectToShow(), getCurrentLevel(), true);
		if (exits == null) exits = new ArrayList<Exit>();
		rooms = database.getRoomsForArea(	mapView.getMapper(), currentArea, getCurrentLevel(), getRectToShow(), mapView.getCurrentRoom(),
											mapView.getCurrentShadowId());
		if (rooms == null) rooms = new ArrayList<Room>();
		setAreas(database.getAreasForRectangle(currentArea.getId(), getRectToShow(), getCurrentLevel()));
		setObjects(database.getObjectsForRectangle(getRectToShow(), currentArea.getId(), getCurrentLevel()));
		zones = database.getZonesForArea(currentArea.getId());
		if (zones == null) zones = new ArrayList<Zone>();
	}

	public void setViewableRectangle(Rectangle rect) {
		theAreaToShow.setBounds(rect);
	}

	/**
	 * @return the currentArea
	 */
	public Area getCurrentArea() {
		return currentArea;
	}

	/**
	 * @param currentArea
	 *            the currentArea to set
	 */
	public void setCurrentArea(Area newArea) {
		if (newArea == null) return;
		if (currentArea == null) {
			currentArea = newArea;
			updateZones(newArea);
		}
		if (currentArea.getId() != newArea.getId()) updateZones(newArea);
		currentArea = newArea;
	}

	public void updateZones(Area newArea) {
		if (newArea == null) return;
		zones = database.getZonesForArea(newArea.getId());
	}

	public void updateZones() {
		if (currentArea == null) return;
		updateZones(currentArea);
	}

	/**
	 * @param defaultBackgroundColour
	 *            the defaultBackgroundColour to set
	 */
	public static void setDefaultBackgroundColour(Color defaultBackgroundColour) {
		MapPanel.defaultBackgroundColour = defaultBackgroundColour;
	}

	public void translate(int x, int y) {
		xPos += x;
		yPos += y;
		theAreaToShow.setLocation(xPos, yPos);
		reCalculateRectangle();
	}

	public void translate(Point closeToBorder) {
		translate(closeToBorder.x, closeToBorder.y);
	}

	/**
	 * @return the zones
	 */
	public ArrayList<Zone> getZones() {
		return zones;
	}

	public Point getCenterPoint() {
		return centerPoint;
	}

	private ArrayList<ZoneLineLabelHolder>		zoneLineLabels;
	private ArrayList<ZoneSquareLabelHolder>	zoneSquareLabels;

	public void registerZoneLineLabel(TextStroke ts, Shape outline, Color colour) {
		if (zoneLineLabels == null) zoneLineLabels = new ArrayList<ZoneLineLabelHolder>();
		zoneLineLabels.add(new ZoneLineLabelHolder(colour, ts, outline));
	}

	public void registerZoneSquareLabel(int x, int y, String text, Color colour, Font fontToWriteWith) {
		if (zoneSquareLabels == null) zoneSquareLabels = new ArrayList<ZoneSquareLabelHolder>();
		zoneSquareLabels.add(new ZoneSquareLabelHolder(x, y, text, colour, fontToWriteWith));
	}

	public void drawZoneLabels(Graphics2D g) {
		g.translate(5, 5);
		if (zoneLineLabels == null) zoneLineLabels = new ArrayList<ZoneLineLabelHolder>();
		if (zoneSquareLabels == null) zoneSquareLabels = new ArrayList<ZoneSquareLabelHolder>();
		Stroke s = g.getStroke();
		Color c = g.getColor();
		for (ZoneLineLabelHolder zlh : zoneLineLabels) {
			Font font = zlh.ts.getFont();
			FontMetrics fm = g.getFontMetrics(font);
			int height = fm.getHeight();
			g.translate(0, height / 2);
			g.setStroke(zlh.ts);
			g.setColor(zlh.colour);
			g.draw(zlh.outline);
			g.translate(0, -height / 2);
		}
		g.setStroke(s);
		g.translate(-5, -5);

		for (ZoneSquareLabelHolder zsl : zoneSquareLabels) {
			g.setColor(zsl.colour);
			g.setFont(zsl.fontToWriteWith);
			g.drawString(zsl.text, zsl.x, zsl.y);
		}
		g.setColor(c);
		zoneLineLabels.clear();
		zoneSquareLabels.clear();
	}

	class ZoneSquareLabelHolder {
		int		x, y;
		String	text;
		Color	colour;
		Font	fontToWriteWith;

		public ZoneSquareLabelHolder(int x, int y, String text, Color colour, Font fontToWriteWith) {
			this.text = text;
			this.x = x;
			this.y = y;
			this.colour = colour;
			this.fontToWriteWith = fontToWriteWith;
		}
	}

	class ZoneLineLabelHolder {
		Color		colour;
		TextStroke	ts;
		Shape		outline;

		public ZoneLineLabelHolder(Color colour, TextStroke ts, Shape outline) {
			this.colour = colour;
			this.ts = ts;
			this.outline = outline;
		}
	}

}

class UpArrow extends Polygon {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -6355629108017754469L;

	public UpArrow(int startX, int startY) {
		addPoint(startX, startY);
		addPoint(startX + 2, startY - 3);
		addPoint(startX + 4, startY);
		addPoint(startX, startY);
	}
}
