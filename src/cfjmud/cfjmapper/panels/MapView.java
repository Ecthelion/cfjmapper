/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SpringLayout;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.custom.UniqueArrayList;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.AreaDialog;
import cfjmud.cfjmapper.dialogs.DirectionChooserDialog;
import cfjmud.cfjmapper.dialogs.MapObjectDialog;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.menus.PopupMenuForArea;
import cfjmud.cfjmapper.menus.PopupMenuForExit;
import cfjmud.cfjmapper.menus.PopupMenuForMapObject;
import cfjmud.cfjmapper.menus.PopupMenuForMapRoom;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.AreaPanel;
import cfjmud.cfjmapper.objects.AreaRepresentation;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.profile.User;

/**
 * @author Ecthelion
 * 
 */
public class MapView extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener, AdjustmentListener, MapperConstants {

	private Room					currentRoom			= null;
	private Room					currentShadowRoom	= null;

	private final Mapper			mapper;
	private final Database			database;
	private Area					currentArea;
	private MapPanel				mainMap;
	private JScrollBar				verticalScrollBar, horizontalScrollBar;
	private int						dragYmod			= 0, dragXMod = 0;
	private int						scrollX, scrollY;
	private Exit					selectedExit;
	private boolean					addNewRoom			= false, addNewExit = false, addNewArea = false, addNewObject = false;
	private int						newExitDirection	= 0;
	private boolean					selectRoomForOldExit, moveSelectedArea;
	private Point					startCoordinates, endCoordinates, startDragPoint, endDragPoint;
	private Room					startRoom, endRoom;
	private Room					dragRoom			= null;
	private MapObject				dragObject			= null;
	private AreaPanel				dragArea;
	private ArrayList<Exit>			highlightedExits	= null;
	private List<Room>				highlightedRooms	= null;
	private ArrayList<Room>			mergeVector			= null;
	private boolean					draggingArea, draggingObject = false, draggingRoom = false, firstExitAdded = false;
	private Point					dragModPoint		= null;
	boolean							multiMarker, justMultiSelected;
	private MapObject				currentMapObject	= null;
	private Area					currentSelectedArea;
	private Room					aRoomWithAView		= null;
	private PopupMenuForMapRoom		popupForMapRoom;
	private PopupMenuForArea		popupForArea;
	private PopupMenuForExit		popupForExit;
	private PopupMenuForMapObject	popupForMapObject;
	private int						currentShadowId;
	private JButton					cornerButton;
	private int						currentRoomId;
	private int						mouseButton;
	private boolean					draggingView;
	private ViewDragThread			draggingThread;
	private boolean					autoUpdateOfPosition;
	private int						oldBarY;
	private int						oldBarX;
	private boolean					debug;
	/**
     * 
     */
	private static final long		serialVersionUID	= -3296663550418977395L;

	public MapView(Mapper mapper) {
		this.mapper = mapper;
		this.database = Database.getInstance();
		draggingThread = null;
		initiateComponents();
	}

	private void initiateComponents() {
		setMainMap(new MapPanel(this));
		highlightedRooms = new UniqueArrayList<Room>();
		highlightedExits = new ArrayList<Exit>();
		popupForMapObject = new PopupMenuForMapObject(mapper);
		popupForArea = new PopupMenuForArea(mapper);
		popupForMapRoom = new PopupMenuForMapRoom(mapper);
		popupForExit = new PopupMenuForExit(mapper);
		cornerButton = new JButton("*"); //$NON-NLS-1$
		getMainMap().addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent ce) {
				mapper.updateMainMap();
			}
		});
		verticalScrollBar = new JScrollBar(JScrollBar.VERTICAL);
		horizontalScrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
		verticalScrollBar.addAdjustmentListener(this);
		horizontalScrollBar.addAdjustmentListener(this);
		mainMap.addMouseListener(this);
		mainMap.addKeyListener(mapper);
		mainMap.addMouseMotionListener(this);

		SpringLayout sl = new SpringLayout();
		int inline = 1;
		sl.putConstraint(SpringLayout.NORTH, getMainMap(), inline, SpringLayout.NORTH, this);
		sl.putConstraint(SpringLayout.WEST, getMainMap(), inline, SpringLayout.WEST, this);
		sl.putConstraint(SpringLayout.EAST, getMainMap(), inline, SpringLayout.WEST, getVerticalScrollBar());
		sl.putConstraint(SpringLayout.SOUTH, getMainMap(), inline, SpringLayout.NORTH, getHorizontalScrollBar());

		sl.putConstraint(SpringLayout.NORTH, getVerticalScrollBar(), inline, SpringLayout.NORTH, this);
		sl.putConstraint(SpringLayout.EAST, getVerticalScrollBar(), inline, SpringLayout.EAST, this);
		sl.putConstraint(SpringLayout.SOUTH, getVerticalScrollBar(), inline, SpringLayout.NORTH, cornerButton);

		sl.putConstraint(SpringLayout.WEST, getHorizontalScrollBar(), inline, SpringLayout.WEST, this);
		sl.putConstraint(SpringLayout.EAST, getHorizontalScrollBar(), inline, SpringLayout.WEST, cornerButton);
		sl.putConstraint(SpringLayout.SOUTH, getHorizontalScrollBar(), inline, SpringLayout.SOUTH, this);

		sl.putConstraint(SpringLayout.SOUTH, cornerButton, inline, SpringLayout.SOUTH, this);
		sl.putConstraint(SpringLayout.EAST, cornerButton, inline, SpringLayout.EAST, this);

		setLayout(sl);
		add(getMainMap());
		add(getHorizontalScrollBar());
		add(getVerticalScrollBar());
		add(cornerButton);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int multiplier = currentArea.getPreferredRoomDistance();
		if (e.isControlDown()) multiplier = currentArea.getPreferredRoomDistance() * 10;

		if (!e.isShiftDown()) {
			moveMainMap(0, e.getWheelRotation() * multiplier);
		} else {
			moveMainMap(e.getWheelRotation() * multiplier, 0);
		}
		mapper.updateMainMap(true);
	}

	void moveMainMap(int x, int y) {
		Point p = mainMap.getCenterPoint();
		p.translate(x, y);
		setMapLocation(p);
	}

	class ViewDragThread extends Thread {
		Point	p	= null;

		public ViewDragThread(Point p) {
			this.p = p;
		}

		public void setPoint(Point p) {
			this.p = p;
		}

		@Override
		public void run() {
			long mod = 100;
			long t = 0;
			System.out.println("Starting Thread."); //$NON-NLS-1$
			while (p != null) {
				t = System.currentTimeMillis() % mod;
				if (t > 0 && t < 50) {
					moveMainMap(isCloseToBorder(p).x, isCloseToBorder(p).y);
					reCalculateScrollBars();
				}
			}
			System.out.println("Ending Thread."); //$NON-NLS-1$
		}
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		Point p = me.getPoint();
		if (mouseButton == MouseEvent.BUTTON2) {
			if (draggingView) {
				Point diff = new Point(p.x - dragModPoint.x, p.y - dragModPoint.y);
				mainMap.translate(diff.x, diff.y);
				mainMap.updateView();
				dragModPoint = me.getPoint();
				horizontalScrollBar.setValue(horizontalScrollBar.getValue() + diff.x + isCloseToBorder(me.getPoint()).x);
				verticalScrollBar.setValue(verticalScrollBar.getValue() + diff.y + isCloseToBorder(me.getPoint()).y);
				if (isCloseToBorder(me.getPoint()).x != 0 || isCloseToBorder(me.getPoint()).y != 0) {
					if (draggingThread == null) {
						draggingThread = new ViewDragThread(p);
						draggingThread.start();
					}
				}
				if (draggingThread != null) draggingThread.setPoint(me.getPoint());
				// calculateScrollBars();
				reCalculateScrollBars();
			} else {
				draggingView = true;
				dragModPoint = me.getPoint();
			}
		} else if (multiMarker) {
			getMainMap().setEndMarkerRectPoint(p);
			repaint();
		} else {
			Object source = me.getSource();
			if (((Container) source).getComponentAt(p) instanceof Room && !draggingRoom && !draggingArea && !draggingObject) {
				draggingRoom = true;
				dragRoom = (Room) ((Container) source).getComponentAt(p);
				dragModPoint = new Point(p.x - dragRoom.getLocation().x, p.y - dragRoom.getLocation().y);
				mainMap.setStartDragPoint(p);
				if (highlightedRooms.contains(dragRoom)) {

				} else {
					for (Room room : highlightedRooms) {
						room.setHighlighted(false);
					}
					for (Exit exit : highlightedExits) {
						exit.setHighlighted(false);
					}
					mainMap.replaceExits(highlightedExits);
					highlightedExits.clear();
					highlightedRooms.clear();
					dragRoom.setHighlighted(true);
					highlightedRooms.add(dragRoom);
					highlightedExits.addAll(database.getExitsForRoom(dragRoom));
					for (Exit exit : highlightedExits) {
						exit.setHighlighted(true);
					}
				}
				mainMap.setDraggingSizes(highlightedRooms);
				mainMap.setDragging(true);
				startDragPoint = me.getPoint();
			}

			if (draggingRoom || draggingArea || draggingObject) {
				maybeMoveTheMainMap(p);
				mainMap.setDraggingPoint(p, dragModPoint);
				mainMap.repaint();
			}

			if (((Container) source).getComponentAt((int) p.getX(), (int) p.getY()) instanceof AreaPanel && !draggingArea && !draggingRoom && !draggingObject) {
				draggingArea = true;
				dragArea = (AreaPanel) ((Container) source).getComponentAt((int) p.getX(), (int) p.getY());
				dragModPoint = new Point((int) p.getX() - (int) dragArea.getLocation().getX(), (int) p.getY() - (int) dragArea.getLocation().getY());

				ArrayList<AreaPanel> panels = new ArrayList<AreaPanel>();
				panels.add(dragArea);
				mainMap.setDraggingAreaSizes(panels);
				mainMap.setDraggingSize(dragArea.getSize());
				mainMap.setDragging(true);
				mainMap.setStartDragPoint(p);
				startDragPoint = me.getPoint();
			}

			if (((Container) source).getComponentAt((int) p.getX(), (int) p.getY()) instanceof MapObject && !draggingArea && !draggingRoom && !draggingObject
				&& !draggingObject) {
				draggingObject = true;
				dragObject = (MapObject) ((Container) source).getComponentAt((int) p.getX(), (int) p.getY());
				dragModPoint = new Point((int) p.getX() - (int) dragObject.getLocation().getX(), (int) p.getY() - (int) dragObject.getLocation().getY());

				ArrayList<MapObject> panels = new ArrayList<MapObject>();
				panels.add(dragObject);
				mainMap.setDraggingObjectSizes(panels);
				mainMap.setDraggingSize(dragObject.getSize());
				mainMap.setStartDragPoint(p);
				mainMap.setDragging(true);
				startDragPoint = me.getPoint();
			}

		}
	}

	private Point isCloseToBorder(Point point) {
		if (point == null) return new Point(0, 0);
		int move = 10;
		int xmod = 0;
		int ymod = 0;
		if (point.getX() < 10) xmod = -move;
		if (point.getX() > (getMainMap().getWidth() - verticalScrollBar.getWidth() - 10)) xmod = move;
		if (point.getY() < 10) ymod = -move;
		if (point.getY() > (getMainMap().getHeight() - horizontalScrollBar.getHeight() - 10)) ymod = move;
		Point toReturn = new Point(xmod, ymod);
		return toReturn;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		e.getPoint().translate(mapper.getTreeViewer().getWidth(), 0);

	}

	@Override
	public void mouseClicked(MouseEvent me) {
		validate();
		mouseButton = me.getButton();

		if (!selectRoomForOldExit) {
			for (Exit exit : getMainMap().getExitVector()) {
				if (exit.isHighlighted()) {
					exit.setHighlighted(false);
					setSelectedExit(null);
				}
			}
		}
		mapper.repaintMainMap();

		if (!addNewRoom && !addNewArea && !addNewExit && !addNewObject) {
			Point point = translatePointToMapCoordinates(me.getPoint(), 0);

			for (Exit exit : getMainMap().getExitVector()) {
				exit.calculateLine();
				if (exit.onLine(point)) {
					exit.setHighlighted(true);
					setSelectedExit(exit);
					maybeShowPopup(me, getSelectedExit());
					break;
				}
			}
			mapper.repaintMainMap();

		}

		if (addNewRoom) {
			Point p = translatePointToMapCoordinates(me.getPoint(), 5);

			Room newRoom = new Room(p, getCurrentLevel(), "", "", getCurrentArea().getId(), ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			newRoom.setId(mapper.addRoom(newRoom));
			newRoom.setCurrentRoom(true);
			if (getCurrentRoom() == null) setCurrentRoom(newRoom);
			newRoom.setHighlighted(true);
			mapper.getToolPanel().resetButtons();
			mapper.updateMainMap();
			mapper.setStatus(""); //$NON-NLS-1$v
			resetClicking();
			if (!mapper.isCenterOnCurrentRoom()) moveViewTo(newRoom);
			// calculateScrollBars(getMainMap().getLocation());
			// calculateScrollBars();
			reCalculateScrollBars();
		} else if (addNewExit) {

			Object ooo = ((JComponent) me.getSource()).getComponentAt((int) me.getPoint().getX(), (int) me.getPoint().getY());

			if (ooo instanceof Room) {
				Room aRoom = (Room) ooo;
				if (firstExitAdded) {
					DirectionChooserDialog cdc = new DirectionChooserDialog(mapper);
					if (cdc.getResult()) {
						newExitDirection += cdc.getDirection();
						endRoom = aRoom;
						if (selectRoomForOldExit) {
							if (getSelectedExit().getTargetRoomId() == 0) {
								getSelectedExit().setTargetRoom(aRoom, cdc.getDirection());
							} else {
								getSelectedExit().setSourceRoom(aRoom, cdc.getDirection());
							}
							database.updateExit(null, getSelectedExit());
						} else {
							Exit cme = new Exit();
							cme.setSourceRoom(startRoom, newExitDirection / BASE);
							cme.setTargetRoom(endRoom, newExitDirection % BASE);
							database.addExit(cme);
							updateView();
						}
					}
					setSelectedExit(null);
					resetClicking();
					addNewExit = false;
					firstExitAdded = false;
					selectRoomForOldExit = false;
					mapper.getToolPanel().resetButtons();
					mapper.setStatus(""); //$NON-NLS-1$
				} else {
					DirectionChooserDialog cdc = new DirectionChooserDialog(mapper);
					if (cdc.getResult()) {
						firstExitAdded = true;
						newExitDirection = cdc.getDirection() * BASE;
						startRoom = aRoom;
					}
					mapper.setStatus("Press on Second room to wich you want to add an Exit."); //$NON-NLS-1$
				}
			}
		} else if (addNewArea) {
			Point daPoint = translatePointToMapCoordinates(me.getPoint(), 0);
			AreaDialog cmad = new AreaDialog(mapper, daPoint, getCurrentArea().getId(), getCurrentLevel());
			if (cmad.getResult()) {
				int newAreaId = database.addArea(cmad.getArea());
				mapper.getTreeViewer().addArea(database.getArea(newAreaId));
				// moveViewTo(database.getArea(newAreaId));
				setMapLocation(database.getArea(newAreaId).getPosition());
				mapper.updateMainMap();
			}
			mapper.getToolPanel().resetButtons();
			resetClicking();
			mapper.setStatus(""); //$NON-NLS-1$
			// calculateScrollBars(mainMap.getLocation());
		} else if (addNewObject) {
			Point daPoint = translatePointToMapCoordinates(me.getPoint(), 0);
			MapObjectDialog mod = new MapObjectDialog(mapper, daPoint, getCurrentArea().getId(), getCurrentLevel());
			if (mod.getResult() != null) {
				database.addObject(mod.getResult());
				setMapLocation(mod.getResult().getPosition());
			}
			resetClicking();
			mapper.updateMainMap();
			mapper.getToolPanel().resetButtons();
			// calculateScrollBars(getMainMap().getLocation());
		} else if (moveSelectedArea) {
			Point daPoint = translatePointToMapCoordinates(me.getPoint(), 0);
			mapper.getTreeViewer().deleteArea(currentSelectedArea);

			if (currentSelectedArea.getParent() == 0) {
				currentSelectedArea.setColour(Color.RED);
				currentSelectedArea.setSize(16, 16);
				currentSelectedArea.setType(Area.OVAL);
			}
			currentSelectedArea.setParent(currentArea.getId());
			currentSelectedArea.setPosition(daPoint);
			@SuppressWarnings("unused")
			AreaDialog ad = new AreaDialog(mapper, currentSelectedArea);
			database.updateArea(currentSelectedArea);
			mapper.getTreeViewer().addArea(currentSelectedArea);
			for (Area area : database.getAreasInArea(currentSelectedArea)) {
				mapper.getTreeViewer().addArea(area);
			}
			setCurrentSelectedArea(null);
			mapper.updateTree();
			mapper.updateMainMap();
			// calculateScrollBars(getMainMap().getLocation());
		}
		resetClicking();
	}

	private Point translatePointToMapCoordinates(Point oldPoint, int modulus) {
		Point toReturn = mainMap.getScaledPoint(oldPoint);
		if (modulus == 0) {
			toReturn.translate(getMapLocation().x, getMapLocation().y);
		} else {
			int tmpXPos = toReturn.x + getMainMap().getLocation().x;
			int tmpYPos = toReturn.y + getMainMap().getLocation().y;
			tmpXPos -= tmpXPos % modulus;
			tmpYPos -= tmpYPos % modulus;
			toReturn.setLocation(tmpXPos, tmpYPos);
		}
		return toReturn;

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		e.getPoint().translate(mapper.getTreeViewer().getWidth(), 0);

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		e.getPoint().translate(mapper.getTreeViewer().getWidth(), 0);

	}

	@Override
	public void mousePressed(MouseEvent me) {
		validate();
		Point thePointToUse = me.getPoint();
		mouseButton = me.getButton();
		if (mouseButton != MouseEvent.BUTTON2) {
			Component component = ((Container) me.getSource()).getComponentAt(thePointToUse);
			if (selectedExit != null) {
				maybeShowPopup(me, selectedExit);
			} else {
				Point p1 = mainMap.getScaledPoint(me.getPoint());
				p1.translate(mainMap.getLocation().x, mainMap.getLocation().y);
				// Check for exit on point... set selectedExit if there is...
				// kind
				for (Exit exit : getMainMap().getExitVector()) {
					exit.calculateLine();
					if (exit.onLine(p1)) {
						exit.setHighlighted(true);
						selectedExit = exit;
						maybeShowPopup(me, selectedExit);
						break;
					}
				}
				maybeShowPopup(me, selectedExit);
			}
			if (component instanceof AreaPanel) mousePressedOnAreaPanel(me);
			if (component instanceof MapObject) mousePressedOnMapObject(me);
			if (component instanceof AreaRepresentation) mousePressedOnAreaRepresentation(me);
			if (component instanceof Room) mousePressedOnRoom(me);
			if (component instanceof MapPanel) mousePressedOnMapPanel(me);
		}
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		mouseButton = me.getButton();

		if (draggingThread != null) {
			draggingThread.setPoint(null);
			draggingThread = null;
		}
		Point thePointToUse = me.getPoint();

		if (multiMarker) {
			getMainMap().setEndMarkerRectPoint(thePointToUse);
			if (!me.isShiftDown()) {
				for (Room room : highlightedRooms) {
					room.setHighlighted(false);
				}
				highlightedRooms.clear();
			}
			for (Exit exit : highlightedExits) {
				exit.setHighlighted(false);
			}
			getMainMap().replaceExits(highlightedExits);
			highlightedExits.clear();
			mapper.repaintMainMap();
			highlightedRooms.addAll(getMainMap().getSelectedRooms());
			for (Room room : highlightedRooms) {
				room.setHighlighted(true);
				// ArrayList<Exit> exits = database.getExitsForRoom(room);
				ArrayList<Exit> exits = getMainMap().getExitsForRoom(room);
				for (Exit exit : exits) {
					exit.setHighlighted(true);
				}
				highlightedExits.addAll(exits);
			}
			mainMap.replaceExits(highlightedExits);
			mapper.repaintMainMap();

			justMultiSelected = true;
		} else if (draggingArea || draggingObject || draggingRoom) {
			endDragPoint = me.getPoint();
			mainMap.setDragging(false);
			endDragPoint.translate(-startDragPoint.x, -startDragPoint.y);
			endDragPoint = mainMap.getScaledPoint(endDragPoint);
			if (draggingArea) {
				dropArea(endDragPoint);
			} else if (draggingObject) {
				dropObject(endDragPoint);
			} else if (draggingRoom) {
				dropRoom(endDragPoint.x, endDragPoint.y);
			}
			updateView();
			startDragPoint = null;
			endDragPoint = null;
			setDragXMod(0);
			setDragYmod(0);
		}
		draggingArea = false;
		draggingObject = false;
		draggingRoom = false;
		multiMarker = false;
		justMultiSelected = false;
		draggingView = false;
		/*
		 * PopupMenu Stuff.
		 */
		if (me.getSource() instanceof MapPanel) {
			Object o = ((Container) me.getSource()).getComponentAt(thePointToUse);
			if (o instanceof Room) {
				maybeShowPopup(me, o);
			} else if (o instanceof AreaPanel) {
				currentSelectedArea = ((AreaPanel) o).getArea();
				maybeShowPopup(me, o);
			} else if (o instanceof MapObject) {
				setCurrentMapObject((MapObject) o);
				maybeShowPopup(me, o);
			} else if (getSelectedExit() != null) {
				maybeShowPopup(me, getSelectedExit());
			}
		}
		/*
		 * End of PopupMenu stuff.
		 */
	}

	/**
	 * @return the currentRoom
	 */
	public Room getCurrentRoom() {
		return currentRoom;
	}

	/**
	 * @param currentRoom
	 *            the currentRoom to set
	 */
	public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
		this.currentArea = database.getArea(currentRoom.getAreaId());
	}

	/**
	 * @return the currentShadowRoom
	 */
	public Room getCurrentShadowRoom() {
		return currentShadowRoom;
	}

	/**
	 * @param currentShadowRoom
	 *            the currentShadowRoom to set
	 */
	public void setCurrentShadowRoom(Room currentShadowRoom) {
		this.currentShadowRoom = currentShadowRoom;
	}

	public void moveShadowRoomToCurrentRoom() {
		currentShadowRoom = currentRoom;
	}

	public void setCurrentRoomId(int currentRoomId) {
		this.currentRoomId = currentRoomId;
	}

	public int getCurrentRoomId() {
		return currentRoomId;
	}

	/**
	 * @return the currentArea
	 */
	public Area getCurrentArea() {
		return currentArea;
	}

	/**
	 * @param currentArea
	 *            the currentArea to set
	 */
	public void setCurrentArea(Area currentArea) {
		this.currentArea = currentArea;
		mainMap.setCurrentArea(currentArea);
	}

	/**
	 * @return the mainMap
	 */
	public MapPanel getMainMap() {
		return mainMap;
	}

	/**
	 * @param mainMap
	 *            the mainMap to set
	 */
	public void setMainMap(MapPanel mainMap) {
		this.mainMap = mainMap;
		getMainMap().addMouseWheelListener(this);
	}

	public void updateView() {
		mainMap.updateView();
		revalidate();
		repaint();
	}

	public Rectangle getRectToShow() {
		return mainMap.getRectToShow();
	}

	private void setMapLocation(Point point) {
		mainMap.setCenterLocation(point);
		calculateScrollBars();
		updateView();
	}

	public void setMapSize(Dimension preferredDimension) {
		mainMap.setSize(preferredDimension);
	}

	public void setPreferredMapSize(Dimension preferredDimension) {
		mainMap.setPreferredSize(preferredDimension);
	}

	@Override
	public boolean requestFocusInWindow() {
		return mainMap.requestFocusInWindow();
	}

	public int getCurrentLevel() {
		return mainMap.getCurrentLevel();
	}

	public Point getMapLocation() {
		return mainMap.getLocation();
	}

	public void calculateScrollBars() {// Point point) {
		autoUpdateOfPosition = true;
		oldBarX = horizontalScrollBar.getValue();
		oldBarY = verticalScrollBar.getValue();
		Rectangle areaDim = database.getAreaDimension(getCurrentArea());
		Rectangle mainMapDim = getRectToShow();
		int newYValue = (int) ((int) mainMapDim.getLocation().getY() + mainMapDim.getHeight() / 2);
		int newYExtent = (int) (areaDim.getHeight() / mainMapDim.getHeight());
		int newYMin = (int) areaDim.getLocation().getY();
		int newYMax = (int) areaDim.getHeight() + (int) areaDim.getLocation().getY();

		int newXValue = (int) ((int) mainMapDim.getLocation().getX() + mainMapDim.getWidth() / 2);
		int newXExtent = (int) (areaDim.getWidth() / mainMapDim.getWidth());
		int newXMin = (int) areaDim.getLocation().getX();
		int newXMax = (int) areaDim.getWidth() + (int) areaDim.getLocation().getX();

		verticalScrollBar.setValues(newYValue, newYExtent, newYMin, newYMax);
		horizontalScrollBar.setValues(newXValue, newXExtent, newXMin, newXMax);

		verticalScrollBar.setBlockIncrement(500);
		horizontalScrollBar.setBlockIncrement(500);

		verticalScrollBar.setUnitIncrement(100);
		horizontalScrollBar.setUnitIncrement(100);
		autoUpdateOfPosition = false;
	}

	public void calculateSizes() {
		Dimension preferredDimension = new Dimension(getWidth() - verticalScrollBar.getWidth(), mapper.getTreeViewer().getHeight()
																								- horizontalScrollBar.getHeight());
		setMapSize(preferredDimension);
		setPreferredMapSize(preferredDimension);
		Dimension d = new Dimension(verticalScrollBar.getWidth(), horizontalScrollBar.getHeight());
		cornerButton.setPreferredSize(d);
		updateView();
		calculateScrollBars();
	}

	public void reCalculateScrollBars() {
		Rectangle areaDim = database.getAreaDimension(getCurrentArea());
		Rectangle mainMapDim = getRectToShow();

		scrollY = verticalScrollBar.getValue();
		int newYExtent = (int) (areaDim.getHeight() / mainMapDim.getHeight());
		int newYMin = (int) areaDim.getLocation().getY();
		int newYMax = (int) (areaDim.getHeight() + areaDim.getLocation().getY());

		scrollX = horizontalScrollBar.getValue();
		int newXExtent = (int) (areaDim.getWidth() / mainMapDim.getWidth());
		int newXMin = (int) areaDim.getLocation().getX();
		int newXMax = (int) (areaDim.getWidth() + areaDim.getLocation().getX());

		verticalScrollBar.setValues(scrollY, newYExtent, newYMin, newYMax);
		horizontalScrollBar.setValues(scrollX, newXExtent, newXMin, newXMax);

		verticalScrollBar.setBlockIncrement(500);
		horizontalScrollBar.setBlockIncrement(500);

		verticalScrollBar.setUnitIncrement(100);
		horizontalScrollBar.setUnitIncrement(100);

		verticalScrollBar.validate();
		horizontalScrollBar.validate();
	}

	public void maybeMoveTheMainMap(Point point) {

		if (point.getX() < 10) {
			int val = horizontalScrollBar.getValue();
			val -= 10;
			dragXMod -= 10;
			horizontalScrollBar.setValue(val);
			horizontalScrollBar.validate();
		}
		if (point.getX() > (getMainMap().getWidth() - verticalScrollBar.getWidth() - 10)) {
			int val = horizontalScrollBar.getValue();
			val += 10;
			dragXMod += 10;
			horizontalScrollBar.setValue(val);
			horizontalScrollBar.validate();
		}
		if (point.getY() < 10) {
			int val = verticalScrollBar.getValue();
			val -= 10;
			dragYmod -= 10;
			verticalScrollBar.setValue(val);
			verticalScrollBar.validate();
		}
		if (point.getY() > (getMainMap().getHeight() - horizontalScrollBar.getHeight() - 10)) {
			int val = verticalScrollBar.getValue();
			val += 10;
			dragYmod += 10;
			verticalScrollBar.setValue(val);
			verticalScrollBar.validate();
		}
	}

	/**
	 * @return the dragYmod
	 */
	public int getDragYmod() {
		return dragYmod;
	}

	/**
	 * @param dragYmod
	 *            the dragYmod to set
	 */
	public void setDragYmod(int dragYmod) {
		this.dragYmod = dragYmod;
	}

	/**
	 * @return the dragXMod
	 */
	public int getDragXMod() {
		return dragXMod;
	}

	/**
	 * @param dragXMod
	 *            the dragXMod to set
	 */
	public void setDragXMod(int dragXMod) {
		this.dragXMod = dragXMod;
	}

	/**
	 * @return the verticalScrollBar
	 */
	public JScrollBar getVerticalScrollBar() {
		return verticalScrollBar;
	}

	/**
	 * @param verticalScrollBar
	 *            the verticalScrollBar to set
	 */
	public void setVerticalScrollBar(JScrollBar verticalScrollBar) {
		this.verticalScrollBar = verticalScrollBar;
	}

	/**
	 * @return the horizontalScrollBar
	 */
	public JScrollBar getHorizontalScrollBar() {
		return horizontalScrollBar;
	}

	/**
	 * @param horizontalScrollBar
	 *            the horizontalScrollBar to set
	 */
	public void setHorizontalScrollBar(JScrollBar horizontalScrollBar) {
		this.horizontalScrollBar = horizontalScrollBar;
	}

	/**
	 * For the ScrollBars
	 */
	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		if (autoUpdateOfPosition) return;
		int xDiff = 0, yDiff = 0;
		if (e.getSource() instanceof JScrollBar) {
			JScrollBar bar = (JScrollBar) e.getSource();
			if (bar.getOrientation() == JScrollBar.VERTICAL) {
				yDiff = bar.getValue() - oldBarY;
				yDiff = (yDiff == -oldBarY ? 0 : yDiff);
				oldBarY = bar.getValue();
			} else {
				xDiff = bar.getValue() - oldBarX;
				xDiff = (xDiff == -oldBarX ? 0 : xDiff);
				oldBarX = bar.getValue();
			}
			moveMainMap(xDiff, yDiff);
			updateView();
		}
	}

	/**
	 * @return the selectedExit
	 */
	public Exit getSelectedExit() {
		return selectedExit;
	}

	/**
	 * @param selectedExit
	 *            the selectedExit to set
	 */
	public void setSelectedExit(Exit selectedExit) {
		this.selectedExit = selectedExit;
	}

	private void resetClicking() {
		addNewRoom = false;
		addNewArea = false;
		addNewObject = false;
		moveSelectedArea = false;
	}

	/**
	 * @return the addNewRoom
	 */
	public boolean isAddNewRoom() {
		return addNewRoom;
	}

	/**
	 * @param addNewRoom
	 *            the addNewRoom to set
	 */
	public void setAddNewRoom(boolean addNewRoom) {
		this.addNewRoom = addNewRoom;
	}

	/**
	 * @return the addNewExit
	 */
	public boolean isAddNewExit() {
		return addNewExit;
	}

	/**
	 * @param addNewExit
	 *            the addNewExit to set
	 */
	public void setAddNewExit(boolean addNewExit) {
		this.addNewExit = addNewExit;
	}

	/**
	 * @return the addNewArea
	 */
	public boolean isAddNewArea() {
		return addNewArea;
	}

	/**
	 * @param addNewArea
	 *            the addNewArea to set
	 */
	public void setAddNewArea(boolean addNewArea) {
		this.addNewArea = addNewArea;
	}

	/**
	 * @return the addNewObject
	 */
	public boolean isAddNewObject() {
		return addNewObject;
	}

	/**
	 * @param addNewObject
	 *            the addNewObject to set
	 */
	public void setAddNewObject(boolean addNewObject) {
		this.addNewObject = addNewObject;
	}

	private void dropArea(Point mod) {
		System.out.println("Dropped Area..."); //$NON-NLS-1$
		Point p = dragArea.getArea().getPosition();
		p.translate(mod.x, mod.y);
		p.translate(getDragXMod(), getDragYmod());
		dragArea.getArea().setPosition(p);
		database.updateArea(dragArea.getArea());
		// moveViewTo(p);
		dragArea = null;
		startDragPoint = null;
		endDragPoint = null;
	}

	private void dropObject(Point mod) {
		if (!dragObject.isLocked()) {
			System.out.println("Dropped Object..."); //$NON-NLS-1$
			Point p = dragObject.getPosition();
			p.translate(mod.x, mod.y);
			p.translate(getDragXMod(), getDragYmod());
			dragObject.setPosition(p);
			dragObject.setLevel(getCurrentLevel());
			database.updateObject(dragObject);
		} else {
			System.out.println("Object Locked, Unlock and try again."); //$NON-NLS-1$
		}
		startDragPoint = null;
		endDragPoint = null;
	}

	private void dropRoom(int x, int y) {
		if (Math.abs(x) < 5 && Math.abs(y) < 5) return;
		x = x - x % 5;
		y = y - y % 5;
		System.out.println("Dropped Room..."); //$NON-NLS-1$
		if (highlightedRooms == null) highlightedRooms = new UniqueArrayList<Room>();
		for (Room room : highlightedRooms) {
			moveRoom(room, x, y);
		}
		if (highlightedRooms != null) if (highlightedRooms.get(0) != null && !highlightedRooms.isEmpty()) moveViewTo(highlightedRooms.get(0));

		if (getCurrentRoom() != null && mapper.isCenterOnCurrentRoom()) {
			setMapLocation(new Point(getCurrentRoom().getPosition()));
		} else if (dragRoom != null) {
			setMapLocation(new Point(dragRoom.getPosition()));
		}
		highlightedExits.clear();
		highlightedRooms.clear();
		startDragPoint = null;
		endDragPoint = null;
		mainMap.updateZones();
		updateView();
	}

	private void moveRoom(Room room, int x, int y) {
		if (room == null) return;
		room.getLocation().translate(x, y);
		room.getPosition().translate(x + getDragXMod(), y + getDragYmod());
		if (getCurrentRoom() != null) {
			if (room.getId() == getCurrentRoom().getId()) {
				setCurrentRoom(room);
			}
		}

		ArrayList<Exit> exits = database.getExitsForRoom(room);
		for (Exit exit : exits) {
			if (exit.getSourceRoomId() == room.getId()) {
				exit.setStartX(exit.getStartX() + x + getDragXMod());
				exit.setStartY(exit.getStartY() + y + getDragYmod());
			} else if (exit.getTargetRoomId() == room.getId()) {
				exit.setEndX(exit.getEndX() + x + getDragXMod());
				exit.setEndY(exit.getEndY() + y + getDragYmod());
			}
			database.updateExit(null, exit);
		}
		database.updateRoom(getCurrentArea().getId(), room);
	}

	public void setDragArea(AreaPanel dragArea) {
		this.dragArea = dragArea;
	}

	public AreaPanel getDragArea() {
		return dragArea;
	}

	public void setDraggingArea(boolean draggingArea) {
		this.draggingArea = draggingArea;
	}

	public void setDraggingObject(boolean draggingObject) {
		this.draggingObject = draggingObject;
	}

	public void setDraggingRoom(boolean draggingRoom) {
		this.draggingRoom = draggingRoom;
	}

	public void setEndCoordinates(Point endCoordinates) {
		this.endCoordinates = endCoordinates;
	}

	public void setEndRoom(Room endRoom) {
		this.endRoom = endRoom;
	}

	public void setFirstExitAdded(boolean firstExitAdded) {
		this.firstExitAdded = firstExitAdded;
	}

	/**
	 * @return the highlightedExits
	 */
	public ArrayList<Exit> getHighlightedExits() {
		return highlightedExits;
	}

	/**
	 * @param highlightedExits
	 *            the highlightedExits to set
	 */
	public void setHighlightedExits(ArrayList<Exit> highlightedExits) {
		this.highlightedExits = highlightedExits;
	}

	/**
	 * @return the highlightedRooms
	 */
	public List<Room> getHighlightedRooms() {
		return highlightedRooms;
	}

	/**
	 * @param highlightedRooms
	 *            the highlightedRooms to set
	 */
	public void setHighlightedRooms(List<Room> highlightedRooms) {
		this.highlightedRooms = highlightedRooms;
	}

	public boolean isDraggingArea() {
		return draggingArea;
	}

	public boolean isDraggingObject() {
		return draggingObject;
	}

	public boolean isDraggingRoom() {
		return draggingRoom;
	}

	public boolean isFirstExitAdded() {
		return firstExitAdded;
	}

	public void mousePressedOnRoom(MouseEvent me) {
		Point theP = me.getPoint();

		Room roomToModify = (Room) ((Container) me.getSource()).getComponentAt((int) theP.getX(), (int) theP.getY());
		mergeVector = database.getRoomsForMerge(roomToModify);

		popupForMapRoom.setMergeRoomsPopupMenuItemEnabled(mergeVector.size() > 1);

		maybeShowPopup(me, me.getSource());
		if (justMultiSelected) {
			justMultiSelected = false;
			return;
		}

		mapper.setStatus(roomToModify.getName() + " - " + roomToModify.getDescription()); //$NON-NLS-1$

		for (Room room : highlightedRooms) {
			room.setHighlighted(false);
		}
		for (Exit exit : highlightedExits) {
			exit.setHighlighted(false);
		}
		getMainMap().replaceExits(highlightedExits);
		mapper.repaintMainMap();

		if (!me.isShiftDown()) {
			highlightedRooms.clear();
			highlightedExits.clear();
		} else {
			justMultiSelected = true;
		}

		if (!roomToModify.isHighLighted()) {

			if (!highlightedRooms.contains(roomToModify)) highlightedRooms.add(roomToModify);
			ArrayList<Exit> exits = database.getExitsForRoom(roomToModify);
			highlightedExits.addAll(exits);

			mapper.repaintMainMap();
			for (Room room : highlightedRooms) {
				room.setHighlighted(true);
			}
			for (Exit exit : highlightedExits) {
				exit.setHighlighted(true);
			}
			getMainMap().replaceExits(highlightedExits);

		} else {
			roomToModify.setHighlighted(false);
			if (highlightedRooms.contains(roomToModify)) highlightedRooms.remove(roomToModify);
			ArrayList<Exit> exits = database.getExitsForRoom(roomToModify);
			for (Exit exit : exits) {
				exit.setHighlighted(false);
			}
			highlightedExits.removeAll(exits);
			getMainMap().replaceExits(highlightedExits);
			mapper.repaintMainMap();
		}
		justMultiSelected = false;
		// mainMap.requestFocusInWindow();
		mapper.repaintMainMap();

	}

	/**
	 * @return the currentMapObject
	 */
	public MapObject getCurrentMapObject() {
		return currentMapObject;
	}

	/**
	 * @param currentMapObject
	 *            the currentMapObject to set
	 */
	public void setCurrentMapObject(MapObject currentMapObject) {
		this.currentMapObject = currentMapObject;
	}

	public void mousePressedOnMapPanel(MouseEvent me) {
		multiMarker = true;
		Point theP = me.getPoint();

		getMainMap().setStartMarkerRectPoint(theP);
	}

	public void mousePressedOnMapObject(MouseEvent me) {
		Point theP = me.getPoint();

		setCurrentMapObject(((MapObject) ((Container) me.getSource()).getComponentAt((int) theP.getX(), (int) theP.getY())));
		maybeShowPopup(me, getCurrentMapObject());
	}

	public Area getCurrentSelectedArea() {
		return currentSelectedArea;
	}

	public void setCurrentSelectedArea(Area currentSelectedArea) {
		this.currentSelectedArea = currentSelectedArea;
	}

	public void mousePressedOnAreaPanel(MouseEvent me) {
		Point theP = me.getPoint();

		setCurrentSelectedArea(((AreaPanel) ((Container) me.getSource()).getComponentAt((int) theP.getX(), (int) theP.getY())).getArea());
		maybeShowPopup(me, getCurrentSelectedArea());
		AreaPanel a = (AreaPanel) ((Container) me.getSource()).getComponentAt((int) theP.getX(), (int) theP.getY());
		if (me.getClickCount() == 1) {}
		if (me.getClickCount() == 2) {
			moveViewTo(a.getArea());
		}
	}

	public void setMoveSelectedArea(boolean moveSelectedArea) {
		this.moveSelectedArea = moveSelectedArea;
	}

	public Point getEndCoordinates() {
		return endCoordinates;
	}

	public Room getEndRoom() {
		return endRoom;
	}

	public void setSelectRoomForOldExit(boolean selectRoomForOldExit) {
		this.selectRoomForOldExit = selectRoomForOldExit;
	}

	public void setStartRoom(Room startRoom) {
		this.startRoom = startRoom;

	}

	public ArrayList<Room> getMergeVector() {
		return mergeVector;
	}

	public void setStartCoordinates(Point startCoordinates) {
		this.startCoordinates = startCoordinates;
	}

	public Point getStartCoordinates() {
		return startCoordinates;
	}

	public Room getStartRoom() {
		return startRoom;
	}

	public void mousePressedOnAreaRepresentation(MouseEvent me) {
		Point theP = me.getPoint();

		Component component = ((Container) me.getSource()).getComponentAt(theP);
		if (me.getClickCount() == 1) {} else if (me.getClickCount() == 2) {
			setCurrentArea(database.getArea(((AreaRepresentation) component).getAreaId()));
			mapper.updateMainMap();
			Room tmpRoom = database.getRoom(getCurrentArea().getId(), ((AreaRepresentation) component).getRoomId());
			setaRoomWithAView(tmpRoom);
			moveViewTo(tmpRoom);

			// mainMap.requestFocusInWindow();
		}

	}

	/**
	 * @return the aRoomWithAView
	 */
	public Room getaRoomWithAView() {
		return aRoomWithAView;
	}

	/**
	 * @param aRoomWithAView
	 *            the aRoomWithAView to set
	 */
	public void setaRoomWithAView(Room aRoomWithAView) {
		this.aRoomWithAView = aRoomWithAView;
	}

	public void maybeShowPopup(MouseEvent me, Object o) {

		if (me.isPopupTrigger()) {
			if (o instanceof Room) {
				Room room = (Room) o;
				popupForMapRoom.setReadNotificationEnabled(!room.getNotification().trim().isEmpty());
				popupForMapRoom.show(me.getComponent(), me.getX(), me.getY());
				popupForMapRoom.setaddRemoveToZoneMenuEnabled(false);
				if (mapper.currentAreaHasZones()) {
					popupForMapRoom.setaddRemoveToZoneMenuEnabled(true);
					popupForMapRoom.setAddToZoneMenuItemEnabled(true);
					popupForMapRoom.setRemoveFromZoneMenuItem(mapper.currentRoomIsConnectedToZone());
				}

			} else if (o instanceof Exit) {
				Exit exit = (Exit) o;
				popupForExit.setSetTargetMenuItemEnabled((exit.getTargetRoomId() == 0 || exit.getSourceRoomId() == 0));
				popupForExit.show(me.getComponent(), me.getX(), me.getY());
			} else if (o instanceof AreaPanel) {
				popupForArea.show(me.getComponent(), me.getX(), me.getY());
			} else if (o instanceof MapObject) {
				popupForMapObject.show(me.getComponent(), me.getX(), me.getY());
			}
		}
	}

	public void moveViewTo(Room room) {
		if (room == null) return;
		room = database.getRoom(room.getAreaId(), room.getId());
		setaRoomWithAView(room);
		mapper.setCurrentLevel(room.getLevel());
		mapper.getToolPanel().setLevel(getCurrentLevel());
		Area tmpArea = database.getArea(room.getAreaId());
		setCurrentArea(tmpArea);
		getMainMap().setCurrentArea(tmpArea);
		moveViewTo(room.getPosition());
		calculateScrollBars();
	}

	public void centerOnCurrentRoom() {
		moveViewTo(getCurrentRoom());
	}

	public void moveViewTo(Point position, int areaId, int level) {
		mapper.setCurrentLevel(level);
		mapper.getToolPanel().setLevel(level);
		Area area = database.getArea(areaId);
		setCurrentArea(area);
		setMapLocation(position);
		mainMap.setCurrentArea(area);
		mainMap.setCurrentLevel(level);
		calculateScrollBars();
		updateView();
	}

	public void moveViewTo(Area area) {
		if (area == null) return;
		setCurrentArea(area);
		ZonesWindow.getInstance(mapper, currentArea.getId()).setArea(currentArea.getId());
		Room tmpRoom = database.getOldestRoomInArea(area);
		if (getCurrentRoom() != null) {
			if ((area.getId() == currentRoom.getAreaId()) && mapper.isCenterOnCurrentRoom()) {
				moveViewTo(currentRoom);
			} else {
				if (tmpRoom != null) {
					if (debug) System.out.println("Oldest Room in Area: " + tmpRoom); //$NON-NLS-1$
					moveViewTo(tmpRoom);
				} else {
					if (debug) System.out.println("No room found..."); //$NON-NLS-1$
					moveViewTo(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2);
				}
			}
		} else {
			if (tmpRoom != null) {
				if (debug) System.out.println("Oldest Room in Area: " + tmpRoom); //$NON-NLS-1$
				moveViewTo(tmpRoom);
			} else {
				if (debug) System.out.println("No room found..."); //$NON-NLS-1$
				moveViewTo(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2);
			}
		}
	}

	public void moveViewTo(int x, int y) {
		moveViewTo(new Point(x, y));
	}

	public void moveViewTo(Point position) {
		setMapLocation(position);
	}

	/**
	 * @return the currentShadowId
	 */
	public int getCurrentShadowId() {
		return currentShadowId;
	}

	/**
	 * @param currentShadowId
	 *            the currentShadowId to set
	 */
	public void setCurrentShadowId(int currentShadowId) {
		this.currentShadowId = currentShadowId;
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setCurrentRoomFor(User user) {
		if (user == null) return;
		Room room = database.getRoom(user.getCurrentAreaId(), user.getCurrentRoomId());
		mapper.gotoRoom(room);
	}

	public void changeAllThisSideExitsTo(int i) {
		for (Room room : getHighlightedRooms()) {
			ArrayList<Exit> exitsForRoom = database.getExitsForRoom(room);
			for (Exit exit : exitsForRoom) {
				if (exit.getSourceRoomId() == room.getId()) {
					exit.setSourceType(i);
				} else {
					exit.setTargetType(i);
				}
				database.updateExit(null, exit);
			}
		}
		getHighlightedExits().clear();
		getHighlightedRooms().clear();
		updateView();
	}

	public void changeAllOtherSideExitsTo(int i) {
		for (Room room : getHighlightedRooms()) {
			ArrayList<Exit> exitsForRoom = database.getExitsForRoom(room);
			for (Exit exit : exitsForRoom) {
				if (exit.getSourceRoomId() == room.getId()) {
					exit.setTargetType(i);
				} else {
					exit.setSourceType(i);
				}
				database.updateExit(null, exit);
			}
		}
		getHighlightedExits().clear();
		getHighlightedRooms().clear();
		updateView();
	}

	public void changeAllExitsInRoomTo(int i) {
		for (Room room : getHighlightedRooms()) {
			ArrayList<Exit> exitsForRoom = database.getExitsForRoom(room);
			for (Exit exit : exitsForRoom) {
				exit.setTargetType(i);
				exit.setSourceType(i);
				database.updateExit(null, exit);
			}
		}
		getHighlightedExits().clear();
		getHighlightedRooms().clear();
		updateView();
	}
}
