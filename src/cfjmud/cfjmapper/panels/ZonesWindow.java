/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Zone;

/**
 * @author Ecthelion
 * 
 */
public class ZonesWindow extends JDialog implements ListSelectionListener, MouseListener {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2745932448492700072L;
	private final JList<Zone>	zones;
	private int					a_id;
	private final Mapper		mapper;
	private static ZonesWindow	instance			= null;

	/**
	 * 
	 */
	private ZonesWindow(Mapper mapper, int a_id) {
		super(mapper, true);
		this.mapper = mapper;
		setModalityType(ModalityType.MODELESS);
		setTitle("Zones"); //$NON-NLS-1$
		this.a_id = a_id;
		zones = new JList<Zone>(Database.getInstance().getZonesForAreaForJList(a_id));
		zones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		zones.addListSelectionListener(this);
		zones.addMouseListener(this);
		zones.setAutoscrolls(true);
		JScrollPane sp = new JScrollPane(zones);
		Dimension d = new Dimension(150, 300);
		sp.setSize(d);
		sp.setPreferredSize(d);
		setLocation((int) (mapper.getLocation().x + mapper.getSize().getWidth() - 200), mapper.getLocation().y + 50);
		add(sp, BorderLayout.CENTER);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		pack();
	}

	public void setArea(int a_id) {
		zones.setListData(Database.getInstance().getZonesForAreaForJList(a_id));
		this.a_id = a_id;
		validate();
		repaint();
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		Zone selectedZone = zones.getSelectedValue();
		if (selectedZone == null) return;

		for (Zone zone : Database.getInstance().getZonesForAreaForJList(a_id)) {
			zone.setHighlighted(false);
		}
		selectedZone.setHighlighted(true);
		Point p = selectedZone.getPosition();
		if (p == null) return;
		mapper.getMapView().moveViewTo(p);
		mapper.repaint();
	}

	/**
	 * @return the a_id
	 */
	public int getA_id() {
		return a_id;
	}

	/**
	 * @return the instance
	 */
	public static ZonesWindow getInstance(Mapper mapper, int a_id) {
		if (instance == null) instance = new ZonesWindow(mapper, a_id);
		return instance;
	}

	public static void toggleVisible(Mapper mapper, int a_id) {
		if (instance == null) instance = new ZonesWindow(mapper, a_id);
		instance.setVisible(!instance.isVisible());
		instance.setArea(a_id);
		instance.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() instanceof JList<?>) {
			if (zones.getSelectedValue() == null) return;
			if (zones.getCellBounds(zones.getSelectedIndex(), zones.getSelectedIndex()).contains(e.getPoint()) && e.isShiftDown()) {
				zones.clearSelection();
				for (Zone zone : Database.getInstance().getZonesForAreaForJList(a_id)) {
					zone.setHighlighted(false);
				}
				mapper.repaint();
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
