/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.interfaces.MapperConstants;

/**
 * @author Ecthelion
 * 
 */
public class Compass extends JComponent implements MapperConstants {

	/**
     * 
     */
	private static final long	serialVersionUID	= -3040666799470567900L;
	int							facing				= NORTH;
	String						fileToUse			= "icons/Arrow.png";		//$NON-NLS-1$
	ImageIcon					icon				= null;
	private final Mapper		mapper;

	public Compass(Mapper mapper) {
		this.mapper = mapper;
		Dimension size = new Dimension(30, 30);
		URL url = this.getClass().getResource("/" + "Arrow.png"); //$NON-NLS-1$ //$NON-NLS-2$
		if (url != null) {
			icon = new ImageIcon(url);
		} else {
			icon = new ImageIcon("./icons/" + "Arrow.png", "Arrow.png"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		setSize(size);
		setPreferredSize(size);
		setBackground(this.mapper.getBackground());
	}

	public int getFacting() {
		return facing;
	}

	private double getRotationForDir(int direction) {
		double theta = 0;
		switch (direction) {
			case NORTH:
				theta = 270 * java.lang.Math.PI / 180;
				break;
			case NORTHEAST:
				theta = 315 * java.lang.Math.PI / 180;
				break;
			case EAST:
				break;
			case SOUTHEAST:
				theta = 45 * java.lang.Math.PI / 180;
				break;
			case SOUTH:
				theta = 90 * java.lang.Math.PI / 180;
				break;
			case SOUTHWEST:
				theta = 135 * java.lang.Math.PI / 180;
				break;
			case WEST:
				theta = 180 * java.lang.Math.PI / 180;
				break;
			case NORTHWEST:
				theta = 225 * java.lang.Math.PI / 180;
				break;
		}
		return theta;
	}

	@Override
	public void paint(Graphics graphics) {
		Graphics2D g = (Graphics2D) graphics;
		AffineTransform orig = g.getTransform();
		AffineTransform at = new AffineTransform(orig);
		int tmpFace = (facing + 1) % 8 + 1;
		at.rotate(getRotationForDir(tmpFace), getWidth() / 2, getHeight() / 2);
		g.setColor(Color.green);
		g.setTransform(at);
		g.drawImage(icon.getImage(), 0, 0, getWidth(), getHeight(), icon.getImageObserver());
		g.setTransform(orig);
	}

	public void setFacing(int facing) {
		this.facing = facing;
		repaint();
	}
}
