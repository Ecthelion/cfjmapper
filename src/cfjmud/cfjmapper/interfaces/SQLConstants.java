/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.interfaces;

/**
 * @author Ecthelion
 * 
 */
public interface SQLConstants extends public_messages, updateSQL, fetchingSQL, insertSQL, createIndexSQL, deleteSQL, createTablesSQL, databaseCreation,
								tableModifications, restoreSQL {}

interface databaseCreation {
	public static final String	mySQLDatabaseCreation		= "CREATE DATABASE IF NOT EXISTS "; //$NON-NLS-1$
	public static final String	postgreSQLDatabaseCreation	= "CREATE DATABASE ";				//$NON-NLS-1$

}

interface restoreSQL {
	public static final String		postgreSQLtruncateTables	= "TRUNCATE TABLE areas,  Exits, icons, objects, roomcolours, terraintypes RESTART IDENTITY";											//$NON-NLS-1$
	public static final String[]	javaDBtruncateTables		= {
			"TRUNCATE TABLE areas", "TRUNCATE TABLE Rooms", "TRUNCATE TABLE Exits", "TRUNCATE TABLE icons", "TRUNCATE TABLE objects", "TRUNCATE TABLE roomcolours", "TRUNCATE TABLE terraintypes" };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
}

interface tableModifications {
	public static final String	modifyAreasTable				= "ALTER TABLE Areas ADD preferredRoomDistance int DEFAULT 35";		//$NON-NLS-1$
	public static final String	modifyExitsTableForAreaReps1	= "ALTER TABLE Exits ADD sourceAreaRep int DEFAULT 0";					//$NON-NLS-1$
	public static final String	modifyExitsTableForAreaReps2	= "ALTER TABLE Exits ADD targetAreaRep int DEFAULT 0";					//$NON-NLS-1$

	public static final String	modifyRoomsAddNotes				= "ALTER TABLE Rooms ADD notes long varchar";							//$NON-NLS-1$

	public static final String	modifyRoomsJavaDBDesc			= "ALTER TABLE Rooms ALTER Description SET DATA TYPE VARCHAR(10000)";	//$NON-NLS-1$
	public static final String	modifyRoomsPostgreSQLDesc		= "ALTER TABLE Rooms MODIFY Description TEXT";							//$NON-NLS-1$
}

interface createTablesSQL {
	public static final String	javaDBcreateObjects				= "CREATE TABLE Objects(o_id int NOT NULL GENERATED ALWAYS AS IDENTITY, icon int, Description varchar(200), xPos int, yPos int, width int, height int, labelposition int, background int, area int, level int, textsize float, font varchar(200), textstyle int, textangle int, textcolour int, locked int)";																																																																		//$NON-NLS-1$
	public static final String	javaDBcreateAreas				= "CREATE TABLE Areas(a_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Name varchar(200), Level int DEFAULT 0,Parent int, xPos int, yPos int, Type int DEFAULT 0, Icon int DEFAULT 0, Width int, Height int, Colour int, BorderType int, BorderColour int, BorderThickness int, BorderShadow int, Raised int, BorderTitle int, LabelText varchar(30), LabelPosition int DEFAULT 0, LabelFontSize float DEFAULT 10, LabelColour int, Font varchar(200), LabelFontStyle int, preferredRoomDistance int DEFAULT 35, RoomRep int)";														//$NON-NLS-1$
	public static final String	javaDBcreateExits				= "CREATE TABLE Exits(e_id int NOT NULL GENERATED ALWAYS AS IDENTITY, startX int, startY int, endX int, endY int, oneWay int, Level int, Type int, sourceRoom int, targetRoom int, sourceLevel int, targetLevel int, sourceArea int, targetArea int, sourceType int, targetType int, ShowStub int, sourceLock int, targetLock int, sourceWalkCommand varchar(50), targetWalkCommand varchar(50), sourceAreaRep int DEFAULT 0, targetAreaRep int DEFAULT 0)";																														//$NON-NLS-1$
	public static final String	javaDBcreateIcons				= "CREATE TABLE Icons(ti_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Icon blob(1M), Name varchar(200) )";																																																																																																																		//$NON-NLS-1$
	public static final String	javaDBcreateRoomColours			= "CREATE TABLE RoomColours(rc_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Name varchar(200), Colour int, Type int DEFAULT 0)";																																																																																																													//$NON-NLS-1$
	public static final String	javaDBcreateRooms				= "CREATE TABLE Rooms(r_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Name varchar(200), Description LONG VARCHAR, Colour int, Level int, xPos int, yPos int, Area int, Type int DEFAULT 0, Alias varchar(30), Notes long varchar)";																																																																																				//$NON-NLS-1$
	public static final String	javaDBcreateTerrainTypes		= "CREATE TABLE TerrainTypes(tp_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Name varchar(200), Indoors int DEFAULT 0, Icon int DEFAULT 0)";																																																																																																										//$NON-NLS-1$
	public static final String	javaDBcreateZones				= "CREATE TABLE zones (z_id int NOT NULL GENERATED ALWAYS AS IDENTITY, a_id int DEFAULT 0, bgcolour int DEFAULT 0, bgstyle int DEFAULT 0, bgref int DEFAULT 0, border int, name varchar(200) NOT NULL, type int DEFAULT 0, paintstyle int DEFAULT 0, description long varchar, labelalignment int DEFAULT 0, labeltext varchar(100), labelcolour int DEFAULT 0, labelfont varchar(100), borderthickness int DEFAULT 0, bordercolour int DEFAULT 0, bordertype int DEFAULT 0, labelfontsize float DEFAULT 10, labelfontstyle int DEFAULT 0)";										//$NON-NLS-1$
	public static final String	javaDBcreateZoneRooms			= "CREATE TABLE zoneRooms (zr_id int NOT NULL GENERATED ALWAYS AS IDENTITY, r_id int, z_id int, index int)";																																																																																																																		//$NON-NLS-1$

	public static final String	postgreSQLcreateAreas			= "CREATE TABLE Areas (a_id SERIAL PRIMARY KEY , Name varchar(200), Level int DEFAULT 0,Parent int, xPos int, yPos int, Type int DEFAULT 0, Icon int DEFAULT 0, Width int, Height int, Colour int, BorderType int, BorderColour int, BorderThickness int, BorderShadow int, Raised int, BorderTitle int, LabelText varchar(30), LabelPosition int DEFAULT 0, LabelFontSize float DEFAULT 10, LabelColour int, Font varchar(200), LabelFontStyle int, preferredRoomDistance int DEFAULT 35, RoomRep int);";																			//$NON-NLS-1$
	public static final String	postgreSQLcreateExits			= "CREATE TABLE Exits (e_id SERIAL PRIMARY KEY , startX int, startY int, endX int, endY int, oneWay int, Level int, Type int, sourceRoom int, targetRoom int, sourceLevel int, targetLevel int, sourceArea int, targetArea int, sourceType int, targetType int, ShowStub int, sourceLock int, targetLock int, sourceWalkCommand varchar(50), targetWalkCommand varchar(50), sourceAreaRep int DEFAULT 0, targetAreaRep int DEFAULT 0);";																																			//$NON-NLS-1$
	public static final String	postgreSQLcreateIcons			= "CREATE TABLE Icons (ti_id SERIAL PRIMARY KEY , Icon bytea , Name varchar(200) );";																																																																																																																								//$NON-NLS-1$
	public static final String	postgreSQLcreateRoomColours		= "CREATE TABLE RoomColours (rc_id SERIAL PRIMARY KEY , Name varchar(200), Colour int, Type int DEFAULT 0);";																																																																																																																		//$NON-NLS-1$
	public static final String	postgreSQLcreateRooms			= "CREATE TABLE Rooms (r_id SERIAL PRIMARY KEY , Name varchar(200), Description TEXT, Colour int, Level int, xPos int, yPos int, Area int, Type int DEFAULT 0, Alias varchar(30), Notes text);";																																																																																													//$NON-NLS-1$
	public static final String	postgreSQLcreateTerrainTypes	= "CREATE TABLE TerrainTypes (tp_id SERIAL PRIMARY KEY , Name varchar(200), Indoors int DEFAULT 0, Icon int DEFAULT 0);";																																																																																																															//$NON-NLS-1$
	public static final String	postgreSQLcreateObjects			= "CREATE TABLE Objects (o_id SERIAL PRIMARY KEY , icon int, Description varchar(200), xPos int, yPos int, width int, height int, labelposition int, background int, area int, level int, textsize float, font varchar(200), textstyle int, textangle int, textcolour int, locked int)";																																																																							//$NON-NLS-1$
	public static final String	postgreSQLcreateZones			= "CREATE TABLE zones (z_id SERIAL PRIMARY KEY, a_id integer DEFAULT 0, bgcolour integer DEFAULT 0, bgstyle integer DEFAULT 0, bgref integer DEFAULT 0, border integer, name character varying(200) NOT NULL, type integer DEFAULT 0, paintstyle integer DEFAULT 0, description text, labelalignment int DEFAULT 0, labeltext character varying(100), labelcolour int DEFAULT 0, labelfont character varying(100), borderthickness int DEFAULT 0, bordercolour int DEFAULT 0, bordertype int DEFAULT 0, labelfontsize double precision DEFAULT 10, labelfontstyle int DEFAULT 0)";	//$NON-NLS-1$
	public static final String	postgreSQLcreateZoneRooms		= "CREATE TABLE zoneRooms ( zr_id SERIAL PRIMARY KEY, r_id integer, z_id integer, index integer)";																																																																																																																					//$NON-NLS-1$

	public static final String	mySQLcreateObjects				= "CREATE TABLE IF NOT EXISTS Objects(o_id INT PRIMARY KEY AUTO_INCREMENT, icon INT, Description TEXT, xPos INT, yPos INT, width INT, height INT, labelposition INT, background INT, area INT, level INT, textsize float, font TEXT, textstyle int, textangle int, textcolour int, locked int);";																																																																					//$NON-NLS-1$
	public static final String	mySQLcreateAreas				= "CREATE TABLE IF NOT EXISTS Areas(a_id INT PRIMARY KEY AUTO_INCREMENT, Name TEXT, Level INT DEFAULT 0,Parent INT, xPos INT, yPos INT, Type INT DEFAULT 0, Icon INT DEFAULT 0, Width INT, Height INT, Colour INT, BorderType INT, BorderColour INT, BorderThickness INT, BorderShadow INT, Raised INT, BorderTitle INT, LabelText TEXT, LabelPosition INT DEFAULT 0, LabelFontSize float DEFAULT 10, LabelColour INT, Font TEXT, LabelFontStyle INT, preferredRoomDistance INT DEFAULT 35, RoomRep int);";																		//$NON-NLS-1$
	public static final String	mySQLcreateExits				= "CREATE TABLE IF NOT EXISTS Exits(e_id INT PRIMARY KEY AUTO_INCREMENT, startX INT, startY INT, endX INT, endY INT, oneWay INT, Level INT, Type INT, sourceRoom INT, targetRoom INT, sourceLevel INT, targetLevel INT, sourceArea INT, targetArea INT, sourceType INT, targetType INT, ShowStub INT, sourceLock INT, targetLock INT, sourceWalkCommand TEXT, targetWalkCommand TEXT, sourceAreaRep INT DEFAULT 0, targetAreaRep INT DEFAULT 0);";																																	//$NON-NLS-1$
	public static final String	mySQLcreateIcons				= "CREATE TABLE IF NOT EXISTS Icons(ti_id INT PRIMARY KEY AUTO_INCREMENT, Icon BLOB, Name TEXT );";																																																																																																																				//$NON-NLS-1$
	public static final String	mySQLcreateRoomColours			= "CREATE TABLE IF NOT EXISTS  RoomColours(rc_id INT PRIMARY KEY AUTO_INCREMENT, Name TEXT, Colour INT, Type INT DEFAULT 0);";																																																																																																														//$NON-NLS-1$
	public static final String	mySQLcreateRooms				= "CREATE TABLE IF NOT EXISTS Rooms(r_id INT PRIMARY KEY AUTO_INCREMENT, Name TEXT, Description TEXT, Colour INT, Level INT, xPos INT, yPos INT, Area INT, Type INT DEFAULT 0, Alias TEXT, Notes TEXT);";																																																																																											//$NON-NLS-1$
	public static final String	mySQLcreateTerrainTypes			= "CREATE TABLE IF NOT EXISTS TerrainTypes(tp_id INT PRIMARY KEY AUTO_INCREMENT, Name TEXT, Indoors INT DEFAULT 0, Icon INT DEFAULT 0);";																																																																																																											//$NON-NLS-1$
	public static final String	mySQLcreateZones				= "CREATE TABLE zones (z_id INT PRIMARY KEY AUTO_INCREMENT, a_id int DEFAULT 0, bgcolour int DEFAULT 0, bgstyle int DEFAULT 0, bgref int DEFAULT 0, border int, name TEXT NOT NULL, type int DEFAULT 0, paintstyle int DEFAULT 0, description TEXT, labelalignment int DEFAULT 0, labeltext TEXT, labelcolour int DEFAULT 0, labelfont TEXT, borderthickness int DEFAULT 0, bordercolour int DEFAULT 0, bordertype int DEFAULT 0, labelfontsize float, labelfontstyle int DEFAULT 0)";																								//$NON-NLS-1$
	public static final String	mySQLcreateZoneRooms			= "CREATE TABLE zoneRooms (zr_id INT PRIMARY KEY AUTO_INCREMENT, r_id int, z_id int, index integer)";																																																																																																																				//$NON-NLS-1$

}

interface createIndexSQL {
	public static final String	postgreSQLcreateExitIndex			= "CREATE INDEX \"ExitIndex\" ON Exits USING btree (startx , starty , endx , endy , sourcelevel , targetlevel , sourcearea , targetarea);"; //$NON-NLS-1$
	public static final String	postgreSQLcreateExitIndexOnRooms	= "CREATE INDEX \"ExitIndexOnRooms\" ON Exits USING btree (sourceroom , targetroom );";													//$NON-NLS-1$
	public static final String	postgreSQLRoomIndex					= "CREATE INDEX \"RoomIndex\" ON Rooms USING btree(xpos , ypos , area );";																	//$NON-NLS-1$
	public static final String	postgreSQLRoomNameIndex				= "CREATE INDEX \"NameIndex\" ON Rooms USING hash (name COLLATE pg_catalog.\"default\" );";												//$NON-NLS-1$
	public static final String	postgreSQLcreateZoneIndex			= "CREATE INDEX \"zoneAreaIndex\" ON zones USING hash (a_id);";																			//$NON-NLS-1$
	public static final String	postgreSQLcreateZoneRoomsIndex		= "CREATE INDEX \"zoneAreaRoomIndex\" ON zoneRooms USING btree (r_id, z_id);";																//$NON-NLS-1$

}

interface deleteSQL {
	public static final String[]	deleteArea				= { "deleteArea", "DELETE FROM Areas WHERE a_id=?" };								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteExit				= { "deleteExit", "DELETE FROM Exits WHERE e_id=?" };								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteExitInArea		= { "deleteExitInArea", "DELETE FROM Exits WHERE sourceArea=? OR targetArea=?" };	//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteIcon				= { "deleteIcon", "DELETE FROM Icons WHERE ti_id = ?" };							//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	deleteObject			= { "deleteObject", "DELETE FROM Objects WHERE o_id=?" };							//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	cleanOutExits			= { "cleanOutExits", //$NON-NLS-1$
			"DELETE FROM EXITS WHERE (targetroom NOT IN (SELECT r_id FROM ROOMS)) AND (sourceroom NOT IN (SELECT r_id FROM ROOMS))" };			//$NON-NLS-1$
	public static final String[]	roomColourDelete		= { "roomColourDelete", "DELETE FROM RoomColours Where rc_id=?" };					//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomDelete				= { "roomDelete", "DELETE FROM Rooms WHERE r_id=?" };								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomDeleteInArea		= { "roomDeleteInArea", "DELETE FROM Rooms WHERE Area=?" };						//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	deleteZone				= { "deleteZone", "DELETE FROM Zones WHERE z_id=?" };								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteZoneRoomsForZone	= { "deleteZoneRoomsForZone", "DELETE FROM ZoneRooms WHERE z_id=?" };				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteRoomInZoneRooms	= { "deleteRoomInZoneRooms", "DELETE FROM ZoneRooms WHERE r_id=? AND z_id=?" };	//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	deleteZoneRoom			= { "deleteZoneRoom", "DELETE FROM ZoneRooms WHERE zr_id=?" };						//$NON-NLS-1$ //$NON-NLS-2$
}

interface insertSQL {
	public static final String[]	addArea				= {
			"addArea", //$NON-NLS-1$
			"INSERT INTO Areas(Name, Parent, Type, xPos, yPos, Icon, Width, Height, Colour, BorderType, BorderColour, BorderThickness, BorderShadow, Raised, BorderTitle, LabelText, LabelPosition, LabelFontSize, LabelColour, Font, LabelFontStyle, Level, preferredRoomDistance, roomrep) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" };	//$NON-NLS-1$
	public static final String[]	addIcon				= { "addIcon", "INSERT INTO Icons(Icon, Name) VALUES (?,?)" };																																																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	exitInsert			= {
			"exitInsert", //$NON-NLS-1$
			"INSERT INTO Exits(startX, startY, endX, endY, oneWay, Level, Type, sourceRoom, targetRoom,  sourceArea, targetArea, sourceLevel, targetLevel, sourceType, targetType, ShowStub, sourceLock, targetLock, sourceWalkCommand, targetWalkCommand, sourcearearep, targetarearep) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?,?,?)" };		//$NON-NLS-1$
	public static final String[]	addObject			= {
			"addObject", //$NON-NLS-1$
			"INSERT INTO Objects(Icon, Description, xPos, yPos, width, height, labelposition, background, area, level , textsize , font , textstyle, textangle , textcolour, locked) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" };																															//$NON-NLS-1$
	public static final String[]	roomInsert			= { "roomInsert", //$NON-NLS-1$
			"INSERT INTO Rooms(Name,Description, Colour, Level, xPos, yPos, Area, type, alias, Notes) VALUES (?,?,?,?,?,?,?,?,?,?)" };																																																						//$NON-NLS-1$
	public static final String[]	roomColourInsert	= { "roomColourInsert", "INSERT INTO RoomColours(Name,Colour, Type) VALUES (?,?,?)" };																																																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	addZone				= {
			"addZone", //$NON-NLS-1$
			"INSERT INTO Zones(a_id, bgcolour, bgstyle, bgref, border, name, type, paintstyle, description, " //$NON-NLS-1$
					+ "labelalignment ,labeltext ,labelcolour ,labelfontsize ,borderthickness ,bordercolour ,bordertype ,labelfont, labelfontstyle) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" };																																						//$NON-NLS-1$
	public static final String[]	addZoneRoom			= { "addZoneRoom", "INSERT INTO ZoneRooms (r_id,z_id,index) VALUES (?,?,?)" };																																																						//$NON-NLS-1$ //$NON-NLS-2$

}

interface fetchingSQL {
	public static final String		genericSelect					= "SELECT * FROM AREAS";																																	//$NON-NLS-1$ //For Testing that the database Exists, not sure if it going to be here...
	public static final String[]	getTotalOfRooms					= { "getTotalOfRooms", "SELECT COUNT(r_id) FROM Rooms" };																									//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	getTotalOfRoomsInArea			= { "getTotalOfRoomsInArea", "SELECT COUNT(r_id) FROM Rooms" };																							//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	exitSelect						= {
			"exitSelect", //$NON-NLS-1$
			"SELECT * FROM Exits WHERE (sourceLevel=? OR targetLevel=?) AND ((startX>=? AND startY>=? AND startX<=? AND  startY<=?) OR (endX>=? AND endY>=? AND endX<=? AND endY<=?)) AND (sourceArea=? OR targetArea=?)" };	//$NON-NLS-1$
	public static final String[]	exitSelect2						= {
			"exitSelect2", //$NON-NLS-1$
			"SELECT * FROM Exits WHERE (sourceLevel=? OR targetLevel=?) AND ((startX>=? AND startY>=? AND startX<=? AND  startY<=?) OR (endX>=? AND endY>=? AND endX<=? AND endY<=?))" };										//$NON-NLS-1$
	public static final String[]	exitSelectByArea				= { "exitSelectByArea", //$NON-NLS-1$
			"SELECT * FROM Exits WHERE (sourceLevel=? OR targetLevel=?) AND (sourceArea=? OR targetArea=?)" };																													//$NON-NLS-1$
	public static final String[]	exitSelectForRoom				= { "exitSelectForRoom", "SELECT * FROM Exits WHERE targetRoom=? OR sourceRoom=?" };																		//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	allExitsForArea					= { "allExitsForArea", "SELECT * FROM Exits WHERE sourceArea=? OR targetArea=?" };																			//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	allObjectsForArea				= { "allObjectsForArea", "SELECT * FROM Objects WHERE area=?" };																							//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	allRoomsForArea					= { "allRoomsForArea", "SELECT * FROM Rooms" };																											//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	areaDimension					= { "areaDimension", "SELECT max(xpos), min(xpos), max(ypos), min(ypos) FROM Rooms" };																		//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	areaObjectsDimension			= {
			"areaObjectsDimension", "SELECT min(xpos), min(ypos), max(xpos+width), max(ypos+height) FROM Objects WHERE area=?" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	areaAreaDimension				= {
			"areaAreaDimension", "SELECT min(xpos), min(ypos), max(xpos)+max(width), max(ypos)+max(height) FROM Areas WHERE type>0 AND parent=?" };																			//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	areaSelect						= { "areaSelect", "SELECT * FROM Areas WHERE a_id=?" };																									//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	maxIconIndex					= { "maxIconIndex", "SELECT MAX(ti_id) FROM Icons" };																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	maxRoomId						= { "maxRoomId", "SELECT MAX(r_id) FROM Rooms" };																											//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	newestArea						= { "newestArea", "SELECT MAX(a_id) FROM AREAS" };																											//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	oldestRoomInArea				= { "oldestRoomInArea", //$NON-NLS-1$
			"SELECT * FROM Rooms WHERE r_id IN (SELECT MIN(r_id) FROM Rooms)" };																																				//$NON-NLS-1$
	public static final String[]	roomColourSelect				= { "roomColourSelect", "SELECT rc_id, Name, Colour, Type FROM RoomColours" };																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectSubAreas					= { "selectSubAreas", "SELECT * FROM Areas WHERE parent=?" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectArea						= { "selectArea", "SELECT * FROM Areas WHERE a_id=?" };																									//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectAreas						= { "selectAreas", "SELECT * FROM Areas ORDER BY Name ASC" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectIcon						= { "selectIcon", "SELECT Icon, Name, ti_id FROM Icons WHERE ti_id=?" };																					//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectIcons						= { "selectIcons", "SELECT ti_id, Icon, Name FROM Icons" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectObjectById				= { "selectObjectById", "SELECT * FROM Objects WHERE o_id=?" };																							//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectObjectsByArea				= { "selectObjectsByArea", "SELECT * FROM Objects WHERE area=? AND level=?" };																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	selectObjectsByRectangle		= { "selectObjectsByRectangle", //$NON-NLS-1$
			"SELECT * FROM Objects WHERE ((xPos+width)>? OR xPos>?) AND xPos<? AND ((yPos+height)>? OR yPos>?) AND yPos<? AND area=? AND level=?" };																			//$NON-NLS-1$
	public static final String		tryToGetAreaRepsFromExits		= "SELECT sourceAreaRep, targetAreaRep FROM Exits";																										//$NON-NLS-1$
	public static final String		tryToGetPreferredDistanceTest	= "SELECT preferredRoomDistance FROM AREAS";																												//$NON-NLS-1$ //Check if it's an old database.
	public static final String		tryToGetRoomNotes				= "SELECT Notes FROM Rooms";																																//$NON-NLS-1$
	public static final String[]	roomSelectByNameBeginning		= { "roomSelectByNameBeginning", "SELECT * FROM Rooms WHERE Name LIKE '?%'" };																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByNameEnd				= { "roomSelectByNameEnd", "SELECT * FROM Rooms WHERE Name LIKE '%?'" };																					//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByNameMiddle			= { "roomSelectByNameMiddle", "SELECT * FROM Rooms WHERE Name LIKE '%?%'" };																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomFind						= { "roomFind", "SELECT * FROM Rooms WHERE r_id=?" };																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomFindByCoordinates			= {
			"roomFindByCoordinates", "SELECT * FROM Rooms WHERE xPos=? AND yPos=? AND Level=? AND Area=?" };																													//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelect						= {
			"roomSelect", "SELECT * FROM Rooms WHERE (Level=? OR Level=?) AND xPos<? AND yPos<? AND xPos>? AND yPos>?" };																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByNameOrAlias			= {
			"roomSelectByNameOrAlias", "SELECT * FROM Rooms WHERE LOWER(Name) LIKE LOWER(?) OR LOWER(Alias) LIKE LOWER(?)" };																									//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByNameOrDesc			= {
			"roomSelectByNameOrDesc", "SELECT * FROM Rooms WHERE LOWER(Name) LIKE LOWER(?) OR LOWER(Description) LIKE LOWER(?)" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByNameOrDescOrAlias	= {
			"roomSelectByNameOrDescOrAlias", "SELECT * FROM Rooms WHERE LOWER(Name) LIKE LOWER(?) OR LOWER(Description) LIKE LOWER(?) OR LOWER(Alias) LIKE LOWER(?)" };														//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByAlias				= { "roomSelectByAlias", "SELECT * FROM Rooms WHERE LOWER(Alias) LIKE LOWER(?)" };																			//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByArea				= { "roomSelectByArea", "SELECT * FROM Rooms WHERE Level=? AND Area=?" };																					//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByDesc				= { "roomSelectByDesc", "SELECT * FROM Rooms WHERE LOWER(Description) LIKE LOWER(?)" };																	//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByDescOrAlias			= {
			"roomSelectByDescOrAlias", "SELECT * FROM Rooms WHERE LOWER(Description) LIKE LOWER(?) OR LOWER(Alias)=LOWER(?)" };																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomSelectByName				= { "roomSelectByName", "SELECT * FROM Rooms WHERE LOWER(Name) LIKE LOWER(?)" };																			//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	zoneSelect						= { "zoneSelect", "SELECT * FROM Zones" };																													//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	zoneRoomsSelect					= {
			"zoneRoomsSelect", "SELECT * FROM Rooms WHERE r_id IN (SELECT r_id FROM zoneRooms WHERE z_id=?)" };																												//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	zoneSelectForArea				= { "zoneSelectForArea", "SELECT * FROM Zones WHERE a_id=? ORDER BY z_id" };																				//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	getZoneSelect					= { "getZoneSelect", "SELECT * FROM Zones WHERE z_id=?" };																									//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	getZoneRoomsForZone				= { "getZoneRoomsForZone", "SELECT * FROM zonerooms WHERE z_id=?" };																						//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	getMaxIndexForZoneRoom			= { "getMaxIndexForZoneRoom", "SELECT max(index) FROM zonerooms WHERE z_id=?" };																			//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	getZoneRoom						= { "getZoneRoom", "SELECT * FROM ZoneRooms WHERE r_id=? AND z_id=?" };																					//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	getMaxZoneId					= { "getMaxZoneId", "SELECT max(z_id) FROM zones" };																										//$NON-NLS-1$ //$NON-NLS-2$
}

interface updateSQL {
	public static final String		fixEnterStallExits				= "UPDATE Exits SET startX=(SELECT xPos FROM Rooms WHERE r_id=sourceRoom), startY=(SELECT yPos FROM Rooms WHERE sourceRoom=r_id) WHERE (type=192 OR type=205 OR TYPE=189 OR TYPE=176)";																												//$NON-NLS-1$
	public static final String[]	exitUpdate						= {
			"exitUpdate", //$NON-NLS-1$
			"UPDATE Exits SET startX=?, startY=?, endX=?, endY=?, oneWay=?, Level=?, Type=?, sourceRoom=?, targetRoom=?,  sourceArea=?, targetArea=?, sourceType=?, targetType=?, ShowStub=?, sourceLock=?, targetLock=?, sourceWalkCommand=?, targetWalkCommand=?, sourceLevel=?, targetLevel=?, sourceAreaRep=?, targetAreaRep=? WHERE e_id=?" };						//$NON-NLS-1$
	public static final String[]	roomColourUpdate				= {
			"roomColourUpdate", "UPDATE RoomColours SET Name = ?, Colour = ?, Type=? WHERE rc_id = ?" };																																																																	//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	deleteDescriptionsInArea		= { "deleteDescriptionsInArea", "UPDATE Rooms SET description=? WHERE area=?" };																																																						//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	updateObject					= {
			"updateObject", //$NON-NLS-1$
			"UPDATE Objects SET Icon=?, Description=?, xPos=?, yPos=?, width=?, height=?, labelposition=?, background=?, area=?, level =?, textsize=?, font=?, textstyle=?, textangle=?, textcolour=?, locked=? WHERE o_id=?" };																																			//$NON-NLS-1$
	public static final String[]	updateArea						= {
			"updateArea", //$NON-NLS-1$
			"UPDATE Areas SET Name = ?, Parent = ?, Type = ?, xPos = ?, yPos = ?, Icon =?, Width = ?, Height =?, Colour=?, BorderType=?, BorderColour=?, BorderThickness=?,BorderShadow=?, Raised=? , BorderTitle=?, LabelText=?, LabelPosition=?, LabelFontSize=?,LabelColour=?, Font=?, LabelFontStyle=?, Level=?, preferredRoomDistance=?, roomRep=? WHERE a_id = ?" };	//$NON-NLS-1$
	public static final String[]	updateIcon						= { "updateIcon", "UPDATE Icons SET Icon = ?, Name=? WHERE ti_id=?" };																																																									//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	updateIcon2						= { "updateIcon2", "UPDATE Icons SET Icon = ? WHERE ti_id=?" };																																																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	updateIconName					= { "updateIconName", "UPDATE Icons SET Name = ? WHERE ti_id=?" };																																																										//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	roomUpdate						= { "roomUpdate", //$NON-NLS-1$
			"UPDATE Rooms SET Name=?, Description=?, Colour=?, Level=?, xPos =?, yPos=?, Area=? , type=? , alias=? , notes=? WHERE r_id=?" };																																																								//$NON-NLS-1$
	public static final String[]	roomUpdateColourByNameString	= { "roomUpdateColourByName", "UPDATE Rooms SET Colour=? WHERE LOWER(Name) LIKE LOWER(?)" };																																																			//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	updateIconForAreasOnIcon		= { "updateIconForAreasOnIcon", "UPDATE areas SET icon=? where icon=?" };																																																								//$NON-NLS-1$ //$NON-NLS-2$
	public static final String[]	updateIconForObjectsOnIcon		= { "updateIconForObjectsOnIcon", "UPDATE objects SET icon=? where icon=?" };																																																							//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	updateZone						= {
			"updateZone", //$NON-NLS-1$
			"UPDATE Zones SET a_id=?, bgcolour=?, bgstyle=?, bgref=?, border=?, name=?, type=?, paintstyle=?, description=?, labelalignment=? ,labeltext=?, labelcolour=?, labelfontsize=?, borderthickness=?, bordercolour=?, bordertype=?, labelfont=?, labelfontstyle=? WHERE z_id=?" };																				//$NON-NLS-1$

	public static final String[]	updateZoneRooms					= { "updateZoneRooms", "UPDATE ZoneRooms SET r_id=?,z_id=? WHERE zr_id=?" };																																																							//$NON-NLS-1$ //$NON-NLS-2$

	public static final String[]	updateZoneRoomIndex				= { "updateZoneRoomIndex", "UPDATE ZoneRooms SET index=? WHERE zr_id=?" };																																																								//$NON-NLS-1$ //$NON-NLS-2$

}

interface public_messages {
	public static final String	addAreaRepresenationForExit		= "Adding information of area representation...";	//$NON-NLS-1$
	public static final String	tablesExists					= "Tables Already Exits...";						//$NON-NLS-1$
	public static final String	addingPreferredRoomDistance		= "Adding preferredRoomDistance to Areas...";		//$NON-NLS-1$
	public static final String	connectionFailure				= "Failed to connect to database!!!\n";			//$NON-NLS-1$
	public static final String	couldntAddAreaRepsForExit		= "Couldn't create new columns in Exits: ";		//$NON-NLS-1$
	public static final String	couldntModifyAreas				= "Couldn't Modify Areas: ";						//$NON-NLS-1$
	public static final String	creatingTables					= "Creating Tables";								//$NON-NLS-1$
	public static final String	defaultAreaName					= "Default";										//$NON-NLS-1$
	public static final String	empty							= "";												//$NON-NLS-1$
	public static final String	exitingProgram					= "Exiting Program.";								//$NON-NLS-1$
	public static final String	failedPrepareStatements			= "Failed to prepare statesments.";				//$NON-NLS-1$
	public static final String	failedToAddArea					= "Failed To Add Area";							//$NON-NLS-1$
	public static final String	failedToAddExit					= "Failed To Add Exit!";							//$NON-NLS-1$

	public static final String	failedToAddObject				= "Failed To Add Object: ";						//$NON-NLS-1$
	public static final String	failedToAddRoom					= "Failed To Add Room!";							//$NON-NLS-1$

	public static final String	failedToAddRoomColourHandle		= "Failed Insertion Of RoomHandle: ";				//$NON-NLS-1$
	public static final String	failedToCleanOutExits			= "Couldn't clean out Exits: ";					//$NON-NLS-1$
	public static final String	failedToCommit					= "Failed to Commit: ";							//$NON-NLS-1$
	public static final String	failedToCreateDefaultValues		= "Failed to create Default Values.";				//$NON-NLS-1$
	public static final String	failedToDeleteArea				= "Failed to delete Area: ";						//$NON-NLS-1$
	public static final String	failedToDeleteExit				= "Failed to Delete Exit: ";						//$NON-NLS-1$

	public static final String	failedToDeleteObject			= "Failed To Delete Object: ";						//$NON-NLS-1$
	public static final String	failedToDeleteRoom				= "Failed to Delete Room: ";						//$NON-NLS-1$
	public static final String	failedToDeleteRoomColourHandle	= "Failed to Delete RoomColourHandle: ";			//$NON-NLS-1$
	public static final String	failedToFetchAllObjectForArea	= "Failed to fetch All Objects For Area: ";		//$NON-NLS-1$
	public static final String	failedToFetchExits				= "Failed to fetch Exits: ";						//$NON-NLS-1$
	public static final String	failedToFetchRoom				= "Failed to fetch room: ";						//$NON-NLS-1$
	public static final String	failedToFetchRooms				= "Failed to fetch Rooms: ";						//$NON-NLS-1$

	public static final String	failedToFetchRoomsForMerge		= "Failed to fetch Rooms for merge: ";				//$NON-NLS-1$
	public static final String	failedToGetAllAreas				= "Failed to fetch all Areas!";					//$NON-NLS-1$
	public static final String	failedToGetAllExitsForArea		= "Failed to fetch All Exits For Area: ";			//$NON-NLS-1$
	public static final String	failedToGetAllObjectsForArea	= "Failed to fetch All Objects For Area: ";		//$NON-NLS-1$
	public static final String	failedToGetAllRoomsForArea		= "Failed to fetch All Rooms For Area: ";			//$NON-NLS-1$
	public static final String	failedToGetArea					= "Failed To Get Area: ";							//$NON-NLS-1$
	public static final String	failedToGetIcon					= "Failed to get Icon: ";							//$NON-NLS-1$

	public static final String	failedToGetLastRoomId			= "Failed to get last Room Id! ";					//$NON-NLS-1$
	public static final String	failedToGetObject				= "Failed TO Get Object: ";						//$NON-NLS-1$
	public static final String	failedToGetObjectsForRectangle	= "Failed To Get Objects For Rectangle: ";			//$NON-NLS-1$
	public static final String	failedToGetOldestRoomForArea	= "Failed to fetch Oldest Room For Area: ";		//$NON-NLS-1$
	public static final String	failedToGetRoom					= "Failed To Get Room: ";							//$NON-NLS-1$
	public static final String	failedToGetRoomColourHandle		= "Failed Fetching Of RoomHandle: ";				//$NON-NLS-1$
	public static final String	failedToGetSizeOfArea			= "Failed to get Size of Area:";					//$NON-NLS-1$
	public static final String	failedToUpdateArea				= "Failed To update Area!";						//$NON-NLS-1$

	public static final String	failedToUpdateExit				= "Failed to Update Exit: ";						//$NON-NLS-1$
	public static final String	failedToUpdateObject			= "FailedToUpdateObject: ";						//$NON-NLS-1$
	public static final String	failedToUpdateRoom				= "Failed to Update Room: ";						//$NON-NLS-1$
	public static final String	failedToUpdateRoomColourHandle	= "Failed Updating Of RoomHandle: ";				//$NON-NLS-1$
}
