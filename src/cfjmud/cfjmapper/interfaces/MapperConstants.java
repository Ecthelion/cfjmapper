/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.interfaces;

/**
 * MapperConstants for cfjMapper
 * 
 * @author Ecthelion
 * 
 */
public interface MapperConstants {
	/**
	 * The Constant for changing alias for the current room.
	 */
	public final static String		ALIAS					= "alias";																		//$NON-NLS-1$
	public static final String[]	areaRepresentationTypes	= { "Label", "None", "\"Room\"", "\"Room\" and Label", "Graphics", "Icon" };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
	public static final int			BASE					= 0x10;
	public static final int			CLIMB_TYPE				= 2;
	public static final int			DERBY					= 0;
	public final static int			DOWN					= 0x0a;
	public static final int			DOWN_UP					= 0xa9;
	public final static int			EAST					= 0x03;
	public static final int			EAST_EAST				= 0x33;
	public static final int			EAST_NORTH				= 0x31;

	public static final int			EAST_NORTHEAST			= 0x32;
	public static final int			EAST_NORTHWEST			= 0x38;
	public static final int			EAST_SOUTH				= 0x35;
	public static final int			EAST_SOUTHEAST			= 0x34;
	public static final int			EAST_SOUTHWEST			= 0x36;
	public static final int			EAST_WEST				= 0x37;
	public static final int			ENTER_STALL_TYPE		= 4;
	public static final int			ENTER_TENT_TYPE			= 3;
	public final static int			ENTERSTALL				= 0x0c;
	public static final int			ENTERSTALL_LEAVE		= 0xcd;																		// LEAVE
																																			// or
																																			// OUT
	public final static int			ENTERTENT				= 0x0b;
	public static final int			ENTERTENT_LEAVE			= 0xbd;																		// LEAVE
																																			// or
																																			// OUT
	/**
	 * The Constant for the Command Journey.
	 */
	public final static String		JOURNEY					= "journey";																	//$NON-NLS-1$
	/**
	 * The Constant for Teleport to Another room.
	 */
	public final static String		JUMP_TO_ROOM			= "jump";																		//$NON-NLS-1$
	public final static int			LEAVE					= 0x0d;

	public static final int			LEAVE_ENTERSTALL		= 0xdc;																		// LEAVE
																																			// or
																																			// OUT
	public static final int			LEAVE_ENTERTENT			= 0xdb;																		// LEAVE
																																			// or
																																			// OUT

	public static final int			LEAVE_TYPE				= 5;
	/**
	 * Constant for the Command MAP
	 */
	public final static String		MAP						= "map";																		//$NON-NLS-1$

	/**
	 * Constant for the command MOVE.
	 */
	public final static String		MOVE					= "move";																		//$NON-NLS-1$
	public static String			SETPROFILE				= "set_profile";																//$NON-NLS-1$

	public static final String[]	movementTypes			= { "Normal", "Swim", "Climb" };												//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	public static final int			MYSQL					= 1;
	public static final int			NORMAL_TYPE				= 0;
	public final static int			NORTH					= 0x01;
	public static final int			NORTH_EAST				= 0x13;
	public static final int			NORTH_NORTH				= 0x11;
	public static final int			NORTH_NORTHEAST			= 0x12;
	public static final int			NORTH_NORTHWEST			= 0x18;

	public static final int			NORTH_SOUTH				= 0x15;
	public static final int			NORTH_SOUTHEAST			= 0x14;
	public static final int			NORTH_SOUTHWEST			= 0x16;
	public static final int			NORTH_WEST				= 0x17;
	public final static int			NORTHEAST				= 0x02;
	public static final int			NORTHEAST_EAST			= 0x23;																		// 35
	public static final int			NORTHEAST_NORTH			= 0x21;																		// 33
	public static final int			NORTHEAST_NORTHEAST		= 0x22;																		// 34

	public static final int			NORTHEAST_NORTHWEST		= 0x28;																		// 40
	public static final int			NORTHEAST_SOUTH			= 0x25;																		// 37
	public static final int			NORTHEAST_SOUTHEAST		= 0x24;																		// 36
	public static final int			NORTHEAST_SOUTHWEST		= 0x26;																		// 38
	public static final int			NORTHEAST_WEST			= 0x27;																		// 39
	public final static int			NORTHWEST				= 0x08;
	public static final int			NORTHWEST_EAST			= 0x83;
	public static final int			NORTHWEST_NORTH			= 0x81;

	public static final int			NORTHWEST_NORTHEAST		= 0x82;
	public static final int			NORTHWEST_NORTHWEST		= 0x88;
	public static final int			NORTHWEST_SOUTH			= 0x85;
	public static final int			NORTHWEST_SOUTHEAST		= 0x84;
	public static final int			NORTHWEST_SOUTHWEST		= 0x86;
	public static final int			NORTHWEST_WEST			= 0x87;
	/**
	 * Adds a Notification for the Current Room...
	 */
	public static final String		NOTIFICATION			= "NOTIFICATION";																//$NON-NLS-1$
	public final static int			OTHER					= 0x00;

	public final static int			OUT						= 0x0d;
	public static final int			OUT_TYPE				= 6;
	public static final int			POSTG					= 2;
	/**
	 * Removes the Notification for the Current room...
	 */
	public static final String		REMOVENOTIFICATION		= "REMOVENOTIFICATION";														//$NON-NLS-1$
	/**
	 * Constant for the Command ShadowMove
	 */
	public final static String		SHMOVE					= "shmove";																	//$NON-NLS-1$
	/**
	 * Constant for the Command SHADOW STOP
	 */
	public final static String		SHSTOP					= "shstop";																	//$NON-NLS-1$

	public final static int			SOUTH					= 0x05;
	public static final int			SOUTH_EAST				= 0x53;
	public static final int			SOUTH_NORTH				= 0x51;
	public static final int			SOUTH_NORTHEAST			= 0x52;
	public static final int			SOUTH_NORTHWEST			= 0x58;
	public static final int			SOUTH_SOUTH				= 0x55;
	public static final int			SOUTH_SOUTHEAST			= 0x54;
	public static final int			SOUTH_SOUTHWEST			= 0x56;

	public static final int			SOUTH_WEST				= 0x57;
	public final static int			SOUTHEAST				= 0x04;
	public static final int			SOUTHEAST_EAST			= 0x43;
	public static final int			SOUTHEAST_NORTH			= 0x41;
	public static final int			SOUTHEAST_NORTHEAST		= 0x42;
	public static final int			SOUTHEAST_NORTHWEST		= 0x48;
	public static final int			SOUTHEAST_SOUTH			= 0x45;
	public static final int			SOUTHEAST_SOUTHEAST		= 0x44;

	public static final int			SOUTHEAST_SOUTHWEST		= 0x46;
	public static final int			SOUTHEAST_WEST			= 0x47;
	public final static int			SOUTHWEST				= 0x06;
	public static final int			SOUTHWEST_EAST			= 0x63;
	public static final int			SOUTHWEST_NORTH			= 0x61;
	public static final int			SOUTHWEST_NORTHEAST		= 0x62;
	public static final int			SOUTHWEST_NORTHWEST		= 0x68;
	public static final int			SOUTHWEST_SOUTH			= 0x65;
	public static final int			SOUTHWEST_SOUTHEAST		= 0x64;
	public static final int			SOUTHWEST_SOUTHWEST		= 0x66;

	public static final int			SOUTHWEST_WEST			= 0x67;
	public static final int			SWIM_TYPE				= 1;
	/**
	 * The Constant for switching to another area, probably never used.
	 */
	public final static String		SWITCH_TO_AREA			= "switch";																	//$NON-NLS-1$
	public static final int			NONE					= 0;
	public static final int			SQUARE					= 1;
	public static final int			ROUNDED_SQUARE			= 2;
	public static final int			OVAL					= 3;
	public static final int			TRIANGLE				= 4;
	public static final int			ICON					= 5;
	public static final int			THUMBNAIL				= 6;
	public static final int			REVERSE_TRIANGLE		= 7;
	public static final int			DIAMOND					= 8;

	public final static int			UP						= 0x09;

	public static final int			UP_DOWN					= 0x9a;
	public final static int			WEST					= 0x07;
	public static final int			WEST_EAST				= 0x73;
	public static final int			WEST_NORTH				= 0x71;
	public static final int			WEST_NORTHEAST			= 0x72;
	public static final int			WEST_NORTHWEST			= 0x78;
	public static final int			WEST_SOUTH				= 0x75;

	public static final int			WEST_SOUTHEAST			= 0x74;
	public static final int			WEST_SOUTHWEST			= 0x76;
	public static final int			WEST_WEST				= 0x77;

	public static final String		tab						= "\t";																		//$NON-NLS-1$
	public static final String		dtab					= "\t\t";																		//$NON-NLS-1$
	public static final String		eol						= "\n";																		//$NON-NLS-1$
}
