/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.interfaces;

/**
 * @author Ecthelion
 * 
 */
public interface PropertiesConstants {

	static String				PROPERTIESFORMAPPER						= "Properties for Mapper";														//$NON-NLS-1$

	static String				propertyFile							= "Mapper.ini";																//$NON-NLS-1$

	public static String		PROFILE									= "profile";																	//$NON-NLS-1$

	public static String		PROPERTYFILENAME						= "PropertyFileName";															//$NON-NLS-1$
	public static String		PROPERTYFILENAME_DEFAULT				= "Mapper.ini";																//$NON-NLS-1$

	public static String		STARTINGXPOS							= "startingXPos";																//$NON-NLS-1$
	public static String		STARTINGXPOS_DEFAULT					= "5000";																		//$NON-NLS-1$

	public static String		STARTINGYPOS							= "startingYPos";																//$NON-NLS-1$
	public static String		STARTINGYPOS_DEFAULT					= "5000";																		//$NON-NLS-1$

	public static String		STARTINGLEVEL							= "startingLevel";																//$NON-NLS-1$
	public static String		STARTINGLEVEL_DEFAULT					= "0";																			//$NON-NLS-1$

	public static String		XPOS									= "xPos";																		//$NON-NLS-1$
	public static String		XPOS_DEFAULT							= "5000";																		//$NON-NLS-1$

	public static String		YPOS									= "yPos";																		//$NON-NLS-1$
	public static String		YPOS_DEFAULT							= "5000";																		//$NON-NLS-1$

	public static String		SPACEBETWEENROOMS						= "spaceBetweenRooms";															//$NON-NLS-1$
	public static String		SPACEBETWEENROOMS_DEFAULT				= "35";																		//$NON-NLS-1$

	public static String		STARTINGROOM							= "startingRoom";																//$NON-NLS-1$
	public static String		STARTINGROOM_DEFAULT					= "1";																			//$NON-NLS-1$

	public static String		STARTINGAREA							= "currentArea";																//$NON-NLS-1$
	public static String		STARTINGAREA_DEFAULT					= "Default_0";																	//$NON-NLS-1$

	public static String		STARTINGAREAID							= "startingAreaId";															//$NON-NLS-1$
	public static String		STARTINGAREAID_DEFAULT					= "1";																			//$NON-NLS-1$
	public static String		WINDOWSTARTX							= "window_startX";																//$NON-NLS-1$
	public static String		WINDOWSTARTX_DEFAULT					= "275";																		//$NON-NLS-1$

	public static String		WINDOWSTARTY							= "window_startY";																//$NON-NLS-1$
	public static String		WINDOWSTARTY_DEFAULT					= "430";																		//$NON-NLS-1$

	public static String		WINDOWWIDTH								= "window_width";																//$NON-NLS-1$
	public static String		WINDOWWIDTH_DEFAULT						= "650";																		//$NON-NLS-1$

	public static String		WINDOWHEIGHT							= "window_height";																//$NON-NLS-1$
	public static String		WINDOWHEIGHT_DEFAULT					= "450";																		//$NON-NLS-1$

	public static String		CONNECTIONSTRING						= "connectionString";															//$NON-NLS-1$
	public static String		CONNECTIONSTRING_DEFAULT				= "jdbc:derby:";																//$NON-NLS-1$

	public static String		PREFERREDDBNAME							= "preferredDBName";															//$NON-NLS-1$
	public static String		PREFERREDDBNAME_DEFAULT					= "CFJMapperDB;create=true";													//$NON-NLS-1$

	public static String		EXITSPOLICY								= "exitsPolicy";																//$NON-NLS-1$
	public static String		EXITSPOLICY_DEFAULT						= "2";																			//$NON-NLS-1$

	public static String		USERNAME								= "userName";																	//$NON-NLS-1$
	public static String		USERNAME_DEFAULT						= "username";																	//$NON-NLS-1$

	public static String		PASSWORD								= "password";																	//$NON-NLS-1$
	public static String		PASSWORD_DEFAULT						= "password";																	//$NON-NLS-1$

	public static String		PORTALALIAS								= "portalAlias";																//$NON-NLS-1$
	public static String		PORTALALIAS_DEFAULT						= "port";																		//$NON-NLS-1$

	public static String		CENTREONCURRENTROOM						= "centreOnCurrentRoom";														//$NON-NLS-1$
	public static String		CENTREONCURRENTROOM_DEFAULT				= Boolean.toString(true);

	public static String		SHOWSPECIALEXITS						= "showSpecialExits";															//$NON-NLS-1$
	public static String		SHOWSPECIALEXITS_DEFAULT				= Boolean.toString(true);

	public static String		SUBTISTUTESPECIALEXITS					= "subtistuteSpecialExits";													//$NON-NLS-1$
	public static String		SUBTISTUTESPECIALEXITS_DEFAULT			= Boolean.toString(false);

	public static String		AUTOJOURNEY								= "autojourney";																//$NON-NLS-1$
	public static String		AUTOJOURNEY_DEFAULT						= Boolean.toString(false);

	public static String		DIVIDERLOCATION							= "divider";																	//$NON-NLS-1$
	public static String		DIVIDERLOCATION_DEFAULT					= "100";																		//$NON-NLS-1$

	public static String		NORMALEXITCOLOUR						= "normalExitColour";															//$NON-NLS-1$
	public static String		SWIMEXITCOLOUR							= "swimExitColour";															//$NON-NLS-1$
	public static String		CLIMBEXITCOLOUR							= "climbExitColour";															//$NON-NLS-1$
	public static String		DEFAULTROOMCOLOUR						= "defaultRoomColour";															//$NON-NLS-1$
	public static String		CURRENTROOMCOLOUR						= "currentRoomColour";															//$NON-NLS-1$
	public static String		DEFAULTBACKGROUNDCOLOUR					= "defaultBackgroundColour";													//$NON-NLS-1$
	public static String		RADIUS									= "radius";																	//$NON-NLS-1$'
	public static String		RADIUS_DEFAULT							= "5000";																		//$NON-NLS-1$
	public static String		ROOMREPRESENTATION						= "roomrepresenation";															//$NON-NLS-1$
	public static String		ROOMREPRESENTATION_DEFAULT				= "0";																			//$NON-NLS-1$
	public static String		GRAPHICEDITORSIDE						= "graphiceditorside";															//$NON-NLS-1$
	public static String		GRAPHICEDITORSIDE_DEFAULT				= "23";																		//$NON-NLS-1$
	public static String		HTML_START								= "html_start";																//$NON-NLS-1$
	public static String		HTML_START_DEFAULT						= "<html>";																	//$NON-NLS-1$
	public static String		HTML_END								= "html_end";																	//$NON-NLS-1$
	public static String		HTML_END_DEFAULT						= "</html>";																	//$NON-NLS-1$
	public static String		HTML_TABLE_START						= "html_table_start";															//$NON-NLS-1$
	public static String		HTML_TABLE_START_DEFAULT				= "<table border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\">";	//$NON-NLS-1$
	public static String		HTML_TABLE_END							= "html_table_end";															//$NON-NLS-1$
	public static String		HTML_TABLE_END_DEFAULT					= "</table>";																	//$NON-NLS-1$
	public static String		HTML_TABLE_TD_START						= "html_table_td_start";														//$NON-NLS-1$
	public static String		HTML_TABLE_TD_START_DEFAULT				= "<td>";																		//$NON-NLS-1$
	public static String		HTML_TABLE_TD_END						= "html_table_td_end";															//$NON-NLS-1$
	public static String		HTML_TABLE_TD_END_DEFAULT				= "</td>";																		//$NON-NLS-1$
	public static String		HTML_TABLE_TR_START						= "html_table_tr_start";														//$NON-NLS-1$
	public static String		HTML_TABLE_TR_START_DEFAULT				= "<tr>";																		//$NON-NLS-1$
	public static String		HTML_TABLE_TR_END						= "html_table_tr_end";															//$NON-NLS-1$
	public static String		HTML_TABLE_TR_END_DEFAULT				= "</tr>";																		//$NON-NLS-1$
	public static String		HTML_CAPTION_START						= "html_caption_start";														//$NON-NLS-1$
	public static String		HTML_CAPTION_START_DEFAULT				= "<caption>Overview for area: ";												//$NON-NLS-1$
	public static String		HTML_CAPTION_END						= "html_caption_end";															//$NON-NLS-1$
	public static String		HTML_CAPTION_END_DEFAULT				= "</caption>";																//$NON-NLS-1$
	public static String		HTML_BODY_START							= "html_body_start";															//$NON-NLS-1$
	public static String		HTML_BODY_START_DEFAULT					= "<body>";																	//$NON-NLS-1$
	public static String		HTML_BODY_END							= "html_body_end";																//$NON-NLS-1$
	public static String		HTML_BODY_END_DEFAULT					= "</body>";																	//$NON-NLS-1$
	public static String		HTML_IMAGE_LINK_START					= "html_image_link";															//$NON-NLS-1$
	public static String		HTML_IMAGE_LINK_START_DEFAULT			= "<a href=\"";																//$NON-NLS-1$
	public static String		HTML_IMAGE_LINK_END						= "html_image_link_end";														//$NON-NLS-1$
	public static String		HTML_IMAGE_LINK_END_DEFAULT				= "\">";																		//$NON-NLS-1$
	public static String		HTML_IMAGE_START						= "html_image_start";															//$NON-NLS-1$
	public static String		HTML_IMAGE_START_DEFAULT				= "<img src=\"";																//$NON-NLS-1$
	public static String		HTML_IMAGE_MIDDLE						= "html_image_middle";															//$NON-NLS-1$
	public static String		HTML_IMAGE_MIDDLE_DEFAULT				= "\" alt=\"";																	//$NON-NLS-1$
	public static String		HTML_IMAGE_MIDDLE_2						= "html_image_middle_2";														//$NON-NLS-1$
	public static String		HTML_IMAGE_MIDDLE_2_DEFAULT				= "\" usemap=\"#";																//$NON-NLS-1$
	public static String		HTML_IMAGE_END							= "html_image_end";															//$NON-NLS-1$
	public static String		HTML_IMAGE_END_DEAFULT					= "\">";																		//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_START				= "html_overview_image_start";													//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_START_DEFAULT		= "<img src=\"";																//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_MIDDLE				= "html_overview_image_middle";												//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_MIDDLE_DEFAULT		= "\" alt=\"";																	//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_MIDDLE_2			= "html_overview_image_middle_2";												//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_MIDDLE_2_DEFAULT	= "\" usemap=\"#";																//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_END					= "html_overview_image_end";													//$NON-NLS-1$
	public static String		HTML_OVERVIEW_IMAGE_END_DEAFULT			= "\" width=\"100%\">";														//$NON-NLS-1$
	public static String		HTML_MAP_START							= "html_map_start";															//$NON-NLS-1$
	public static String		HTML_MAP_START_DEFAULT					= "<map name=\"";																//$NON-NLS-1$
	public static String		HTML_MAP_END							= "html_map_end";																//$NON-NLS-1$
	public static String		HTML_MAP_END_DEFAULT					= "\">";																		//$NON-NLS-1$
	public static String		HTML_MAP_CLOSE_TAG						= "html_map_close_tag";														//$NON-NLS-1$
	public static String		HTML_MAP_CLOSE_TAG_DEFAULT				= "</map>";																	//$NON-NLS-1$
	public static String		HTML_MAP_AREA_START						= "html_map_area_start";														//$NON-NLS-1$
	public static String		HTML_MAP_AREA_START_DEFAULT				= "<area shape=\"";															//$NON-NLS-1$
	public static String		HTML_MAP_AREA_SECOND					= "html_map_area_second";														//$NON-NLS-1$
	public static String		HTML_MAP_AREA_SECOND_DEFAULT			= "\" coords=\"";																//$NON-NLS-1$
	public static String		HTML_MAP_AREA_THIRD						= "html_map_area_third";														//$NON-NLS-1$
	public static String		HTML_MAP_AREA_THIRD_DEFAULT				= "\" href=\"";																//$NON-NLS-1$
	public static String		HTML_MAP_AREA_FOURTH					= "html_map_area_fourth";														//$NON-NLS-1$
	public static String		HTML_MAP_AREA_FOUTH_DEFAULT				= "\" alt=\"";																	//$NON-NLS-1$
	public static String		HTML_MAP_AREA_END						= "html_map_area_end";															//$NON-NLS-1$
	public static String		HTML_MAP_AREA_END_DEFAULT				= "\">";																		//$NON-NLS-1$

	public static String		HTML_WIDTH_OF_LINK_SIZE					= "html_width_of_link_size";													//$NON-NLS-1$
	public static String		HTML_WIDTH_OF_LINK_SIZE_DEFAULT			= "1280";																		//$NON-NLS-1$
	// <area shape="rect" coords="0,0,82,126" href="sun.htm" alt="Sun">
	// <map name="planetmap">
	// usemap="#planetmap">

	public static String		CURRENTSHADOWCOLOUR						= "currentshadowcolour";														//$NON-NLS-1$

	public static final String	ENTERSTALLREPRESENTATION				= "enter_stall_representation";												//$NON-NLS-1$
	public static final String	ENTERSTALLREPRESENTATIONDEFAULT			= "0";																			//$NON-NLS-1$
	public static final String	ENTERTENTREPRESENTATION					= "enter_tent_representation";													//$NON-NLS-1$
	public static final String	ENTERTENTREPRESENTATIONDEFAULT			= "0";																			//$NON-NLS-1$

	public static final String	ALPAVALUE								= "alphavalue";																//$NON-NLS-1$
	public static final String	ALPAVALUEDEFAULT						= "0.1";																		//$NON-NLS-1$
	public static final String	MINSIDE									= "minside";																	//$NON-NLS-1$
	public static final String	MINSIDEDEFAULT							= "100";																		//$NON-NLS-1$
	public static final String	MAXSIDE									= "maxside";																	//$NON-NLS-1$
	public static final String	MAXSIDEDEFAULT							= "10000";																		//$NON-NLS-1$
}
