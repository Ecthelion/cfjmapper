/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

/**
 * 
 * @author Ecthelion
 * 
 */
public class MudCommand {

	private final String[]	arguments;
	private final String	commandName;
	private final String	commandType;

	public MudCommand(String commandName, String commandType, String[] arguments) {

		this.commandName = commandName;
		this.commandType = commandType;
		this.arguments = arguments;
	}

	public String[] getArguments() {
		return arguments;
	}

	public String getCommandName() {
		return commandName;
	}

	public String getCommandType() {
		return commandType;
	}

	@Override
	public String toString() {
		String toReturn = ""; //$NON-NLS-1$
		toReturn += commandName;
		toReturn += " - "; //$NON-NLS-1$
		toReturn += commandType;
		toReturn += " - ["; //$NON-NLS-1$
		for (String str : arguments) {
			toReturn += str + " - "; //$NON-NLS-1$
		}
		toReturn += "]"; //$NON-NLS-1$
		return toReturn;
	}
}
