/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.awt.Point;
import java.util.ArrayList;

import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.Room;

/**
 * 
 * @author Ecthelion
 * 
 */
public class ExitsManager {
	// sourceLock, targetLock, sourceWalkCommand, targetWalkCommand);

	// private static int policy = 0;
	private static Database			database	= null;
	private static ArrayList<Exit>	queuedExits	= new ArrayList<Exit>();

	// private static int currentArea;

	protected static Point calculateDirDiff(int dir, int xPos, int yPos, int spaceBetweenRooms) {
		if (database == null) database = Database.getInstance();
		Point pointToReturn = new Point(xPos, yPos);
		switch (dir) {
			case Exit.NORTH:
				pointToReturn.translate(0, -spaceBetweenRooms);
				break;
			case Exit.NORTHEAST:
				pointToReturn.translate(spaceBetweenRooms, -spaceBetweenRooms);
				break;
			case Exit.EAST:
				pointToReturn.translate(spaceBetweenRooms, 0);
				break;
			case Exit.SOUTHEAST:
				pointToReturn.translate(spaceBetweenRooms, spaceBetweenRooms);
				break;
			case Exit.SOUTH:
				pointToReturn.translate(0, spaceBetweenRooms);
				break;
			case Exit.SOUTHWEST:
				pointToReturn.translate(-spaceBetweenRooms, spaceBetweenRooms);
				break;
			case Exit.WEST:
				pointToReturn.translate(-spaceBetweenRooms, 0);
				break;
			case Exit.NORTHWEST:
				pointToReturn.translate(-spaceBetweenRooms, -spaceBetweenRooms);
				break;
			default:
				pointToReturn.translate(0, 0);
				break;
		}
		return pointToReturn;
	}

	public static void commitExits() {
		if (database == null) database = Database.getInstance();
		database.addExits(queuedExits);
		queuedExits.clear();
	}

	/**
	 * Has value of one in Mapper Creates the exits connects them to the
	 * existing rooms. "Connect  Stubs"
	 * 
	 * @param sourceRoom
	 * @param targetRoom
	 * @param direction
	 * @param exits
	 * @param currentLevel
	 * @param currentAreaId
	 * @param xPos
	 * @param yPos
	 * @param spaceBetweenRooms
	 * @param startType
	 * @param endType
	 */
	public static void connectStubs(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int xPos, int yPos,
			int spaceBetweenRooms, int[] startType, int[] endType) {
		boolean debug = false;
		// currentArea = currentAreaId;
		if (debug) {
			System.out.println("*************************"); //$NON-NLS-1$
			System.out.println(sourceRoom);
			System.out.println(targetRoom);
			System.out.println(direction);
			System.out.println(exits.length);
			System.out.println(currentAreaId);
			System.out.println(currentLevel);
			System.out.println(xPos);
			System.out.println(yPos);
			System.out.println("*************************"); //$NON-NLS-1$
		}
		int rDir = reverseDir(direction);
		// Room tmpRoom = null;
		Exit tmpExit = null;
		if (database == null) database = Database.getInstance();

		// Connecting to the room we came from.
		if (sourceRoom != null) tmpExit = hasExitInThatDir(direction, sourceRoom);

		if (tmpExit != null) {
			if (debug) System.out.println("tmpExit in stubs: " + tmpExit.getId()); //$NON-NLS-1$
			tmpExit.setTargetRoom(targetRoom, reverseDir(direction));
			database.updateExit(null, tmpExit);
		} else {
			int st = 0;
			int et = 0;
			for (int i = 0; i < exits.length; i++) {
				if (exits[i] == direction) {
					st = startType[i];
					et = endType[i];
				}
			}
			queueExit(sourceRoom, targetRoom, direction, currentAreaId, st, et);
		}

		commitExits();
		for (int i = 0; i < exits.length; i++) {
			if (exits[i] != rDir) {
				Room room = isRoomInThatDir(exits[i], currentLevel, xPos, yPos, spaceBetweenRooms, currentAreaId);
				if (room != null) {
					Exit exit = hasExitInThatDir(reverseDir(exits[i]), room);
					if (exit != null) {
						exit.setTargetRoom(targetRoom, exits[i]);
						database.updateExit(null, exit);
					} else {
						queueExit(targetRoom, null, exits[i], currentAreaId, startType[i], endType[i]);
					}
				} else {
					queueExit(targetRoom, null, exits[i], currentAreaId, startType[i], endType[i]);
				}
			}
		}
		commitExits();
	}

	/**
	 * 
	 * @param sourceRoom
	 *            The room from which we went.
	 * @param targetRoom
	 *            The New Room that we want to make Exits To.
	 * @param direction
	 *            The Direction From sourceRoom to targetRoom
	 * @param exits
	 *            The Exits in targetRoom
	 * @param currentLevel
	 *            Current Level
	 * @param currentAreaId
	 *            Current Area Id
	 * @param xPos
	 * @param yPos
	 * @param spaceBetweenRooms
	 *            The number of Pixels between each room.
	 * @param startType
	 *            Array of movementTypes of of the Exits In targetRoom
	 * @param endType
	 *            Array of how the above ends.
	 */
	public static void createAllExits(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int xPos, int yPos,
			int spaceBetweenRooms, int[] startType, int[] endType) {
		boolean debug = false;
		// currentArea = currentAreaId;

		if (debug) {
			System.out.println("*****create All Exits*****"); //$NON-NLS-1$
			System.out.println(sourceRoom);
			System.out.println(targetRoom);
			System.out.println(direction);
			System.out.println(exits.length);
			System.out.println(currentAreaId);
			System.out.println(currentLevel);
			System.out.println(xPos);
			System.out.println(yPos);
			System.out.println("**************************"); //$NON-NLS-1$
		}

		if (database == null) database = Database.getInstance();
		for (int i = 1; i <= 8; i++) {
			Room tmpRoom = isRoomInThatDir(i, currentLevel, xPos, yPos, spaceBetweenRooms, currentAreaId);
			Exit exit = null;
			if (tmpRoom != null) exit = hasExitInThatDir(reverseDir(i), tmpRoom);
			if (exit != null) {
				if (exit.getTargetRoomId() == 0) {
					if (targetRoom != null) {
						exit.setTargetRoom(targetRoom, i);
						database.updateExit(null, exit);
					}
				}
			} else {
				queueExit(targetRoom, tmpRoom, i, currentAreaId, 0, 0);
			}
		}
		commitExits();
	}

	/**
	 * Has a value of 2 in Mapper
	 * 
	 * @param sourceRoom
	 * @param targetRoom
	 * @param direction
	 * @param exits
	 * @param currentLevel
	 * @param currentAreaId
	 * @param xPos
	 * @param yPos
	 * @param spaceBetweenRooms
	 * @param startType
	 * @param endType
	 */
	public static void createExistsToExistingRooms(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int xPos,
			int yPos, int spaceBetweenRooms, int[] startType, int[] endType) {
		boolean debug = false;
		// currentArea = currentAreaId;

		if (debug) {
			System.out.println("Entering createExistsToExistingRooms"); //$NON-NLS-1$
			System.out.println(sourceRoom);
			System.out.println("************************************"); //$NON-NLS-1$
			System.out.println(targetRoom);
			System.out.println("************************************"); //$NON-NLS-1$
			System.out.println(direction);
			System.out.println("************************************"); //$NON-NLS-1$
			System.out.println("createExistsToExistingRooms --- Length of Exits: " + exits.length); //$NON-NLS-1$
			for (int out : exits)
				System.out.println(out + ", "); //$NON-NLS-1$
			System.out.println("\n************************************"); //$NON-NLS-1$
			for (int out : startType)
				System.out.println(out + ", "); //$NON-NLS-1$
			System.out.println("\n************************************"); //$NON-NLS-1$
			for (int out : endType)
				System.out.println(out + ", "); //$NON-NLS-1$
			System.out.println("\n************************************"); //$NON-NLS-1$
		}
		if (database == null) database = Database.getInstance();
		int rDir = reverseDir(direction);
		Room tmpRoom = null;
		Exit tmpExit = null;
		if (sourceRoom != null) tmpExit = hasExitInThatDir(direction, sourceRoom);
		//System.out.println("tmpExit: " + tmpExit); //$NON-NLS-1$
		if (tmpExit != null) {
			tmpExit.setTargetRoom(targetRoom, reverseDir(direction));
			database.updateExit(null, tmpExit);
		} else {
			int st = 0;
			int et = 0;
			for (int i = 0; i < exits.length; i++) {
				if (exits[i] == rDir) {
					st = startType[i];
					et = endType[i];
					break;
				}
			}
			queueExit(sourceRoom, targetRoom, direction, currentAreaId, st, et);
		}

		// Creating the existing exits without connecting them!
		for (int i = 0; i < exits.length; i++) {
			if (exits[i] != rDir) {
				queueExit(targetRoom, null, exits[i], currentAreaId, startType[i], endType[i]);
			}
		}
		commitExits();
		// Connect to Existing rooms
		// Check for rooms in each direction...
		// if room there check for exit and delete it, then connect the new exit
		// to it.
		// If no room there, ignore it...
		if (debug) System.out.println("Connect to Existing Rooms..."); //$NON-NLS-1$
		for (int i = 1; i <= 8; i++) {
			if (i == rDir) continue;
			tmpRoom = isRoomInThatDir(i, currentLevel, xPos, yPos, spaceBetweenRooms, currentAreaId);
			if (tmpRoom != null) {
				Exit tExit = hasExitInThatDir(reverseDir(i), tmpRoom);
				Exit yExit = hasExitInThatDir(i, targetRoom);
				int et = 0;
				int st = 0;
				if (tExit != null) {
					et = tExit.getSourceType();
					st = et;
					database.deleteExit(tExit);
				}

				if (yExit != null) {
					yExit.setTargetRoom(tmpRoom, reverseDir(i));

					database.updateExit(null, yExit);
				} else {
					queueExit(targetRoom, tmpRoom, i, currentAreaId, st, et);
				}
			}
		}

		if (debug) System.out.println("Trying to connect UP"); //$NON-NLS-1$
		tmpRoom = isRoomInThatDir(Exit.UP, currentLevel, xPos, yPos, spaceBetweenRooms, currentAreaId);
		if (tmpRoom != null) {
			Exit exit = hasExitInThatDir(Exit.DOWN, tmpRoom);
			if (exit != null) {
				exit.setTargetRoom(targetRoom, Exit.UP);
				database.updateExit(null, exit);
			} else {
				queueExit(targetRoom, tmpRoom, Exit.UP, currentAreaId, 0, 0);
			}

		}
		if (debug) System.out.println("Trying to connect DOWN"); //$NON-NLS-1$
		tmpRoom = isRoomInThatDir(Exit.DOWN, currentLevel, xPos, yPos, spaceBetweenRooms, currentAreaId);
		if (tmpRoom != null) {
			Exit exit = hasExitInThatDir(Exit.UP, tmpRoom);
			if (exit != null) {
				exit.setTargetRoom(targetRoom, Exit.DOWN);
				database.updateExit(null, exit);
			} else {
				queueExit(targetRoom, tmpRoom, Exit.DOWN, currentAreaId, 0, 0);
			}
		}
		commitExits();
	}

	public static Exit createExit(Room sourceRoom, Room targetRoom, int direction, int currentAreaId, int sourceType, int targetType) {
		// boolean debug = true;
		if (database == null) database = Database.getInstance();
		Exit toReturn = new Exit();
		if (sourceRoom != null) toReturn.setSourceRoom(sourceRoom, direction);
		if (targetRoom != null) toReturn.setTargetRoom(targetRoom, reverseDir(direction));
		toReturn.setSourceType(sourceType);
		toReturn.setTargetType(targetType);
		database.addExit(toReturn);
		return toReturn;
	}

	public static void createExits(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int[] startType,
			int[] endType) {
		if (database == null) database = Database.getInstance();
	}

	public static void createExitsAndConnectToExistingRooms(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId,
			int xPos, int yPos, int spaceBetweenRooms, int[] startType, int[] endType) {}

	public static void createExitsButDoNotConnect(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int xPos,
			int yPos, int spaceBetweenRooms, int[] startType, int[] endType) {}

	public static void createOnlyWalkedExits(Room sourceRoom, Room targetRoom, int direction, int[] exits, int currentLevel, int currentAreaId, int xPos,
			int yPos, int spaceBetweenRooms, int[] startType, int[] endType) {
		if (database == null) database = Database.getInstance();
		createExit(sourceRoom, targetRoom, direction, currentAreaId, startType[0], endType[0]);
	}

	protected static Exit hasExitInThatDir(int direction, Room theRoom) {
		if (theRoom == null) return null;
		if (database == null) database = Database.getInstance();
		ArrayList<Exit> tmpExits = database.getExitsForRoom(theRoom);
		for (Exit exit : tmpExits) {
			if (exit.getSourceRoomId() == theRoom.getId() && exit.getType() / Exit.BASE == direction) { return exit; }
			if (exit.getTargetRoomId() == theRoom.getId() && exit.getType() % Exit.BASE == direction) { return exit; }
		}
		return null;
	}

	protected static Room isRoomInThatDir(int direction, int currentLevel, int xPos, int yPos, int spaceBetweenRooms, int currentAreaId) {
		if (database == null) database = Database.getInstance();

		Room tmpRoom = null;
		int theLevelToCheck = currentLevel;
		Point tmpPoint = calculateDirDiff(direction, xPos, yPos, spaceBetweenRooms);
		if (direction == Exit.UP) theLevelToCheck++;
		if (direction == Exit.DOWN) theLevelToCheck--;
		if (direction == Exit.ENTERSTALL) theLevelToCheck -= 1000000;
		if (direction == Exit.ENTERTENT) theLevelToCheck -= 1000000;
		if (direction == Exit.LEAVE) theLevelToCheck += 1000000;

		tmpRoom = database.getRoomAtPoint(currentAreaId, tmpPoint, theLevelToCheck);
		return tmpRoom;
	}

	public static Exit queueExit(Room sourceRoom, Room targetRoom, int direction, int currentAreaId, int sourceType, int targetType) {
		Exit toReturn = new Exit();
		if (sourceRoom != null) toReturn.setSourceRoom(sourceRoom, direction);
		if (targetRoom != null) toReturn.setTargetRoom(targetRoom, reverseDir(direction));

		toReturn.setSourceType(sourceType);
		toReturn.setTargetType(targetType);
		queuedExits.add(toReturn);
		return toReturn;
	}

	protected static int reverseDir(int dir) {
		if (database == null) database = Database.getInstance();
		int toReturn = Exit.OTHER;
		switch (dir) {
			case Exit.NORTH:
				toReturn = Exit.SOUTH;
				break;
			case Exit.NORTHEAST:
				toReturn = Exit.SOUTHWEST;
				break;
			case Exit.EAST:
				toReturn = Exit.WEST;
				break;
			case Exit.SOUTHEAST:
				toReturn = Exit.NORTHWEST;
				break;
			case Exit.SOUTH:
				toReturn = Exit.NORTH;
				break;
			case Exit.SOUTHWEST:
				toReturn = Exit.NORTHEAST;
				break;
			case Exit.WEST:
				toReturn = Exit.EAST;
				break;
			case Exit.NORTHWEST:
				toReturn = Exit.SOUTHEAST;
				break;
			case Exit.UP:
				toReturn = Exit.DOWN;
				break;
			case Exit.DOWN:
				toReturn = Exit.UP;
				break;
			case Exit.ENTERSTALL:
				toReturn = Exit.LEAVE;
				break;
			case Exit.ENTERTENT:
				toReturn = Exit.LEAVE;
				break;
			case Exit.LEAVE:
				toReturn = Exit.ENTERSTALL;
				break;
			default:
				toReturn = Exit.OTHER;
				break;
		}
		return toReturn;
	}

	public static Point translateCoordinates(int direction, Room aRoom) {
		if (database == null) database = Database.getInstance();
		int x = 0, y = 0;
		switch (direction) {
			case Exit.NORTH:
				x = ((int) aRoom.getPosition().getX() + (aRoom.getWidth() / 2));
				y = (int) aRoom.getPosition().getY();
				break;
			case Exit.NORTHEAST:
				x = ((int) aRoom.getPosition().getX() + aRoom.getWidth());
				y = (int) aRoom.getPosition().getY();
				break;
			case Exit.EAST:
				x = ((int) aRoom.getPosition().getX() + aRoom.getWidth());
				y = ((int) aRoom.getPosition().getY() + (aRoom.getHeight() / 2));
				break;
			case Exit.SOUTHEAST:
				x = ((int) aRoom.getPosition().getX() + aRoom.getWidth());
				y = ((int) aRoom.getPosition().getY() + aRoom.getHeight());
				break;
			case Exit.SOUTH:
				x = ((int) aRoom.getPosition().getX() + (aRoom.getWidth() / 2));
				y = ((int) aRoom.getPosition().getY() + aRoom.getHeight());
				break;
			case Exit.SOUTHWEST:
				x = (int) aRoom.getPosition().getX();
				y = ((int) aRoom.getPosition().getY() + aRoom.getHeight());
				break;
			case Exit.WEST:
				x = (int) aRoom.getPosition().getX();
				y = ((int) aRoom.getPosition().getY() + (aRoom.getHeight() / 2));
				break;
			case Exit.NORTHWEST:
				x = (int) aRoom.getPosition().getX();
				y = (int) aRoom.getPosition().getY();
				break;
			case Exit.DOWN:
				x = ((int) aRoom.getPosition().getX() + aRoom.getWidth() + 1);
				y = ((int) aRoom.getPosition().getY() + aRoom.getHeight() - 4);
				break;
			case Exit.UP:
				x = ((int) aRoom.getPosition().getX() + aRoom.getWidth() + 1);
				y = (int) aRoom.getPosition().getY() + 4;
				break;
			case Exit.ENTERTENT:
			case Exit.ENTERSTALL:
				x = (int) aRoom.getPosition().getX();
				y = (int) aRoom.getPosition().getY();
				break;
			case Exit.OTHER:
			default:
				x = (int) aRoom.getPosition().getX() - 3;
				y = (int) aRoom.getPosition().getY() + 3;
		}
		return new Point(x, y);
	}
}
