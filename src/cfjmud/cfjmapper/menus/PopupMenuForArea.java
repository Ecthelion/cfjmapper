/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import cfjmud.cfjmapper.Mapper;

/**
 * @author Ecthelion
 * 
 */
public class PopupMenuForArea extends JPopupMenu {

	/**
     * 
     */
	private static final long	serialVersionUID	= 3539241889605167422L;
	private final JMenuItem		deleteAreaMenuItem;
	private final JMenuItem		propertiesAreaMenuItem;

	public PopupMenuForArea(final Mapper mapper) {

		deleteAreaMenuItem = new JMenuItem("Delete Area"); //$NON-NLS-1$

		deleteAreaMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.deleteAreaMenuItemActionPerformed(false);
			}
		});

		propertiesAreaMenuItem = new JMenuItem("Properties"); //$NON-NLS-1$
		propertiesAreaMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.propertiesForAreaMenuItemActionPerformed(null);
			}
		});
		add(deleteAreaMenuItem);
		add(propertiesAreaMenuItem);
	}
}
