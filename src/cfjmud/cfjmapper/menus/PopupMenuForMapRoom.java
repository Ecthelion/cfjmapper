/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.objects.Exit;

/**
 * @author Ecthelion
 * 
 */
public class PopupMenuForMapRoom extends JPopupMenu {

	/**
     * 
     */
	private static final long	serialVersionUID	= 956898221784467928L;
	private final JMenuItem		addColourTriggerFromRoomNameMenuItem;
	private final JMenuItem		deleteRoomPopupMenuItem;
	private final JMenuItem		mergeRoomsPopupMenuItem;
	private final JMenuItem		moveRoomPopupMenuItem;
	private final JMenuItem		moveRoomToAreaPopupMenuItem;
	private final JMenuItem		properyRoomPopupMenuItem;
	private final JMenuItem		readNotificationMenuItem;
	private final JMenuItem		removeNotification;
	private final JMenuItem		setPositionHerePopupMenuItem;
	private final JMenu			changeExitMenu, changeBothSidesMenu, changeThisSideMenu, changeOtherSideMenu;
	private final JMenuItem		changeAllExitsToNormal, changeAllExitsToClimb, changeAllExitsToSwim, changeThisSideExitsToNormal, changeThisSideExitsToClimh,
			changeThisSideExitsToSwim, changeOtherSideExitsToNormal, changeOtherSideExitsToClimh, changeOtherSideExitsToSwim;
	private final JMenu			addRemoveToZoneMenu;
	private final JMenuItem		addToZoneMenuItem;
	private final JMenuItem		removeFromZoneMenuItem;

	public PopupMenuForMapRoom(final Mapper mapper) {

		changeExitMenu = new JMenu("Change all Exits in room to..."); //$NON-NLS-1$
		changeAllExitsToNormal = new JMenuItem("Normal"); //$NON-NLS-1$
		changeAllExitsToNormal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getMapView().changeAllExitsInRoomTo(Exit.NORMAL_TYPE);
			}
		});

		changeAllExitsToClimb = new JMenuItem("Climb"); //$NON-NLS-1$
		changeAllExitsToClimb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getMapView().changeAllExitsInRoomTo(Exit.CLIMB_TYPE);
			}
		});
		changeAllExitsToSwim = new JMenuItem("Swim"); //$NON-NLS-1$
		changeAllExitsToSwim.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getMapView().changeAllExitsInRoomTo(Exit.SWIM_TYPE);
			}
		});

		changeBothSidesMenu = new JMenu("Both..."); //$NON-NLS-1$
		changeThisSideMenu = new JMenu("This..."); //$NON-NLS-1$
		changeThisSideExitsToNormal = new JMenuItem("Normal"); //$NON-NLS-1$
		changeThisSideExitsToNormal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getMapView().changeAllThisSideExitsTo(Exit.NORMAL_TYPE);

			}
		});
		changeThisSideExitsToClimh = new JMenuItem("Climb"); //$NON-NLS-1$
		changeThisSideExitsToClimh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getMapView().changeAllThisSideExitsTo(Exit.CLIMB_TYPE);
			}
		});
		changeThisSideExitsToSwim = new JMenuItem("Swim"); //$NON-NLS-1$
		changeThisSideExitsToSwim.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getMapView().changeAllThisSideExitsTo(Exit.SWIM_TYPE);
			}
		});
		changeOtherSideMenu = new JMenu("Other..."); //$NON-NLS-1$
		changeOtherSideExitsToNormal = new JMenuItem("Normal"); //$NON-NLS-1$
		changeOtherSideExitsToNormal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getMapView().changeAllOtherSideExitsTo(Exit.NORMAL_TYPE);
			}
		});
		changeOtherSideExitsToClimh = new JMenuItem("Climb"); //$NON-NLS-1$
		changeOtherSideExitsToClimh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getMapView().changeAllOtherSideExitsTo(Exit.CLIMB_TYPE);
			}
		});
		changeOtherSideExitsToSwim = new JMenuItem("Swim"); //$NON-NLS-1$
		changeOtherSideExitsToSwim.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getMapView().changeAllOtherSideExitsTo(Exit.SWIM_TYPE);
			}
		});

		changeExitMenu.add(changeBothSidesMenu);
		changeExitMenu.add(changeThisSideMenu);
		changeExitMenu.add(changeOtherSideMenu);
		changeThisSideMenu.add(changeThisSideExitsToNormal);
		changeThisSideMenu.add(changeThisSideExitsToClimh);
		changeThisSideMenu.add(changeThisSideExitsToSwim);
		changeOtherSideMenu.add(changeOtherSideExitsToNormal);
		changeOtherSideMenu.add(changeOtherSideExitsToClimh);
		changeOtherSideMenu.add(changeOtherSideExitsToSwim);
		changeBothSidesMenu.add(changeAllExitsToNormal);
		changeBothSidesMenu.add(changeAllExitsToClimb);
		changeBothSidesMenu.add(changeAllExitsToSwim);
		deleteRoomPopupMenuItem = new JMenuItem("Delete Room"); //$NON-NLS-1$
		deleteRoomPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.deleteRoomPopupMenuItemActionPerfformed();
			}
		});

		setPositionHerePopupMenuItem = new JMenuItem("Set Current Room"); //$NON-NLS-1$
		setPositionHerePopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setPositionHereMenuItemActionPerfformed();
			}
		});

		properyRoomPopupMenuItem = new JMenuItem("Properties"); //$NON-NLS-1$
		properyRoomPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.properyRoomPopupMenuItemActionPerfformed();
			}
		});

		moveRoomToAreaPopupMenuItem = new JMenuItem("Move to Area"); //$NON-NLS-1$
		moveRoomToAreaPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.moveRoomToAreaPopupMenuItemActionPerformed();
			}
		});

		moveRoomPopupMenuItem = new JMenuItem("Move Room"); //$NON-NLS-1$
		moveRoomPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.moveRoomPopupMenuItemActionPerformed();
			}
		});

		mergeRoomsPopupMenuItem = new JMenuItem("Merge Rooms"); //$NON-NLS-1$
		mergeRoomsPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.mergeRoomsPopupMenuItemActionPerformed();
			}
		});

		addColourTriggerFromRoomNameMenuItem = new JMenuItem("Add Colour Trigger From Room Name"); //$NON-NLS-1$
		addColourTriggerFromRoomNameMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.addColourTriggerFromRoomNameMenuItemActionPerformed();
			}
		});

		readNotificationMenuItem = new JMenuItem("Read Notification"); //$NON-NLS-1$
		readNotificationMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.readNotificationActionPerformed();
			}
		});

		removeNotification = new JMenuItem("Remove Notification"); //$NON-NLS-1$
		removeNotification.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.removeNotificationActionPerformed();
			}
		});

		addRemoveToZoneMenu = new JMenu("Zones"); //$NON-NLS-1$
		addToZoneMenuItem = new JMenuItem("Add to Zone..."); //$NON-NLS-1$
		removeFromZoneMenuItem = new JMenuItem("Remove from Zone."); //$NON-NLS-1$
		removeFromZoneMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.removeFromZoneMenuItemActionPerformed();
			}
		});
		addToZoneMenuItem.setEnabled(false);
		addToZoneMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.addToZoneMenuItemActionPerformed();
			}
		});
		addRemoveToZoneMenu.setEnabled(false);
		removeFromZoneMenuItem.setEnabled(false);
		addRemoveToZoneMenu.add(addToZoneMenuItem);
		addRemoveToZoneMenu.add(removeFromZoneMenuItem);

		add(setPositionHerePopupMenuItem);
		add(deleteRoomPopupMenuItem);
		add(moveRoomPopupMenuItem);
		add(moveRoomToAreaPopupMenuItem);
		add(mergeRoomsPopupMenuItem);
		add(addColourTriggerFromRoomNameMenuItem);
		add(changeExitMenu);
		add(readNotificationMenuItem);
		add(removeNotification);
		add(addRemoveToZoneMenu);
		add(properyRoomPopupMenuItem);
	}

	public void setMergeRoomsPopupMenuItemEnabled(boolean enabled) {
		mergeRoomsPopupMenuItem.setEnabled(enabled);
	}

	public void setReadNotificationEnabled(boolean b) {
		readNotificationMenuItem.setEnabled(b);
		removeNotification.setEnabled(b);
	}

	public void setaddRemoveToZoneMenuEnabled(boolean enabled) {
		addRemoveToZoneMenu.setEnabled(enabled);
	}

	public void setAddToZoneMenuItemEnabled(boolean enabled) {
		addToZoneMenuItem.setEnabled(enabled);
	}

	public void setRemoveFromZoneMenuItem(boolean enabled) {
		removeFromZoneMenuItem.setEnabled(enabled);
	}
}
