/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import cfjmud.cfjmapper.Mapper;

/**
 * @author Ecthelion
 * 
 */
public class PopupMenuForAreaTree extends JPopupMenu {

	/**
     * 
     */
	private static final long	serialVersionUID	= -4736644327744137942L;
	private final JMenuItem		addAreaToCurrentAreaMenuItem;
	private final JMenuItem		addChildAreaPopupMenuItem;
	private final JMenuItem		addNewAreaPopupMenuItem;
	private final JMenuItem		changeNameAreaPopupMenu;
	private final JMenuItem		deleteAreaPopupMenuItem;
	private final JMenuItem		exportAreaToFileMenuItem;
	private final JMenuItem		propertiesForAreaInTreePopupMenuItem;
	private final JMenuItem		saveAreaAsThumbNailMenuItem;
	private final JMenuItem		setCurrentAreaPopupMenu;
	private final JMenuItem		saveAreaAsImagePopupMenuItem;

	public PopupMenuForAreaTree(final Mapper mapper) {
		saveAreaAsThumbNailMenuItem = new JMenuItem("Save Area As Thumbnail"); //$NON-NLS-1$
		saveAreaAsThumbNailMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.saveAreaSelectedInTreeAsThumbnail();
			}
		});

		addAreaToCurrentAreaMenuItem = new JMenuItem("Place in another Area"); //$NON-NLS-1$
		addAreaToCurrentAreaMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.moveArea();
			}
		});
		addChildAreaPopupMenuItem = new JMenuItem("Add Sub Area"); //$NON-NLS-1$
		addChildAreaPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.addArea(false);
			}
		});
		propertiesForAreaInTreePopupMenuItem = new JMenuItem("Properties"); //$NON-NLS-1$
		propertiesForAreaInTreePopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.propertiesForAreaMenuItemActionPerformed(ae);
			}
		});
		addNewAreaPopupMenuItem = new JMenuItem("Add New Area"); //$NON-NLS-1$
		addNewAreaPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.addArea(true);
			}
		});
		deleteAreaPopupMenuItem = new JMenuItem("Delete Area"); //$NON-NLS-1$
		deleteAreaPopupMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.deleteAreaMenuItemActionPerformed(true);
			}
		});
		exportAreaToFileMenuItem = new JMenuItem("Export area to file..."); //$NON-NLS-1$
		exportAreaToFileMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.exportAreaAsXMLMenuItemActionPerformed();
			}
		});
		changeNameAreaPopupMenu = new JMenuItem("Rename Area"); //$NON-NLS-1$
		changeNameAreaPopupMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.changeNameAreaPopupMenuActionPerformed();
			}
		});
		setCurrentAreaPopupMenu = new JMenuItem("Set Current Area"); //$NON-NLS-1$
		setCurrentAreaPopupMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setCurrentAreaPopupMenuActionPerformed();
			}
		});

		saveAreaAsImagePopupMenuItem = new JMenuItem("Save Area As Image"); //$NON-NLS-1$
		saveAreaAsImagePopupMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.saveAreaSelectedInTreeAsImage_NEW();
			}
		});

		add(setCurrentAreaPopupMenu);
		add(changeNameAreaPopupMenu);
		add(addChildAreaPopupMenuItem);
		add(addNewAreaPopupMenuItem);
		add(exportAreaToFileMenuItem);
		add(deleteAreaPopupMenuItem);
		add(saveAreaAsImagePopupMenuItem);
		add(saveAreaAsThumbNailMenuItem);
		add(addAreaToCurrentAreaMenuItem);
		add(propertiesForAreaInTreePopupMenuItem);

	}
}
