/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.RoomColour;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.IconManager;
import cfjmud.cfjmapper.dialogs.MSShapeEditorDialog;
import cfjmud.cfjmapper.dialogs.RoomFinderDialog;
import cfjmud.cfjmapper.dialogs.ZoneDialog;
import cfjmud.cfjmapper.panels.ZonesWindow;
import cfjmud.cfjmapper.xml.Backup;
import cfjmud.cfjmapper.xml.Importer;
import cfjmud.cfjmapper.xml.OldImporter;

/**
 * @author Ecthelion
 * 
 */
public class MapperMenuBar extends JMenuBar {
	/**
     * 
     */
	private static final long		serialVersionUID	= 3477284384628435708L;
	public static final String		aboutMessage		= "<html>CFJMapper - a mapper for mapping stuff, at the moment only on Discworld<br><br>" //$NON-NLS-1$
															+ "Copyright (C) 2013  Carl F Jansson<br><br>" //$NON-NLS-1$
															+ "This program is free software: you can redistribute it and/or modify<br>" //$NON-NLS-1$
															+ "it under the terms of the GNU General Public License as published by<br>" //$NON-NLS-1$
															+ "the Free Software Foundation, either version 3 of the License, or<br>" //$NON-NLS-1$
															+ "(at your option) any later version.<br>" + //$NON-NLS-1$

															"This program is distributed in the hope that it will be useful,<br>" //$NON-NLS-1$
															+ "but WITHOUT ANY WARRANTY; without even the implied warranty of<br>" //$NON-NLS-1$
															+ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>" //$NON-NLS-1$
															+ "GNU General Public License for more details.<br><br>" + //$NON-NLS-1$

															"You should have received a copy of the GNU General Public License<br>" //$NON-NLS-1$
															+ "along with this program.  If not, see <http://www.gnu.org/licenses>.</html>";	//$NON-NLS-1$
	private JRadioButtonMenuItem	connectStubsSubMenuItem;
	private JRadioButtonMenuItem	createToExistingIncStubSubMenuItem;
	private JRadioButtonMenuItem	makeAllExitsAndConnectSubMenuItem;
	private final Mapper			mapper;

	private JRadioButtonMenuItem	noExitsExceptWalkedSubMenuItem;

	public MapperMenuBar(Mapper mapper) {
		this.mapper = mapper;
		add(createArchiveMenu());
		add(createEditMenu());
		add(createHelpMenu());
	}

	private JMenu createArchiveMenu() {
		JMenu archiveMenu, backupMenu, exportImportMenuItem;

		JMenuItem exitMenuItem;
		JMenuItem exportToXmlMenuItem;
		JMenuItem backupToxmlMenuItem;
		JMenuItem restoreMenuItem;
		JMenuItem sendConnectToMush;
		final JRadioButtonMenuItem keepAboveMenuItem;
		JMenuItem oldImportMenuItem;
		JMenuItem importMenuItem;
		JMenuItem profilesMenuItem;

		profilesMenuItem = new JMenuItem("Profile Manager"); //$NON-NLS-1$
		profilesMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getProfileManager().showDialog();
			}
		});

		backupMenu = new JMenu("Backup..."); //$NON-NLS-1$
		exportImportMenuItem = new JMenu("Import/Export..."); //$NON-NLS-1$

		backupToxmlMenuItem = new JMenuItem("Backup"); //$NON-NLS-1$
		backupToxmlMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Backup(mapper).save();
			}
		});

		restoreMenuItem = new JMenuItem("Old Restore"); //$NON-NLS-1$
		restoreMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new OldImporter(mapper, true);
				mapper.resetTree();
			}
		});

		sendConnectToMush = new JMenuItem("Send activation signal to Mushclient."); //$NON-NLS-1$
		sendConnectToMush.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.sendStartUpToMushClient();
			}
		});
		exportToXmlMenuItem = new JMenuItem("Export"); //$NON-NLS-1$
		keepAboveMenuItem = new JRadioButtonMenuItem("Keep Above"); //$NON-NLS-1$
		keepAboveMenuItem.setSelected(false);
		keepAboveMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (!mapper.isAlwaysOnTop()) {
					keepAboveMenuItem.setSelected(true);
					mapper.setAlwaysOnTop(true);
				} else {
					keepAboveMenuItem.setSelected(false);
					mapper.setAlwaysOnTop(false);
				}

			}
		});
		oldImportMenuItem = new JMenuItem("Old Import!!"); //$NON-NLS-1$
		oldImportMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				new OldImporter(mapper, false);
				mapper.resetTree();
			}
		});
		importMenuItem = new JMenuItem("Import"); //$NON-NLS-1$
		importMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				new Importer(mapper, false);
				mapper.resetTree();
			}
		});
		exitMenuItem = new JMenuItem("Exit"); //$NON-NLS-1$
		exitMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.getCommunicator().sendShutDown();
				MapperProperties.getInstance().save(mapper);
				System.out.println("Exiting Program."); //$NON-NLS-1$
				System.exit(0);
			}
		});

		archiveMenu = new JMenu("Archive"); //$NON-NLS-1$
		archiveMenu.add(sendConnectToMush);
		exportImportMenuItem.add(exportToXmlMenuItem);
		exportImportMenuItem.add(oldImportMenuItem);
		exportImportMenuItem.add(importMenuItem);
		backupMenu.add(backupToxmlMenuItem);
		backupMenu.add(restoreMenuItem);
		archiveMenu.add(backupMenu);
		archiveMenu.add(exportImportMenuItem);
		archiveMenu.add(profilesMenuItem);
		archiveMenu.add(keepAboveMenuItem);
		archiveMenu.add(exitMenuItem);

		return archiveMenu;

	}

	private JMenu createEditMenu() {
		JMenu editMenu;
		// JMenuItem copyMenuItem;
		// JMenuItem cutMenuItem;
		// JMenuItem pasteMenuItem;
		final JCheckBoxMenuItem replaceExitsMenuItem;
		JMenuItem cleanTheDatabaseMenuItem;
		JMenuItem findRoomMenuButton;
		JCheckBoxMenuItem advancedFollowingMenuItem;
		final JCheckBoxMenuItem centerOnCurrentRoom;
		JMenuItem propertiesMenuItem;
		JMenuItem editColourTriggersMenuItem;
		final JMenuItem showIconManagerMenuItem;
		JMenu mappingLevelSubMenu;
		JCheckBoxMenuItem keyboardMappingMenuItem;
		final JMenuItem showSpecialExitsLabelMenuItem;
		final JCheckBoxMenuItem autojourneyCheckBoxMenuItem;
		JMenuItem editShapeMenuItem;
		JMenuItem editZonesMenuItem;
		JMenuItem showZoneListMenuItem;

		showZoneListMenuItem = new JMenuItem("Show Zone List..."); //$NON-NLS-1$
		showZoneListMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ZonesWindow.toggleVisible(mapper, mapper.getCurrentAreaId());
			}
		});

		editZonesMenuItem = new JMenuItem("Edit Zones"); //$NON-NLS-1$
		editZonesMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				@SuppressWarnings("unused")
				ZoneDialog zd = new ZoneDialog(mapper);
			}
		});

		editShapeMenuItem = new JMenuItem("Edit Graphics For Rooms"); //$NON-NLS-1$
		editShapeMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new MSShapeEditorDialog(mapper);
			}
		});
		autojourneyCheckBoxMenuItem = new JCheckBoxMenuItem("Autojourney"); //$NON-NLS-1$
		autojourneyCheckBoxMenuItem.setSelected(mapper.isAutojourney());
		autojourneyCheckBoxMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.setAutojourney(autojourneyCheckBoxMenuItem.isSelected());
			}
		});

		replaceExitsMenuItem = new JCheckBoxMenuItem("Substitute special exits"); //$NON-NLS-1$  
		String subSpecialExits = MapperProperties.getInstance().getProperty(MapperProperties.SUBTISTUTESPECIALEXITS);
		mapper.setReplaceExits((subSpecialExits == null ? false : Boolean.parseBoolean(subSpecialExits)));
		replaceExitsMenuItem.setSelected(mapper.isReplaceExits());
		replaceExitsMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.setReplaceExits(replaceExitsMenuItem.isSelected());
			}
		});
		showSpecialExitsLabelMenuItem = new JCheckBoxMenuItem("Show Special Exits"); //$NON-NLS-1$
		String specialExits = MapperProperties.getInstance().getProperty(MapperProperties.SHOWSPECIALEXITS);
		showSpecialExitsLabelMenuItem.setSelected((specialExits == null ? true : Boolean.parseBoolean(specialExits)));
		showSpecialExitsLabelMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.setShowSpecialExits(showSpecialExitsLabelMenuItem.isSelected());
			}
		});

		findRoomMenuButton = new JMenuItem("Find Room..."); //$NON-NLS-1$
		findRoomMenuButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new RoomFinderDialog(mapper);
			}
		});
		keyboardMappingMenuItem = new JCheckBoxMenuItem("Keyboard Creation"); //$NON-NLS-1$
		keyboardMappingMenuItem.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				mapper.keyboardMappingMenuItemActionPerformed(ie);
			}
		});

		cleanTheDatabaseMenuItem = new JMenuItem("Clean out the Database"); //$NON-NLS-1$
		cleanTheDatabaseMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Database.getInstance().cleanOutExits();
			}
		});

		advancedFollowingMenuItem = new JCheckBoxMenuItem("Shadow Following"); //$NON-NLS-1$
		advancedFollowingMenuItem.setSelected(mapper.isShadowing());
		advancedFollowingMenuItem.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				mapper.advancedFollowingMenuItemActionPerformed(ie);
			}

		});
		propertiesMenuItem = new JMenuItem("Properties"); //$NON-NLS-1$

		propertiesMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				MapperProperties.showPreferenceDialog(mapper);
				mapper.updateMainMap();
			}
		});
		editColourTriggersMenuItem = new JMenuItem("Edit Colour Triggers"); //$NON-NLS-1$
		editColourTriggersMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				RoomColour.showColourChooser(mapper);
				mapper.updateMainMap();
			}
		});

		showIconManagerMenuItem = new JMenuItem("Manage Icons"); //$NON-NLS-1$
		showIconManagerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				new IconManager(mapper, false);
			}
		});

		ButtonGroup group = new ButtonGroup();
		mappingLevelSubMenu = new JMenu("Exits Policy"); //$NON-NLS-1$
		noExitsExceptWalkedSubMenuItem = new JRadioButtonMenuItem("Create only walked..."); //$NON-NLS-1$
		int exitsPolicy = Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.EXITSPOLICY));
		if (exitsPolicy == 0) noExitsExceptWalkedSubMenuItem.setSelected(true);
		group.add(noExitsExceptWalkedSubMenuItem);
		noExitsExceptWalkedSubMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setConnectionType(0, false);
			}
		});
		connectStubsSubMenuItem = new JRadioButtonMenuItem("Connect stubs..."); //$NON-NLS-1$
		group.add(connectStubsSubMenuItem);
		connectStubsSubMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setConnectionType(1, false);
			}
		});
		if (exitsPolicy == 1) connectStubsSubMenuItem.setSelected(true);
		createToExistingIncStubSubMenuItem = new JRadioButtonMenuItem("Connect stubs and make exits to existing rooms..."); //$NON-NLS-1$
		group.add(createToExistingIncStubSubMenuItem);
		createToExistingIncStubSubMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setConnectionType(2, false);
			}
		});
		if (exitsPolicy == 2) createToExistingIncStubSubMenuItem.setSelected(true);
		makeAllExitsAndConnectSubMenuItem = new JRadioButtonMenuItem("Make ALL exits and connect them..."); //$NON-NLS-1$
		group.add(makeAllExitsAndConnectSubMenuItem);
		makeAllExitsAndConnectSubMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				mapper.setConnectionType(3, false);
			}
		});
		if (exitsPolicy == 3) makeAllExitsAndConnectSubMenuItem.setSelected(true);
		mapper.setConnectionType(exitsPolicy, false);

		centerOnCurrentRoom = new JCheckBoxMenuItem("Automatically centre on current room", true); //$NON-NLS-1$
		String enabledString = MapperProperties.getInstance().getProperty(MapperProperties.CENTREONCURRENTROOM);

		centerOnCurrentRoom.setSelected((enabledString == null ? true : Boolean.parseBoolean(enabledString)));
		centerOnCurrentRoom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.setCenterOnCurrentRoom(centerOnCurrentRoom.isSelected());
			}
		});
		mappingLevelSubMenu.add(noExitsExceptWalkedSubMenuItem);
		mappingLevelSubMenu.add(connectStubsSubMenuItem);
		mappingLevelSubMenu.add(createToExistingIncStubSubMenuItem);
		mappingLevelSubMenu.add(makeAllExitsAndConnectSubMenuItem);
		editMenu = new JMenu("Edit"); //$NON-NLS-1$
		// editMenu.add(copyMenuItem);
		// editMenu.add(pasteMenuItem);
		// editMenu.add(keyboardMappingMenuItem);
		editMenu.add(findRoomMenuButton);
		editMenu.add(replaceExitsMenuItem);
		editMenu.add(showSpecialExitsLabelMenuItem);
		editMenu.add(centerOnCurrentRoom);
		editMenu.add(advancedFollowingMenuItem);
		editMenu.add(autojourneyCheckBoxMenuItem);
		editMenu.add(editColourTriggersMenuItem);
		editMenu.add(showIconManagerMenuItem);
		editMenu.add(mappingLevelSubMenu);
		editMenu.add(editShapeMenuItem);
		editMenu.add(editZonesMenuItem);
		editMenu.add(showZoneListMenuItem);

		editMenu.add(propertiesMenuItem);

		return editMenu;
	}

	private JMenu createHelpMenu() {
		final JMenu helpMenu;
		final JMenuItem aboutMenuItem;
		final JMenuItem numberOfRoomsMenuItem;
		final JMenuItem helpMenuItem;
		helpMenu = new JMenu("Help"); //$NON-NLS-1$
		aboutMenuItem = new JMenuItem("About"); //$NON-NLS-1$
		helpMenuItem = new JMenuItem("Help"); //$NON-NLS-1$
		helpMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(mapper, "Want help? Call 911, 999, 112 or what ever the fucking number is at your fucking place.", //$NON-NLS-1$
												"Help?", JOptionPane.PLAIN_MESSAGE); //$NON-NLS-1$
			}
		});
		numberOfRoomsMenuItem = new JMenuItem("Total Number of Rooms..."); //$NON-NLS-1$
		numberOfRoomsMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		aboutMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// JOptionPane.showMessageDialog(arg0, arg1, arg2, arg3)
				JOptionPane.showMessageDialog(mapper, aboutMessage, "GNU", JOptionPane.OK_OPTION); //$NON-NLS-1$

			}
		});
		helpMenu.add(helpMenuItem);
		helpMenu.add(numberOfRoomsMenuItem);
		helpMenu.add(aboutMenuItem);
		return helpMenu;
	}

	public void setSelectedIndexOfConnection(int index) {
		noExitsExceptWalkedSubMenuItem.setSelected(false);
		connectStubsSubMenuItem.setSelected(false);
		createToExistingIncStubSubMenuItem.setSelected(false);
		makeAllExitsAndConnectSubMenuItem.setSelected(false);
		switch (index) {
			case 0:
				noExitsExceptWalkedSubMenuItem.setSelected(true);
				break;
			case 1:
				connectStubsSubMenuItem.setSelected(true);
				break;
			case 2:
				createToExistingIncStubSubMenuItem.setSelected(true);
				break;
			case 3:
				makeAllExitsAndConnectSubMenuItem.setSelected(true);
				break;
			default:
				break;
		}
	}
}
