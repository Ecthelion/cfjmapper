/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.ToolBarButton;
import cfjmud.cfjmapper.panels.Compass;

/**
 * @author Ecthelion
 * 
 */
public class MainToolBar extends JToolBar implements MouseListener {
	private static Color		disabledColour		= Color.LIGHT_GRAY;
	private static Color		enabledColour		= Color.GREEN;
	private static Color		highlightColour		= Color.DARK_GRAY;
	/**
     * 
     */
	private static final long	serialVersionUID	= -3445869993777834536L;

	/**
	 * @return the disabledColour
	 */
	public static Color getDisabledColour() {
		return disabledColour;
	}

	/**
	 * @return the enabledColour
	 */
	public static Color getEnabledColour() {
		return enabledColour;
	}

	/**
	 * @return the highlightColour
	 */
	public static Color getHighlightColour() {
		return highlightColour;
	}

	/**
	 * @param disabledColour
	 *            the disabledColour to set
	 */
	public static void setDisabledColour(Color disabledColour) {
		MainToolBar.disabledColour = disabledColour;
	}

	/**
	 * @param enabledColour
	 *            the enabledColour to set
	 */
	public static void setEnabledColour(Color enabledColour) {
		MainToolBar.enabledColour = enabledColour;
	}

	/**
	 * @param highlightColour
	 *            the highlightColour to set
	 */
	public static void setHighlightColour(Color highlightColour) {
		MainToolBar.highlightColour = highlightColour;
	}

	private final ToolBarButton					addObjectButton, toMapOrNotToMapButton, addSubAreaButton, addExitButton, addRoomButton;

	private final Compass						compass;

	private final JComboBox<String>				connectionComboBox, zoomComboBox;

	private final JSpinner						levelSpinner;

	private final Mapper						mapper;

	private final PopupMenuForDirectionArrow	popupMenuForDirectionArrow;
	private final ToolBarButton					recenterViewOnCurrentRoom;

	public MainToolBar(final Mapper mapper) {
		this.mapper = mapper;
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 1));
		String[] connectionComboItems = { "Create only walked...", "Connect stubs...", "Connect to existing rooms...", "Connect and make ALL exits" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		connectionComboBox = new JComboBox<String>(connectionComboItems);
		String[] zoomLevels = new String[20];

		for (int i = 0; i < zoomLevels.length; i++) {
			zoomLevels[i] = ((i + 1) * 10) + "%"; //$NON-NLS-1$
		}

		zoomComboBox = new JComboBox<String>(zoomLevels);
		zoomComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				zoomComboBoxActionPerformed(arg0);
				mapper.getMainMapToRequestFocus();
			}

		});
		zoomComboBox.setSelectedIndex(9);
		levelSpinner = new JSpinner();
		compass = new Compass(mapper);
		compass.addMouseListener(this);
		popupMenuForDirectionArrow = new PopupMenuForDirectionArrow(compass);
		toMapOrNotToMapButton = new ToolBarButton(ToolBarButton.TYPE.MAP);
		addSubAreaButton = new ToolBarButton(ToolBarButton.TYPE.ADDAREA);
		addExitButton = new ToolBarButton(ToolBarButton.TYPE.ADDEXIT);
		addRoomButton = new ToolBarButton(ToolBarButton.TYPE.ADDROOM);
		addObjectButton = new ToolBarButton(ToolBarButton.TYPE.ADDPICTURE);
		recenterViewOnCurrentRoom = new ToolBarButton(ToolBarButton.TYPE.RECENTERROOM);
		addRoomButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				resetButtons();
				if (mapper.isAddNewRoom()) {
					addRoomButton.setPressed(false);
					addRoomButton.repaint();
					mapper.setAddNewRoom(false);
					mapper.setStatus(""); //$NON-NLS-1$
				} else {
					addRoomButton.setPressed(true);
					addRoomButton.repaint();
					mapper.setAddNewRoom(true);
					mapper.setStatus("Press on location in map where you want a new room."); //$NON-NLS-1$
				}
				mapper.setAddNewExit(false);
				mapper.setAddNewArea(false);
				mapper.getMainMapToRequestFocus();
			}
		});
		addExitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				resetButtons();
				if (mapper.isAddNewExit()) {
					addExitButton.setPressed(false);
					mapper.setAddNewExit(false);
					mapper.setStatus(""); //$NON-NLS-1$
				} else {
					addExitButton.setPressed(true);
					mapper.setAddNewExit(true);
					mapper.setStatus("Press on the first room to add Exit."); //$NON-NLS-1$
				}
				mapper.setAddNewRoom(false);
				mapper.setAddNewArea(false);
				repaint();
				mapper.getMainMapToRequestFocus();
			}
		});
		addSubAreaButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				resetButtons();
				if (mapper.isAddNewArea()) {
					addSubAreaButton.setPressed(false);
					mapper.setAddNewArea(false);
					mapper.setStatus(""); //$NON-NLS-1$
				} else {
					addSubAreaButton.setPressed(true);
					mapper.setAddNewArea(true);
					mapper.setStatus("Press on background to add sub area."); //$NON-NLS-1$
				}
				mapper.setAddNewExit(false);
				mapper.setAddNewRoom(false);
				repaint();
				mapper.getMainMapToRequestFocus();
			}
		});
		levelSpinner.setValue(new Integer(mapper.getCurrentLevel()));
		levelSpinner.getEditor().setEnabled(false);
		levelSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				Integer i = (Integer) levelSpinner.getValue();
				mapper.setCurrentLevel(i.intValue());
				mapper.getMapView().updateView();
				mapper.getMainMapToRequestFocus();
			}
		});
		toMapOrNotToMapButton.setForeground(getDisabledColour());
		toMapOrNotToMapButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetButtons();
				if (!toMapOrNotToMapButton.isPressed()) {
					toMapOrNotToMapButton.setPressed(true);
					mapper.getCommunicator().sendMap();
					mapper.setKeyboardMapping(true);
					mapper.setCurrentShadowRoom(mapper.getCurrentRoom());
					mapper.setStatus("Mapping!"); //$NON-NLS-1$
				} else {
					toMapOrNotToMapButton.setPressed(false);
					mapper.setKeyboardMapping(false);
					mapper.getCommunicator().sendMove();
					mapper.setCurrentShadowRoom(mapper.getCurrentRoom());
					mapper.setStatus("Following!"); //$NON-NLS-1$
				}
				mapper.getMainMapToRequestFocus();
			}
		});

		addObjectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.addObjectButtonActionPerformed();
				mapper.getMainMapToRequestFocus();
			}
		});

		connectionComboBox.setSelectedIndex(Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.EXITSPOLICY)));
		connectionComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.setConnectionType(connectionComboBox.getSelectedIndex(), true);
				mapper.getMainMapToRequestFocus();
			}
		});
		addRoomButton.addKeyListener(mapper);
		addExitButton.addKeyListener(mapper);

		recenterViewOnCurrentRoom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getMapView().centerOnCurrentRoom();
			}
		});

		add(toMapOrNotToMapButton);
		add(addRoomButton);
		add(addExitButton);
		add(addSubAreaButton);
		add(addObjectButton);
		add(levelSpinner);
		add(connectionComboBox);
		add(compass);
		add(zoomComboBox);
		add(recenterViewOnCurrentRoom);
	}

	public int getFacing() {
		return compass.getFacting();
	}

	public void resetButtons() {
		addRoomButton.setPressed(false);
		addExitButton.setPressed(false);
		addSubAreaButton.setPressed(false);
		addObjectButton.setPressed(false);
		repaint();
	}

	public void setColourOfAddExitButton(Color colour) {
		addExitButton.setPressed(true);
	}

	public void setColourOfAddObjectButton(Color colour) {
		addObjectButton.setPressed(true);
	}

	public void setColourOfAddRoomButton(Color colour) {
		addRoomButton.setPressed(true);
	}

	public void setColourOfAddSubAreaButton(Color colour) {
		addSubAreaButton.setPressed(true);
	}

	public void setFacing(int facing) {
		compass.setFacing(facing);
	}

	public void setLevel(int level) {
		if (levelSpinner == null) return;
		levelSpinner.setValue(new Integer(level));
	}

	public void setSelectedIndexOfComboBox(int index) {
		connectionComboBox.setSelectedIndex(index);
	}

	protected void zoomComboBoxActionPerformed(ActionEvent arg0) {
		double selectedIndex = zoomComboBox.getSelectedIndex();
		double zoom = (selectedIndex + 1) * 0.1d;
		mapper.setZoomLevel(zoom);
		mapper.getMainMapToRequestFocus();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		maybeShowPopup(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		maybeShowPopup(e);
	}

	private void maybeShowPopup(MouseEvent e) {
		if (e.isPopupTrigger()) {
			popupMenuForDirectionArrow.show(e.getComponent(), e.getPoint().x, e.getPoint().y);
		}
	}

}
