/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import cfjmud.cfjmapper.Mapper;

/**
 * @author Ecthelion
 * 
 */
public class PopupMenuForExit extends JPopupMenu {

	/**
     * 
     */
	private static final long	serialVersionUID	= -5706392292922837922L;
	private final JMenuItem		changeToClimbMenuItem;
	private final JMenuItem		changeToNormalMenuItem;
	private final JMenuItem		changeToSwimMenuItem;
	private final JMenuItem		deleteExitMenuItem;
	private final JMenu			exitTypeChangeMenu;
	private final JMenuItem		propertiesMenuItem;
	private final JMenuItem		setTargetRoomMenuItem;

	public PopupMenuForExit(final Mapper mapper) {
		deleteExitMenuItem = new JMenuItem("Delete Exit"); //$NON-NLS-1$
		deleteExitMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.deleteExitMenuItemActionPerformed();
			}
		});
		setTargetRoomMenuItem = new JMenuItem("Set Target Room"); //$NON-NLS-1$
		setTargetRoomMenuItem.setEnabled(false);
		setTargetRoomMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.setTargetRoomInExitMenuItemActionPerformed();
			}
		});
		propertiesMenuItem = new JMenuItem("Properties"); //$NON-NLS-1$
		propertiesMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.propertiesMenuItemForExitActionPerformed();
			}
		});
		changeToNormalMenuItem = new JMenuItem("to Normal."); //$NON-NLS-1$
		changeToNormalMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.changeToNormalMenuItemActionPerformed();
			}
		});
		changeToClimbMenuItem = new JMenuItem("to Climb."); //$NON-NLS-1$
		changeToClimbMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.changeToClimbMenuItemActionPerformed();

			}
		});
		changeToSwimMenuItem = new JMenuItem("to Swim."); //$NON-NLS-1$
		changeToSwimMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.changeToSwimMenuItemActionPerformed();

			}
		});
		exitTypeChangeMenu = new JMenu("Change type..."); //$NON-NLS-1$
		exitTypeChangeMenu.add(changeToNormalMenuItem);
		exitTypeChangeMenu.add(changeToClimbMenuItem);
		exitTypeChangeMenu.add(changeToSwimMenuItem);

		add(deleteExitMenuItem);
		add(setTargetRoomMenuItem);
		add(exitTypeChangeMenu);
		add(propertiesMenuItem);
	}

	public void setSetTargetMenuItemEnabled(boolean enabled) {
		setTargetRoomMenuItem.setEnabled(enabled);
	}
}
