/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import cfjmud.cfjmapper.panels.Compass;

/**
 * @author Ecthelion
 * 
 */
public class PopupMenuForDirectionArrow extends JPopupMenu implements ActionListener {

	/**
     * 
     */
	private static final long	serialVersionUID	= 87342207116764360L;
	private final JMenuItem[]	menuItems;
	private final String[]		menuItemsString		= { "North", "Northeast", "East", "Southeast", "South", "Southwest", "West", "Northwest" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$
	private final Compass		compass;

	public PopupMenuForDirectionArrow(Compass compass) {
		this.compass = compass;
		menuItems = new JMenuItem[menuItemsString.length];
		int i = 0;
		for (String str : menuItemsString) {
			menuItems[i] = new JMenuItem(str);
			menuItems[i].addActionListener(this);
			menuItems[i].setName(Integer.toString(i + 1));
			add(menuItems[i]);
			i++;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JMenuItem item = (JMenuItem) arg0.getSource();
		compass.setFacing(Integer.parseInt(item.getName()));
	}
}
