/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * @author Ecthelion
 * 
 */
public class ContractHandler {

	public static int	ACCEPTED		= 0;
	public static int	NOT_ACCEPTED	= 1;
	private static int	toReturn		= NOT_ACCEPTED;

	public static int showContractConfirmer() {
		final JDialog dialog = new JDialog();
		JButton yesButton = new JButton("I Accept the Terms."); //$NON-NLS-1$
		yesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toReturn = ACCEPTED;
				dialog.setVisible(false);
			}
		});
		JButton noButton = new JButton("Exit."); //$NON-NLS-1$
		noButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.setVisible(false);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(yesButton);
		buttonPanel.add(noButton);
		JLabel confirmLabel = new JLabel("GNU GENERAL PUBLIC LICENSE", JLabel.CENTER); //$NON-NLS-1$
		JTextArea textArea = new JTextArea(Contract.string());
		textArea.setEditable(false);
		JScrollPane jsp = new JScrollPane(textArea);
		Dimension dim = new Dimension(450, 500);
		jsp.setSize(dim);
		jsp.setPreferredSize(dim);
		Box box = new Box(BoxLayout.Y_AXIS);
		box.add(confirmLabel);
		box.add(jsp);
		box.add(buttonPanel);
		dialog.add(box);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
		return toReturn;
	}
}

class Contract {
	static String[]	text	= { "                   GNU GENERAL PUBLIC LICENSE", "                   Version 3, 29 June 2007", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>", "Everyone is permitted to copy and distribute verbatim copies", //$NON-NLS-1$ //$NON-NLS-2$
			"of this license document, but changing it is not allowed.", "", "                        Preamble", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"The GNU General Public License is a free, copyleft license for", "software and other kinds of works.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The licenses for most software and other practical works are designed", "to take away your freedom to share and change the works.  By contrast,", //$NON-NLS-1$ //$NON-NLS-2$
			"the GNU General Public License is intended to guarantee your freedom to", //$NON-NLS-1$
			"share and change all versions of a program--to make sure it remains free", //$NON-NLS-1$
			"software for all its users.  We, the Free Software Foundation, use the", //$NON-NLS-1$
			"GNU General Public License for most of our software; it applies also to", "any other work released this way by its authors.  You can apply it to", //$NON-NLS-1$ //$NON-NLS-2$
			"your programs, too.", "", "When we speak of free software, we are referring to freedom, not", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"price.  Our General Public Licenses are designed to make sure that you", "have the freedom to distribute copies of free software (and charge for", //$NON-NLS-1$ //$NON-NLS-2$
			"them if you wish), that you receive source code or can get it if you", "want it, that you can change the software or use pieces of it in new", //$NON-NLS-1$ //$NON-NLS-2$
			"free programs, and that you know you can do these things.", "", "To protect your rights, we need to prevent others from denying you", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"these rights or asking you to surrender the rights.  Therefore, you have", //$NON-NLS-1$
			"certain responsibilities if you distribute copies of the software, or if", "you modify it: responsibilities to respect the freedom of others.", //$NON-NLS-1$ //$NON-NLS-2$
			"", "For example, if you distribute copies of such a program, whether", "gratis or for a fee, you must pass on to the recipients the same", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"freedoms that you received.  You must make sure that they, too, receive", //$NON-NLS-1$
			"or can get the source code.  And you must show them these terms so they", "know their rights.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Developers that use the GNU GPL protect your rights with two steps:", "(1) assert copyright on the software, and (2) offer you this License", //$NON-NLS-1$ //$NON-NLS-2$
			"giving you legal permission to copy, distribute and/or modify it.", "", "For the developers\' and authors\' protection, the GPL clearly explains", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"that there is no warranty for this free software.  For both users' and", "authors\' sake, the GPL requires that modified versions be marked as", //$NON-NLS-1$ //$NON-NLS-2$
			"changed, so that their problems will not be attributed erroneously to", "authors of previous versions.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Some devices are designed to deny users access to install or run", "modified versions of the software inside them, although the manufacturer", //$NON-NLS-1$ //$NON-NLS-2$
			"can do so.  This is fundamentally incompatible with the aim of", "protecting users' freedom to change the software.  The systematic", //$NON-NLS-1$ //$NON-NLS-2$
			"pattern of such abuse occurs in the area of products for individuals to", "use, which is precisely where it is most unacceptable.  Therefore, we", //$NON-NLS-1$ //$NON-NLS-2$
			"have designed this version of the GPL to prohibit the practice for those", "products.  If such problems arise substantially in other domains, we", //$NON-NLS-1$ //$NON-NLS-2$
			"stand ready to extend this provision to those domains in future versions", "of the GPL, as needed to protect the freedom of users.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Finally, every program is threatened constantly by software patents.", "States should not allow patents to restrict development and use of", //$NON-NLS-1$ //$NON-NLS-2$
			"software on general-purpose computers, but in those that do, we wish to", "avoid the special danger that patents applied to a free program could", //$NON-NLS-1$ //$NON-NLS-2$
			"make it effectively proprietary.  To prevent this, the GPL assures that", "patents cannot be used to render the program non-free.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The precise terms and conditions for copying, distribution and", "modification follow.", "", "                   TERMS AND CONDITIONS", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"0. Definitions.", "", "\"This License\" refers to version 3 of the GNU General Public License.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"\"Copyright\" also means copyright-like laws that apply to other kinds of", "works, such as semiconductor masks.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"\"The Program\" refers to any copyrightable work licensed under this", "License.  Each licensee is addressed as \"you\".  \"Licensees\" and", //$NON-NLS-1$ //$NON-NLS-2$
			"\"recipients\" may be individuals or organizations.", "", "To \"modify\" a work means to copy from or adapt all or part of the work", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"in a fashion requiring copyright permission, other than the making of an", //$NON-NLS-1$
			"exact copy.  The resulting work is called a \"modified version\" of the", "earlier work or a work \"based on\" the earlier work.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"A \"covered work\" means either the unmodified Program or a work based", "on the Program.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"To \"propagate\" a work means to do anything with it that, without", "permission, would make you directly or secondarily liable for", //$NON-NLS-1$ //$NON-NLS-2$
			"infringement under applicable copyright law, except executing it on a", "computer or modifying a private copy.  Propagation includes copying,", //$NON-NLS-1$ //$NON-NLS-2$
			"distribution (with or without modification), making available to the", "public, and in some countries other activities as well.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"To \"convey\" a work means any kind of propagation that enables other", //$NON-NLS-1$
			"parties to make or receive copies.  Mere interaction with a user through", "a computer network, with no transfer of a copy, is not conveying.", //$NON-NLS-1$ //$NON-NLS-2$
			"", "An interactive user interface displays \"Appropriate Legal Notices\"", "to the extent that it includes a convenient and prominently visible", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"feature that (1) displays an appropriate copyright notice, and (2)", "tells the user that there is no warranty for the work (except to the", //$NON-NLS-1$ //$NON-NLS-2$
			"extent that warranties are provided), that licensees may convey the", "work under this License, and how to view a copy of this License.  If", //$NON-NLS-1$ //$NON-NLS-2$
			"the interface presents a list of user commands or options, such as a", "menu, a prominent item in the list meets this criterion.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"1. Source Code.", "", "The \"source code\" for a work means the preferred form of the work", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"for making modifications to it.  \"Object code\" means any non-source", "form of a work.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"A \"Standard Interface\" means an interface that either is an official", "standard defined by a recognized standards body, or, in the case of", //$NON-NLS-1$ //$NON-NLS-2$
			"interfaces specified for a particular programming language, one that", "is widely used among developers working in that language.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The \"System Libraries\" of an executable work include anything, other", "than the work as a whole, that (a) is included in the normal form of", //$NON-NLS-1$ //$NON-NLS-2$
			"packaging a Major Component, but which is not part of that Major", "Component, and (b) serves only to enable use of the work with that", //$NON-NLS-1$ //$NON-NLS-2$
			"Major Component, or to implement a Standard Interface for which an", "implementation is available to the public in source code form.  A", //$NON-NLS-1$ //$NON-NLS-2$
			"\"Major Component\", in this context, means a major essential component", "(kernel, window system, and so on) of the specific operating system", //$NON-NLS-1$ //$NON-NLS-2$
			"(if any) on which the executable work runs, or a compiler used to", "produce the work, or an object code interpreter used to run it.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The \"Corresponding Source\" for a work in object code form means all", "the source code needed to generate, install, and (for an executable", //$NON-NLS-1$ //$NON-NLS-2$
			"work) run the object code and to modify the work, including scripts to", "control those activities.  However, it does not include the work's", //$NON-NLS-1$ //$NON-NLS-2$
			"System Libraries, or general-purpose tools or generally available free", "programs which are used unmodified in performing those activities but", //$NON-NLS-1$ //$NON-NLS-2$
			"which are not part of the work.  For example, Corresponding Source", "includes interface definition files associated with source files for", //$NON-NLS-1$ //$NON-NLS-2$
			"the work, and the source code for shared libraries and dynamically", "linked subprograms that the work is specifically designed to require,", //$NON-NLS-1$ //$NON-NLS-2$
			"such as by intimate data communication or control flow between those", "subprograms and other parts of the work.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The Corresponding Source need not include anything that users", "can regenerate automatically from other parts of the Corresponding", "Source.", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"", "The Corresponding Source for a work in source code form is that", "same work.", "", "2. Basic Permissions.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
			"All rights granted under this License are granted for the term of", "copyright on the Program, and are irrevocable provided the stated", //$NON-NLS-1$ //$NON-NLS-2$
			"conditions are met.  This License explicitly affirms your unlimited", "permission to run the unmodified Program.  The output from running a", //$NON-NLS-1$ //$NON-NLS-2$
			"covered work is covered by this License only if the output, given its", "content, constitutes a covered work.  This License acknowledges your", //$NON-NLS-1$ //$NON-NLS-2$
			"rights of fair use or other equivalent, as provided by copyright law.", "", "You may make, run and propagate covered works that you do not", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"convey, without conditions so long as your license otherwise remains", "in force.  You may convey covered works to others for the sole purpose", //$NON-NLS-1$ //$NON-NLS-2$
			"of having them make modifications exclusively for you, or provide you", "with facilities for running those works, provided that you comply with", //$NON-NLS-1$ //$NON-NLS-2$
			"the terms of this License in conveying all material for which you do", "not control copyright.  Those thus making or running the covered works", //$NON-NLS-1$ //$NON-NLS-2$
			"for you must do so exclusively on your behalf, under your direction", "and control, on terms that prohibit them from making any copies of", //$NON-NLS-1$ //$NON-NLS-2$
			"your copyrighted material outside their relationship with you.", "", "Conveying under any other circumstances is permitted solely under", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"the conditions stated below.  Sublicensing is not allowed; section 10", "makes it unnecessary.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"3. Protecting Users' Legal Rights From Anti-Circumvention Law.", "", "No covered work shall be deemed part of an effective technological", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"measure under any applicable law fulfilling obligations under article", "11 of the WIPO copyright treaty adopted on 20 December 1996, or", //$NON-NLS-1$ //$NON-NLS-2$
			"similar laws prohibiting or restricting circumvention of such", "measures.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"When you convey a covered work, you waive any legal power to forbid", "circumvention of technological measures to the extent such circumvention", //$NON-NLS-1$ //$NON-NLS-2$
			"is effected by exercising rights under this License with respect to", "the covered work, and you disclaim any intention to limit operation or", //$NON-NLS-1$ //$NON-NLS-2$
			"modification of the work as a means of enforcing, against the work's", "users, your or third parties' legal rights to forbid circumvention of", //$NON-NLS-1$ //$NON-NLS-2$
			"technological measures.", "", "4. Conveying Verbatim Copies.", "", "You may convey verbatim copies of the Program's source code as you", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"receive it, in any medium, provided that you conspicuously and", "appropriately publish on each copy an appropriate copyright notice;", //$NON-NLS-1$ //$NON-NLS-2$
			"keep intact all notices stating that this License and any", "non-permissive terms added in accord with section 7 apply to the code;", //$NON-NLS-1$ //$NON-NLS-2$
			"keep intact all notices of the absence of any warranty; and give all", "recipients a copy of this License along with the Program.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"You may charge any price or no price for each copy that you convey,", "and you may offer support or warranty protection for a fee.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"5. Conveying Modified Source Versions.", "", "You may convey a work based on the Program, or the modifications to", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"produce it from the Program, in the form of source code under the", "terms of section 4, provided that you also meet all of these conditions:", //$NON-NLS-1$ //$NON-NLS-2$
			"", "a) The work must carry prominent notices stating that you modified", "it, and giving a relevant date.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"b) The work must carry prominent notices stating that it is", "released under this License and any conditions added under section", //$NON-NLS-1$ //$NON-NLS-2$
			"7.  This requirement modifies the requirement in section 4 to", "keep intact all notices", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"c) You must license the entire work, as a whole, under this", "License to anyone who comes into possession of a copy.  This", //$NON-NLS-1$ //$NON-NLS-2$
			"License will therefore apply, along with any applicable section 7", "additional terms, to the whole of the work, and all its parts,", //$NON-NLS-1$ //$NON-NLS-2$
			"regardless of how they are packaged.  This License gives no", "permission to license the work in any other way, but it does not", //$NON-NLS-1$ //$NON-NLS-2$
			"invalidate such permission if you have separately received it.", "", "d) If the work has interactive user interfaces, each must display", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Appropriate Legal Notices; however, if the Program has interactive", "interfaces that do not display Appropriate Legal Notices, your", //$NON-NLS-1$ //$NON-NLS-2$
			"work need not make them do so.", "", "A compilation of a covered work with other separate and independent", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"works, which are not by their nature extensions of the covered work,", "and which are not combined with it such as to form a larger program,", //$NON-NLS-1$ //$NON-NLS-2$
			"in or on a volume of a storage or distribution medium, is called an", "\"aggregate\" if the compilation and its resulting copyright are not", //$NON-NLS-1$ //$NON-NLS-2$
			"used to limit the access or legal rights of the compilation's users", "beyond what the individual works permit.  Inclusion of a covered work", //$NON-NLS-1$ //$NON-NLS-2$
			"in an aggregate does not cause this License to apply to the other", "parts of the aggregate.", "", "6. Conveying Non-Source Forms.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"You may convey a covered work in object code form under the terms", "of sections 4 and 5, provided that you also convey the", //$NON-NLS-1$ //$NON-NLS-2$
			"machine-readable Corresponding Source under the terms of this License,", "in one of these ways:", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"a) Convey the object code in, or embodied in, a physical product", "(including a physical distribution medium), accompanied by the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source fixed on a durable physical medium", "customarily used for software interchange.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"b) Convey the object code in, or embodied in, a physical product", "(including a physical distribution medium), accompanied by a", //$NON-NLS-1$ //$NON-NLS-2$
			"written offer, valid for at least three years and valid for as", "long as you offer spare parts or customer support for that product", //$NON-NLS-1$ //$NON-NLS-2$
			"model, to give anyone who possesses the object code either (1) a", "copy of the Corresponding Source for all the software in the", //$NON-NLS-1$ //$NON-NLS-2$
			"product that is covered by this License, on a durable physical", "medium customarily used for software interchange, for a price no", //$NON-NLS-1$ //$NON-NLS-2$
			"more than your reasonable cost of physically performing this", "conveying of source, or (2) access to copy the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source from a network server at no charge.", "", "c) Convey individual copies of the object code with a copy of the", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"written offer to provide the Corresponding Source.  This", "alternative is allowed only occasionally and noncommercially, and", //$NON-NLS-1$ //$NON-NLS-2$
			"only if you received the object code with such an offer, in accord", "with subsection 6b.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"d) Convey the object code by offering access from a designated", "place (gratis or for a charge), and offer equivalent access to the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source in the same way through the same place at no", "further charge.  You need not require recipients to copy the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source along with the object code.  If the place to", "copy the object code is a network server, the Corresponding Source", //$NON-NLS-1$ //$NON-NLS-2$
			"may be on a different server (operated by you or a third party)", "that supports equivalent copying facilities, provided you maintain", //$NON-NLS-1$ //$NON-NLS-2$
			"clear directions next to the object code saying where to find the", "Corresponding Source.  Regardless of what server hosts the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source, you remain obligated to ensure that it is", "available for as long as needed to satisfy these requirements.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"e) Convey the object code using peer-to-peer transmission, provided", "you inform other peers where the object code and Corresponding", //$NON-NLS-1$ //$NON-NLS-2$
			"Source of the work are being offered to the general public at no", "charge under subsection 6d.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"A separable portion of the object code, whose source code is excluded", "from the Corresponding Source as a System Library, need not be", //$NON-NLS-1$ //$NON-NLS-2$
			"included in conveying the object code work.", "", "A \"User Product\" is either (1) a \"consumer product\", which means any", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"tangible personal property which is normally used for personal, family,", //$NON-NLS-1$
			"or household purposes, or (2) anything designed or sold for incorporation", //$NON-NLS-1$
			"into a dwelling.  In determining whether a product is a consumer product,", //$NON-NLS-1$
			"doubtful cases shall be resolved in favor of coverage.  For a particular", "product received by a particular user, \"normally used\" refers to a", //$NON-NLS-1$ //$NON-NLS-2$
			"typical or common use of that class of product, regardless of the status", "of the particular user or of the way in which the particular user", //$NON-NLS-1$ //$NON-NLS-2$
			"actually uses, or expects or is expected to use, the product.  A product", //$NON-NLS-1$
			"is a consumer product regardless of whether the product has substantial", //$NON-NLS-1$
			"commercial, industrial or non-consumer uses, unless such uses represent", "the only significant mode of use of the product.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"\"Installation Information\" for a User Product means any methods,", "procedures, authorization keys, or other information required to install", //$NON-NLS-1$ //$NON-NLS-2$
			"and execute modified versions of a covered work in that User Product from", //$NON-NLS-1$
			"a modified version of its Corresponding Source.  The information must", "suffice to ensure that the continued functioning of the modified object", //$NON-NLS-1$ //$NON-NLS-2$
			"code is in no case prevented or interfered with solely because", "modification has been made.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"If you convey an object code work under this section in, or with, or", "specifically for use in, a User Product, and the conveying occurs as", //$NON-NLS-1$ //$NON-NLS-2$
			"part of a transaction in which the right of possession and use of the", "User Product is transferred to the recipient in perpetuity or for a", //$NON-NLS-1$ //$NON-NLS-2$
			"fixed term (regardless of how the transaction is characterized), the", "Corresponding Source conveyed under this section must be accompanied", //$NON-NLS-1$ //$NON-NLS-2$
			"by the Installation Information.  But this requirement does not apply", "if neither you nor any third party retains the ability to install", //$NON-NLS-1$ //$NON-NLS-2$
			"modified object code on the User Product (for example, the work has", "been installed in ROM).", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"The requirement to provide Installation Information does not include a", //$NON-NLS-1$
			"requirement to continue to provide support service, warranty, or updates", //$NON-NLS-1$
			"for a work that has been modified or installed by the recipient, or for", //$NON-NLS-1$
			"the User Product in which it has been modified or installed.  Access to a", "network may be denied when the modification itself materially and", //$NON-NLS-1$ //$NON-NLS-2$
			"adversely affects the operation of the network or violates the rules and", "protocols for communication across the network.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Corresponding Source conveyed, and Installation Information provided,", "in accord with this section must be in a format that is publicly", //$NON-NLS-1$ //$NON-NLS-2$
			"documented (and with an implementation available to the public in", "source code form), and must require no special password or key for", //$NON-NLS-1$ //$NON-NLS-2$
			"unpacking, reading or copying.", "", "7. Additional Terms.", "", "\"Additional permissions\" are terms that supplement the terms of this", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"License by making exceptions from one or more of its conditions.", "Additional permissions that are applicable to the entire Program shall", //$NON-NLS-1$ //$NON-NLS-2$
			"be treated as though they were included in this License, to the extent", "that they are valid under applicable law.  If additional permissions", //$NON-NLS-1$ //$NON-NLS-2$
			"apply only to part of the Program, that part may be used separately", "under those permissions, but the entire Program remains governed by", //$NON-NLS-1$ //$NON-NLS-2$
			"this License without regard to the additional permissions.", "", "When you convey a copy of a covered work, you may at your option", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"remove any additional permissions from that copy, or from any part of", "it.  (Additional permissions may be written to require their own", //$NON-NLS-1$ //$NON-NLS-2$
			"removal in certain cases when you modify the work.)  You may place", "additional permissions on material, added by you to a covered work,", //$NON-NLS-1$ //$NON-NLS-2$
			"for which you have or can give appropriate copyright permission.", "", "Notwithstanding any other provision of this License, for material you", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"add to a covered work, you may (if authorized by the copyright holders of", "that material) supplement the terms of this License with terms:", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"a) Disclaiming warranty or limiting liability differently from the", "terms of sections 15 and 16 of this License; or", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"b) Requiring preservation of specified reasonable legal notices or", "author attributions in that material or in the Appropriate Legal", //$NON-NLS-1$ //$NON-NLS-2$
			"Notices displayed by works containing it; or", "", "c) Prohibiting misrepresentation of the origin of that material, or", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"requiring that modified versions of such material be marked in", "reasonable ways as different from the original version; or", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"d) Limiting the use for publicity purposes of names of licensors or", "authors of the material; or", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"e) Declining to grant rights under trademark law for use of some", "trade names, trademarks, or service marks; or", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"f) Requiring indemnification of licensors and authors of that", "material by anyone who conveys the material (or modified versions of", //$NON-NLS-1$ //$NON-NLS-2$
			"it) with contractual assumptions of liability to the recipient, for", "any liability that these contractual assumptions directly impose on", //$NON-NLS-1$ //$NON-NLS-2$
			"those licensors and authors.", "", "All other non-permissive additional terms are considered \"further", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"restrictions\" within the meaning of section 10.  If the Program as you", "received it, or any part of it, contains a notice stating that it is", //$NON-NLS-1$ //$NON-NLS-2$
			"governed by this License along with a term that is a further", "restriction, you may remove that term.  If a license document contains", //$NON-NLS-1$ //$NON-NLS-2$
			"a further restriction but permits relicensing or conveying under this", "License, you may add to a covered work material governed by the terms", //$NON-NLS-1$ //$NON-NLS-2$
			"of that license document, provided that the further restriction does", "not survive such relicensing or conveying.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"If you add terms to a covered work in accord with this section, you", "must place, in the relevant source files, a statement of the", //$NON-NLS-1$ //$NON-NLS-2$
			"additional terms that apply to those files, or a notice indicating", "where to find the applicable terms.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Additional terms, permissive or non-permissive, may be stated in the", "form of a separately written license, or stated as exceptions;", //$NON-NLS-1$ //$NON-NLS-2$
			"the above requirements apply either way.", "", "8. Termination.", "", "You may not propagate or modify a covered work except as expressly", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"provided under this License.  Any attempt otherwise to propagate or", "modify it is void, and will automatically terminate your rights under", //$NON-NLS-1$ //$NON-NLS-2$
			"this License (including any patent licenses granted under the third", "paragraph of section 11).", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"However, if you cease all violation of this License, then your", "license from a particular copyright holder is reinstated (a)", //$NON-NLS-1$ //$NON-NLS-2$
			"provisionally, unless and until the copyright holder explicitly and", "finally terminates your license, and (b) permanently, if the copyright", //$NON-NLS-1$ //$NON-NLS-2$
			"holder fails to notify you of the violation by some reasonable means", "prior to 60 days after the cessation.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Moreover, your license from a particular copyright holder is", "reinstated permanently if the copyright holder notifies you of the", //$NON-NLS-1$ //$NON-NLS-2$
			"violation by some reasonable means, this is the first time you have", "received notice of violation of this License (for any work) from that", //$NON-NLS-1$ //$NON-NLS-2$
			"copyright holder, and you cure the violation prior to 30 days after", "your receipt of the notice.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Termination of your rights under this section does not terminate the", "licenses of parties who have received copies or rights from you under", //$NON-NLS-1$ //$NON-NLS-2$
			"this License.  If your rights have been terminated and not permanently", "reinstated, you do not qualify to receive new licenses for the same", //$NON-NLS-1$ //$NON-NLS-2$
			"material under section 10.", "", "9. Acceptance Not Required for Having Copies.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"You are not required to accept this License in order to receive or", "run a copy of the Program.  Ancillary propagation of a covered work", //$NON-NLS-1$ //$NON-NLS-2$
			"occurring solely as a consequence of using peer-to-peer transmission", "to receive a copy likewise does not require acceptance.  However,", //$NON-NLS-1$ //$NON-NLS-2$
			"nothing other than this License grants you permission to propagate or", "modify any covered work.  These actions infringe copyright if you do", //$NON-NLS-1$ //$NON-NLS-2$
			"not accept this License.  Therefore, by modifying or propagating a", "covered work, you indicate your acceptance of this License to do so.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"10. Automatic Licensing of Downstream Recipients.", "", "Each time you convey a covered work, the recipient automatically", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"receives a license from the original licensors, to run, modify and", "propagate that work, subject to this License.  You are not responsible", //$NON-NLS-1$ //$NON-NLS-2$
			"for enforcing compliance by third parties with this License.", "", "An \"entity transaction\" is a transaction transferring control of an", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"organization, or substantially all assets of one, or subdividing an", "organization, or merging organizations.  If propagation of a covered", //$NON-NLS-1$ //$NON-NLS-2$
			"work results from an entity transaction, each party to that", "transaction who receives a copy of the work also receives whatever", //$NON-NLS-1$ //$NON-NLS-2$
			"licenses to the work the party's predecessor in interest had or could", "give under the previous paragraph, plus a right to possession of the", //$NON-NLS-1$ //$NON-NLS-2$
			"Corresponding Source of the work from the predecessor in interest, if", "the predecessor has it or can get it with reasonable efforts.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"You may not impose any further restrictions on the exercise of the", "rights granted or affirmed under this License.  For example, you may", //$NON-NLS-1$ //$NON-NLS-2$
			"not impose a license fee, royalty, or other charge for exercise of", "rights granted under this License, and you may not initiate litigation", //$NON-NLS-1$ //$NON-NLS-2$
			"(including a cross-claim or counterclaim in a lawsuit) alleging that", "any patent claim is infringed by making, using, selling, offering for", //$NON-NLS-1$ //$NON-NLS-2$
			"sale, or importing the Program or any portion of it.", "", "11. Patents.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"A \"contributor\" is a copyright holder who authorizes use under this", "License of the Program or a work on which the Program is based.  The", //$NON-NLS-1$ //$NON-NLS-2$
			"work thus licensed is called the contributor's \"contributor version\".", "", "A contributor's \"essential patent claims\" are all patent claims", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"owned or controlled by the contributor, whether already acquired or", "hereafter acquired, that would be infringed by some manner, permitted", //$NON-NLS-1$ //$NON-NLS-2$
			"by this License, of making, using, or selling its contributor version,", "but do not include claims that would be infringed only as a", //$NON-NLS-1$ //$NON-NLS-2$
			"consequence of further modification of the contributor version.  For", "purposes of this definition, \"control\" includes the right to grant", //$NON-NLS-1$ //$NON-NLS-2$
			"patent sublicenses in a manner consistent with the requirements of", "this License.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Each contributor grants you a non-exclusive, worldwide, royalty-free", "patent license under the contributor's essential patent claims, to", //$NON-NLS-1$ //$NON-NLS-2$
			"make, use, sell, offer for sale, import and otherwise run, modify and", "propagate the contents of its contributor version.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"In the following three paragraphs, a \"patent license\" is any express", "agreement or commitment, however denominated, not to enforce a patent", //$NON-NLS-1$ //$NON-NLS-2$
			"(such as an express permission to practice a patent or covenant not to", "sue for patent infringement).  To \"grant\" such a patent license to a", //$NON-NLS-1$ //$NON-NLS-2$
			"party means to make such an agreement or commitment not to enforce a", "patent against the party.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"If you convey a covered work, knowingly relying on a patent license,", "and the Corresponding Source of the work is not available for anyone", //$NON-NLS-1$ //$NON-NLS-2$
			"to copy, free of charge and under the terms of this License, through a", "publicly available network server or other readily accessible means,", //$NON-NLS-1$ //$NON-NLS-2$
			"then you must either (1) cause the Corresponding Source to be so", "available, or (2) arrange to deprive yourself of the benefit of the", //$NON-NLS-1$ //$NON-NLS-2$
			"patent license for this particular work, or (3) arrange, in a manner", "consistent with the requirements of this License, to extend the patent", //$NON-NLS-1$ //$NON-NLS-2$
			"license to downstream recipients.  \"Knowingly relying\" means you have", "actual knowledge that, but for the patent license, your conveying the", //$NON-NLS-1$ //$NON-NLS-2$
			"covered work in a country, or your recipient's use of the covered work", "in a country, would infringe one or more identifiable patents in that", //$NON-NLS-1$ //$NON-NLS-2$
			"country that you have reason to believe are valid.", "", "If, pursuant to or in connection with a single transaction or", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"arrangement, you convey, or propagate by procuring conveyance of, a", "covered work, and grant a patent license to some of the parties", //$NON-NLS-1$ //$NON-NLS-2$
			"receiving the covered work authorizing them to use, propagate, modify", "or convey a specific copy of the covered work, then the patent license", //$NON-NLS-1$ //$NON-NLS-2$
			"you grant is automatically extended to all recipients of the covered", "work and works based on it.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"A patent license is \"discriminatory\" if it does not include within", "the scope of its coverage, prohibits the exercise of, or is", //$NON-NLS-1$ //$NON-NLS-2$
			"conditioned on the non-exercise of one or more of the rights that are", "specifically granted under this License.  You may not convey a covered", //$NON-NLS-1$ //$NON-NLS-2$
			"work if you are a party to an arrangement with a third party that is", "in the business of distributing software, under which you make payment", //$NON-NLS-1$ //$NON-NLS-2$
			"to the third party based on the extent of your activity of conveying", "the work, and under which the third party grants, to any of the", //$NON-NLS-1$ //$NON-NLS-2$
			"parties who would receive the covered work from you, a discriminatory", "patent license (a) in connection with copies of the covered work", //$NON-NLS-1$ //$NON-NLS-2$
			"conveyed by you (or copies made from those copies), or (b) primarily", "for and in connection with specific products or compilations that", //$NON-NLS-1$ //$NON-NLS-2$
			"contain the covered work, unless you entered into that arrangement,", "or that patent license was granted, prior to 28 March 2007.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Nothing in this License shall be construed as excluding or limiting", "any implied license or other defenses to infringement that may", //$NON-NLS-1$ //$NON-NLS-2$
			"otherwise be available to you under applicable patent law.", "", "12. No Surrender of Others' Freedom.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			"If conditions are imposed on you (whether by court order, agreement or", "otherwise) that contradict the conditions of this License, they do not", //$NON-NLS-1$ //$NON-NLS-2$
			"excuse you from the conditions of this License.  If you cannot convey a", //$NON-NLS-1$
			"covered work so as to satisfy simultaneously your obligations under this", //$NON-NLS-1$
			"License and any other pertinent obligations, then as a consequence you may", //$NON-NLS-1$
			"not convey it at all.  For example, if you agree to terms that obligate you", //$NON-NLS-1$
			"to collect a royalty for further conveying from those to whom you convey", //$NON-NLS-1$
			"the Program, the only way you could satisfy both those terms and this", "License would be to refrain entirely from conveying the Program.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"13. Use with the GNU Affero General Public License.", "", "Notwithstanding any other provision of this License, you have", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"permission to link or combine any covered work with a work licensed", "under version 3 of the GNU Affero General Public License into a single", //$NON-NLS-1$ //$NON-NLS-2$
			"combined work, and to convey the resulting work.  The terms of this", "License will continue to apply to the part which is the covered work,", //$NON-NLS-1$ //$NON-NLS-2$
			"but the special requirements of the GNU Affero General Public License,", "section 13, concerning interaction through a network will apply to the", //$NON-NLS-1$ //$NON-NLS-2$
			"combination as such.", "", "14. Revised Versions of this License.", "", "The Free Software Foundation may publish revised and/or new versions of", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"the GNU General Public License from time to time.  Such new versions will", //$NON-NLS-1$
			"be similar in spirit to the present version, but may differ in detail to", "address new problems or concerns.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Each version is given a distinguishing version number.  If the", "Program specifies that a certain numbered version of the GNU General", //$NON-NLS-1$ //$NON-NLS-2$
			"Public License \"or any later version\" applies to it, you have the", "option of following the terms and conditions either of that numbered", //$NON-NLS-1$ //$NON-NLS-2$
			"version or of any later version published by the Free Software", "Foundation.  If the Program does not specify a version number of the", //$NON-NLS-1$ //$NON-NLS-2$
			"GNU General Public License, you may choose any version ever published", "by the Free Software Foundation.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"If the Program specifies that a proxy can decide which future", "versions of the GNU General Public License can be used, that proxy's", //$NON-NLS-1$ //$NON-NLS-2$
			"public statement of acceptance of a version permanently authorizes you", "to choose that version for the Program.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"Later license versions may give you additional or different", "permissions.  However, no additional obligations are imposed on any", //$NON-NLS-1$ //$NON-NLS-2$
			"author or copyright holder as a result of your choosing to follow a", "later version.", "", "15. Disclaimer of Warranty.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY", "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT", //$NON-NLS-1$ //$NON-NLS-2$
			"HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY", //$NON-NLS-1$
			"OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,", //$NON-NLS-1$
			"THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR", //$NON-NLS-1$
			"PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM", //$NON-NLS-1$
			"IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF", "ALL NECESSARY SERVICING, REPAIR OR CORRECTION.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"16. Limitation of Liability.", "", "IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS", //$NON-NLS-1$
			"THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY", //$NON-NLS-1$
			"GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE", //$NON-NLS-1$
			"USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF", //$NON-NLS-1$
			"DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD", //$NON-NLS-1$
			"PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),", //$NON-NLS-1$
			"EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF", "SUCH DAMAGES.", "", "17. Interpretation of Sections 15 and 16.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			"If the disclaimer of warranty and limitation of liability provided", "above cannot be given local legal effect according to their terms,", //$NON-NLS-1$ //$NON-NLS-2$
			"reviewing courts shall apply local law that most closely approximates", "an absolute waiver of all civil liability in connection with the", //$NON-NLS-1$ //$NON-NLS-2$
			"Program, unless a warranty or assumption of liability accompanies a", "copy of the Program in return for a fee.", "", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			"                 END OF TERMS AND CONDITIONS" };	//$NON-NLS-1$

	public static String string() {
		String toReturn = ""; //$NON-NLS-1$
		for (String line : text) {
			toReturn += line + "\n"; //$NON-NLS-1$
		}
		return toReturn;
	}
}
