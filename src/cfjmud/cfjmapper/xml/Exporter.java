/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.xml;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.objects.ZoneRoom;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;

/**
 * @author Ecthelion
 * 
 */
public class Exporter {
	private final int		areaId;
	private final Database	database;
	private final boolean	debug	= true;
	Mapper					mapper;

	private ZipOutputStream	zipFile;

	public Exporter(Mapper mapper, int areaid) {
		this.mapper = mapper;
		this.database = Database.getInstance();
		this.areaId = areaid;
		save();
	}

	private void debug(String string) {
		if (debug) System.out.println(string);

	}

	private HashSet<Area> getChildrenOfAreas(HashSet<Area> t, Area area) {
		HashSet<Area> toReturn = new HashSet<Area>();
		for (Area a : database.getAreas()) {
			if (area.getId() == a.getParent()) {
				toReturn.add(a);
				toReturn.addAll(getChildrenOfAreas(toReturn, a));
			}

		}
		return toReturn;
	}

	private File showAndSetFile() {
		File file = null;
		JFileChooser fileChooser = new JFileChooser(new File(".")); //$NON-NLS-1$
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		FileFilter ff = new FileFilter() {

			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) return true;
				String name = f.getName();
				String[] parts = name.split("\\."); //$NON-NLS-1$
				if (parts.length > 1) {
					if (parts[1].equalsIgnoreCase("cfj")) { //$NON-NLS-1$
						return true;
					} else {
						return false;
					}

				}
				return false;
			}

			@Override
			public String getDescription() {
				return "CFJ Files"; //$NON-NLS-1$
			}
		};
		fileChooser.setFileFilter(ff);
		int returnVal = fileChooser.showSaveDialog(mapper);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fileChooser.getSelectedFile();
		} else {
			return null;
		}

		String fileName = file.getName();
		System.out.println("Filename: " + fileName); //$NON-NLS-1$
		String parts[] = file.getName().split("\\."); //$NON-NLS-1$
		if (parts.length < 2) {
			file = new File(file.getAbsolutePath() + ".cfj"); //$NON-NLS-1$
		}
		return file;
	}

	private void save() {
		System.out.println("Saving as XML. Areaid: " + areaId); //$NON-NLS-1$
		HashSet<Area> areaHashSet = new HashSet<Area>();
		Hashtable<Integer, MapperIcon> iconVector = new Hashtable<Integer, MapperIcon>();
		Hashtable<Integer, MapObject> objectVector = new Hashtable<Integer, MapObject>();

		File file = showAndSetFile();
		if (file == null) return;

		// Fetch all the items...
		if (areaId == 0) {
			areaHashSet.addAll(database.getAreas());
		} else {
			areaHashSet.add(database.getArea(areaId));
			areaHashSet.addAll(getChildrenOfAreas(areaHashSet, database.getArea(areaId)));
		}
		Hashtable<Integer, ArrayList<Room>> roomVector = new Hashtable<Integer, ArrayList<Room>>();
		Hashtable<Integer, ArrayList<Exit>> exitsVector = new Hashtable<Integer, ArrayList<Exit>>();
		Hashtable<Integer, Exit> genericExits = new Hashtable<Integer, Exit>();
		Hashtable<Integer, Integer> roomGraphics = new Hashtable<Integer, Integer>();
		ArrayList<Zone> zoneList = new ArrayList<Zone>();
		ArrayList<ZoneRoom> zoneRoomList = new ArrayList<ZoneRoom>();
		objectVector = new Hashtable<Integer, MapObject>();
		debug("Size Of IconVector: " + iconVector.size()); //$NON-NLS-1$
		debug("IconVector: " + iconVector.toString()); //$NON-NLS-1$

		try {
			for (Area area : areaHashSet) {
				ArrayList<Room> roomList = new ArrayList<Room>();
				ArrayList<Exit> exitList = new ArrayList<Exit>();
				for (Zone zone : database.getZonesForAreaForDeletionAndSave(area.getId())) {
					zoneList.add(zone);
					for (ZoneRoom zoneRoom : database.getZoneRoomsForZone(zone)) {
						zoneRoomList.add(zoneRoom);
					}
				}
				for (Room room : database.getRoomsForArea(area.getId())) {
					roomGraphics.put(new Integer(room.getType()), new Integer(room.getType()));
					roomList.add(room);
				}
				roomVector.put(new Integer(area.getId()), roomList);
				for (Exit exit : database.getExistsInsideArea(area.getId())) {
					exitList.add(exit);
				}

				for (Exit exit : database.getExitsForBetweenArea(area.getId())) {
					genericExits.put(new Integer(exit.getId()), exit);
				}
				exitsVector.put(new Integer(area.getId()), exitList);
				for (MapObject object : database.getObjectsForArea(area.getId())) {
					objectVector.put(new Integer(object.getId()), object);
				}
				int icon = area.getIcon();
				if (icon != 0) iconVector.put(new Integer(icon), database.getIcon(icon));
			}
			for (MapObject object : objectVector.values()) {
				int icon = object.getIconId();
				if (icon != 0) iconVector.put(new Integer(icon), database.getIcon(icon));
			}
			ArrayList<Exit> tmp = new ArrayList<>(genericExits.values());
			exitsVector.put(new Integer(-1), tmp);
			debug("Before creating streams..."); //$NON-NLS-1$
			zipFile = new ZipOutputStream(new FileOutputStream(file));
			debug("ZipOutputstream created."); //$NON-NLS-1$
			writeAreas(areaHashSet, zipFile);
			writeRooms(roomVector, zipFile);
			writeObjects(objectVector.values(), zipFile);
			writeExits(exitsVector, zipFile);
			writeRoomGraphics(roomGraphics, zipFile);
			writeZones(zoneList, zipFile);
			writeZoneRooms(zoneRoomList, zipFile);

			Hashtable<String, MapperIcon> clenser = new Hashtable<String, MapperIcon>();

			for (MapperIcon icon : iconVector.values()) {
				clenser.put(icon.toFileName(), icon);
			}

			writeIcons(clenser.values(), zipFile);

			debug("End Root Added, xml"); // //$NON-NLS-1$
			// zipFile.finish();
			for (MapperIcon icon : clenser.values()) {
				debug("In the Icon Loop..."); //$NON-NLS-1$
				zipFile.putNextEntry(new ZipEntry(icon.toFileName()));
				debug("New Zip Entry: " + icon.toFileName()); //$NON-NLS-1$
				Image image = icon.getImage();
				debug("Image gotten"); //$NON-NLS-1$
				BufferedImage bi = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TRANSLUCENT);
				debug("BufferedImage Created..."); //$NON-NLS-1$
				Graphics2D g2 = bi.createGraphics();
				debug("Graphics2D gotten..."); //$NON-NLS-1$
				g2.drawImage(image, null, null);
				debug("Image Drawn..."); //$NON-NLS-1$
				ImageIO.write(bi, "png", zipFile); //$NON-NLS-1$
				debug("Image Written..."); //$NON-NLS-1$
			}
			zipFile.flush();
			debug("OutputStreamWriter Flushed"); //$NON-NLS-1$
			zipFile.close();
			debug("OutPutSrteamWriter Closed"); //$NON-NLS-1$
		} catch (Exception e) {
			System.out.println("Couldn't Save To Disc." + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	private void writeZoneRooms(ArrayList<ZoneRoom> zoneRoomList, ZipOutputStream zipFile2) throws IOException {
		zipFile.putNextEntry(new ZipEntry("zonerooms.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (ZoneRoom zoneRoom : zoneRoomList) {

			zipFile.write(zoneRoom.toXml().getBytes());
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
	}

	private void writeZones(ArrayList<Zone> zoneList, ZipOutputStream zipFile2) throws IOException {
		zipFile.putNextEntry(new ZipEntry("zones.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (Zone zone : zoneList) {

			zipFile.write(zone.toXml().getBytes());
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
	}

	private void writeRoomGraphics(Hashtable<Integer, Integer> roomGraphics, ZipOutputStream zipFile2) throws IOException {
		zipFile.putNextEntry(new ZipEntry("roomgraphics.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (Integer i : roomGraphics.values()) {
			RoomGraphic rg = RoomGraphicsManager.getInstance().getShape(i.intValue());
			if (rg != null) zipFile.write(rg.toXml().getBytes());
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
		debug("Exits Added, xml"); //$NON-NLS-1$
	}

	private void writeIcons(Collection<MapperIcon> values, ZipOutputStream zipFile) throws IOException {
		zipFile.putNextEntry(new ZipEntry("icons.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (MapperIcon icon : values) { //
			zipFile.write(icon.toXml().getBytes(), 0, icon.toXml().getBytes().length);
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
		debug("Exits Added, xml"); //$NON-NLS-1$
	}

	private void writeObjects(Collection<MapObject> objects, ZipOutputStream zipFile) throws IOException {
		zipFile.putNextEntry(new ZipEntry("objects.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (MapObject object : objects) {
			zipFile.write(object.toXml().getBytes());
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
		debug("Objects Addded, xml"); //$NON-NLS-1$
	}

	private void writeAreas(Collection<Area> areaHashSet, ZipOutputStream zipFile) throws IOException {
		zipFile.putNextEntry(new ZipEntry("areas.xml")); //$NON-NLS-1$
		zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
		zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
		for (Area area : areaHashSet) {
			zipFile.write(area.toXml().getBytes());
		}
		zipFile.write("</root>".getBytes()); //$NON-NLS-1$
	}

	private void writeRooms(Hashtable<Integer, ArrayList<Room>> roomList, ZipOutputStream zipFile) throws IOException {
		for (Integer i : roomList.keySet()) {
			zipFile.putNextEntry(new ZipEntry("rooms_" + i + ".xml")); //$NON-NLS-1$ //$NON-NLS-2$

			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			ArrayList<Room> rooms = roomList.get(i);
			for (Room room : rooms) {
				zipFile.write(room.toXml().getBytes());
			}
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$
		}
	}

	private void writeExits(Hashtable<Integer, ArrayList<Exit>> exitList, ZipOutputStream zipFile) throws IOException {
		for (Integer i : exitList.keySet()) {
			String fileName = "exits_" + i + ".xml"; //$NON-NLS-1$ //$NON-NLS-2$
			if (i.intValue() == -1) fileName = "exits.xml"; //$NON-NLS-1$
			zipFile.putNextEntry(new ZipEntry(fileName));

			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			ArrayList<Exit> exits = exitList.get(i);
			for (Exit exit : exits) {
				zipFile.write(exit.toXml().getBytes());
			}
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$
		}
	}
}
