/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.xml;

import java.awt.Color;
import java.awt.Font;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Hashtable;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import cfjmud.cfjmapper.RoomColourHandle;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.objects.ZoneRoom;

/**
 * @author Ecthelion
 * 
 */
public class XMLHandler extends DefaultHandler {
	@SuppressWarnings("unused")
	static private Writer											out;

	private final ArrayList<Area>									areaVector;
	private Area													currentArea;
	private Exit													currentExit;
	private MapperIcon												currentIcon;
	private final Database											database;
	String															currentElement		= "";		//$NON-NLS-1$
	String															currentMainElement	= "";		//$NON-NLS-1$
	private MapObject												currentObject;
	private Room													currentRoom;

	boolean															debugga				= false;
	private final ArrayList<Exit>									exitsVector;
	private ArrayList<MapperIcon>									iconVector;
	private final ArrayList<MapObject>								mapObjectVector;
	private final ArrayList<Room>									roomVector;
	private final ArrayList<RoomColourHandle>						colourTriggerVector;
	private RoomColourHandle										currentColourTrigger;
	private final Hashtable<Integer, Hashtable<Integer, Integer>>	roomExchangeTable;
	private Hashtable<Integer, Integer>								areaExchangeTable;

	StringBuffer													textBuffer;

	private final ArrayList<Zone>									zoneVector;

	private final ArrayList<ZoneRoom>								zoneRoomVector;

	private Zone													currentZone;

	private ZoneRoom												currentZoneRoom;

	public XMLHandler() {
		roomVector = new ArrayList<Room>();
		areaVector = new ArrayList<Area>();
		exitsVector = new ArrayList<Exit>();
		mapObjectVector = new ArrayList<MapObject>();
		iconVector = new ArrayList<MapperIcon>();
		colourTriggerVector = new ArrayList<RoomColourHandle>();
		roomExchangeTable = new Hashtable<Integer, Hashtable<Integer, Integer>>();
		zoneVector = new ArrayList<Zone>();
		zoneRoomVector = new ArrayList<ZoneRoom>();
		this.database = Database.getInstance();
	}

	@Override
	public void characters(char buf[], int offset, int len) throws SAXException {
		String s = new String(buf, offset, len);
		if (textBuffer == null) {
			textBuffer = new StringBuffer(s);
		} else {
			textBuffer.append(s);
		}
	}

	private void debug(String s) throws SAXException {
		if (debugga) {
			System.out.print(s);
			System.out.flush();
		}
	}

	private void echoText() throws SAXException {
		if (textBuffer == null) return;
		nl();
		debug("CHARS:   "); //$NON-NLS-1$

		String s = "" + textBuffer; //$NON-NLS-1$
		debug("The Current Element: " + currentElement + " - " + s + " "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (!s.trim().equals("")) { //$NON-NLS-1$
			debug("The Current Element: " + currentElement + s); //$NON-NLS-1$
			/* ********************* AREA ********************* */
			if (currentElement.equalsIgnoreCase("areaname")) currentArea.setName(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaId")) currentArea.setId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areabordercolour")) currentArea.setBorderColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaborderraised")) currentArea.setBorderRaised(Boolean.parseBoolean(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaborderthickness")) currentArea.setBorderThickness(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areabordertitle")) currentArea.setBorderTitle(Boolean.parseBoolean(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areabordertype")) currentArea.setBorderType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areafont")) currentArea.setFont(new Font(s, 0, 0)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaheight")) currentArea.setHeight(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areawidth")) currentArea.setWidth(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaicon")) currentArea.setIcon(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealabeltext")) currentArea.setLabel(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealabelcolour")) currentArea.setLabelColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealabelfontsize")) currentArea.setLabelFontSize((int) Float.parseFloat(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealabelfontstyle")) currentArea.setLabelFontStyle(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealabelposition")) currentArea.setLabelPosition(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("arealevel")) currentArea.setLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaparent")) currentArea.setParent(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areashadowcolour")) currentArea.setShadowColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areatype")) currentArea.setType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaxpos")) currentArea.setXPos(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areaypos")) currentArea.setYPos(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("areacolour")) currentArea.setColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("preferredRoomDistance")) currentArea.setPreferredRoomDistance(Integer.parseInt(s)); //$NON-NLS-1$
			/* ************************************************ */
			/* ********************* ROOM ********************* */
			if (currentElement.equalsIgnoreCase("roomid")) currentRoom.setId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomname")) currentRoom.setName(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomdescription")) currentRoom.setDescription(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomcolour")) currentRoom.setColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomlevel")) currentRoom.setLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomxPos")) currentRoom.setXPosition(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomyPos")) currentRoom.setYPosition(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomareaforroom")) currentRoom.setAreaId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomtype")) currentRoom.setType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomalias")) currentRoom.setAlias(s); //$NON-NLS-1$
			/* ************************************************ */
			/* ********************* EXIT ********************* */
			if (currentElement.equalsIgnoreCase("exittartx")) currentExit.setStartX(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitstarty")) currentExit.setStartY(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitendx")) currentExit.setEndX(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitendy")) currentExit.setEndY(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourceroom")) currentExit.setSourceRoomId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetroom")) currentExit.setTargetRoomId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourcelevel")) currentExit.setSourceLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetlevel")) currentExit.setTargetLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourcearea")) currentExit.setSourceArea(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetarea")) currentExit.setTargetArea(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourcetype")) currentExit.setSourceType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetype")) currentExit.setTargetType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitshowstub")) currentExit.setShowStub(Boolean.parseBoolean(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittype")) currentExit.setType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitlevel")) currentExit.setLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetwalkcommand")) currentExit.setTargetWalkCommand(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourcewalkcommand")) currentExit.setSourceWalkCommand(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exittargetAreaRep")) currentExit.setTargetAreaRep(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitsourceAreaRep")) currentExit.setSourceAreaRep(Integer.parseInt(s)); //$NON-NLS-1$exitisvisible
			if (currentElement.equalsIgnoreCase("exitisinvisible")) currentExit.setInvisible((Boolean.parseBoolean(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("exitisoneway")) currentExit.setOneWay(Boolean.parseBoolean(s)); //$NON-NLS-1$exitisvisible

			/* ************************************************ */
			/* ******************** OBJECT ******************** */
			if (currentElement.equalsIgnoreCase("mapobjectlabel")) currentObject.setLabel(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectlabelposition")) currentObject.setLabelPosition(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectlevel")) currentObject.setLevel(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectfont")) currentObject.setFont(Font.decode(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectfontsize")) currentObject.setFontSize(Float.parseFloat(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectfontstyle")) currentObject.setFontStyle(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjecticonid")) currentObject.setIconId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectxpos")) currentObject.setxPos(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectypos")) currentObject.setyPos(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectareaid")) currentObject.setAreaId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectid")) currentObject.setId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectwidth")) currentObject.setWidth(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectheight")) currentObject.setHeight(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mapobjectbackgroundcolour")) currentObject.setBackground(new Color(Integer.parseInt(s))); //$NON-NLS-1$

			/* ************************************************ */
			/* ********************* ICON ********************* */
			if (currentElement.equalsIgnoreCase("mappericonname")) currentIcon.setName(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mappericonid")) currentIcon.setId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("mappericonfilename")) currentIcon.setFileName(s); //$NON-NLS-1$
			/* ************************************************ */
			/* ***************** ROOMCOLOURTRIGGER ************ */
			if (currentElement.equalsIgnoreCase("roomcolourtext")) currentColourTrigger.setText(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomcolourtype")) currentColourTrigger.setType(Integer.parseInt(s.trim())); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("roomcolourcolour")) currentColourTrigger.setColour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			/* ************************************************ */
			/* ********************* ZONE ********************* */
			if (currentElement.equalsIgnoreCase("zoneid")) currentZone.setZone_id(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonename")) currentZone.setName(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonedescription")) currentZone.setDescription(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonearea")) currentZone.setArea_id(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonebackgroundstyle")) currentZone.setBackGroundStyle(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonebackgroundref")) currentZone.setBackGroundReference(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonebackgroundcolour")) currentZone.setBackGroundColor(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonetype")) currentZone.setType(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zoneborder")) currentZone.setBorder(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonefillstyle")) currentZone.setFillStyle(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonelabelalignment")) currentZone.setLabelalignment(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonelabeltext")) currentZone.setLabeltext(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonelabelcolour")) currentZone.setLabelcolour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonelabelfont")) currentZone.setLabelfont(s); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zoneborderthickness")) currentZone.setBorderthickness(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonebordercolour")) currentZone.setBordercolour(new Color(Integer.parseInt(s))); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonebordertype")) currentZone.setBordertype(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zonelabelfontsize")) currentZone.setLabelfontsize(Float.parseFloat(s)); //$NON-NLS-1$

			/* ************************************************ */
			/* ******************* ZONEROOM ********************* */
			if (currentElement.equalsIgnoreCase("zoneroomroomid")) currentZoneRoom.setR_id(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zoneroomareaid")) currentZoneRoom.setA_id(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zoneroomindex")) currentZoneRoom.setIndex(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("zoneroomzoneid")) currentZoneRoom.setZ_id(Integer.parseInt(s)); //$NON-NLS-1$
			/* ************************************************ */
		}

		textBuffer = null;
	}

	@Override
	public void endDocument() throws SAXException {
		nl();
		debug("END DOCUMENT"); //$NON-NLS-1$

	}

	@Override
	public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
		echoText();
		nl();
		debug("END_ELM: "); //$NON-NLS-1$

		String eName = sName; // element name
		if ("".equals(eName)) eName = qName; // not namespace-aware //$NON-NLS-1$
		//System.out.print(eName + " - "); //$NON-NLS-1$
		//System.out.println("" + areaVector + " - " + mapObjectVector + " - " + mapObjectVector + " - " + roomVector + " - " + colourTriggerVector); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

		if (eName.equalsIgnoreCase("area")) { //$NON-NLS-1$
			debug("Adding area to Vector"); //$NON-NLS-1$
			areaVector.add(currentArea);
		}
		if (eName.equalsIgnoreCase("mapobject")) { //$NON-NLS-1$
			debug("Adding mapobject to Vector"); //$NON-NLS-1$
			mapObjectVector.add(currentObject);
		}
		if (eName.equalsIgnoreCase("mappericon")) { //$NON-NLS-1$
			debug("Adding mappericon to Vector"); //$NON-NLS-1$
			iconVector.add(currentIcon);
		}
		if (eName.equalsIgnoreCase("exit")) { //$NON-NLS-1$
			debug("Adding exit to Database"); //$NON-NLS-1$
			if (areaExchangeTable != null && roomExchangeTable != null) {

				/*
				 * if (debugga) { System.out.println("\nDatabase: " + database);
				 * //$NON-NLS-1$ System.out.println("areaExchangeTable: " +
				 * areaExchangeTable); //$NON-NLS-1$
				 * System.out.println("roomExchangeTable: " +
				 * roomExchangeTable); //$NON-NLS-1$
				 * System.out.println("currentExit: " + currentExit);
				 * //$NON-NLS-1$ }
				 */
				int sourceRoom = currentExit.getSourceRoomId();
				int targetRoom = currentExit.getTargetRoomId();
				int sourceArea = currentExit.getSourceArea();
				int targetArea = currentExit.getTargetArea();
				if (debugga) System.out.println("Old: " + sourceArea + " - " + targetArea + " - " + sourceRoom + " - " + targetRoom); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (sourceArea == 0 || targetArea == 0) {
					if (sourceArea == 0) {
						sourceArea = targetArea;
					} else {
						targetArea = sourceArea;
					}
				}
				Hashtable<Integer, Integer> sourceAreaTable = roomExchangeTable.get(new Integer(sourceArea));
				Hashtable<Integer, Integer> targetAreaTable = roomExchangeTable.get(new Integer(targetArea));
				Integer newSourceRoom = (sourceAreaTable == null ? Integer.valueOf(0) : sourceAreaTable.get(new Integer(sourceRoom)));
				Integer newTargetRoom = (targetAreaTable == null ? Integer.valueOf(0) : targetAreaTable.get(new Integer(targetRoom)));
				Integer newSourceArea = areaExchangeTable.get(new Integer(sourceArea));
				Integer newTargetArea = areaExchangeTable.get(new Integer(targetArea));
				if (debugga) System.out.println("New: " + newSourceRoom + " - " + newTargetRoom + " - " + newSourceArea + " - " + newTargetArea); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				currentExit.setSourceRoomId((newSourceRoom == null ? 0 : newSourceRoom.intValue()));
				currentExit.setTargetRoomId((newTargetRoom == null ? 0 : newTargetRoom.intValue()));
				currentExit.setSourceArea((newSourceArea == null ? 0 : newSourceArea.intValue()));
				currentExit.setTargetArea((newTargetArea == null ? 0 : newTargetArea.intValue()));
				database.addExitFromXml(currentExit);
			} else {
				System.out.println("Coder been stupid, add: " + (roomExchangeTable == null ? "RoomExchangeTable!" : "") //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									+ (areaExchangeTable == null ? ", AreaExchangeTable!" : "")); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		if (eName.equalsIgnoreCase("room")) { //$NON-NLS-1$
			roomVector.add(currentRoom);
		}

		if (eName.equalsIgnoreCase("roomcolour")) { //$NON-NLS-1$
			colourTriggerVector.add(currentColourTrigger);
		}

		if (eName.equalsIgnoreCase("zone")) { //$NON-NLS-1$
			zoneVector.add(currentZone);
		}
		if (eName.equalsIgnoreCase("zoneroom")) { //$NON-NLS-1$
			zoneRoomVector.add(currentZoneRoom);
		}

		debug("</" + eName + ">"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @return the areaVector
	 */
	public ArrayList<Area> getAreaVector() {
		return areaVector;
	}

	/**
	 * @return the exitsVector
	 */
	public ArrayList<Exit> getExitsVector() {
		return exitsVector;
	}

	/**
	 * @return the iconVector
	 */
	public ArrayList<MapperIcon> getIconVector() {
		return iconVector;
	}

	/**
	 * @return the mapObjectVector
	 */
	public ArrayList<MapObject> getMapObjectVector() {
		return mapObjectVector;
	}

	public ArrayList<RoomColourHandle> getRoomColourVector() {
		return colourTriggerVector;
	}

	/**
	 * @return the roomVector
	 */
	public ArrayList<Room> getRoomVector() {
		ArrayList<Room> tmp = new ArrayList<Room>();
		for (Room room : roomVector) {
			tmp.add(room);
		}
		roomVector.clear();
		return tmp;
	}

	private void nl() throws SAXException {
		String lineEnd = System.getProperty("line.separator"); //$NON-NLS-1$
		debug(lineEnd);
	}

	/**
	 * @param iconVector
	 *            the iconVector to set
	 */
	public void setIconVector(ArrayList<MapperIcon> iconVector) {
		this.iconVector = iconVector;
	}

	@Override
	public void startDocument() throws SAXException {
		debug("START DOCUMENT"); //$NON-NLS-1$
		debug("<?xml version='1.0' encoding='UTF-8'?>"); //$NON-NLS-1$
	}

	@Override
	public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
		echoText();
		nl();
		debug("ELEMENT: "); //$NON-NLS-1$
		String eName = sName; // element name
		if ("".equals(eName)) //$NON-NLS-1$
								eName = qName; // not namespace-aware

		if (eName.equalsIgnoreCase("room")) { //$NON-NLS-1$
			debug("Creating New Room."); //$NON-NLS-1$
			currentRoom = new Room();
		} else if (eName.equalsIgnoreCase("exit")) { //$NON-NLS-1$
			debug("Creating New Exit."); //$NON-NLS-1$
			currentExit = new Exit();
		} else if (eName.equalsIgnoreCase("area")) { //$NON-NLS-1$
			debug("Creating New Area."); //$NON-NLS-1$
			currentArea = new Area();
		} else if (eName.equalsIgnoreCase("mappericon")) { //$NON-NLS-1$
			debug("Creating New MapperIcon."); //$NON-NLS-1$
			currentIcon = new MapperIcon();
		} else if (eName.equalsIgnoreCase("mapobject")) { //$NON-NLS-1$
			debug("Creating New MapObject."); //$NON-NLS-1$
			currentObject = new MapObject();
		} else if (eName.equalsIgnoreCase("roomcolour")) { //$NON-NLS-1$
			currentColourTrigger = new RoomColourHandle();
		} else if (eName.equalsIgnoreCase("zone")) { //$NON-NLS-1$
			currentZone = new Zone();
		} else if (eName.equalsIgnoreCase("zoneroom")) { //$NON-NLS-1$
			currentZoneRoom = new ZoneRoom();
		}
		currentElement = eName;

		debug("<" + eName); //$NON-NLS-1$
		debug(">"); //$NON-NLS-1$
	}

	/**
	 * @return the roomExchangeTable
	 */
	public Hashtable<Integer, Integer> getRoomExchangeTable(int areaId) {
		return roomExchangeTable.get(new Integer(areaId));
	}

	/**
	 * @param roomExchangeTable
	 *            the roomExchangeTable to set
	 */
	public void addRoomExchangeTable(int areaId, Hashtable<Integer, Integer> table) {
		roomExchangeTable.put(new Integer(areaId), table);
	}

	/**
	 * @return the areaExchangeTable
	 */
	public Hashtable<Integer, Integer> getAreaExchangeTable() {
		return areaExchangeTable;
	}

	/**
	 * @param areaExchangeTable
	 *            the areaExchangeTable to set
	 */
	public void setAreaExchangeTable(Hashtable<Integer, Integer> areaExchangeTable) {
		this.areaExchangeTable = areaExchangeTable;
	}

	public ArrayList<Zone> getZoneVector() {
		return zoneVector;
	}

	public ArrayList<ZoneRoom> getZoneRoomVector() {
		return zoneRoomVector;
	}

}
