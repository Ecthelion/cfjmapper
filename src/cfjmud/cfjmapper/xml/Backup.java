/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;

/**
 * @author Ecthelion
 * 
 */
public class Backup {
	private ZipOutputStream	zipFile;		// The real file to save to, in the
											// end at
	private final Mapper	mapper;
	private final Database	database;
	private final boolean	debug	= true;

	public Backup(Mapper mapper) {
		this.mapper = mapper;
		this.database = Database.getInstance();
	}

	public void save() {
		File file;
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		FileFilter ff = new FileFilter() {

			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) return true;
				String name = f.getName();
				String[] parts = name.split("\\."); //$NON-NLS-1$
				if (parts.length > 1) {
					if (parts[1].equalsIgnoreCase("cfj")) { //$NON-NLS-1$
						return true;
					} else {
						return false;
					}

				}
				return false;
			}

			@Override
			public String getDescription() {
				return "CFJ Files"; //$NON-NLS-1$
			}
		};
		fileChooser.setFileFilter(ff);
		int returnVal = fileChooser.showSaveDialog(mapper);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fileChooser.getSelectedFile();
		} else {
			return;
		}

		String fileName = file.getName();
		System.out.println("Filename: " + fileName); //$NON-NLS-1$
		String parts[] = file.getName().split("\\."); //$NON-NLS-1$
		if (parts.length < 2) {
			file = new File(file.getAbsolutePath() + ".cfj"); //$NON-NLS-1$
		}
		try {
			zipFile = new ZipOutputStream(new FileOutputStream(file));

			zipFile.putNextEntry(new ZipEntry("areas.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllAreas(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			zipFile.putNextEntry(new ZipEntry("rooms.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllRooms(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			zipFile.putNextEntry(new ZipEntry("exits.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllExits(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			zipFile.putNextEntry(new ZipEntry("objects.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllObjects(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			database.writeAllIcons(zipFile);
			zipFile.putNextEntry(new ZipEntry("icons.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllIconsAsXml(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			zipFile.putNextEntry(new ZipEntry("colourtriggers.xml")); //$NON-NLS-1$
			zipFile.write("<?xml version='1.0' encoding='UTF-8'?>\n".getBytes()); //$NON-NLS-1$
			zipFile.write("<root>\n".getBytes()); //$NON-NLS-1$
			database.writeAllColourTriggers(zipFile);
			zipFile.write("</root>".getBytes()); //$NON-NLS-1$

			zipFile.flush();
			zipFile.close();
		} catch (IOException e) {
			System.out.println("Backup Failed!\n" + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	private void debug(String string) {
		if (debug) System.out.println(string);
	}

}
