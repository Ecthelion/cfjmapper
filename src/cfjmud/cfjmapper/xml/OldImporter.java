/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.xml;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.RoomColourHandle;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.objects.Room;

/**
 * @author Ecthelion
 * 
 */
public class OldImporter {
	private int							areaToImportTo;
	Database							database;
	Hashtable<String, BufferedImage>	images;
	Mapper								mapper;
	ZipFile								zipFile;
	boolean								restore	= false;

	public OldImporter(Mapper mapper, boolean restore) {
		database = Database.getInstance();
		this.mapper = mapper;
		this.restore = restore;
		// Prepare First then...
		File file;
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		fileChooser.setFileFilter(getFilter());
		int returnVal = fileChooser.showOpenDialog(mapper);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fileChooser.getSelectedFile();
		} else {
			return;
		}
		if (restore) {
			int result = JOptionPane.showConfirmDialog(mapper, "Really want to restore the Map, this will delete ALL areas.", "Restore?", //$NON-NLS-1$ //$NON-NLS-2$
														JOptionPane.YES_NO_OPTION);
			if (result == JOptionPane.NO_OPTION) return;
			database.truncateDB();
		}
		try {
			zipFile = new ZipFile(file);
			load();
		} catch (Exception e) {
			System.out.println("Error! Couldn't load file: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	private FileFilter getFilter() {
		FileFilter ff = new FileFilter() {

			@Override
			public boolean accept(File f) {
				if (f.isDirectory()) return true;
				String name = f.getName();
				String[] parts = name.split("\\."); //$NON-NLS-1$
				if (parts.length > 1) {
					if (parts[1].equalsIgnoreCase("cfj")) { //$NON-NLS-1$
						return true;
					} else {
						return false;
					}

				}
				return false;
			}

			@Override
			public String getDescription() {
				return "CFJ Files (.cfj)"; //$NON-NLS-1$
			}
		};
		return ff;
	}

	private void load() throws SAXException, IOException, ParserConfigurationException {
		images = new Hashtable<String, BufferedImage>();
		for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements();) {
			ZipEntry entry = e.nextElement();
			String name = entry.getName();
			if (!name.equals("area.xml") && !name.equals("areas.xml") && !name.equals("rooms.xml") && !name.equals("objects.xml") && !name.equals("icons.xml") && !name.equals("exits.xml") && !name.equals("colourtriggers.xml")) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
				int BUFFER = 4096;
				int size = (int) entry.getSize();
				if (size == -1) continue;
				BufferedInputStream dest = new BufferedInputStream(zipFile.getInputStream(entry), BUFFER);
				images.put(name, ImageIO.read(dest));
			}
		}
		OldXMLHandler handler = new OldXMLHandler();

		SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
		ZipEntry areas = zipFile.getEntry("areas.xml"); //$NON-NLS-1$
		ZipEntry rooms = zipFile.getEntry("rooms.xml"); //$NON-NLS-1$
		ZipEntry objects = zipFile.getEntry("objects.xml"); //$NON-NLS-1$
		ZipEntry icons = zipFile.getEntry("icons.xml"); //$NON-NLS-1$
		ZipEntry exits = zipFile.getEntry("exits.xml"); //$NON-NLS-1$
		saxParser.parse(zipFile.getInputStream(areas), handler);
		saxParser.parse(zipFile.getInputStream(rooms), handler);
		saxParser.parse(zipFile.getInputStream(objects), handler);
		saxParser.parse(zipFile.getInputStream(icons), handler);
		if (restore) {
			ZipEntry roomColourtriggers = zipFile.getEntry("colourtriggers.xml"); //$NON-NLS-1$
			saxParser.parse(zipFile.getInputStream(roomColourtriggers), handler);
			System.out.println(handler.getRoomColourVector().size());
			for (RoomColourHandle handle : handler.getRoomColourVector()) {
				if (!handle.getText().isEmpty()) database.addColourForRoomType(handle);
			}
		}

		Hashtable<Integer, Integer> icontable = addIcons(handler.getIconVector());
		Hashtable<Integer, Integer> areatable = addAreas(handler.getAreaVector(), icontable);
		System.out.println("Areas Imported"); //$NON-NLS-1$
		addObjects(handler.getMapObjectVector(), areatable, icontable);
		database.startRestore();
		Hashtable<Integer, Integer> roomtable = addRooms(handler.getRoomVector(), areatable);
		System.out.println("Rooms Imported"); //$NON-NLS-1$
		handler.setAreaExchangeTable(areatable);
		handler.setRoomExchangeTable(roomtable);
		saxParser.parse(zipFile.getInputStream(exits), handler);
		System.out.println("Exits Imported"); //$NON-NLS-1$
		database.endRestore();
	}

	private Hashtable<Integer, Integer> addRooms(ArrayList<Room> roomVector, Hashtable<Integer, Integer> areatable) {
		Hashtable<Integer, Integer> toReturn = new Hashtable<Integer, Integer>();
		int oldId = 0;
		int newId = 0;
		for (Room room : roomVector) {
			int areaId = areatable.get(new Integer(room.getAreaId())).intValue();
			//System.out.println("Replacing old AreaId:" + room.getAreaId() + " in room to:" + areaId); //$NON-NLS-1$ //$NON-NLS-2$
			try {
				oldId = room.getId();
				room.setAreaId(areaId);
				newId = database.addXmlRoom(room);
				toReturn.put(new Integer(oldId), new Integer(newId));
				room.setId(newId);
			} catch (Exception e) {
				System.out.println("Failed to update Rooms: " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
		return toReturn;
	}

	private Hashtable<Integer, Integer> addIcons(ArrayList<MapperIcon> iconVector) {
		Hashtable<Integer, Integer> iconTransferTable = new Hashtable<Integer, Integer>();
		int oldId = 0;
		int newId = 0;
		for (MapperIcon icon : iconVector) {
			try {
				oldId = icon.getId();
				BufferedImage image = images.get(icon.toFileName());
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(image, "png", os); //$NON-NLS-1$
				InputStream fis = new ByteArrayInputStream(os.toByteArray());
				newId = database.addIcon(fis, icon.toFileName(), os.toByteArray().length);
				iconTransferTable.put(new Integer(oldId), new Integer(newId));
			} catch (Exception e) {
				System.out.println("Failed! : " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
				continue;
			}
		}
		return iconTransferTable;
	}

	private void addObjects(ArrayList<MapObject> objects, Hashtable<Integer, Integer> areaTable, Hashtable<Integer, Integer> icontable) {
		for (MapObject object : objects) {
			Integer newAreaId = areaTable.get(new Integer(object.getAreaId()));
			Integer iconId = icontable.get(object.getIconId());
			if (newAreaId != null) object.setAreaId(newAreaId.intValue());
			if (iconId != null) object.setIconId(iconId.intValue());
			database.addObject(object);
		}
	}

	private Hashtable<Integer, Integer> addAreas(ArrayList<Area> areas, Hashtable<Integer, Integer> icontable) {
		Hashtable<Integer, Integer> areaTranferTable = new Hashtable<Integer, Integer>();
		int oldId = 0;
		int newId = 0;
		for (Area area : areas) {
			boolean hasParentInList = false;
			for (Area subArea : areas) {
				if (area.getParent() == subArea.getId()) {
					hasParentInList = true;
				}
			}
			if (!hasParentInList) area.setParent(areaToImportTo);
		}
		try {
			for (Area area : areas) {
				oldId = area.getId();
				newId = database.addArea(area);
				areaTranferTable.put(new Integer(oldId), new Integer(newId));
				area.setId(newId);
			}
			for (Area area : areas) {
				Integer iconId = icontable.get(new Integer(area.getIcon()));
				if (iconId != null) area.setIcon(iconId.intValue());
				Integer areaId = areaTranferTable.get(new Integer(area.getParent()));
				//System.out.println("Setting parent of:" + area.getId() + " to: " + areaId + " (Old Parent Was: " + area.getParent() + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				if (areaId != null) area.setParent(areaId.intValue());
				database.updateArea(area);
			}
		} catch (Exception e) {
			System.out.println("Failed to Update Areas: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		return areaTranferTable;
	}
}
