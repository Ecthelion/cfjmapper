/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.StringTokenizer;

/**
 * 
 * @author Ecthelion
 * 
 */
public class Communicator extends Thread {
	InetAddress				address			= null;
	int						bufferLength	= 4096;
	private final boolean	debug			= false;
	int						listeningPort	= 4444;
	Mapper					mapper;
	DatagramPacket			packet			= null;
	int						sendingPort		= 4445;

	public Communicator(Mapper mapper) {
		this.mapper = mapper;
	}

	private MudCommand createChangeAliasCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[2];
		args[0] = rec[2];
		return new MudCommand(Mapper.ALIAS, Messages.getString("Communicator.0"), args); //$NON-NLS-1$
	}

	private MudCommand createJourneyCommand(String recieved) {
		String[] rec = recieved.split(Messages.getString("Communicator.6")); //$NON-NLS-1$
		if (rec.length < 3) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.JOURNEY, Messages.getString("Communicator.8"), args); //$NON-NLS-1$
	}

	private MudCommand createMapCommand(String recieved) {
		String[] rec = recieved.split(Messages.getString("Communicator.9")); //$NON-NLS-1$
		if (rec.length < 6) return null;
		String[] args = new String[4];
		args[0] = rec[2];
		args[1] = rec[3];
		args[2] = rec[4];
		args[3] = rec[5];
		// new MudCommand("map", "", args);
		return new MudCommand(Mapper.MAP, Messages.getString("Communicator.11"), args); //$NON-NLS-1$
	}

	private MudCommand createMoveCommand(String received) {
		String[] rec = received.split(Messages.getString("Communicator.12")); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.MOVE, Messages.getString("Communicator.15"), args); //$NON-NLS-1$
	}

	private MudCommand createNotificationCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.NOTIFICATION, "", args); //$NON-NLS-1$
	}

	private MudCommand createSetProfileCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.SETPROFILE, "", args); //$NON-NLS-1$
	}

	private MudCommand createRemoveNotificationCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.REMOVENOTIFICATION, "", args); //$NON-NLS-1$
	}

	private MudCommand createSHMoveCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.SHMOVE, "", args); //$NON-NLS-1$
	}

	private MudCommand createSHStopCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[1];
		args[0] = rec[2];
		return new MudCommand(Mapper.SHSTOP, "", args); //$NON-NLS-1$
	}

	private MudCommand createTeleportCommand(String received) {
		String[] rec = received.split(":"); //$NON-NLS-1$
		if (rec.length < 2) return null;
		String[] args = new String[2];
		args[0] = rec[2];
		return new MudCommand(Mapper.JUMP_TO_ROOM, "", args); //$NON-NLS-1$
	}

	@Override
	public void run() {
		DatagramSocket socket = null;
		try {
			address = InetAddress.getByName("127.0.0.1"); //$NON-NLS-1$
			socket = new DatagramSocket(listeningPort, address);
			byte[] buf = new byte[bufferLength];
			// DatagramPacket packet = new DatagramPacket(buf, buf.length,
			// address, listeningPort);
			packet = new DatagramPacket(buf, buf.length);
			while (true) {
				socket.receive(packet);
				String received = new String(packet.getData()).trim();
				if (debug) System.out.println(received);

				StringTokenizer stringTokenizer = new StringTokenizer(received, ":"); //$NON-NLS-1$
				if (received.equalsIgnoreCase("Mapper:Exit")) { //$NON-NLS-1$
					break;
				}
				if (stringTokenizer.nextToken().equalsIgnoreCase("Mapper")) { //$NON-NLS-1$
					String command = stringTokenizer.nextToken();
					if (command.equalsIgnoreCase("MAP")) { //$NON-NLS-1$
						mapper.sendCommand(createMapCommand(received));
					} else if (command.equalsIgnoreCase("MOVE")) { //$NON-NLS-1$
						mapper.sendCommand(createMoveCommand(received));
					} else if (command.equalsIgnoreCase("JOURNEY")) { //$NON-NLS-1$
						mapper.sendCommand(createJourneyCommand(received));
					} else if (command.equalsIgnoreCase("TELEPORT")) { //$NON-NLS-1$
						mapper.sendCommand(createTeleportCommand(received));
					} else if (command.equalsIgnoreCase("ALIAS")) { //$NON-NLS-1$
						mapper.sendCommand(createChangeAliasCommand(received));
					} else if (command.equalsIgnoreCase("SHMOVE")) { //$NON-NLS-1$SHSTOP
						mapper.sendCommand(createSHMoveCommand(received));
					} else if (command.equalsIgnoreCase("SHSTOP")) { //$NON-NLS-1$
						mapper.sendCommand(createSHStopCommand(received));
					} else if (command.equalsIgnoreCase("NOTIFICATION")) { //$NON-NLS-1$
						mapper.sendCommand(createNotificationCommand(received));
					} else if (command.equalsIgnoreCase("REMOVENOTIFICATION")) { //$NON-NLS-1$
						mapper.sendCommand(createRemoveNotificationCommand(received));
					} else if (command.equalsIgnoreCase("SETPROFILE")) { //$NON-NLS-1$
						mapper.sendCommand(createSetProfileCommand(received));
					}

				}

				buf = new byte[bufferLength];
				packet.setData(buf);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		} finally {
			if (socket != null) socket.close();
		}
	}

	private void send(String msg) {
		try {
			DatagramSocket socket = new DatagramSocket();// sendingPort,
			// address);
			byte[] toSend = msg.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(toSend, toSend.length, address, sendingPort);
			socket.send(sendPacket);
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Messages.getString("Communicator.1") + e.toString()); //$NON-NLS-1$
		}
	}

	/**
	 * Sends a signal to the mushClient that the mapper is in Map mode
	 */
	public void sendMap() {
		send(Messages.getString("Communicator.4")); //$NON-NLS-1$
	}

	/**
	 * Sends a signal to the mushClient that the mapper is in Move mode
	 */
	public void sendMove() {
		send(Messages.getString("Communicator.5")); //$NON-NLS-1$
	}

	/**
	 * pattern should be in the format "^(w|west)$" with replacement for example
	 * "wander alley"
	 * 
	 * @param pattern
	 *            the exits to replace
	 * @param replacement
	 *            the replacement of the above
	 */
	public void sendReplace(String pattern, String replacement) {
		send("MAPPER:EXITREPLACE:" + pattern + ":" + replacement); //$NON-NLS-1$ //$NON-NLS-2$

	}

	/**
	 * Sending signal to mushClient that the Mapper is shutting down
	 */
	public void sendShutDown() {
		send(Messages.getString("Communicator.3")); //$NON-NLS-1$
	}

	/**
	 * Sending signal to mushClient that the Mapper is starting up
	 */
	public void sendStartUp() {
		send(Messages.getString("Communicator.2")); //$NON-NLS-1$
	}

}
