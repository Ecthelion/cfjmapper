/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.plaf.synth.SynthLookAndFeel;

import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.RoomRep;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.AreaChooser;
import cfjmud.cfjmapper.dialogs.AreaDialog;
import cfjmud.cfjmapper.dialogs.ExitDialog;
import cfjmud.cfjmapper.dialogs.ExportAsImageDialog;
import cfjmud.cfjmapper.dialogs.MapObjectDialog;
import cfjmud.cfjmapper.dialogs.MoveDialog;
import cfjmud.cfjmapper.dialogs.RoomEditDialog;
import cfjmud.cfjmapper.dialogs.RoomFinderDialog;
import cfjmud.cfjmapper.dialogs.ZoneDialog;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.menus.MainToolBar;
import cfjmud.cfjmapper.menus.MapperMenuBar;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.AreaPanel;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.panels.AreaTreeViewer;
import cfjmud.cfjmapper.panels.MapPanel;
import cfjmud.cfjmapper.panels.MapView;
import cfjmud.cfjmapper.panels.ZonesWindow;
import cfjmud.cfjmapper.profile.ProfileManager;
import cfjmud.cfjmapper.profile.User;
import cfjmud.cfjmapper.xml.Exporter;

/**
 * The Main Class! :P
 * 
 * @author Ecthelion
 * 
 */
public class Mapper extends JFrame implements MapperConstants, KeyListener, WindowListener, ComponentListener, FocusListener {

	private static String		lastPath			= "";					//$NON-NLS-1$

	private static Mapper		mapper;

	public static Color			oldColor			= Color.RED;
	/**
     * 
     */
	private static final long	serialVersionUID	= 7868853740270598676L;
	private static int			startingXPos		= Integer.MAX_VALUE / 2, startingYPos = Integer.MAX_VALUE / 2;

	private static ImageIcon	theIconToUse;

	private static String		profileName			= null;

	private static boolean		showProfileDialogAtStartUp;

	public static ImageIcon getIcon() {
		return theIconToUse;
	}

	/**
	 * @return the startingXPos
	 */
	public static int getStartingXPos() {
		return startingXPos;
	}

	/**
	 * @return the startingYPos
	 */
	public static int getStartingYPos() {
		return startingYPos;
	}

	public static void main(String[] args) {
		System.out.println("CFJMapper - " + Mapper.class.getPackage().getImplementationVersion() + " Copyright (C) 2013  Carl F Jansson aka Ecthelion\n" //$NON-NLS-1$ //$NON-NLS-2$
							+ "This program comes with ABSOLUTELY NO WARRANTY; for details see `About'.\n" //$NON-NLS-1$
							+ "This is free software, and you are welcome to redistribute it\n" + "under certain conditions; see `About' for details.\n"); //$NON-NLS-1$ //$NON-NLS-2$

		File file = new File("Mapper.ini"); //$NON-NLS-1$
		if (!file.exists()) {
			if (ContractHandler.showContractConfirmer() != ContractHandler.ACCEPTED) {
				System.exit(-1);
			}

			int repo = JOptionPane.showConfirmDialog(null, "First Time Running, Edit The Pref's?", "Preferences", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
			MapperProperties.getInstance().createDefaultValues();
			if (repo == JOptionPane.YES_OPTION) {
				MapperProperties.showPreferenceDialog(mapper);
			}
			MapperProperties.getInstance().save();

		}
		if (args.length != 0) {
			boolean noLog = false;
			boolean setProfile = false;
			for (String arg : args) {
				if (setProfile) {
					setProfile = false;
					System.out.println("Setting to profile: " + arg); //$NON-NLS-1$
					setProfile(arg);
				}
				if (arg.equalsIgnoreCase("-nl")) { //$NON-NLS-1$
					noLog = true;
				}
				if (arg.equalsIgnoreCase("-p")) { //$NON-NLS-1$
					setProfile = true;
				}

				if (arg.equalsIgnoreCase("-cpd")) { //$NON-NLS-1$
					setShowProfileDialogAtStartup(true);
				}

				if (arg.equalsIgnoreCase("-help")) { //$NON-NLS-1$
					System.out.println("-help\t\t\tShow this..."); //$NON-NLS-1$
					System.out.println("-p <profilename>\tSet the Profile."); //$NON-NLS-1$
					System.out.println("-nl\t\t\tShow error messages directly, don't use the CFJLog.txt logFile."); //$NON-NLS-1$
					System.out.println("-cpd\t\t\tShow a dialog at startup to choose the Profile to be used."); //$NON-NLS-1$
					System.exit(0);
				}
			}
			if (!noLog) {
				try {
					File logFile = new File("CFJLog.txt"); //$NON-NLS-1$
					PrintStream ps = new PrintStream(logFile);
					System.setOut(ps);
				} catch (FileNotFoundException e) {
					System.out.println(e.toString());
					e.printStackTrace();
				}
			}
		}
		//Font font = Font.decode("Font SansSerif 10"); //$NON-NLS-1$
		try {
			setLookAndFeel();
		} catch (UnsupportedLookAndFeelException | ClassNotFoundException | IllegalAccessException | InstantiationException | ParseException | IOException e) {
			System.out.println("Couldn't find the L&F: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		Locale.setDefault(Locale.ENGLISH);
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		System.out.println("Starting up the Mapper..."); //$NON-NLS-1$
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Mapper.mapper = new Mapper();
			}
		});
	}

	private static void setShowProfileDialogAtStartup(boolean showProfileDialogAtStartUp) {
		Mapper.showProfileDialogAtStartUp = showProfileDialogAtStartUp;

	}

	private static void setProfile(String profileName) {
		Mapper.profileName = profileName;
	}

	private final static boolean	custom	= false;

	private static void setLookAndFeel() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException,
			ParseException, IOException {

		if (custom) {
			String lfFile = "lnf.xml"; //$NON-NLS-1$ 
			SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
			InputStream is = Mapper.class.getResourceAsStream(lfFile);
			if (is != null) {
				lookAndFeel.load(is, Mapper.class);
			} else {
				File file = new File(lfFile);
				FileInputStream fi = new FileInputStream(file);
				lookAndFeel.load(fi, Mapper.class);
			}
			if (lookAndFeel != null) {
				UIManager.setLookAndFeel(lookAndFeel);
			}
		} else {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		}
	}

	/**
	 * @param startingXPos
	 *            the startingXPos to set
	 */
	public static void setStartingXPos(int startingXPos) {
		Mapper.startingXPos = startingXPos;
	}

	/**
	 * @param startingYPos
	 *            the startingYPos to set
	 */
	public static void setStartingYPos(int startingYPos) {
		Mapper.startingYPos = startingYPos;
	}

	private JDialog								colourChooser;
	private JColorChooser						colourChooserPanel;
	private Communicator						communicator		= null;
	private Point								copyPosition;
	private Color								currentColour;
	private Room								notificationRoom	= null;
	private Database							database			= null;

	private final Hashtable<Integer, String>	notifications;
	private MapperProperties					properties			= null;
	private JScrollPane							scrollpaneForTreeViewForAreas;

	private int									startLevel;

	private int									endLevel;

	private int									startArea;

	private int									endArea;

	private int									connectVar			= 1;
	private boolean								shadowing			= true, completlyNewMapDatabase, replaceExits = false, repaintingMainMap = false,
			showSpecialExits, keyboardMapping = false, updateMainMap = false, autojourney = false, centerOnCurrentRoom = true;
	private final LinkedList<Integer>			shadowQueue;
	private JSplitPane							splitPane;

	private JPanel								statusField;

	private JLabel								statusLabel;

	private MapperMenuBar						theMenuBar;
	private MainToolBar							toolPanel;
	long										totTime				= 0, no = 0;
	private AreaTreeViewer						treeViewer;
	private final ProfileManager				profileManager;

	MapView										mapView;

	/**
	 * 
	 * The Constructor of the Class <class>Mapper</class>... what else can it
	 * be... bother...
	 * 
	 * @author Ecthelion
	 */
	public Mapper() {
		super();
		mapper = this;
		createColourChooser();
		notifications = new Hashtable<Integer, String>();
		communicator = new Communicator(this);
		communicator.start();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		URL url = this.getClass().getResource("/Icon.png"); //$NON-NLS-1$
		if (url != null) {
			theIconToUse = new ImageIcon(url);
		} else {
			theIconToUse = new ImageIcon("./icons/Icon.png", "The Icon"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		setIconImage(theIconToUse.getImage());
		setIconImage(Mapper.getIcon().getImage());
		properties = MapperProperties.getInstance();
		Dimension preferredSize = new Dimension(Integer.parseInt(properties.getProperty(MapperProperties.WINDOWWIDTH)),
												Integer.parseInt(properties.getProperty(MapperProperties.WINDOWHEIGHT)));
		setSize(preferredSize);
		setPreferredSize(preferredSize);
		setLocation(Integer.parseInt(properties.getProperty(MapperProperties.WINDOWSTARTX)),
					Integer.parseInt(properties.getProperty(MapperProperties.WINDOWSTARTY)));
		setTitle("CFJMapper - " + Mapper.class.getPackage().getImplementationVersion()); //$NON-NLS-1$
		database = Database.getInstance();
		profileManager = new ProfileManager(mapper);
		if (showProfileDialogAtStartUp) profileName = profileManager.showStartUpDialog();
		if (profileManager.hasUser(profileName)) profileManager.setCurrentUser(profileName);
		String enabledString = MapperProperties.getInstance().getProperty(MapperProperties.CENTREONCURRENTROOM);
		centerOnCurrentRoom = (enabledString == null ? true : Boolean.parseBoolean(enabledString));

		String showSpecialExitsString = MapperProperties.getInstance().getProperty(MapperProperties.SHOWSPECIALEXITS);
		String autojourneyString = MapperProperties.getInstance().getProperty(MapperProperties.AUTOJOURNEY);
		if (autojourneyString != null) setAutojourney(Boolean.parseBoolean(autojourneyString));
		showSpecialExits = (showSpecialExitsString == null ? true : Boolean.parseBoolean(showSpecialExitsString));
		mapView = new MapView(this);
		int currentAreaId = -1;
		int currentRoomId = -1;
		if (profileManager.getCurrentAreaId() != -1 && profileManager.getCurrentRoomId() != -1) {
			currentAreaId = profileManager.getCurrentAreaId();
			currentRoomId = profileManager.getCurrentRoomId();
		} else {
			currentAreaId = Integer.parseInt(properties.getProperty(MapperProperties.STARTINGAREAID));
			currentRoomId = Integer.parseInt(properties.getProperty(MapperProperties.STARTINGROOM));
		}
		mapView.setCurrentArea(database.getArea(currentAreaId));
		mapView.setCurrentRoomId(currentRoomId);

		if (mapView.getCurrentArea() == null) {
			mapView.setCurrentArea(database.getAnyArea());
		}

		try {
			if (mapView.getCurrentArea() == null) {
				database.createDefaultValues();
				mapView.setCurrentArea(database.getAnyArea());
			}
		} catch (SQLException e) {
			System.out.println("Failed to create Default Values." + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
		currentAreaId = (mapView.getCurrentArea() != null ? mapView.getCurrentArea().getId() : 0);
		currentRoomId = mapView.getCurrentRoomId();
		Room tmpRoom = database.getRoom(currentAreaId, currentRoomId);
		if (tmpRoom == null) {
			Room tr = database.getOldestRoomInArea(mapView.getCurrentArea().getId());
			mapView.setCurrentRoomId(0);
			if (mapView.getCurrentArea() != null && tr != null) {
				mapView.setCurrentRoomId(tr.getId());
				mapView.setCurrentRoom(tr);
			}
		}
		shadowQueue = new LinkedList<Integer>();
		initiateComponents();
		updateMainMap();
		communicator.sendStartUp();
		communicator.sendMove();
		if (tmpRoom != null) gotoRoom(tmpRoom);
	}

	/**
	 * 
	 * @param newArea
	 */
	public void addArea(boolean newArea) {
		String s = (String) JOptionPane.showInputDialog(this, "What do you want the name of your new area to be?", "New Area\\!", JOptionPane.PLAIN_MESSAGE, //$NON-NLS-1$ //$NON-NLS-2$
														null, null, "Pizza"); //$NON-NLS-1$
		int newAreaId = 0;
		if (newArea && s != null) {
			newAreaId = database.addArea(s, 0);
			System.out.println("To Be Adding a new area of the name: " + s); //$NON-NLS-1$
		} else if (!newArea && s != null) {
			if (treeViewer.getSelectedArea() != null) {
				newAreaId = database.addArea(s, treeViewer.getSelectedArea().getId());
			} else {
				newAreaId = database.addArea(s, 0);
			}
		} else {
			return;
		}
		// treeViewer.setAreas(database.getAreas());
		treeViewer.addArea(database.getArea(newAreaId));
		validate();

	}

	public void addColourTriggerFromRoomNameMenuItemActionPerformed() {
		if (mapView.getHighlightedRooms().size() == 0) return;
		String theNameOfTheRoom = mapView.getHighlightedRooms().get(0).getName();
		RoomColourHandle handle = RoomColour.showAddColourDialog(this, theNameOfTheRoom);
		if (handle != null) {
			RoomColour.addColour(handle);
		} else {
			return;
		}
		int answer = JOptionPane.showConfirmDialog(this, "Change All Colours on the Map?", "Colour Change", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$

		if (answer == JOptionPane.YES_OPTION) database.updateColoursForRoomName(handle);
		mapView.updateView();
	}

	private void addNotificationForCurrentRoom(String string) {
		mapView.getCurrentRoom().setNotification(string);
		notificationRoom = mapView.getCurrentRoom();
		if (string.trim().isEmpty()) {
			notifications.remove(new Integer(mapView.getCurrentRoom().getId()));
		} else {
			notifications.put(new Integer(mapView.getCurrentRoom().getId()), string);
		}

		mapView.updateView();
	}

	/**
     * 
     */
	public void addObjectButtonActionPerformed() {
		toolPanel.resetButtons();
		if (mapView.isAddNewObject()) {
			mapView.setAddNewObject(false);
			setStatus(""); //$NON-NLS-1$
		} else {
			toolPanel.setColourOfAddObjectButton(MainToolBar.getEnabledColour());
			mapView.setAddNewObject(true);
			setStatus("Press on map where you want a new Picture."); //$NON-NLS-1$
		}
	}

	/**
	 * Adds a new Room at point p
	 * 
	 * @param room
	 * @param point
	 * @return
	 */
	public int addRoom(Room room) {
		setStatus(room.getName());
		return database.addRoom(room);
	}

	public void advancedFollowingMenuItemActionPerformed(ItemEvent ie) {
		if (ie.getStateChange() == ItemEvent.SELECTED) {
			shadowing = true;
			mapper.setStatus("Shadow is Following..."); //$NON-NLS-1$
		} else if (ie.getStateChange() == ItemEvent.DESELECTED) {
			shadowing = false;
			mapper.setStatus("Shadow is not Following..."); //$NON-NLS-1$
		}
		mapView.setCurrentShadowId(mapView.getCurrentRoomId());
		mapView.moveShadowRoomToCurrentRoom();
		shadowQueue.clear();
		updateMainMap();

	}

	/**
     * 
     */
	@Deprecated
	public void centerViewport() {
		System.out.println("NOT TO BE USED ANYMORE\\!\\!\\!\\! Never worked anyway. ;)"); //$NON-NLS-1$
	}

	/**
	 * Change the alias on currentRoom.
	 * 
	 * @param alias
	 * @return
	 */
	private boolean changeAliasOnCurrentRoom(String alias) {
		boolean debug = false;
		if (debug) System.out.println("Changing alias to: " + alias); //$NON-NLS-1$
		mapView.getCurrentRoom().setAlias(alias);
		database.updateRoom(mapView.getCurrentArea().getId(), mapView.getCurrentRoom());
		updateMainMap();
		return true;
	}

	public void changeNameAreaPopupMenuActionPerformed() {
		if (treeViewer.getSelectedArea() == null) return;
		Area area = treeViewer.getSelectedArea();
		String inputValue = JOptionPane.showInputDialog("Please select a new Name: ", area.getName()); //$NON-NLS-1$
		area.setName(inputValue);
		database.updateArea(area);
		updateTree();
	}

	public void changeToClimbMenuItemActionPerformed() {

		if (mapView.getSelectedExit() != null) {
			mapView.getSelectedExit().setSourceType(CLIMB_TYPE);
			mapView.getSelectedExit().setTargetType(CLIMB_TYPE);
			database.updateExit(null, mapView.getSelectedExit());
			mapView.updateView();
		}
	}

	public void changeToNormalMenuItemActionPerformed() {
		if (mapView.getSelectedExit() != null) {
			mapView.getSelectedExit().setSourceType(NORMAL_TYPE);
			mapView.getSelectedExit().setTargetType(NORMAL_TYPE);
			database.updateExit(null, mapView.getSelectedExit());
			mapView.updateView();
		}
	}

	public void changeToSwimMenuItemActionPerformed() {
		if (mapView.getSelectedExit() != null) {
			mapView.getSelectedExit().setSourceType(SWIM_TYPE);
			mapView.getSelectedExit().setTargetType(SWIM_TYPE);
			database.updateExit(null, mapView.getSelectedExit());
			mapView.updateView();
		}
	}

	public void close() {
		communicator.sendShutDown();
		properties.save(this);
		setVisible(false);
		dispose();
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// Invoked when the component has been made invisible.
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// Invoked when the component's position changes.
	}

	@Override
	public void componentResized(ComponentEvent e) {
		mapView.calculateSizes();
		// Invoked when the component's size changes.
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// Invoked when the component has been made visible.
	}

	/**
	 * 
	 * @param exits
	 *            array with HEX ints...
	 * @return array with HEX ints...
	 * @author Ecthelion
	 */
	private int[] correctExits(int[] exits) {
		boolean debug = false;
		int[] toReturn = new int[exits.length];
		for (int i = 0; i < exits.length; i++) {
			if (debug) System.out.println(exits[i]);
			toReturn[i] = exits[i] % BASE;
		}
		return toReturn;
	}

	private void createColourChooser() {
		ActionListener colorChooserOkButtonListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentColour = colourChooserPanel.getColor();
				colourChooser.setVisible(false);
			}
		};

		ActionListener colourChooserCancelButtonListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				currentColour = null;
				colourChooser.setVisible(false);
			}
		};
		colourChooserPanel = new JColorChooser();
		colourChooser = JColorChooser.createDialog(mapper, "Choose Colour", true, colourChooserPanel, colorChooserOkButtonListener, //$NON-NLS-1$
													colourChooserCancelButtonListener);
	}

	private void createExit(Room oldRoom, Room newRoom, int direction, int startType, int endType) {
		ExitsManager.createExit(oldRoom, newRoom, direction, mapView.getCurrentArea().getId(), startType, endType);
	}

	private void createExits(Room oldRoom, Room newRoom, int direction, int[] exits, int[] startType, int[] endType) {
		/*
		 * mappingLevelSubMenu.add(noExitsExceptWalkedSubMenuItem);
		 * mappingLevelSubMenu.add(connectStubsSubMenuItem);
		 * mappingLevelSubMenu.add(createToExistingIncStubSubMenuItem);
		 * mappingLevelSubMenu.add(makeAllExitsAndConnectSubMenuItem);
		 */
		Point np = newRoom.getPosition();
		// xPos = (int) newRoom.getPosition().getX();
		// yPos = (int) newRoom.getPosition().getY();
		switch (connectVar) {
			case 0: // Only create the walked exit., int currentLevel, int
				// currentAreaId
				ExitsManager.createOnlyWalkedExits(	oldRoom, newRoom, direction, exits, newRoom.getLevel(), mapView.getCurrentArea().getId(), np.x, np.y,
													mapView.getCurrentArea().getPreferredRoomDistance(), startType, endType);
				break;
			case 1: // Create Exits in exits[] and connect them if possible...
				ExitsManager.connectStubs(	oldRoom, newRoom, direction, exits, newRoom.getLevel(), mapView.getCurrentArea().getId(), np.x, np.y,
											mapView.getCurrentArea().getPreferredRoomDistance(), startType, endType);
				break;
			case 2: //
				ExitsManager.createExistsToExistingRooms(	oldRoom, newRoom, direction, exits, newRoom.getLevel(), mapView.getCurrentArea().getId(), np.x,
															np.y,
															mapView.getCurrentArea().getPreferredRoomDistance(), startType, endType);
				break;
			case 3:
				ExitsManager.createAllExits(oldRoom, newRoom, direction, exits, newRoom.getLevel(), mapView.getCurrentArea().getId(), np.x, np.y,
											mapView.getCurrentArea().getPreferredRoomDistance(), startType, endType);
				break;
		}
	}

	public void deleteAreaMenuItemActionPerformed(boolean tree) {
		Area areaToDelete;
		if (tree) {
			areaToDelete = treeViewer.getSelectedArea();
		} else {
			areaToDelete = mapView.getCurrentSelectedArea();
		}

		if (areaToDelete == null) return;
		if (areaToDelete.getId() == 0) return;

		int answer = JOptionPane.showConfirmDialog(this, "Do You Really Want to Delete the Area?\nIt will delete ALL the rooms AND Areas within!", //$NON-NLS-1$
													"Delete Area", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
		if (answer == JOptionPane.YES_OPTION) {
			database.deleteArea(areaToDelete);
			treeViewer.deleteArea(areaToDelete);
		}
		checkForAreasAndRepairIfNoneLeft();
		updateMainMap();
	}

	private void checkForAreasAndRepairIfNoneLeft() {
		if (database.getAreas().isEmpty()) {
			try {
				database.createDefaultValues();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mapView.setCurrentRoom(null);
			mapView.setCurrentShadowRoom(null);
			mapView.setCurrentRoomId(0);
			mapView.setCurrentShadowId(0);
			treeViewer.setAreas(database.getAreas());
			treeViewer.setSelectedArea(database.getAnyArea());
			setCurrentArea(database.getAnyArea());
			updateMainMap();
		}

	}

	public void deleteExitMenuItemActionPerformed() {

		if (JOptionPane.showConfirmDialog(this, "Really Delete The Exit?", "Delete Exit?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) { //$NON-NLS-1$ //$NON-NLS-2$
			if (mapView.getSelectedExit() != null) {
				database.deleteExit(mapView.getSelectedExit());
				mapView.setSelectedExit(null);
				updateMainMap();
			}
		}
	}

	public void deleteMapObjectMenuItemActionPerformed() {
		int n = JOptionPane.showConfirmDialog(this, "Do you really want to delete the Picture?", "Picture Deletion", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
		if (n == JOptionPane.OK_OPTION) {
			database.deleteObject(mapView.getCurrentMapObject());
			updateMainMap();
		}
	}

	private void deleteRoom(Room room, boolean dontAsk) {
		if (room == null) return;
		int n = (dontAsk ? JOptionPane.YES_OPTION : JOptionPane.NO_OPTION);
		if (!dontAsk) n = JOptionPane.showConfirmDialog(this, "Do you really want to delete the room?", "Room Deletion", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
		if ((n == JOptionPane.YES_OPTION) || dontAsk) {
			ArrayList<Exit> vector = database.getExitsForRoom(room);
			for (Exit exit : vector) {
				int roomId = room.getId();
				if (exit.getTargetRoomId() == roomId) {
					exit.setTargetRoomId(0);
				} else {
					exit.setSourceRoomId(0);
				}

				if (exit.getTargetRoomId() == 0 && exit.getSourceRoomId() == 0) {
					database.deleteExit(exit);
				} else {
					database.updateExit(null, exit);
				}
			}
			database.deleteRoom(room.getAreaId(), room);
		}
		// database.cleanOutExits();
		updateMainMap();

	}

	public void deleteRoomPopupMenuItemActionPerfformed() {
		int n = JOptionPane.showConfirmDialog(this, "Do you really want to delete the room?", "Room Deletion", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
		if (n == JOptionPane.YES_OPTION) {
			for (Room room : mapView.getHighlightedRooms()) {
				deleteRoom(room, true);
			}
			mapView.getHighlightedRooms().clear();
		}
	}

	public void editMapObjectMenuItemActionPerformed() {
		MapObjectDialog mapObjectDialog = new MapObjectDialog(mapper, mapView.getCurrentMapObject());
		database.updateObject(mapObjectDialog.getResult());
		mapView.updateView();
	}

	public void exportAreaAsXMLMenuItemActionPerformed() {
		Area area = treeViewer.getSelectedArea();
		if (area != null) {
			new Exporter(this, area.getId());
		} else {
			new Exporter(this, 0);
		}
	}

	@SuppressWarnings("unused")
	private void exportPictureForArea() {
		// TODO fix later...
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		mapView.requestFocusInWindow();
	}

	@Override
	public void focusLost(FocusEvent arg0) {}

	private String formatJourneyString(String path) {
		path = path.replaceAll("\\.", ""); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("You arrive after having journeyed ", ""); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("and", ","); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("miles?", ""); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("one", "1"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("two", "2"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("three", "3"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("four", "4"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("five", "5"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("six", "6"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("seven", "7"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("eight", "8"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("nine", "9"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("ten", "10"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("eleven", "11"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("twelve", "12"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("thirteen", "13"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("fourteen", "14"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("fifteen", "15"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("sixteen", "16"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("seventeen", "17"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("eightteen", "18"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("nineteen", "19"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("twenty", "20"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("north", "n"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("northeast", "ne"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("east", "e"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("southeast", "se"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("south", "s"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("southwest", "sw"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("west", "w"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("northwest", "nw"); //$NON-NLS-1$ //$NON-NLS-2$
		path = path.replaceAll("(  )+", "-"); //$NON-NLS-1$ //$NON-NLS-2$
		return path;
	}

	/**
	 * @return the colourChooser
	 */
	public JDialog getColourChooser(Color colour, Window parent) {
		colourChooserPanel.setColor(colour);
		if (parent == null) {
			colourChooser.setLocationRelativeTo(mapper);
		} else {
			colourChooser.setLocationRelativeTo(parent);
		}
		colourChooser.setVisible(true);
		return colourChooser;
	}

	/**
	 * @return the communicator
	 */
	public Communicator getCommunicator() {
		return communicator;
	}

	public Point getCopyPosition() {
		return copyPosition;
	}

	public Area getCurrentArea() {
		return mapView.getCurrentArea();
	}

	/**
	 * @return the currentAreaId
	 */
	public int getCurrentAreaId() {
		return (mapView.getCurrentArea() == null ? 0 : mapView.getCurrentArea().getId());
	}

	/**
	 * @return the currentColour
	 */
	public Color getCurrentColour() {
		return currentColour;
	}

	/**
	 * @return the currentLevel
	 */
	public int getCurrentLevel() {
		return mapView.getCurrentLevel();
	}

	/**
	 * @return the currentMapObject
	 */
	public MapObject getCurrentMapObject() {
		return mapView.getCurrentMapObject();
	}

	/**
	 * @return the currentRoom
	 */
	public Room getCurrentRoom() {
		return mapView.getCurrentRoom();
	}

	/**
	 * @return the currentRoomId
	 */
	public int getCurrentRoomId() {
		return mapView.getCurrentRoomId();
	}

	/**
	 * @return the currentSelectedArea
	 */
	public Area getCurrentSelectedArea() {
		return mapView.getCurrentSelectedArea();
	}

	/**
	 * @return the currentShadowId
	 */
	public int getCurrentShadowId() {
		return mapView.getCurrentShadowId();
	}

	public Room getCurrentShadowRoom() {
		return mapView.getCurrentShadowRoom();
	}

	/**
	 * @return the dragArea
	 */
	public AreaPanel getDragArea() {
		return mapView.getDragArea();
	}

	/**
	 * @return the endArea
	 */
	public int getEndArea() {
		return endArea;
	}

	/**
	 * @return the endCoordinates
	 */
	public Point getEndCoordinates() {
		return mapView.getEndCoordinates();
	}

	/**
	 * @return the endLevel
	 */
	public int getEndLevel() {
		return endLevel;
	}

	/**
	 * @return the endRoom
	 */
	public Room getEndRoom() {
		return mapView.getEndRoom();
	}

	private Room getEndRoomFromJourney(int[] path) {
		Room nRoom, oRoom;
		nRoom = oRoom = mapView.getCurrentRoom();
		for (int i = (isAutojourney() ? 1 : 0); i < path.length; i++) {
			Exit exit = ExitsManager.hasExitInThatDir(path[i], nRoom);
			if (exit != null) {
				oRoom = nRoom;
				if (exit.getTargetRoomId() == nRoom.getId()) {
					nRoom = database.getRoom(exit.getSourceArea(), exit.getSourceRoomId());
				} else if (exit.getSourceRoomId() == nRoom.getId()) {
					nRoom = database.getRoom(exit.getTargetArea(), exit.getTargetRoomId());
				}
			}
		}

		return (nRoom != null ? nRoom : oRoom);
	}

	/**
	 * @return the lastPath
	 */
	public String getLastPath() {
		return lastPath;
	}

	public void getMainMapToRequestFocus() {
		mapView.requestFocusInWindow();
	}

	private String getPatternForExit(int dir) {
		String toReturn = "^("; //$NON-NLS-1$
		toReturn += Exit.getShortForDir(dir);
		toReturn += "|"; //$NON-NLS-1$
		toReturn += Exit.getLongForDir(dir);
		toReturn += "|)$"; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * @return the spaceBetweenRooms
	 */
	public int getSpaceBetweenRooms() {
		return mapView.getCurrentArea().getPreferredRoomDistance();
	}

	/**
	 * @return the startArea
	 */
	public int getStartArea() {
		return startArea;
	}

	/**
	 * @return the startCoordinates
	 */
	public Point getStartCoordinates() {
		return mapView.getStartCoordinates();
	}

	/**
	 * @return the startLevel
	 */
	public int getStartLevel() {
		return startLevel;
	}

	/**
	 * @return the startRoom
	 */
	public Room getStartRoom() {
		return mapView.getStartRoom();
	}

	/**
	 * 
	 * @param exits
	 *            array with HEX ints...
	 * @return array with HEX ints...
	 * @author Ecthelion
	 */
	private int[] getTypesForExits(int[] exits) {
		int[] toReturn = new int[exits.length];
		for (int i = 0; i < exits.length; i++) {
			toReturn[i] = (exits[i] / (BASE * 10));
		}
		return toReturn;
	}

	/**
	 * Set current Room
	 * 
	 * @param tmpRoom
	 *            the Room to which we want to teleport
	 * @author Ecthelion
	 */
	public void gotoRoom(Room room) {
		if (room == null) return;
		if (mapView.getCurrentRoom() != null) mapView.getCurrentRoom().setCurrentRoom(false);
		if (mapView.getCurrentRoom() != null) mapView.getCurrentRoom().setHighlighted(false);
		mapView.setCurrentRoom(room);
		mapView.setaRoomWithAView(null);
		profileManager.updateCurrentUser(room.getAreaId(), room.getId());
		setStatus(mapView.getCurrentRoom().getName());
		mapView.getCurrentRoom().setCurrentRoom(true);
		mapView.getCurrentRoom().setHighlighted(true);
		ArrayList<Exit> exits = database.getExitsForRoom(mapView.getCurrentRoom());
		for (Exit exit : exits) {
			if (exit == null) {
				System.out.println("Hmm, null Exit WTF!"); //$NON-NLS-1$
				continue;
			}
			if (replaceExits) {
				if (!exit.getSourceWalkCommand().isEmpty()) {
					if (exit.getSourceRoomId() == room.getId()) {
						sendReplaceToCommunicator(getPatternForExit(exit.getType() / BASE), exit.getSourceWalkCommand());
					}
				}
				if (!exit.getTargetWalkCommand().isEmpty()) {
					if (exit.getTargetRoomId() == room.getId()) {
						sendReplaceToCommunicator(getPatternForExit(exit.getType() % BASE), exit.getTargetWalkCommand());
					}
				}

				if (exit.getTargetType() == SWIM_TYPE || exit.getTargetType() == CLIMB_TYPE) {
					if (exit.getTargetRoomId() == room.getId()) {
						sendReplaceToCommunicator(getPatternForExit(exit.getType() % BASE), exit.translateTargetTypeToText() + " " //$NON-NLS-1$
																							+ Exit.getShortForDir(exit.getType() % BASE));
					}
				}

				if (exit.getSourceType() == SWIM_TYPE || exit.getSourceType() == CLIMB_TYPE) {
					if (exit.getSourceRoomId() == room.getId()) {
						sendReplaceToCommunicator(getPatternForExit(exit.getType() / BASE), exit.translateSourceTypeToText() + " " //$NON-NLS-1$
																							+ Exit.getShortForDir(exit.getType() / BASE));
					}
				}
			}

		}
		setCurrentArea(room.getAreaId());
		mapView.moveViewTo(room);
	}

	public void sendReplaceToCommunicator(String pattern, String replacement) {
		communicator.sendReplace(pattern, replacement);
	}

	private void initiateComponents() {
		setCurrentLevel(Integer.parseInt(properties.getProperty(MapperProperties.STARTINGLEVEL)));

		treeViewer = new AreaTreeViewer(database.getAreas(), this);
		scrollpaneForTreeViewForAreas = new JScrollPane(treeViewer);
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollpaneForTreeViewForAreas, mapView);
		String divider = properties.getProperty(MapperProperties.DIVIDERLOCATION, MapperProperties.DIVIDERLOCATION_DEFAULT);
		splitPane.setDividerLocation(Integer.parseInt(divider));
		splitPane.setOneTouchExpandable(true);
		splitPane.setContinuousLayout(false);
		splitPane.setAutoscrolls(false);
		toolPanel = new MainToolBar(this);
		theMenuBar = new MapperMenuBar(this);
		setJMenuBar(theMenuBar);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().addKeyListener(this);
		addWindowListener(this);
		getContentPane().addComponentListener(this);
		toolPanel.addKeyListener(this);
		treeViewer.addKeyListener(this);
		// theMenuBar.addMouseListener(this);
		statusField = new JPanel();
		statusLabel = new JLabel(" "); //$NON-NLS-1$
		statusField.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 1));
		statusLabel.setSize(new Dimension(50, 20));
		statusLabel.setMinimumSize(new Dimension(50, 20));
		statusField.add(statusLabel);
		statusField.setSize(new Dimension(50, 20));
		statusField.setPreferredSize(new Dimension(50, 20));
		statusField.setMinimumSize(new Dimension(50, 20));
		statusField.setBorder(new EtchedBorder(EtchedBorder.LOWERED));

		toolPanel.setBorder(new BevelBorder(BevelBorder.RAISED));

		getContentPane().add(toolPanel, BorderLayout.NORTH);
		getContentPane().add(splitPane, BorderLayout.CENTER);
		getContentPane().add(statusField, BorderLayout.SOUTH);
		mapView.moveShadowRoomToCurrentRoom();
		if (mapView.getCurrentRoom() != null) {
			mapView.moveViewTo(mapView.getCurrentRoom());
		} else {
			if (mapView.getCurrentArea() != null) {
				Room tmpRoom = database.getOldestRoomInArea(mapView.getCurrentArea().getId());
				if (tmpRoom != null) {
					mapView.moveViewTo(tmpRoom);
				} else {
					completlyNewMapDatabase = true;
				}
			}
		}
		int val = Integer.parseInt(properties.getProperty(MapperProperties.EXITSPOLICY));
		setConnectionType(val, null);
		pack();
		setVisible(true);
		if (isCompletlyNewMapDatabase()) {
			setStatus("Press \"Add Room\" then on the Map, press \"Following\" so it changes to \"Mapping\"... start mapping!"); //$NON-NLS-1$
			setCompletlyNewMapDatabase(false);
		}
		System.out.println("Mapper Started..."); //$NON-NLS-1$
	}

	/**
	 * @return the addNewArea
	 */
	public boolean isAddNewArea() {
		return mapView.isAddNewArea();
	}

	/**
	 * @return the addNewExit
	 */
	public boolean isAddNewExit() {
		return mapView.isAddNewExit();
	}

	/**
	 * @return the addNewObject
	 */
	public boolean isAddNewObject() {
		return mapView.isAddNewObject();
	}

	/**
	 * @return the addNewRoom
	 */
	public boolean isAddNewRoom() {
		return mapView.isAddNewRoom();
	}

	/**
	 * @return the autojourney
	 */
	public boolean isAutojourney() {
		return autojourney;
	}

	/**
	 * @return the completlyNewMapDatabase
	 */
	public boolean isCompletlyNewMapDatabase() {
		return completlyNewMapDatabase;
	}

	/**
	 * @return the draggingArea
	 */
	public boolean isDraggingArea() {
		return mapView.isDraggingArea();
	}

	/**
	 * @return the draggingObject
	 */
	public boolean isDraggingObject() {
		return mapView.isDraggingObject();
	}

	/**
	 * @return the draggingRoom
	 */
	public boolean isDraggingRoom() {
		return mapView.isDraggingRoom();
	}

	/**
	 * @return the firstExitAdded
	 */
	public boolean isFirstExitAdded() {
		return mapView.isFirstExitAdded();
	}

	/**
	 * @return the keyboardMapping
	 */
	public boolean isKeyboardMapping() {
		return keyboardMapping;
	}

	/**
	 * @return the replaceExits
	 */
	public boolean isReplaceExits() {
		return replaceExits;
	}

	public boolean isShadowing() {
		return shadowing;
	}

	public boolean isShowSpecialExits() {
		return showSpecialExits;
	}

	public void keyboardMappingMenuItemActionPerformed(ItemEvent ie) {
		keyboardMapping = (ie.getStateChange() == 1 ? true : false);
		if (keyboardMapping) {
			System.out.println("Keyboard Mapping activated"); //$NON-NLS-1$
		} else {
			System.out.println("Keyboard Mapping deactivated"); //$NON-NLS-1$
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_F) {
			new RoomFinderDialog(this);
		}

		if (e.isAltDown() && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Q) {
			database.fixExitsInMapThatsBorked();
		}

		if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			if (mapView.getHighlightedRooms().isEmpty()) return;
			int n = JOptionPane.showConfirmDialog(	this,
													"Really delete room" + (mapView.getHighlightedRooms().size() > 1 ? "s" : "") + "?", "Room deletion", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
			if (n == JOptionPane.YES_OPTION) {
				for (Room room : mapView.getHighlightedRooms()) {
					deleteRoom(room, true);
				}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent e) {}

	private int left(int dir) {
		if (dir == NORTH) return WEST;
		if (dir == NORTHEAST) return NORTHWEST;
		if (dir == EAST) return NORTH;
		if (dir == SOUTHEAST) return NORTHEAST;
		if (dir == SOUTH) return EAST;
		if (dir == SOUTHWEST) return SOUTHEAST;
		if (dir == WEST) return SOUTH;
		if (dir == NORTHWEST) return SOUTHWEST;
		return OTHER;
	}

	/**
	 * Maps towards the directions, if not a room there, create a new one, else
	 * goto the old room.
	 * 
	 * @author Ecthelion
	 * @param direction
	 *            the Direction towards to map
	 * @param roomName
	 *            the Name of the Room
	 * @param roomDescription
	 *            the Description of the Room.
	 * @param exits
	 *            the Exits in the new room
	 * @param type
	 *            the Type of move to get to the room (Swim/Climb/enter...)
	 * @return true if Successful to move or map...
	 */
	private boolean map(int direction, String roomName, String roomDescription, String exits, int type) {
		boolean debug = false;
		int[] iii = translateExitsToDirs(exits);
		if (debug) System.out.println("iii before corrections:" + toStringJu(iii)); //$NON-NLS-1$
		int[] startTypes = getTypesForExits(iii);
		int[] endTypes = getTypesForExits(iii);
		iii = correctExits(iii);
		Point cp = new Point(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2);
		int areaId = (mapView.getCurrentArea() != null ? mapView.getCurrentArea().getId() : 1);
		if (mapView.getCurrentRoom() != null) {
			cp = mapView.getCurrentRoom().getPosition();
			areaId = mapView.getCurrentRoom().getAreaId();
		}
		if (debug) {
			System.out.println(direction + " | " + exits + " | " + toStringJu(iii) + " | " + toStringJu(startTypes) + " | " + toStringJu(endTypes)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}
		int levelmod = 0;
		Room tmpRoom = ExitsManager.isRoomInThatDir(direction, mapView.getCurrentLevel(), cp.x, cp.y, mapView.getCurrentArea().getPreferredRoomDistance(),
													mapView.getCurrentArea().getId());
		Exit exitInDir = ExitsManager.hasExitInThatDir(direction, mapView.getCurrentRoom());

		if (debug) {
			System.out.println("Exits In Dir: " + exitInDir); //$NON-NLS-1$
			System.out.println("tmpRoom: " + tmpRoom); //$NON-NLS-1$
			System.out.println("currentLevel: " + mapView.getCurrentLevel()); //$NON-NLS-1$
			System.out.println("direction: " + mapView.getCurrentLevel()); //$NON-NLS-1$
		}

		if (tmpRoom == null && keyboardMapping && exitInDir == null) {
			if (debug) System.out.println("Creating a room walking from an non-existing exit..."); //$NON-NLS-1$
			Point p = ExitsManager.calculateDirDiff(direction, cp.x, cp.y, mapView.getCurrentArea().getPreferredRoomDistance());
			if (direction == DOWN) levelmod = -1;
			if (direction == UP) levelmod = 1;

			if (direction == ENTERSTALL || direction == ENTERTENT) levelmod = -1000000;
			if (direction == LEAVE || direction == OUT) levelmod = 1000000;

			int currentLevel = mapView.getCurrentLevel() + levelmod;
			setCurrentLevel(currentLevel);
			toolPanel.setLevel(currentLevel);

			Room newRoom = new Room(p, currentLevel, roomName, roomDescription, areaId, ""); //$NON-NLS-1$
			newRoom.setName(roomName);
			newRoom.setDescription(roomDescription);
			newRoom.setColour(RoomColour.getColorForName(roomName));
			newRoom.setId(addRoom(newRoom));
			if (mapView.getCurrentRoom() != null) {
				createExits(mapView.getCurrentRoom(), newRoom, direction, iii, startTypes, endTypes);
			}
			mapView.setCurrentShadowRoom(newRoom);
			mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
			gotoRoom(newRoom);
		} else if (tmpRoom == null && keyboardMapping && exitInDir != null) {
			if (exitInDir.getSourceRoomId() == 0 || exitInDir.getTargetRoomId() == 0) {
				Point p = ExitsManager.calculateDirDiff(direction, cp.x, cp.y, mapView.getCurrentArea().getPreferredRoomDistance());
				if (direction == DOWN) levelmod = -1;
				if (direction == UP) levelmod = 1;
				if (direction == ENTERSTALL || direction == ENTERTENT) levelmod = -1000000;
				if (direction == LEAVE || direction == OUT) levelmod = 1000000;

				int currentLevel = mapView.getCurrentLevel() + levelmod;
				Room newRoom = new Room(p, currentLevel, roomName, roomDescription, areaId, ""); //$NON-NLS-1$
				newRoom.setName(roomName);
				newRoom.setDescription(roomDescription);
				newRoom.setColour(RoomColour.getColorForName(roomName));
				newRoom.setId(addRoom(newRoom));
				exitInDir.setTargetRoom(tmpRoom, direction);
				database.updateExit(null, exitInDir);
				if (mapView.getCurrentRoom() != null) createExits(mapView.getCurrentRoom(), newRoom, direction, iii, startTypes, endTypes);
				mapView.setCurrentShadowRoom(newRoom);
				mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
				gotoRoom(newRoom);

			} else {
				move(direction);
			}
		} else if (tmpRoom != null && keyboardMapping && exitInDir != null) {
			if (debug) System.out.println("Connecting an Existing Room to an Existing exit."); //$NON-NLS-1$
			if (exitInDir.getTargetRoomId() == 0) {
				exitInDir.setTargetRoom(tmpRoom, ExitsManager.reverseDir(direction));
				database.updateExit(null, exitInDir);
				mapView.setCurrentShadowRoom(tmpRoom);
				mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
				gotoRoom(tmpRoom);
			} else if (exitInDir.getSourceRoomId() == 0) {
				exitInDir.setSourceRoom(tmpRoom, direction);
				database.updateExit(null, exitInDir);
				mapView.setCurrentShadowRoom(tmpRoom);
				mapView.setCurrentRoomId(mapView.getCurrentShadowRoom().getId());
				gotoRoom(tmpRoom);
			} else {
				move(exitInDir);
			}
		} else if (tmpRoom != null && keyboardMapping && exitInDir == null) {
			if (mapView.getCurrentRoom() != null) createExit(mapView.getCurrentRoom(), tmpRoom, direction, type, type);
			mapView.setCurrentShadowRoom(tmpRoom);
			mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
			gotoRoom(tmpRoom);
		} else {
			move(direction);
		}
		return true;
	}

	public void mergeRoomsPopupMenuItemActionPerformed() {
		ArrayList<Exit> tmpExitsVector = new ArrayList<Exit>();
		boolean isInTheRoom = false;
		for (Room room : mapView.getMergeVector()) {
			if (room.getId() == room.getId()) isInTheRoom = true;
		}
		Room theRoomToKeep = mapView.getMergeVector().get(0);
		for (Room room : mapView.getMergeVector()) {
			tmpExitsVector.addAll(database.getExitsForRoom(room));
		}
		for (Exit exit : tmpExitsVector) {
			for (Room room : mapView.getMergeVector()) {
				if (room.getId() == theRoomToKeep.getId()) continue;
				if (exit.getSourceRoomId() == room.getId()) {
					exit.setSourceRoom(theRoomToKeep, exit.getType() / BASE);
				} else if (exit.getTargetRoomId() == room.getId()) {
					exit.setTargetRoom(theRoomToKeep, exit.getType() % BASE);
				}
				database.deleteRoom(room.getAreaId(), room);
				database.updateExit(null, exit);
			}
		}

		// Delete double exits.
		tmpExitsVector.clear();
		tmpExitsVector.addAll(database.getExitsForRoom(theRoomToKeep));
		Hashtable<String, Exit> exitsTable = new Hashtable<String, Exit>();
		for (Exit exit : tmpExitsVector) {
			String key = exit.getSourceRoomId() + " - " + exit.getTargetRoomId() + " - " + exit.getType(); //$NON-NLS-1$ //$NON-NLS-2$
			if (exitsTable.get(key) == null) {
				exitsTable.put(key, exit);
			} else {
				database.deleteExit(exit);
			}
		}

		mapView.getMergeVector().clear();
		if (isInTheRoom) {
			gotoRoom(theRoomToKeep);
		}
		updateMainMap();

	}

	/**
	 * Move with an existing exit...
	 * 
	 * @param exit
	 *            the Exit to Follow
	 * @return true if successful
	 * @author Ecthelion
	 */
	private boolean move(Exit exit) {
		if (exit == null) return false;
		Room tmpRoom = null;
		if (exit.getTargetRoomId() == mapView.getCurrentRoom().getId()) {
			if (exit.getSourceRoomId() != 0) tmpRoom = database.getRoom(exit.getSourceArea(), exit.getSourceRoomId());
		} else {
			if (exit.getTargetRoomId() != 0) tmpRoom = database.getRoom(exit.getTargetArea(), exit.getTargetRoomId());
		}
		if (tmpRoom != null) {
			if (keyboardMapping) {
				mapView.setCurrentShadowRoom(tmpRoom);
				mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
			}
			gotoRoom(tmpRoom);
			return true;
		}
		return false;
	}

	/**
	 * Try to move in a direction.
	 * 
	 * @param direction
	 *            the Direction to Try
	 * @return true if successful
	 * @author Ecthelion
	 */
	private boolean move(int direction) {
		Exit tmpExit = ExitsManager.hasExitInThatDir(direction, mapView.getCurrentRoom());
		if (mapView.getCurrentShadowRoom() == null || shadowQueue.isEmpty()) {
			mapView.moveShadowRoomToCurrentRoom();
			mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
		}
		if (tmpExit != null) {
			if (tmpExit.isOneWay() && tmpExit.getTargetRoomId() == mapView.getCurrentRoom().getId()) {
				return false;
			} else {
				return move(tmpExit);
			}
		}

		return false;
	}

	public void moveArea() {

		if (JOptionPane.showConfirmDialog(this, "Really Move the Area?", "Move the Area?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {//$NON-NLS-1$ //$NON-NLS-2$
			if (treeViewer.getSelectedArea() == null) return;
			mapView.setCurrentSelectedArea(treeViewer.getSelectedArea());
			mapView.setMoveSelectedArea(true);
			setStatus("Press on location in the Map to move it there."); //$NON-NLS-1$
		}
	}

	/**
	 * Showing a menu for moving a room
	 * 
	 * @TODO THe MoveRoomPopupMenuITemActioPerformed
	 */
	public void moveRoomPopupMenuItemActionPerformed() {
		MoveDialog cfjmmmd = new MoveDialog(this);
		if (cfjmmmd.getResult()) {
			Point mod = cfjmmmd.getPointMod();
			int lvlMod = cfjmmmd.getLevelMod();
			for (Room room : mapView.getHighlightedRooms()) {
				int newX = (int) room.getPosition().getX() + (int) mod.getX();
				int newY = (int) room.getPosition().getY() + (int) mod.getY();
				room.setLocation((int) room.getLocation().getX() + (int) mod.getX(), (int) room.getLocation().getY() + (int) mod.getY());
				room.setPosition(new Point(newX, newY));
				room.setLevel(room.getLevel() + lvlMod);
				database.updateRoom(mapView.getCurrentArea().getId(), room);
				for (Exit exit : database.getExitsForRoom(room)) {
					if (exit.getSourceRoomId() == room.getId()) {
						exit.setSourceRoom(room, exit.getType() / BASE);
					} else if (exit.getTargetRoomId() == room.getId()) {
						exit.setTargetRoom(room, exit.getType() % BASE);
					}
					database.updateExit(null, exit);
				}
			}
		}
		updateMainMap();
		mapView.getHighlightedRooms().clear();
		mapView.getHighlightedExits().clear();
	}

	public void moveRoomToAreaPopupMenuItemActionPerformed() {
		AreaChooser chooser = new AreaChooser(this, database.getAreas());
		Area area = chooser.getResult();
		int oldArea = mapView.getCurrentArea().getId();
		int oldCurrentRoomId = -1;
		int newCurrentRoomId = -1;
		if (area != null) {
			setCurrentArea(area);
			for (Room room : mapView.getHighlightedRooms()) {
				if (room.getId() == mapView.getCurrentRoom().getId()) {
					oldCurrentRoomId = mapView.getCurrentRoom().getId();
				}
				ArrayList<Exit> exitsForRoom = database.getExitsForRoom(room);
				room.setAreaId(area.getId());
				int newId = database.updateRoom(oldArea, room);

				if (oldCurrentRoomId != -1) {
					newCurrentRoomId = newId;
				}

				for (Exit exit : exitsForRoom) {
					Exit old_exit = exit.clone();
					if (room.getId() == exit.getSourceRoomId()) {
						exit.setSourceArea(area.getId());
						exit.setSourceRoomId(newId);
					} else {
						exit.setTargetArea(area.getId());
						exit.setTargetRoomId(newId);
					}

					if (exit.getTargetRoomId() == 0) {
						exit.setTargetArea(area.getId());
					}
					if (exit.getSourceRoomId() == 0) {
						exit.setSourceArea(area.getId());
					}
					database.updateExit(old_exit, exit);
				}
			}
			if (newCurrentRoomId != -1) {
				mapView.setCurrentRoom(database.getRoom(area.getId(), newCurrentRoomId));
				if (centerOnCurrentRoom) mapView.moveViewTo(mapView.getCurrentRoom());
			}
			mapView.updateView();
		}
	}

	/**
	 * Moving the "Shadow Room", that is, the room in where you really are.
	 * 
	 * @param exit
	 * @return
	 */
	private boolean moveShadowRoom(Exit exit) {
		if (exit == null) return false;
		Room tmpRoom = null;
		if (exit.getTargetRoomId() == mapView.getCurrentShadowRoom().getId()) {
			tmpRoom = database.getRoom(exit.getSourceArea(), exit.getSourceRoomId());
		} else {
			tmpRoom = database.getRoom(exit.getTargetArea(), exit.getTargetRoomId());
		}
		if (tmpRoom != null) {
			shadowGotoRoom(tmpRoom);
			return true;
		}
		return false;
	}

	/**
	 * Moving the "Shadow Room", that is, the room in where you really are.
	 * 
	 * @param direction
	 * @return
	 */
	private boolean moveShadowRoom(int direction) {
		Exit tmpExit = ExitsManager.hasExitInThatDir(direction, mapView.getCurrentShadowRoom());
		if (tmpExit != null) {
			if (tmpExit.isOneWay() && tmpExit.getTargetRoomId() == mapView.getCurrentShadowRoom().getId()) {
				return false;
			} else {
				return moveShadowRoom(tmpExit);
			}
		}
		return false;
	}

	public void propertiesForAreaMenuItemActionPerformed(ActionEvent ae) {
		if (treeViewer.getSelectedArea() == null) return;
		AreaDialog ad = null;
		if (ae == null) {
			ad = new AreaDialog(mapper, database.getArea(mapView.getCurrentSelectedArea().getId()));
		} else {
			ad = new AreaDialog(mapper, treeViewer.getSelectedArea());
		}

		if (ad.getResult()) {
			database.updateArea(ad.getArea());
			updateTree();
			mapView.updateView();
		}
		treeViewer.repaint();

	}

	public void propertiesMenuItemForExitActionPerformed() {
		@SuppressWarnings("unused")
		ExitDialog ed = new ExitDialog(this, mapView.getSelectedExit());
		mapView.updateView();
	}

	public void properyRoomPopupMenuItemActionPerfformed() {
		RoomEditDialog ted = new RoomEditDialog(mapper, mapView.getHighlightedRooms());
		if (ted.getResult()) {
			for (Room room : mapView.getHighlightedRooms()) {
				database.updateRoom(mapView.getCurrentArea().getId(), room);
				for (Exit exit : database.getExitsForRoom(room)) {
					if (exit.getSourceRoomId() == room.getId()) {
						exit.setSourceRoom(room, exit.getType() / BASE);
					} else if (exit.getTargetRoomId() == room.getId()) {
						exit.setTargetRoom(room, exit.getType() % BASE);
					}
					database.updateExit(null, exit);
				}
			}
		}
		mapView.updateView();
	}

	public void readNotificationActionPerformed() {
		JOptionPane.showMessageDialog(mapper, mapView.getCurrentRoom().getNotification());
	}

	public void removeNotificationActionPerformed() {
		notificationRoom.setNotification(""); //$NON-NLS-1$
		notifications.remove(new Integer(notificationRoom.getId()));
		mapView.updateView();
	}

	public void repaintMainMap() {
		if (repaintingMainMap) {
			return;
		} else {
			repaintingMainMap = true;
			mapView.repaint();
			repaintingMainMap = false;
		}
	}

	public void resetTree() {
		treeViewer.setAreas(database.getAreas());
	}

	private int right(int dir) {
		if (dir == NORTH) return EAST;
		if (dir == NORTHEAST) return SOUTHEAST;
		if (dir == EAST) return SOUTH;
		if (dir == SOUTHEAST) return SOUTHWEST;
		if (dir == SOUTH) return WEST;
		if (dir == SOUTHWEST) return NORTHWEST;
		if (dir == WEST) return NORTH;
		if (dir == NORTHWEST) return NORTHEAST;
		return OTHER;
	}

	public String roomNotification(int id) {
		String toReturn = notifications.get(new Integer(id));
		if (toReturn == null) {
			return ""; //$NON-NLS-1$
		} else {
			return toReturn;
		}
	}

	/**
	 * Interprets the command from MushClient (or whatever client that has
	 * implemented the interface...)
	 * 
	 * @param command
	 * @return
	 * @throws MudException
	 * @author Ecthelion
	 * 
	 */
	public boolean sendCommand(MudCommand command) throws MudException {
		boolean debug = false;
		String commandName = command.getCommandName();
		// String commandType = command.getCommandType();
		String[] arguments = command.getArguments();
		if (debug) System.out.println("commandName: " + commandName); //$NON-NLS-1$

		if (commandName.equalsIgnoreCase(MOVE)) {
			int direction = translateExit(arguments[0].trim());
			shadowQueue.offer(new Integer(direction));
			toolPanel.setFacing(direction);
			return move(direction);
		} else if (commandName.equalsIgnoreCase(MAP)) {
			if (debug) System.out.println("Lets Map!"); //$NON-NLS-1$
			String roomName = arguments[1];
			String roomDescription = arguments[2];
			String exits = arguments[3];
			int direction = translateExit(arguments[0].trim());
			int type = translateType(arguments[0].trim());
			toolPanel.setFacing(direction);
			return map(direction, roomName, roomDescription, exits, type);
		} else if (commandName.equalsIgnoreCase(JOURNEY)) {
			int[] journeyDirections = translatJourneyString(arguments[0]);
			Room room = getEndRoomFromJourney(journeyDirections);
			mapView.setCurrentRoom(room);
			if (mapView.getCurrentShadowRoom() != null) mapView.getCurrentShadowRoom().setShadowRoom(false);
			mapView.moveShadowRoomToCurrentRoom();
			mapView.setCurrentShadowId(mapView.getCurrentRoom().getId());
			shadowQueue.clear();
			gotoRoom(room);
		} else if (commandName.equalsIgnoreCase(JUMP_TO_ROOM)) {
			String alias = arguments[0];
			return teleportToRoom(alias.trim());
		} else if (commandName.equalsIgnoreCase(ALIAS)) {
			String alias = arguments[0];
			return changeAliasOnCurrentRoom(alias.trim());
		} else if (commandName.equalsIgnoreCase(SHMOVE)) {
			if (shadowQueue == null) return false;
			if (debug) System.out.println("SHMove: " + shadowQueue.toString()); //$NON-NLS-1$
			Integer poll = shadowQueue.poll();
			if (poll != null) {
				return moveShadowRoom(poll.intValue());
			} else {
				return false;
			}
		} else if (commandName.equalsIgnoreCase(SHSTOP)) {
			if (keyboardMapping) return false;
			if (debug) System.out.println("STOP!"); //$NON-NLS-1$
			while (shadowQueue.poll() != null) {
				System.out.println("Removing Queueueue...."); //$NON-NLS-1$
			}
			gotoRoom(mapView.getCurrentShadowRoom());
		} else if (commandName.equalsIgnoreCase(NOTIFICATION)) {
			String noticification = arguments[0];
			addNotificationForCurrentRoom(noticification);
		} else if (commandName.equalsIgnoreCase(REMOVENOTIFICATION)) {
			addNotificationForCurrentRoom(""); //$NON-NLS-1$
		} else if (commandName.equalsIgnoreCase(SETPROFILE)) {
			String profileName = arguments[0];
			if (profileName.equalsIgnoreCase("default")) { //$NON-NLS-1$
				profileManager.setCurrentUser((User) null);
				int areaId = Integer.parseInt(properties.getProperty(MapperProperties.STARTINGAREAID, MapperProperties.STARTINGAREAID_DEFAULT));
				int roomId = Integer.parseInt(properties.getProperty(MapperProperties.STARTINGROOM, MapperProperties.STARTINGROOM_DEFAULT));
				Room room = database.getRoom(areaId, roomId);
				gotoRoom(room);
			} else {
				profileManager.setCurrentUser(profileName);
				mapView.setCurrentRoomFor(profileManager.getCurrentUser());
				System.out.println("Setting to profile: " + profileName); //$NON-NLS-1$

			}
		} else {
			throw new MudException("The Command " + commandName + " Doesn't Exist!"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return false;
	}

	public void sendStartUpToMushClient() {
		communicator.sendStartUp();
	}

	/**
	 * @param addNewArea
	 *            the addNewArea to set
	 */
	public void setAddNewArea(boolean addNewArea) {
		mapView.setAddNewArea(addNewArea);
	}

	/**
	 * @param addNewExit
	 *            the addNewExit to set
	 */
	public void setAddNewExit(boolean addNewExit) {
		mapView.setAddNewExit(addNewExit);
	}

	/**
	 * @param addNewObject
	 *            the addNewObject to set
	 */
	public void setAddNewObject(boolean addNewObject) {
		mapView.setAddNewObject(addNewObject);
	}

	/**
	 * @param addNewRoom
	 *            the addNewRoom to set
	 */
	public void setAddNewRoom(boolean addNewRoom) {
		mapView.setAddNewRoom(addNewRoom);
	}

	/**
	 * @param autojourney
	 *            the autojourney to set
	 */
	public void setAutojourney(boolean autojourney) {
		this.autojourney = autojourney;
		MapperProperties.getInstance().setProperty(MapperProperties.AUTOJOURNEY, Boolean.toString(autojourney));
	}

	public void setCenterOnCurrentRoom(boolean centerOnCurrentRoom) {
		this.centerOnCurrentRoom = centerOnCurrentRoom;
		properties.setProperty(MapperProperties.CENTREONCURRENTROOM, Boolean.toString(centerOnCurrentRoom));
	}

	/**
	 * @param completlyNewMapDatabase
	 *            the completlyNewMapDatabase to set
	 */
	public void setCompletlyNewMapDatabase(boolean completlyNewMapDatabase) {
		this.completlyNewMapDatabase = completlyNewMapDatabase;
	}

	public void setConnectionType(int selectedIndex, Boolean combobox) {
		connectVar = selectedIndex;
		if (combobox == null) {
			toolPanel.setSelectedIndexOfComboBox(connectVar);
			theMenuBar.setSelectedIndexOfConnection(connectVar);
			return;
		} else if (combobox.booleanValue()) {
			if (theMenuBar != null) theMenuBar.setSelectedIndexOfConnection(connectVar);
		} else if (!combobox.booleanValue()) {
			if (toolPanel != null) toolPanel.setSelectedIndexOfComboBox(connectVar);
		}
		properties.setProperty(MapperProperties.EXITSPOLICY, new Integer(connectVar).toString());
	}

	public void setCopyPosition(Point copyPosition) {
		this.copyPosition = copyPosition;

	}

	public void setCurrentArea(Area currentArea) {
		mapView.setCurrentArea(currentArea);
		treeViewer.setSelectedArea(currentArea);
		ZonesWindow.getInstance(this, currentArea.getId()).setArea(currentArea.getId());
	}

	public void setCurrentArea(int areaId) {
		setCurrentArea(database.getArea(areaId));
	}

	public void setCurrentAreaPopupMenuActionPerformed() {
		if (treeViewer.getSelectedArea() == null) return;
		mapView.moveViewTo(treeViewer.getSelectedArea());
	}

	/**
	 * @param currentLevel
	 *            the currentLevel to set
	 */
	public void setCurrentLevel(int currentLevel) {
		mapView.getMainMap().setCurrentLevel(currentLevel);
	}

	public void setCurrentShadowRoom(Room currentShadowRoom) {
		mapView.setCurrentShadowRoom(currentShadowRoom);
		mapView.setCurrentShadowId((currentShadowRoom == null ? 0 : currentShadowRoom.getId()));
	}

	/**
	 * @param draggingArea
	 *            the draggingArea to set
	 */
	public void setDraggingArea(boolean draggingArea) {
		mapView.setDraggingArea(draggingArea);
	}

	/**
	 * @param draggingObject
	 *            the draggingObject to set
	 */
	public void setDraggingObject(boolean draggingObject) {
		mapView.setDraggingObject(draggingObject);
	}

	/**
	 * @param draggingRoom
	 *            the draggingRoom to set
	 */
	public void setDraggingRoom(boolean draggingRoom) {
		mapView.setDraggingRoom(draggingRoom);

	}

	/**
	 * @param endArea
	 *            the endArea to set
	 */
	public void setEndArea(int endArea) {
		this.endArea = endArea;
	}

	/**
	 * @param endCoordinates
	 *            the endCoordinates to set
	 */
	public void setEndCoordinates(Point endCoordinates) {
		mapView.setEndCoordinates(endCoordinates);
	}

	/**
	 * @param endLevel
	 *            the endLevel to set
	 */
	public void setEndLevel(int endLevel) {
		this.endLevel = endLevel;
	}

	/**
	 * @param endRoom
	 *            the endRoom to set
	 */
	public void setEndRoom(Room endRoom) {
		mapView.setEndRoom(endRoom);
	}

	/**
	 * @param firstExitAdded
	 *            the firstExitAdded to set
	 */
	public void setFirstExitAdded(boolean firstExitAdded) {
		mapView.setFirstExitAdded(firstExitAdded);
	}

	/**
	 * @param keyboardMapping
	 *            the keyboardMapping to set
	 */
	public void setKeyboardMapping(boolean keyboardMapping) {
		this.keyboardMapping = keyboardMapping;
	}

	/**
	 * @param lastPath
	 *            the lastPath to set
	 */
	public void setLastPath(String lastPath) {
		Mapper.lastPath = lastPath;
	}

	@SuppressWarnings("unused")
	@Deprecated
	private void setObjectsInMainMap() {
		mapView.updateView();
	}

	public void setPositionHereMenuItemActionPerfformed() {
		if (mapView.getHighlightedRooms().size() == 0) return;
		if (mapView.getCurrentRoom() != null) mapView.getCurrentRoom().setCurrentRoom(false);
		mapView.getHighlightedRooms().get(0).setCurrentRoom(true);
		mapView.setCurrentRoomId(mapView.getHighlightedRooms().get(0).getId());
		mapView.setCurrentShadowId(mapView.getHighlightedRooms().get(0).getId());
		mapView.setCurrentRoom(mapView.getHighlightedRooms().get(0));
		mapView.moveShadowRoomToCurrentRoom();
		shadowQueue.clear();
		profileManager.updateCurrentUser(mapView.getCurrentArea().getId(), mapView.getCurrentRoomId());
		updateMainMap();
	}

	/**
	 * @param replaceExits
	 *            the replaceExits to set
	 */
	public void setReplaceExits(boolean replaceExits) {
		this.replaceExits = replaceExits;
		properties.setProperty(MapperProperties.SUBTISTUTESPECIALEXITS, Boolean.toString(replaceExits));
	}

	public void setShadowing(boolean shadowing) {
		this.shadowing = shadowing;
	}

	public void setShowSpecialExits(boolean showSpecialExits) {
		this.showSpecialExits = showSpecialExits;
		properties.setProperty(MapperProperties.SHOWSPECIALEXITS, Boolean.toString(showSpecialExits));
		repaintMainMap();
	}

	/**
	 * @param spaceBetweenRooms
	 *            the spaceBetweenRooms to set
	 */
	public void setSpaceBetweenRooms(int spaceBetweenRooms) {
		mapView.getCurrentArea().setPreferredRoomDistance(spaceBetweenRooms);
	}

	/**
	 * @param startArea
	 *            the startArea to set
	 */
	public void setStartArea(int startArea) {
		this.startArea = startArea;
	}

	/**
	 * @param startCoordinates
	 *            the startCoordinates to set
	 */
	public void setStartCoordinates(Point startCoordinates) {
		mapView.setStartCoordinates(startCoordinates);
	}

	/**
	 * @param startLevel
	 *            the startLevel to set
	 */
	public void setStartLevel(int startLevel) {
		this.startLevel = startLevel;
	}

	/**
	 * @param startRoom
	 *            the startRoom to set
	 */
	public void setStartRoom(Room startRoom) {
		mapView.setStartRoom(startRoom);
	}

	public void setStatus(String status) {
		if (status.trim().isEmpty()) status = "..."; //$NON-NLS-1$
		statusLabel.setText(status);
	}

	/**
	 * Sets the target/source room of an old exit the a certain room. TODO Check
	 * if it works... :P
	 */
	public void setTargetRoomInExitMenuItemActionPerformed() {
		if (mapView.getSelectedExit().getTargetRoomId() == 0) {
			mapView.setStartRoom(database.getRoom(mapView.getSelectedExit().getSourceArea(), mapView.getSelectedExit().getSourceRoomId()));
		} else if (mapView.getSelectedExit().getSourceRoomId() == 0) {
			mapView.setStartRoom(database.getRoom(mapView.getSelectedExit().getTargetArea(), mapView.getSelectedExit().getTargetRoomId()));
		}
		mapView.setAddNewExit(true);
		mapView.setFirstExitAdded(true);
		mapView.setSelectRoomForOldExit(true);
		setStatus("Select Room To Connect."); //$NON-NLS-1$
	}

	public void setZoomLevel(double zoom) {
		mapView.getMainMap().setScale(zoom);
		mapView.updateView();
	}

	private void shadowGotoRoom(Room theRoom) {

		if (!shadowing || keyboardMapping) {
			mapView.setCurrentShadowId(mapView.getCurrentRoomId());
			mapView.moveShadowRoomToCurrentRoom();
			shadowQueue.clear();
			updateMainMap();
			return;
		}

		if (theRoom == null) return;
		if (mapView.getCurrentShadowRoom() != null) mapView.getCurrentShadowRoom().setShadowRoom(false);
		mapView.setCurrentShadowRoom(theRoom);
		mapView.setCurrentShadowId(mapView.getCurrentShadowRoom().getId());
		mapView.getCurrentShadowRoom().setShadowRoom(true);
		updateMainMap();
	}

	private int[] splitJourneyArrayToInt(String[] path) {
		ArrayList<Integer> toReturn = new ArrayList<Integer>();
		for (String p : path) {
			String[] split = p.trim().split("-"); //$NON-NLS-1$
			int upTo = Integer.parseInt(split[0]);
			int direction = translateExit(split[1]);
			for (int i = 0; i < upTo; i++) {
				toReturn.add(new Integer(direction));
			}
		}
		int[] intToReturn = new int[toReturn.size()];

		for (int i = 0; i < intToReturn.length; i++) {
			intToReturn[i] = toReturn.get(i).intValue();
		}

		return intToReturn;
	}

	private String[] splitJourneyString(String path) {
		return path.split(","); //$NON-NLS-1$
	}

	/**
	 * Tries to teleport to a room with a certain alias.
	 * 
	 * @param alias
	 * @return true if Successful
	 * @author Ecthelion
	 */
	private boolean teleportToRoom(String alias) {
		Room room = database.getRoomByAlias(alias);
		if (room != null) {
			gotoRoom(room);
			if (mapView.getCurrentShadowRoom() != null) mapView.getCurrentShadowRoom().setShadowRoom(false);
			mapView.setCurrentShadowRoom(room);
			mapView.getCurrentShadowRoom().setShadowRoom(true);
			return true;
		}

		return false;
	}

	/**
	 * Transforms an array of integers into a String like: 1:2:3:4:5: ...
	 * 
	 * @param ints
	 *            the integers to be transformed.
	 * @return a String version of the array inputed.
	 * @author Ecthelion
	 */
	private String toStringJu(int[] ints) {
		String toReturn = ""; //$NON-NLS-1$
		for (int i : ints)
			toReturn += i + ":"; //$NON-NLS-1$
		return toReturn;
	}

	private int translateExit(String dir) {
		String tmp = dir.replaceAll("climb|swim", ""); //$NON-NLS-1$ //$NON-NLS-2$
		if (tmp.trim().equalsIgnoreCase("n")) return NORTH; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("ne")) return NORTHEAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("e")) return EAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("se")) return SOUTHEAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("s")) return SOUTH; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("sw")) return SOUTHWEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("w")) return WEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("nw")) return NORTHWEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("u")) return UP; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("d")) return DOWN; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("north")) return NORTH; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("northeast")) return NORTHEAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("east")) return EAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("southeast")) return SOUTHEAST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("south")) return SOUTH; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("southwest")) return SOUTHWEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("west")) return WEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("northwest")) return NORTHWEST; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("up")) return UP; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("down")) return DOWN; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("enter tent")) return ENTERTENT; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("enter stall")) return ENTERSTALL; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("leave")) return LEAVE; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("out")) return OUT; //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("forward")) return toolPanel.getFacing(); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("fw")) return toolPanel.getFacing(); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("backward")) return ExitsManager.reverseDir(toolPanel.getFacing()); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("bw")) return ExitsManager.reverseDir(toolPanel.getFacing()); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("left")) return left(toolPanel.getFacing()); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("lt")) return left(toolPanel.getFacing()); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("right")) return right(toolPanel.getFacing()); //$NON-NLS-1$
		if (tmp.trim().equalsIgnoreCase("rt")) return right(toolPanel.getFacing()); //$NON-NLS-1$
		return OTHER;
	}

	private int[] translateExitsToDirs(String exits) {
		String forward = getFacingString(0);
		String backward = getFacingString(1);
		String right = getFacingString(2);
		String left = getFacingString(3);
		exits = exits.trim();
		exits = exits.replaceAll("and", ","); //$NON-NLS-1$ //$NON-NLS-2$
		exits = exits.replaceAll("journey", ""); //$NON-NLS-1$ //$NON-NLS-2$
		exits = exits.replaceAll("\\[|\\]", ""); //$NON-NLS-1$ //$NON-NLS-2$
		exits = exits.replaceAll("leave", Integer.toString(LEAVE)); //$NON-NLS-1$ 
		exits = exits.replaceAll("swim", Integer.toString(BASE * 1)); //$NON-NLS-1$ 
		exits = exits.replaceAll("enter stall", Integer.toString(ENTERSTALL)); //$NON-NLS-1$ 
		exits = exits.replaceAll("leave", Integer.toString(LEAVE)); //$NON-NLS-1$ 
		exits = exits.replaceAll("enter tent", Integer.toString(ENTERTENT)); //$NON-NLS-1$ 
		exits = exits.replaceAll("climb", Integer.toString(BASE * 2)); //$NON-NLS-1$ 
		exits = exits.replaceAll("forward", forward); //$NON-NLS-1$
		exits = exits.replaceAll("backward", backward); //$NON-NLS-1$
		exits = exits.replaceAll("right", right); //$NON-NLS-1$
		exits = exits.replaceAll("left", left); //$NON-NLS-1$
		exits = exits.replaceAll("northeast", Integer.toString(NORTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("southeast", Integer.toString(SOUTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("southwest", Integer.toString(SOUTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("northwest", Integer.toString(NORTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("north-east", Integer.toString(NORTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("south-east", Integer.toString(SOUTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("south-west", Integer.toString(SOUTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("north-west", Integer.toString(NORTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("north", Integer.toString(NORTH)); //$NON-NLS-1$ 
		exits = exits.replaceAll("east", Integer.toString(EAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("south", Integer.toString(SOUTH)); //$NON-NLS-1$ 
		exits = exits.replaceAll("west", Integer.toString(WEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("up", Integer.toString(UP)); //$NON-NLS-1$ 
		exits = exits.replaceAll("down", Integer.toString(DOWN)); //$NON-NLS-1$ 
		exits = exits.replaceAll("out", Integer.toString(OUT)); //$NON-NLS-1$ 
		exits = exits.replaceAll("ne", Integer.toString(NORTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("se", Integer.toString(SOUTHEAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("sw", Integer.toString(SOUTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("nw", Integer.toString(NORTHWEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("n", Integer.toString(NORTH)); //$NON-NLS-1$ 
		exits = exits.replaceAll("e", Integer.toString(EAST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("s", Integer.toString(SOUTH)); //$NON-NLS-1$ 
		exits = exits.replaceAll("w", Integer.toString(WEST)); //$NON-NLS-1$ 
		exits = exits.replaceAll("u", Integer.toString(UP)); //$NON-NLS-1$ 
		exits = exits.replaceAll("d", Integer.toString(DOWN)); //$NON-NLS-1$
		exits = exits.replaceAll("fw", forward); //$NON-NLS-1$
		exits = exits.replaceAll("bw", backward); //$NON-NLS-1$
		exits = exits.replaceAll("rt", right); //$NON-NLS-1$
		exits = exits.replaceAll("lt", left); //$NON-NLS-1$
		exits = exits.replaceAll(" ", ""); //$NON-NLS-1$ //$NON-NLS-2$
		// System.out.println(exits);

		String exitsArray[] = exits.split(","); //$NON-NLS-1$
		int intArray[] = new int[exitsArray.length];
		for (int i = 0; i < exitsArray.length; i++) {
			String exit = exitsArray[i].trim();
			try {
				int q = Integer.parseInt(exit);
				intArray[i] = q;
			} catch (Exception e) {
				// Set everything not an integer, i.e. not a valid exit, to 0.
				intArray[i] = 0;
			}
		}
		// Cleaning out everything not valid...
		int counter = intArray.length;
		for (int w : intArray)
			if (w == 0) counter--;

		int[] toReturn = new int[counter];
		int r = 0;

		for (int w : intArray) {
			if (w != 0) toReturn[r++] = w;
		}

		return toReturn;
	}

	private String getFacingString(int direction) {
		int facing = toolPanel.getFacing();

		switch (direction) {
			case 0:
				return Integer.toString(facing);
			case 1:
				return Integer.toString(ExitsManager.reverseDir(facing));
			case 2:
				return Integer.toString(right(facing));
			case 3:
				return Integer.toString(left(facing));
		}
		return null;
	}

	private int translateType(String args) {
		if (args.contains("swim")) return SWIM_TYPE; //$NON-NLS-1$
		if (args.contains("climb")) return CLIMB_TYPE; //$NON-NLS-1$
		if (args.contains("enter tent")) return ENTER_TENT_TYPE; //$NON-NLS-1$
		if (args.contains("enter stall")) return ENTER_STALL_TYPE; //$NON-NLS-1$
		if (args.contains("leave")) return LEAVE_TYPE; //$NON-NLS-1$
		if (args.contains("out")) return OUT_TYPE; //$NON-NLS-1$
		return 0;
	}

	private int[] translatJourneyString(String path) {
		path = formatJourneyString(path);
		String[] split = splitJourneyString(path);
		return splitJourneyArrayToInt(split);
	}

	public void updateMainMap() {
		updateMainMap(false);
	}

	public void updateMainMap(boolean slide) {
		if (updateMainMap) {
			return;
		} else {
			updateMainMap = true;
			if (!database.isReady()) return;
			int currentAreaId = (mapView.getCurrentArea() == null ? 1 : mapView.getCurrentArea().getId());
			if (database.getArea(currentAreaId) == null || mapView.getCurrentArea() == null) {
				currentAreaId = database.getAnyAreaId();
				setCurrentArea(database.getArea(currentAreaId));
			}

			mapView.updateView();
			// if (!slide && centerOnCurrentRoom)
			// mapView.calculateScrollBars((mapView.getaRoomWithAView() == null
			// ? mapView.getCurrentRoom() : mapView.getaRoomWithAView()));
			repaintMainMap();
			updateMainMap = false;
		}
	}

	public void updateTree() {
		if (mapView.getCurrentSelectedArea() != null) treeViewer.updateArea(mapView.getCurrentSelectedArea());
		treeViewer.revalidate();
	}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {
		properties.save(this);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		properties.save(this);
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		properties.save(this);
	}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	public void windowGainedFocus(WindowEvent e) {}

	@Override
	public void windowIconified(WindowEvent e) {
		properties.save(this);
	}

	public void windowLostFocus(WindowEvent e) {
		properties.save(this);
	}

	@Override
	public void windowOpened(WindowEvent e) {}

	public void windowStateChanged(WindowEvent e) {}

	public void backupMapActionPerformed() {
		new Exporter(this, 0);
	}

	public void saveAreaSelectedInTreeAsImage() {
		treeViewer.getSelectedArea().saveAreaAsPicture(mapper);
	}

	public Color getNormalExitColour() {
		return Exit.getNormalColour();
	}

	public Color getSwimExitColour() {
		return Exit.getSwimColour();
	}

	public Color getClimbExitColour() {
		return Exit.getClimbColour();
	}

	public void resetColoursInMainMap() {
		Room.setDefaultRoomColour(null);
		Room.setCurrentRoomColour(null);
		Room.setCurrentShadowColour(null);
		Exit.setClimbColour(null);
		Exit.setSwimColour(null);
		Exit.setNormalColour(null);
		MapPanel.setDefaultBackgroundColour(null);
		RoomRep.setCurrentType(null);
		mapView.updateView();
		toolPanel.repaint();
	}

	public void requestFocusInWindowForMapView() {
		mapView.requestFocusInWindow();
	}

	public AreaTreeViewer getTreeViewer() {
		return treeViewer;
	}

	public MainToolBar getToolPanel() {
		return toolPanel;
	}

	public boolean isCenterOnCurrentRoom() {
		return centerOnCurrentRoom;
	}

	public MapView getMapView() {
		return mapView;
	}

	public int getDividerLocation() {
		return splitPane.getDividerLocation();
	}

	public void setCurrentProfile(User user) {
		profileManager.setCurrentUser(user);
		mapView.setCurrentRoomFor(user);
		if (user == null) {
			properties.remove(MapperProperties.PROFILE);
		} else {
			properties.setProperty(MapperProperties.PROFILE, user.getUserName());
		}
	}

	public ProfileManager getProfileManager() {
		return profileManager;
	}

	public void saveAreaSelectedInTreeAsThumbnail() {
		treeViewer.getSelectedArea().saveAreaAsThumbnail(mapper);
	}

	public void removeFromZoneMenuItemActionPerformed() {
		for (Room room : mapView.getHighlightedRooms()) {
			database.deleteZoneReferencesForRoom(room.getId(), getCurrentAreaId());
		}
		updateMainMap();
	}

	public void addToZoneMenuItemActionPerformed() {
		Zone zone = ZoneDialog.showZoneChooser(this);
		if (zone == null) return;
		for (Room room : mapView.getHighlightedRooms()) {
			database.addRoomToZone(zone, room);
		}
		getMapView().getMainMap().updateZones();
		updateMainMap();
	}

	public boolean currentAreaHasZones() {
		return database.getZonesForArea(getCurrentAreaId()).size() > 0;
	}

	public boolean currentRoomIsConnectedToZone() {
		if (mapView.getHighlightedRooms().size() == 0) return false;

		return !database.getZonesForRoom(mapView.getHighlightedRooms().get(0).getId(), getCurrentAreaId()).isEmpty();
	}

	public void saveAreaSelectedInTreeAsImage_NEW() {
		Area area = treeViewer.getSelectedArea();
		if (area == null) return;
		new ExportAsImageDialog(this, area);
	}

}
