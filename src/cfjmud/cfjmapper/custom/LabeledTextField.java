/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;

/**
 * @author Ecthelion
 * 
 */
public class LabeledTextField extends JComponent implements KeyListener, ChangeListener, ActionListener, MouseListener {

	/**
     * 
     */
	private static final long					serialVersionUID	= -240354518427570183L;
	String[]									boo					= { "false", "true" };	//$NON-NLS-1$ //$NON-NLS-2$
	private final JComponent					field;
	private String								id;
	private final JLabel						label;
	private Mapper								mapper;
	private ArrayList<PropertyChangeListener>	propertyListeners;
	private boolean								saved				= false;
	private Object								savedState			= null;
	private boolean								onTop				= false;

	/**
     * 
     */
	public LabeledTextField(String id) {
		this.id = id;
		label = new JLabel();
		field = new JFormattedTextField();
		((JFormattedTextField) field).addActionListener(this);
		initiateComponents();
	}

	public LabeledTextField(String id, String labelText, String defaultText, boolean onTop) {
		this.id = id;
		label = new JLabel(labelText);
		JTextArea toAdd = new JTextArea(defaultText);
		field = new JScrollPane(toAdd);
		this.onTop = onTop;
		toAdd.addKeyListener(this);
		toAdd.setColumns(10);
		toAdd.setRows(5);
		initiateComponents();
	}

	/**
	 * Initiates a {@link LabeledTextField} with a Combobox showing true/false
	 * 
	 * @param labelText
	 *            The Text for the Label
	 * @param value
	 *            The Value for the Combobox.
	 */
	@SuppressWarnings("unchecked")
	public LabeledTextField(String id, String labelText, Boolean value) {
		this.id = id;
		label = new JLabel(labelText);
		field = new JComboBox<String>(boo);
		((JComboBox<String>) field).setSelectedIndex((value.booleanValue() ? 1 : 0));
		((JComboBox<String>) field).addActionListener(this);
		initiateComponents();
	}

	/**
	 * {@link LabeledTextField} for colours.
	 * 
	 * @param id
	 *            Property Id.
	 * @param labelText
	 *            Text For label
	 * @param value
	 *            oldColourValue
	 * @param mapper
	 *            link To Mapper for ColourChooser
	 */
	public LabeledTextField(String id, String labelText, Color value, Mapper mapper) {

		this.id = id;
		this.mapper = mapper;
		label = new JLabel(labelText);
		field = new JLabel();
		((JLabel) field).setBackground(value);
		((JLabel) field).setOpaque(true);
		field.addMouseListener(this);
		initiateComponents();
	}

	public LabeledTextField(String id, String labelText, CurrentRoomRepresentationLabel[] labels, int selectedIndex, Mapper mapper) {
		this.id = id;
		this.mapper = mapper;
		label = new JLabel(labelText);
		field = new JComboBox<CurrentRoomRepresentationLabel>(labels);
		initiateComponents();
	}

	@SuppressWarnings("unchecked")
	public LabeledTextField(String id, String labelText, JComponent[] labels, int selectedIndex, ListCellRenderer<?> renderer, Mapper mapper) {
		this.id = id;
		this.mapper = mapper;
		label = new JLabel(labelText);
		field = new JComboBox<JComponent>(labels);
		((JComboBox<JComponent>) field).setRenderer((ListCellRenderer<? super JComponent>) renderer);
		((JComboBox<JComponent>) field).setSelectedIndex(selectedIndex);
		((JComboBox<JComponent>) field).addActionListener(this);
		initiateComponents();
	}

	/**
	 * Initiates a {@link LabeledTextField} with a Spinner using double-values.
	 * 
	 * @param labelText
	 *            the Text to show on the label.
	 * @param minValue
	 *            Minimum Value.
	 * @param maxValue
	 *            Maximum Value.
	 * @param value
	 *            Current Value.
	 * @param step
	 *            Stepping.
	 */
	public LabeledTextField(String id, String labelText, double minValue, double maxValue, double value, double step) {
		this.id = id;
		label = new JLabel(labelText);
		SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, step);
		field = new JSpinner(model);
		((JSpinner) field).addChangeListener(this);
		initiateComponents();
	}

	/**
	 * Initiates a {@link LabeledTextField} with a Spinner using float-values.
	 * 
	 * @param labelText
	 *            the Text to show on the label.
	 * @param minValue
	 *            Minimum Value.
	 * @param maxValue
	 *            Maximum Value.
	 * @param value
	 *            Current Value.
	 * @param step
	 *            Stepping.
	 */
	public LabeledTextField(String id, String labelText, float minValue, float maxValue, float value, float step) {
		label = new JLabel(labelText);
		SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, step);
		field = new JSpinner(model);
		((JSpinner) field).addChangeListener(this);
		initiateComponents();
	}

	/**
	 * Initiates a {@link LabeledTextField} with a float number.
	 * 
	 * @param labelText
	 *            The Text for the Label
	 * @param value
	 *            Da Value
	 */
	public LabeledTextField(String id, String labelText, Float value) {
		this.id = id;
		label = new JLabel(labelText);
		field = new JFormattedTextField(value);
		((JFormattedTextField) field).addActionListener(this);
		initiateComponents();
	}

	/**
	 * Initiates a {@link LabeledTextField} with a Spinner using int-values.
	 * 
	 * @param labelText
	 *            the Text to show on the label.
	 * @param minValue
	 *            Minimum Value.
	 * @param maxValue
	 *            Maximum Value.
	 * @param value
	 *            Current Value.
	 * @param step
	 *            Stepping.
	 */
	public LabeledTextField(String id, String labelText, int minValue, int maxValue, int value, int step) {
		this.id = id;
		label = new JLabel(labelText);
		SpinnerNumberModel model = new SpinnerNumberModel(value, minValue, maxValue, step);
		field = new JSpinner(model);
		((JSpinner) field).addChangeListener(this);
		initiateComponents();
	}

	/**
     * 
     */
	public LabeledTextField(String id, String labelText, Integer value) {
		this.id = id;
		label = new JLabel(labelText);
		field = new JFormattedTextField(value);
		((JFormattedTextField) field).addActionListener(this);
		initiateComponents();
	}

	/**
     * 
     */
	public LabeledTextField(String id, String labelText, String value) {
		this.id = id;
		label = new JLabel(labelText);
		field = new JFormattedTextField(value);
		((JFormattedTextField) field).addActionListener(this);
		initiateComponents();
	}

	@SuppressWarnings("unchecked")
	public LabeledTextField(String id, String labelText, String[] strings, String value) {
		this.id = id;
		label = new JLabel(labelText);
		field = new JComboBox<String>(strings);
		String toSet = ""; //$NON-NLS-1$
		for (String str : strings) {
			if (str.equalsIgnoreCase(value)) {
				toSet = str;
			}
		}
		((JComboBox<String>) field).setSelectedItem(toSet);
		((JComboBox<String>) field).addActionListener(this);
		initiateComponents();
	}

	public LabeledTextField(String id, String labelText, RoomGraphic[] shapes, RoomGraphic selectedItem, RoomGraphic roomGraph, Mapper mapper) {
		this.id = id;
		label = new JLabel(labelText);
		JComboBox<RoomGraphic> tmpField = new JComboBox<RoomGraphic>(shapes);
		tmpField.setRenderer(roomGraph);
		if (selectedItem != null) {
			tmpField.setSelectedItem(selectedItem);
		} else {
			tmpField.setSelectedIndex(0);
		}
		tmpField.addActionListener(this);
		field = tmpField;
		initiateComponents();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		propertyChanged();
	}

	public void addPropertiesChangedListener(PropertyChangeListener listener) {
		propertyListeners.add(listener);
	}

	public Object getValue() {
		if (field instanceof JFormattedTextField) {
			return ((JFormattedTextField) field).getValue();
		} else if (field instanceof JComboBox<?>) {
			return ((JComboBox<?>) field).getSelectedItem();
		} else if (field instanceof JSpinner) {
			return ((JSpinner) field).getValue();
		} else if (field instanceof JLabel) {
			return field.getBackground();
		} else {
			return null;
		}
	}

	/**
	 * Places Components...
	 */
	private void initiateComponents() {
		propertyListeners = new ArrayList<PropertyChangeListener>();
		field.addKeyListener(this);
		Dimension pref = new Dimension(175, 25);
		// label.setSize(pref);
		label.setPreferredSize(pref);
		label.setMinimumSize(pref);
		label.setToolTipText(label.getText());
		// field.setSize(pref);
		if (field instanceof JScrollPane) {
			field.setPreferredSize(new Dimension((int) pref.getWidth(), 100));
			field.setSize(new Dimension((int) pref.getWidth(), 100));
			field.setMinimumSize(new Dimension((int) pref.getWidth(), 100));
		} else {
			field.setPreferredSize(pref);
			field.setMinimumSize(pref);
		}
		field.setToolTipText(label.getText());
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		setLayout(gbl);
		c.insets = new Insets(2, 2, 2, 2);
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 0;
		c.gridx = 0;
		add(label, c);
		if (!onTop) {
			c.gridx = 1;
		} else {
			c.gridy = 1;
		}
		add(field, c);
	}

	/**
	 * @return the saved
	 */
	public boolean isSaved() {
		return saved;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		propertyChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent arg0) {
		propertyChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent arg0) {
		propertyChanged();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		mapper.getColourChooser(field.getBackground(), null);
		Mapper.oldColor = mapper.getCurrentColour();
		if (mapper.getCurrentColour() != null) {
			field.setBackground(mapper.getCurrentColour());
			propertyChanged();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	private void propertyChanged() {
		if (field instanceof JComboBox<?>) {
			if (((JComboBox<?>) field).getSelectedItem() instanceof RoomGraphic) {
				for (PropertyChangeListener pcl : propertyListeners) {
					pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, ((JComboBox<?>) field).getSelectedItem()));
				}
			} else {
				for (PropertyChangeListener pcl : propertyListeners) {
					pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, new Integer(((JComboBox<?>) field).getSelectedIndex())));
				}
			}
			savedState = ((JComboBox<?>) field).getSelectedItem();
		} else if (field instanceof JFormattedTextField) {
			for (PropertyChangeListener pcl : propertyListeners) {
				try {
					((JFormattedTextField) field).commitEdit();
				} catch (ParseException e) {
					if (!((JFormattedTextField) field).getText().isEmpty()) JOptionPane.showMessageDialog(mapper, e.toString());
				}
				pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, ((JFormattedTextField) field).getValue()));
			}
			savedState = ((JFormattedTextField) field).getValue();
		} else if (field instanceof JSpinner) {
			for (PropertyChangeListener pcl : propertyListeners) {
				pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, ((JSpinner) field).getValue()));
			}
			savedState = ((JSpinner) field).getValue();
		} else if (field instanceof JLabel) {
			for (PropertyChangeListener pcl : propertyListeners) {
				pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, ((JLabel) field).getBackground()));
			}
			savedState = ((JLabel) field).getBackground();
		} else if (field instanceof JScrollPane) {
			JScrollPane pane = (JScrollPane) field;
			JTextArea area = (JTextArea) pane.getViewport().getView();
			for (PropertyChangeListener pcl : propertyListeners) {
				if (area != null) {
					pcl.propertyChange(new PropertyChangeEvent(this, id, savedState, area.getText()));
				} else {
					System.out.println("Huh! Null!"); //$NON-NLS-1$
				}
			}
			savedState = area.getText();
		}
	}

	/**
	 * @param saved
	 *            the saved to set
	 */
	public void setSaved(boolean saved) {
		this.saved = saved;
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		propertyChanged();
	}

	public void setValue(Integer integer) {
		((JFormattedTextField) field).setValue(integer);
	}

	/**
	 * @return the onTop
	 */
	public boolean isOnTop() {
		return onTop;
	}

	/**
	 * @param onTop
	 *            the onTop to set
	 */
	public void setOnTop(boolean onTop) {
		this.onTop = onTop;
	}
}
