/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSShape;

/**
 * @author Ecthelion
 * 
 */
public class RoomRep extends JLabel implements ListCellRenderer<RoomRep> {
	/**
     * 
     */
	private static final long	serialVersionUID	= -6927947431543978606L;

	public enum TYPE {
		THREEDEESQUARE, SQUARE, CIRCLE, CIRCLETHIN, CIRCLENO, SQUARETHIN, SQUARETHICK, DEFAULT
	}

	private final TYPE	type;
	private static TYPE	currentType	= null;

	public RoomRep(TYPE type) {
		this.type = type;
		Dimension dim = new Dimension(16, 16);
		setSize(dim);
		setPreferredSize(dim);
		setMinimumSize(dim);
		setMaximumSize(dim);
		setText(""); //$NON-NLS-1$
	}

	@Override
	public void paint(Graphics g1) {
		Graphics2D g = (Graphics2D) g1;
		int x = getWidth() / 2 - 8;
		int width = 15;
		switch (type) {
			case THREEDEESQUARE:
				g.setColor(Room.getDefaultRoomColour());
				g.fill3DRect(x, 1, width, getHeight() - 1, true);
				break;
			case CIRCLE:
				g.setColor(Room.getDefaultRoomColour());
				g.fillOval(x, 1, width, getHeight() - 1);
				g.setColor(Color.BLACK);
				g.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				g.drawOval(x, 1, width, getHeight() - 1);
				break;
			case CIRCLETHIN:
				g.setColor(Room.getDefaultRoomColour());
				g.fillOval(x, 1, width, getHeight() - 1);
				g.setColor(Color.BLACK);
				g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				g.drawOval(x, 1, width, getHeight() - 1);
				break;
			case CIRCLENO:
				g.setColor(Room.getDefaultRoomColour());
				g.fillOval(x, 1, width, getHeight() - 1);
				break;
			case SQUARE:
				g.setColor(Room.getDefaultRoomColour());
				g.fillRect(x, 1, width, getHeight() - 1);
				break;
			case SQUARETHIN:
				g.setColor(Room.getDefaultRoomColour());
				g.fillRect(x, 1, width, getHeight() - 1);
				g.setColor(Color.BLACK);
				g.drawRect(x, 1, width, getHeight() - 1);
				break;
			case SQUARETHICK:
				g.setColor(Room.getDefaultRoomColour());
				g.fillRect(x, 1, width, getHeight() - 1);
				g.setColor(Color.BLACK);
				g.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				g.drawRect(x, 1, width, getHeight() - 1);
				break;
			case DEFAULT:
				g.setPaint(Color.BLACK);
				g.drawString("Default", getWidth() / 2 - 10, getHeight() / 2 + 4); //$NON-NLS-1$
				break;
			default:
				break;
		}
	}

	public static RoomRep[] getTypes() {
		RoomRep[] toReturn = new RoomRep[7];
		toReturn[0] = new RoomRep(TYPE.THREEDEESQUARE);
		toReturn[1] = new RoomRep(TYPE.SQUARE);
		toReturn[2] = new RoomRep(TYPE.CIRCLE);
		toReturn[3] = new RoomRep(TYPE.CIRCLETHIN);
		toReturn[4] = new RoomRep(TYPE.CIRCLENO);
		toReturn[5] = new RoomRep(TYPE.SQUARETHIN);
		toReturn[6] = new RoomRep(TYPE.SQUARETHICK);

		return toReturn;
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends RoomRep> list, RoomRep value, int index, boolean isSelected, boolean cellHasFocus) {
		return value;
	}

	/**
	 * @return the currentType
	 */
	public static TYPE getCurrentType(int areaId) {
		if (currentType == null) {
			int type = Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.ROOMREPRESENTATION,
																					MapperProperties.ROOMREPRESENTATION_DEFAULT));
			setCurrentType(getType(type + 1));
		}
		Area currentArea = Database.getInstance().getArea(areaId);
		if (currentArea != null) if (currentArea.getRoomRep() != 0) { return getType(currentArea.getRoomRep()); }

		return currentType;
	}

	public static void setCurrentType(TYPE currentType) {
		RoomRep.currentType = currentType;
	}

	public static void paintRoomRep(Room room, Graphics2D graphics) {
		int startX = -1;
		int startY = -1;
		int width = room.getWidth() + 2;
		int height = room.getHeight() + 2;
		Color oldC = graphics.getColor();
		Stroke s = graphics.getStroke();
		switch (RoomRep.getCurrentType(room.getAreaId())) {
			case THREEDEESQUARE:
			default:
				graphics.fill3DRect(1, 1, room.getWidth() - 1, room.getHeight() - 1, true);
				if (room.isHighLighted()) {
					graphics.setColor(Color.CYAN);
					graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				}
				break;
			case CIRCLE:
				startX = -1;
				startY = -1;
				width = room.getWidth() + 2;
				height = room.getHeight() + 2;
				graphics.fillOval(startX, startY, width, height);
				oldC = graphics.getColor();
				graphics.setColor(Color.BLACK);
				s = graphics.getStroke();
				graphics.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				graphics.drawOval(startX, startY, width, height);
				graphics.setColor(oldC);
				graphics.setStroke(s);
				if (room.isHighLighted()) {
					Stroke s1 = graphics.getStroke();
					graphics.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					graphics.setColor(Color.CYAN);
					graphics.setStroke(s1);
					graphics.drawOval(startX, startY, width, height);
				}
				break;
			case CIRCLETHIN:
				startX = 0;
				startY = 0;
				width = room.getWidth();
				height = room.getHeight();
				graphics.fillOval(startX, startY, width, height);
				oldC = graphics.getColor();
				graphics.setColor(Color.BLACK);
				s = graphics.getStroke();
				graphics.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				graphics.drawOval(startX, startY, width, height);
				graphics.setColor(oldC);
				graphics.setStroke(s);
				if (room.isHighLighted()) {
					Stroke s1 = graphics.getStroke();
					graphics.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					graphics.setColor(Color.CYAN);
					graphics.setStroke(s1);
					graphics.drawOval(startX, startY, width, height);
				}
				break;
			case CIRCLENO:
				startX = -1;
				startY = -1;
				width = room.getWidth() + 2;
				height = room.getHeight() + 2;
				graphics.fillOval(startX, startY, width, height);
				if (room.isHighLighted()) {
					s = graphics.getStroke();
					graphics.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					graphics.setColor(Color.CYAN);
					graphics.setStroke(s);
					graphics.drawOval(startX, startY, width, height);
				}
				break;
			case SQUARE:
				graphics.fillRect(1, 1, room.getWidth() - 1, room.getHeight() - 1);
				if (room.isHighLighted()) {
					graphics.setColor(Color.CYAN);
					graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				}
				break;
			case SQUARETHIN:
				graphics.fillRect(1, 1, room.getWidth() - 1, room.getHeight() - 1);
				graphics.setColor(Color.BLACK);
				graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				if (room.isHighLighted()) {
					graphics.setColor(Color.CYAN);
					graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				}
				break;
			case SQUARETHICK:
				graphics.fillRect(1, 1, room.getWidth() - 1, room.getHeight() - 1);
				graphics.setColor(Color.BLACK);
				s = graphics.getStroke();
				graphics.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
				graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				graphics.setStroke(s);
				if (room.isHighLighted()) {
					graphics.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					graphics.setColor(Color.CYAN);
					graphics.drawRect(0, 0, room.getWidth(), room.getHeight());
				}
		}
	}

	private static TYPE getType(int roomRep) {
		TYPE toReturn = TYPE.THREEDEESQUARE;
		switch (roomRep) {
			case 2:
				toReturn = TYPE.SQUARE;
				break;
			case 3:
				toReturn = TYPE.CIRCLE;
				break;
			case 4:
				toReturn = TYPE.CIRCLETHIN;
				break;
			case 5:
				toReturn = TYPE.CIRCLENO;
				break;
			case 6:
				toReturn = TYPE.SQUARETHIN;
				break;
			case 7:
				toReturn = TYPE.SQUARETHICK;
				break;
			case 1:
			default:
				toReturn = TYPE.THREEDEESQUARE;
				break;
		}

		return toReturn;
	}

	public static void paintRoomGraphics(Room room, Graphics2D graphics) {
		if (room.getType() == 0) return;
		double mod = 0;
		switch (RoomRep.getCurrentType(room.getAreaId())) {
			case SQUARETHICK:
				mod = -1.0;
				break;
			default:
				mod = 0;
				break;
		}
		graphics.translate(mod, mod);
		RoomGraphicsManager manager = RoomGraphicsManager.getInstance();
		RoomGraphic roomGraphic = manager.getShape(room.getType());
		if (roomGraphic == null) return;
		for (MSShape shape : roomGraphic.getShapes()) {
			graphics.setColor(shape.getColour());
			if (shape.isFilled()) {
				graphics.fill(shape);
			} else {
				graphics.draw(shape);
			}
			if (room.isHighLighted()) {
				Color oc = graphics.getColor();
				graphics.setColor(Color.CYAN);
				graphics.draw(roomGraphic.getBounds());
				graphics.setColor(oc);
			}
		}
		graphics.translate(-mod, -mod);
	}

	public static RoomRep[] getTypesForArea() {
		RoomRep[] toReturn = new RoomRep[8];

		toReturn[0] = new RoomRep(TYPE.DEFAULT);
		toReturn[1] = new RoomRep(TYPE.THREEDEESQUARE);
		toReturn[2] = new RoomRep(TYPE.SQUARE);
		toReturn[3] = new RoomRep(TYPE.CIRCLE);
		toReturn[4] = new RoomRep(TYPE.CIRCLETHIN);
		toReturn[5] = new RoomRep(TYPE.CIRCLENO);
		toReturn[6] = new RoomRep(TYPE.SQUARETHIN);
		toReturn[7] = new RoomRep(TYPE.SQUARETHICK);
		return toReturn;
	}
}
