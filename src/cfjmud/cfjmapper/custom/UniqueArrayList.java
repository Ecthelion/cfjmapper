/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ecthelion
 * 
 */
public class UniqueArrayList<E> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 9106213815483612651L;
	private final boolean		debug				= false;

	public UniqueArrayList() {
		super();
	}

	@Override
	public boolean add(E object) {
		if (debug) System.out.println("Object: " + object.toString()); //$NON-NLS-1$
		if (contains(object)) return false;
		return super.add(object);
	}

	@Override
	public boolean addAll(Collection<? extends E> objects) {
		for (E object : objects) {
			add(object);
		}
		return true;
	}
}
