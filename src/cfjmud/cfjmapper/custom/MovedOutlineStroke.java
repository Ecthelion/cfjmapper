/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

/**
 * @author Ecthelion
 * 
 */
public class MovedOutlineStroke extends BasicStroke {

	int			border		= 10;
	int			roomWidth	= 16;
	private int	totalAngle;

	public MovedOutlineStroke(int border) {
		this.border = border;
	}

	public MovedOutlineStroke() {}

	public MovedOutlineStroke(int border, int roomWidth) {
		this.border = border;
		this.roomWidth = roomWidth;
	}

	@Override
	public Shape createStrokedShape(Shape shape) {
		boolean debug = true;
		// shape = super.createStrokedShape(shape);
		PathIterator pi = shape.getPathIterator(null);
		GeneralPath toReturn = new GeneralPath();
		float points[] = new float[6];
		float startX = 0, startY = 0, currentX, currentY, oldX = 0, oldY = 0;
		double oldAngle = 0;
		double oldestAngle = 0;
		boolean first = false;
		if (debug) System.out.println("****************************************"); //$NON-NLS-1$
		float oldDx = 0f, oldDy = 0f;
		while (!pi.isDone()) {
			int result = pi.currentSegment(points);
			switch (result) {
				case PathIterator.SEG_MOVETO:
					first = true;
					startX = currentX = oldX = points[0];
					startY = currentY = oldY = points[1];
					break;
				case PathIterator.SEG_CLOSE:
					points[0] = startX;
					points[1] = startY;
				case PathIterator.SEG_LINETO:
					currentX = points[0];
					currentY = points[1];
					float dx = currentX - oldX;
					float dy = currentY - oldY;
					if (oldDx == 0) {
						oldDx = dx;
					}
					// float distance = (float) Math.sqrt(dx * dx + dy * dy);

					double angle = Math.atan2(dy, dx);
					// angle += Math.PI;
					int[] coords = calculateDiff(angle, oldAngle, oldestAngle, dx, dy, oldDx, oldDy, first);
					oldestAngle = oldAngle;
					oldAngle = angle;
					int toUseX = (int) (currentX + coords[0]);
					int toUseY = (int) (currentY + coords[1]);
					if (first) {
						first = false;
						toReturn.moveTo(toUseX, toUseY);
					} else {
						toReturn.lineTo(toUseX, toUseY);
					}
					oldX = currentX;
					oldY = currentY;
					break;
				default:
					break;
			}
			pi.next();
		}
		toReturn.closePath();
		totalAngle = 0;
		return toReturn;
	}

	private int[] calculateDiff(double angle, double oldAngle, double oldestAngle, float dx, float dy, float oldDx, float oldDy, boolean first) {
		int[] toReturn = new int[2];
		int degree = (int) (angle * 180 / Math.PI);
		int oldDegree = (int) (oldAngle * 180 / Math.PI);
		int oldestDegree = (int) (oldestAngle * 180 / Math.PI);
		if (degree < 0) degree += 360;
		if (oldDegree < 0) oldDegree += 360;
		if (oldestDegree < 0) oldDegree += 360;
		totalAngle += degree;
		totalAngle %= 360;
		if (first) {
			if (degree == 0) {
				toReturn[0] = -border;
				toReturn[1] = -border;
			}
		} else {

		}
		System.out.println(degree + " - " + totalAngle); //$NON-NLS-1$
		return toReturn;
	}
}
