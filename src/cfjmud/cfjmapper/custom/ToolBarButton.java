/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import cfjmud.cfjmapper.menus.MainToolBar;
import cfjmud.cfjmapper.objects.Room;

/**
 * @author Ecthelion
 * 
 */
public class ToolBarButton extends JComponent implements MouseListener, MouseMotionListener {
	public enum TYPE {
		ADDAREA, ADDEXIT, ADDPICTURE, ADDROOM, MAP, RECENTERROOM
	}

	/**
     * 
     */
	private static final long				serialVersionUID	= 4337660756503306895L;
	private final ArrayList<ActionListener>	actionListeners;
	private ImageIcon						icon;
	private boolean							pressed				= false;

	private boolean							raised				= false;				;

	private final TYPE						type;

	public ToolBarButton(TYPE type) {
		this.type = type;
		actionListeners = new ArrayList<ActionListener>();
		addMouseListener(this);
		addMouseMotionListener(this);
		switch (type) {
			case MAP:
				icon = loadIcon("earth-th.png"); //$NON-NLS-1$
				setToolTipText("<html>Blue Seas: Following.<br>Green Ones: Mapping.</br></html>"); //$NON-NLS-1$
				break;
			case ADDAREA:
				icon = loadIcon("Treasure map.png"); //$NON-NLS-1$
				setToolTipText("<html>Add Area<br>Grey: Nothing.</br><br>Green: Press on Background where you want the new area to be represented.</br></html>"); //$NON-NLS-1$
				break;
			case ADDEXIT:
				setToolTipText("<html>Add Exit<br>Grey: Nothing.</br><br>Green: Press on a room to add an Exit.</br><br>then on another to add the second</br></html>"); //$NON-NLS-1$
				break;
			case ADDROOM:
				setToolTipText("<html>Add Room<br>Grey: Nothing.<br>Green: Press on background to add room.</br></html>"); //$NON-NLS-1$
				break;
			case ADDPICTURE:
				setToolTipText("<html>Add Picture<br>Grey: Nothing.<br>Green: Press on background to add Picture.</br></html>"); //$NON-NLS-1$
				break;
			case RECENTERROOM:
				setToolTipText("<html>Re-Center to Current Room<br>Just press this effing button.</br></html>"); //$NON-NLS-1$
			default:
				break;
		}
		Dimension preferredButtonDimension = new Dimension(30, 30);
		setPreferredSize(preferredButtonDimension);
		setSize(preferredButtonDimension);
		setBackground(MainToolBar.getDisabledColour());
	}

	public void addActionListener(ActionListener al) {
		actionListeners.add(al);
	}

	private void drawAddAreaComponent(Graphics2D g) {
		g.drawImage(icon.getImage(), 3, 3, getWidth() - 6, getHeight() - 6, this);
		g.setColor((pressed ? Color.green : Color.black));
		g.drawLine(3, 11, 15, 11);
		g.drawLine(3, 12, 15, 12);
		g.drawLine(8, 5, 8, 18);
		g.drawLine(9, 5, 9, 18);
	}

	private void drawCenterOnCurrentRoomComponent(Graphics2D g) {
		Room tmpRoom = new Room();
		g.translate(tmpRoom.getWidth() / 2, tmpRoom.getHeight() / 2);
		tmpRoom.setCurrentRoom(true);
		tmpRoom.paint(g);
		g.translate(-tmpRoom.getWidth() / 2, -tmpRoom.getHeight() / 2);
	}

	private void drawAddExitComponent(Graphics2D g) {
		int height = getHeight();
		int width = getWidth();
		if (isPressed()) {
			g.setColor(MainToolBar.getEnabledColour());
		} else {
			g.setColor(MainToolBar.getDisabledColour());
		}
		g.fillOval(1, 1, width - 2, height - 2);
		g.setColor(Color.black);
		g.drawString("Exit", width / 2 - 9, height / 2 + 5); //$NON-NLS-1$
	}

	private void drawAddPictureComponent(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.drawLine(17, 10, 17, 20);
		g.drawLine(18, 11, 18, 20);
		if (isPressed()) {
			g.setColor(MainToolBar.getEnabledColour());
		} else {
			g.setColor(MainToolBar.getDisabledColour());
		}
		Polygon frame = new Polygon();
		frame = new Polygon();
		frame.addPoint(10, 5);
		frame.addPoint(20, 5);
		frame.addPoint(15, 20);
		frame.addPoint(5, 20);
		frame.addPoint(10, 5);
		g.fillPolygon(frame);
		g.setColor(Color.BLACK);
		g.draw(frame);
	}

	private void drawAddRoomComponent(Graphics2D g) {
		if (isPressed()) {
			g.setColor(MainToolBar.getEnabledColour());
		} else {
			g.setColor(MainToolBar.getDisabledColour());
		}

		g.fill3DRect(7, 7, 16, 16, pressed);
		g.setColor(Color.BLACK);
		g.drawLine(3, 11, 15, 11);
		g.drawLine(3, 12, 15, 12);
		g.drawLine(8, 5, 8, 18);
		g.drawLine(9, 5, 9, 18);
	}

	private void drawMapComponent(Graphics2D g) {
		if (isPressed()) {
			g.setColor(MainToolBar.getEnabledColour());
		} else {
			g.setColor(Color.BLUE);
		}
		g.fillOval(3, 3, getWidth() - 6, getHeight() - 6);

		g.drawImage(icon.getImage(), 3, 3, getWidth() - 6, getHeight() - 6, this);
	}

	/**
	 * @return the pressed
	 */
	public boolean isPressed() {
		return pressed;
	}

	private ImageIcon loadIcon(String string) {
		URL url = this.getClass().getResource("/" + string); //$NON-NLS-1$
		ImageIcon iconToReturn = null;
		if (url != null) {
			iconToReturn = new ImageIcon(url);
		} else {
			iconToReturn = new ImageIcon("./icons/" + string, string); //$NON-NLS-1$
		}
		return iconToReturn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int i = 1;
		for (ActionListener al : actionListeners) {
			al.actionPerformed(new ActionEvent(this, i++, "MouseClicked")); //$NON-NLS-1$
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent
	 * )
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		setBackground(MainToolBar.getHighlightColour());
		raised = true;
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		setBackground(MainToolBar.getDisabledColour());
		raised = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(getBackground());
		g2.draw3DRect(0, 0, getWidth(), getHeight(), !raised);
		switch (type) {
			case MAP:
				drawMapComponent(g2);
				break;
			case ADDAREA:
				drawAddAreaComponent(g2);
				break;
			case ADDEXIT:
				drawAddExitComponent(g2);
				break;
			case ADDPICTURE:
				drawAddPictureComponent(g2);
				break;
			case ADDROOM:
				drawAddRoomComponent(g2);
				break;
			case RECENTERROOM:
				drawCenterOnCurrentRoomComponent(g2);
			default:
				break;
		}
	}

	public void removeActionListener(ActionListener al) {
		actionListeners.remove(al);
	}

	/**
	 * @param pressed
	 *            the pressed to set
	 */
	public void setPressed(boolean pressed) {
		this.pressed = pressed;
	}

}
