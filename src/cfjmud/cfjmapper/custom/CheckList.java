/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

/**
 * @author Ecthelion
 * 
 */
public class CheckList<E> extends JPanel {

	/**
	 * 
	 */
	private static final long		serialVersionUID	= -4366447016805748168L;

	DefaultListModel<CheckListItem>	listModel;
	JList<CheckListItem>			list;
	JScrollPane						scrollPane;
	boolean							defaulSelected		= false;

	public CheckList() {
		initiatecomponents();
		listModel = new DefaultListModel<CheckListItem>();
		list.setModel(listModel);
		init();
	}

	public CheckList(boolean defaulSelected) {
		this.defaulSelected = defaulSelected;
		initiatecomponents();
		listModel = new DefaultListModel<CheckListItem>();
		list.setModel(listModel);
		init();
	}

	private void initiatecomponents() {
		list = new JList<CheckListItem>();
		scrollPane = new JScrollPane(list);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
	}

	public void setData(List<E> data) {
		listModel.removeAllElements();
		for (E e : data) {
			listModel.addElement(new CheckListItem(e));
		}
	}

	public void setData(E[] data) {
		listModel.removeAllElements();
		for (E e : data) {
			listModel.addElement(new CheckListItem(e));
		}
	}

	public List<E> getSelectedItems() {
		ArrayList<E> salida = new ArrayList<E>();
		CheckListItem item;
		for (int i = 0; i < listModel.getSize(); i++) {
			item = listModel.get(i);
			if (item.isSelected) salida.add(item.getContenido());
		}
		return salida;
	}

	public void setSelectedItems(List<E> selection) {

		for (int i = 0; i < listModel.getSize(); i++)
			listModel.get(i).setSelected(false);

		for (E item : selection)
			for (int i = 0; i < listModel.getSize(); i++)
				if (listModel.get(i).equals(item)) {
					listModel.get(i).setSelected(true);
					break;
				}

		this.repaint();
	}

	private void init() {
		// Use a CheckListRenderer (see below)
		// to renderer list cells

		list.setCellRenderer(new CheckListRenderer());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		list.addMouseListener(new MouseAdapter() {

			@SuppressWarnings("unchecked")
			@Override
			public void mouseClicked(MouseEvent event) {

				if (list.isEnabled() == false) return;

				JList<E> list = (JList<E>) event.getSource();

				int index = list.locationToIndex(event.getPoint());
				if (index < 0) return;
				CheckListItem item = (CheckListItem) list.getModel().getElementAt(index);
				item.setSelected(!item.isSelected());
				list.repaint(list.getCellBounds(index, index));

			}
		});

	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		list.setEnabled(enabled);
		scrollPane.setEnabled(enabled);
	}

	class CheckListRenderer extends JCheckBox implements ListCellRenderer<CheckListItem> {

		static final long	serialVersionUID	= -4366447016805748169L;

		@Override
		public Component getListCellRendererComponent(JList<? extends CheckList<E>.CheckListItem> list, CheckListItem value, int index, boolean isSelected,
				boolean hasFocus) {
			setEnabled(list.isEnabled());
			setSelected(value.isSelected());
			setFont(list.getFont());
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			setText(value.toString());
			return this;
		}
	}

	class CheckListItem {

		private final String	label;
		private boolean			isSelected	= false;
		private E				content		= null;

		public CheckListItem(String label) {
			this.label = label;
			isSelected = defaulSelected;
		}

		public CheckListItem(E content) {
			this.content = content;
			this.label = content.toString();
			isSelected = defaulSelected;
		}

		public CheckListItem(String label, E content) {
			this.label = label;
			this.content = content;
			isSelected = defaulSelected;
		}

		@SuppressWarnings("unused")
		private CheckListItem() {
			this.label = "<emoty>"; //$NON-NLS-1$
			this.content = null;
			isSelected = defaulSelected;
		}

		public E getContenido() {
			return content;
		}

		public void setContenido(E content) {
			this.content = content;
		}

		public boolean isSelected() {
			return isSelected;
		}

		public void setSelected(boolean isSelected) {
			this.isSelected = isSelected;
		}

		@Override
		public String toString() {
			return label;
		}

		@Override
		public boolean equals(Object obj) {
			return content.equals(obj);
		}
	}

}
