/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.custom;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

/**
 * @author Ecthelion
 * 
 */
public class CurrentRoomRepresentation {
	public enum TYPE {
		OVAL, RECTANGLE, STICKFIGURE
	}

	private static int			current		= 0;
	private static TYPE			currentType	= TYPE.OVAL;

	static ArrayList<Polygon>	stickFigure;

	public static Shape getCurrentRep() {
		switch (currentType) {
			case OVAL:
				return new Ellipse2D.Float(0f, 0f, 11f, 11f);
			case STICKFIGURE:
				if (stickFigure == null) stickFigure = loadFigures();
				if (current > stickFigure.size()) {
					current = 0;
				}
				return stickFigure.get(current++);
			case RECTANGLE:
				return new Rectangle(0, 0, 11, 11);
			default:
				return new Ellipse2D.Float(0f, 0f, 11f, 11f);
		}

	}

	/**
	 * @return the currentType
	 */
	public static TYPE getCurrentType() {
		return currentType;
	}

	private static ArrayList<Polygon> loadFigures() {
		ArrayList<Polygon> toReturn = new ArrayList<Polygon>();
		Polygon p = new Polygon();
		p.addPoint(5, 0);
		p.addPoint(6, 0);
		p.addPoint(7, 1);
		p.addPoint(1, 1);
		p.addPoint(0, 2);
		p.addPoint(1, 2);
		p.addPoint(1, 4);
		p.addPoint(0, 4);
		p.addPoint(1, 5);
		p.addPoint(1, 6);
		p.addPoint(2, 5);
		p.addPoint(3, 5);
		p.addPoint(3, 2);
		p.addPoint(4, 2);
		p.addPoint(4, 5);
		p.addPoint(5, 5);
		p.addPoint(5, 2);
		p.addPoint(6, 2);
		p.addPoint(6, 3);
		p.addPoint(7, 3);
		p.addPoint(7, 4);
		p.addPoint(7, 5);
		p.addPoint(6, 5);

		return toReturn;
	}

	/**
	 * @param currentType
	 *            the currentType to set
	 */
	public static void setCurrentType(TYPE currentType) {
		CurrentRoomRepresentation.currentType = currentType;
	}

}

class CurrentRoomRepresentationLabel extends Canvas {
	/**
     * 
     */
	private static final long	serialVersionUID	= -1232514264799425836L;
	private final String		label;
	private final Shape[]		shapes;

	public CurrentRoomRepresentationLabel(String label, Shape[] shapes) {
		this.shapes = shapes;
		this.label = label;

	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		FontMetrics metrics = g2.getFontMetrics(g.getFont());
		int textWidth = metrics.stringWidth(label);
		g2.setColor(Color.WHITE);
		g2.drawRect(0, 0, textWidth + 5, getHeight());
		g2.drawString(label, 5, getHeight());
		int xPos = 0;
		int gap = 3;
		for (Shape shape : shapes) {
			g.translate(xPos, 0);
			g2.drawRect(xPos, 0, (int) shape.getBounds().getWidth() + gap, getHeight());
			g2.draw(shape);
			xPos += shape.getBounds().getWidth() + gap;
		}

	}
}
