/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Room;

/**
 * @author Ecthelion
 * 
 */
public class RoomFinderDialog extends JDialog implements MouseListener {

	static String[]				columnHeaders		= { "Id", "Name", "Description" };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	/**
     * 
     */
	private static final long	serialVersionUID	= -6399970492745569102L;
	Database					database;
	JCheckBox					nameCheckBox, aliasCheckBox, descriptionCheckBox;
	JPanel						optionsPanel, searchPanel;
	Mapper						parent;
	JTable						roomTable;
	JButton						searchButton, showRoomLocationButton, moveToToButton, closeButton;
	JTextField					searchField;
	JLabel						searchLabel, optionsLabel;
	RoomTableModel				tableModel;

	public RoomFinderDialog(Mapper parent) {
		super(parent);
		this.parent = parent;
		database = Database.getInstance();
		initiateComponents();
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	private void initiateComponents() {
		tableModel = new RoomTableModel(columnHeaders, 10);
		tableModel.setColumnCount(columnHeaders.length);
		tableModel.setColumnIdentifiers(columnHeaders);
		roomTable = new JTable(tableModel);
		roomTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roomTable.addMouseListener(this);
		JScrollPane scrollPane = new JScrollPane(roomTable);
		scrollPane.setSize(new Dimension(300, 150));
		scrollPane.setPreferredSize(new Dimension(300, 150));
		nameCheckBox = new JCheckBox("Names"); //$NON-NLS-1$
		nameCheckBox.setSelected(true);
		aliasCheckBox = new JCheckBox("Alias"); //$NON-NLS-1$
		descriptionCheckBox = new JCheckBox("Descriptions"); //$NON-NLS-1$
		optionsLabel = new JLabel("To Search In:"); //$NON-NLS-1$
		optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridLayout(0, 1));
		optionsPanel.add(optionsLabel);
		optionsPanel.add(nameCheckBox);
		optionsPanel.add(aliasCheckBox);
		optionsPanel.add(descriptionCheckBox);
		searchLabel = new JLabel("Text:"); //$NON-NLS-1$
		searchField = new JTextField(30);
		searchField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				searchActionPerformed();
			}
		});
		showRoomLocationButton = new JButton("Move View To"); //$NON-NLS-1$
		showRoomLocationButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (roomTable.getSelectedRow() != -1) {
					moveViewToRoom(tableModel.getRoom(roomTable.getSelectedRow()));
				}
			}
		});
		moveToToButton = new JButton("Move Current Location To"); //$NON-NLS-1$
		moveToToButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (roomTable.getSelectedRow() != -1) {
					parent.gotoRoom(tableModel.getRoom(roomTable.getSelectedRow()));
				}
			}
		});
		closeButton = new JButton("Close"); //$NON-NLS-1$
		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		searchButton = new JButton("Search"); //$NON-NLS-1$
		searchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				searchActionPerformed();
			}

		});
		searchPanel = new JPanel();
		searchPanel.add(searchLabel);
		searchPanel.add(searchField);
		searchPanel.add(searchButton);
		searchPanel.add(showRoomLocationButton);
		searchPanel.add(moveToToButton);
		searchPanel.add(closeButton);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		add(searchPanel, BorderLayout.SOUTH);
		add(optionsPanel, BorderLayout.EAST);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			moveViewToRoom(tableModel.getRoom(roomTable.getSelectedRow()));
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	private void moveViewToRoom(Room room) {
		parent.getMapView().moveViewTo(room);
	}

	protected void searchActionPerformed() {
		if (!nameCheckBox.isSelected() && !descriptionCheckBox.isSelected() && !aliasCheckBox.isSelected()) {
			JOptionPane.showMessageDialog(this, "You must search in Something!"); //$NON-NLS-1$
		} else if (searchField.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "You must write something search on!"); //$NON-NLS-1$
		} else {
			System.out.println("Searching for: " + searchField.getText()); //$NON-NLS-1$
			int arg = (nameCheckBox.isSelected() ? 1 : 0) + (descriptionCheckBox.isSelected() ? 2 : 0) + (aliasCheckBox.isSelected() ? 4 : 0);
			ArrayList<Room> vec = database.searchRooms(searchField.getText(), arg);
			System.out.println("Vector: " + vec); //$NON-NLS-1$
			tableModel.setDataVector(database.searchRooms(searchField.getText(), arg));
		}
	}
}

class RoomTableModel extends DefaultTableModel {
	/**
     * 
     */
	private static final long	serialVersionUID	= -5533711982482101726L;
	ArrayList<Room>				roomVector;

	public RoomTableModel() {
		super();
	}

	public RoomTableModel(String[] headers, int size) {
		super(headers, size);
	}

	public Room getRoom(int selectedRow) {
		return roomVector.get(selectedRow);
	}

	@Override
	public boolean isCellEditable(int i, int u) {
		return false;
	}

	public void setDataVector(ArrayList<Room> roomVector) {
		this.roomVector = roomVector;
		String[][] rows = new String[roomVector.size()][3];
		int i = 0;
		for (Room room : roomVector) {
			rows[i++] = room.toTableRow();
		}
		setDataVector(rows, RoomFinderDialog.columnHeaders);
	}
}
