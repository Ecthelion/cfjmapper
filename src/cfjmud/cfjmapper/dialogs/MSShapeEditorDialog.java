/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSCubicCurve;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSLine;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSOval;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSPoint;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSQuadricCurve;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSRectangle;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSShape;

/**
 * @author Ecthelion
 * 
 */
public class MSShapeEditorDialog extends JDialog implements MouseListener, ListSelectionListener, MouseMotionListener, PropertyChangeListener, KeyListener {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8178141197194684915L;
	private Square[][]			checkers;
	private JList<RoomGraphic>	shapeList;
	private JList<MSShape>		subShapeList;

	private JLabel				nameLabel, idLabel, statusLabel;
	private JTextField			nameField, idField;
	private JButton				addButton, deleteButton, closeButton, lineButton, pointButton, quadButton, curveButton, rectButton, ovalButton, colourButton,
			upButton, downButton, deleteShapeButton, saveButton, copyButton, copySubShapeButton, pasteSubShapeButton;
	private JCheckBox			filledBox;
	private int					side				= 24;
	private JPanel				checkersPanel, shapesPanel, buttonPanel, generalInformationPanel, listPanel, propertiesPanel;
	private Color				currentColour		= Color.BLACK;
	private final Mapper		mapper;
	private RoomGraphic			currentRoomGraphic;
	private final Dimension		roomDimInPixels		= new Dimension(15, 15);
	int							border				= 4;
	JScrollPane					jp1, jp2;
	private ArrayList<MSShape>	currentSelectedShapes, copyArray;

	protected enum SHAPE {
		NONE, POINT, LINE, RECTANGLE, QUAD, CCURVE, OVAL;

	}

	private SHAPE		currentShape	= SHAPE.NONE;
	private boolean		firstSet;
	private Point		firstPoint;
	private CardLayout	cardLayout;

	public MSShapeEditorDialog(Mapper mapper) {
		super(mapper, true);
		side = Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.GRAPHICEDITORSIDE, MapperProperties.GRAPHICEDITORSIDE_DEFAULT));
		this.mapper = mapper;
		initiateComponents();
	}

	private void draw(RoomGraphic rg) {
		if (rg == null) return;
		BufferedImage bi = new BufferedImage(side, side, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, side, side);
		g.setColor(Color.WHITE);
		g.fillRect(border + 1, border + 1, 15, 15);
		g.translate(border, border);
		for (MSShape shape : rg.getShapes()) {
			g.setColor(shape.getColour());
			if (shape.isFilled()) {
				g.fill(shape);
			} else {
				g.draw(shape);
			}
		}
		for (int r = 0; r < bi.getHeight(); r++) {
			for (int c = 0; c < bi.getWidth(); c++) {
				checkers[r][c].setBackground(new Color(bi.getRGB(c, r)));
			}
		}
		markSelected(rg);
	}

	private void markSelected(RoomGraphic rg) {
		BufferedImage bi = new BufferedImage(side, side, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, side, side);
		g.setColor(Color.WHITE);
		g.fillRect(border + 1, border + 1, 15, 15);
		g.translate(border, border);
		for (MSShape shape : rg.getShapes()) {
			if (currentSelectedShapes.contains(shape)) {
				g.setColor(Color.BLACK);
			} else {
				g.setColor(Color.WHITE);
			}
			if (shape.isFilled()) {
				g.fill(shape);
			} else {
				g.draw(shape);
			}
		}
		for (int r = 0; r < bi.getHeight(); r++) {
			for (int c = 0; c < bi.getWidth(); c++) {
				checkers[r][c].setSelected(bi.getRGB(c, r) == Color.BLACK.getRGB());
			}
		}
		repaint();
	}

	private void resetButtons() {
		lineButton.setForeground(Color.BLACK);
		pointButton.setForeground(Color.BLACK);
		quadButton.setForeground(Color.BLACK);
		curveButton.setForeground(Color.BLACK);
		rectButton.setForeground(Color.BLACK);
		ovalButton.setForeground(Color.BLACK);
	}

	private void initiateComponents() {
		currentSelectedShapes = new ArrayList<MSShape>();
		copyArray = new ArrayList<MSShape>();
		checkersPanel = createCheckersPanel();
		shapesPanel = createShapesPanel();
		copyButton = new JButton("Copy"); //$NON-NLS-1$
		copyButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		copyButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				RoomGraphic rg = shapeList.getSelectedValue();
				if (rg != null) {
					RoomGraphicsManager.getInstance().addShape(rg.copy());
					shapeList.setListData(RoomGraphicsManager.getInstance().getShapes());
				}
			}
		});
		addButton = new JButton("Add"); //$NON-NLS-1$
		addButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				RoomGraphic rg = new RoomGraphic();
				rg.setName("*new*"); //$NON-NLS-1$
				rg.setId(RoomGraphicsManager.getInstance().getHighestId() + 1);
				RoomGraphicsManager.getInstance().addShape(rg);
				shapeList.setListData(RoomGraphicsManager.getInstance().getShapes());
			}
		});
		deleteButton = new JButton("Del"); //$NON-NLS-1$
		deleteButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(MSShapeEditorDialog.this, "Really delete Graphics?") == JOptionPane.YES_OPTION) { //$NON-NLS-1$
					//System.out.println("Shall delete: " + shapeList.getSelectedValue()); //$NON-NLS-1$
					RoomGraphicsManager.getInstance().deleteGraphics(shapeList.getSelectedValue());
					RoomGraphicsManager.getInstance().saveShapes();
					shapeList.setListData(RoomGraphicsManager.getInstance().getShapes());
				}
			}
		});
		closeButton = new JButton("Close"); //$NON-NLS-1$
		closeButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});

		saveButton = new JButton("Save"); //$NON-NLS-1$
		saveButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				RoomGraphicsManager.getInstance().saveShapes();
				mapper.updateMainMap();
			}
		});

		generalInformationPanel = createGeneralInformationPanel();

		buttonPanel = new JPanel();
		statusLabel = new JLabel("..."); //$NON-NLS-1$
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(statusLabel);
		buttonPanel.add(saveButton);
		buttonPanel.add(closeButton);
		colourButton.setForeground(currentColour);
		colourButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		colourButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getColourChooser(colourButton.getForeground(), MSShapeEditorDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				if (theNewColor != null) {
					currentColour = theNewColor;
				} else {
					currentColour = Color.BLACK;
				}

				colourButton.setForeground(currentColour);

			}
		});

		int checkWidth = 700 / side;
		Dimension dim = new Dimension(75, side * checkWidth);
		shapeList = new JList<RoomGraphic>(RoomGraphicsManager.getInstance().getShapes());
		subShapeList = new JList<MSShape>();
		jp1 = new JScrollPane(shapeList);
		jp2 = new JScrollPane(subShapeList);
		jp1.setSize(dim);
		jp1.setPreferredSize(dim);
		jp2.setSize(dim);
		jp2.setPreferredSize(dim);
		jp2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		subShapeList.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		shapeList.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		subShapeList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (subShapeList.getSelectedIndices().length == 1) {
					MSShape shape = subShapeList.getSelectedValue();
					if (shape instanceof MSRectangle) {
						cardLayout.show(propertiesPanel, SHAPE.RECTANGLE.toString());
					} else if (shape instanceof MSLine) {
						cardLayout.show(propertiesPanel, SHAPE.LINE.toString());
					} else if (shape instanceof MSPoint) {
						cardLayout.show(propertiesPanel, SHAPE.POINT.toString());
					} else if (shape instanceof MSQuadricCurve) {
						cardLayout.show(propertiesPanel, SHAPE.QUAD.toString());
					} else if (shape instanceof MSCubicCurve) {
						cardLayout.show(propertiesPanel, SHAPE.CCURVE.toString());
					} else if (shape instanceof MSOval) {
						cardLayout.show(propertiesPanel, SHAPE.OVAL.toString());
					} else {
						cardLayout.show(propertiesPanel, SHAPE.NONE.toString());
					}
					getCurrentPanel().setCurrentShape(shape);
					currentSelectedShapes.clear();
					currentSelectedShapes.add(shape);
					if (shapeList.getSelectedValue() != null) {
						draw(shapeList.getSelectedValue());
					}
				} else {
					cardLayout.show(propertiesPanel, SHAPE.NONE.toString());
					currentSelectedShapes.clear();
					for (MSShape shape : subShapeList.getSelectedValuesList()) {
						currentSelectedShapes.add(shape);
					}
					if (shapeList.getSelectedValue() != null) {
						draw(shapeList.getSelectedValue());
					}
				}
			}
		});

		shapeList.addListSelectionListener(this);
		listPanel = new JPanel();
		upButton = new JButton("Up"); //$NON-NLS-1$
		upButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		upButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentRoomGraphic != null) {
					if (subShapeList.getSelectedIndex() == 0) return;
					int newIndex = currentRoomGraphic.moveItemUp(subShapeList.getSelectedIndex());
					updateSubShapeList();
					if (newIndex >= 0 && newIndex < subShapeList.getModel().getSize()) {
						subShapeList.setSelectedIndex(newIndex);
					}
					draw(currentRoomGraphic);
					currentSelectedShapes.clear();
				}
			}
		});
		downButton = new JButton("Down"); //$NON-NLS-1$
		downButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		downButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentRoomGraphic != null) {
					if ((subShapeList.getModel().getSize() - 1) == subShapeList.getSelectedIndex()) return;
					int newIndex = currentRoomGraphic.moveItemDown(subShapeList.getSelectedIndex());
					updateSubShapeList();
					if (newIndex >= 0 && newIndex < subShapeList.getModel().getSize()) {
						subShapeList.setSelectedIndex(newIndex);
					}
					draw(currentRoomGraphic);
				}
			}
		});
		deleteShapeButton = new JButton("Del"); //$NON-NLS-1$
		deleteShapeButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		deleteShapeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (currentRoomGraphic != null) {
					int oldIndex = subShapeList.getSelectedIndex();
					currentRoomGraphic.deleteShape(subShapeList.getSelectedIndex());
					updateSubShapeList();
					if (oldIndex >= 0 && oldIndex < subShapeList.getModel().getSize()) {
						subShapeList.setSelectedIndex(oldIndex);
					} else {
						subShapeList.setSelectedIndex(subShapeList.getModel().getSize() - 1);
					}
					draw(currentRoomGraphic);
				}
			}
		});

		copySubShapeButton = new JButton("Copy Shape(s)"); //$NON-NLS-1$
		copySubShapeButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		copySubShapeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				copyArray.clear();
				copyArray.addAll(currentSelectedShapes);
				pasteSubShapeButton.setEnabled(true);
			}
		});
		pasteSubShapeButton = new JButton("Paste"); //$NON-NLS-1$
		pasteSubShapeButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		pasteSubShapeButton.setEnabled(false);
		pasteSubShapeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (MSShape shape : copyArray) {
					currentRoomGraphic.addShape(shape);
				}
				draw(currentRoomGraphic);
				updateSubShapeList();
				RoomGraphicsManager.getInstance().saveShapes();
			}
		});

		GridBagLayout gbl2 = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		listPanel.setLayout(gbl2);
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.insets = new Insets(1, 1, 1, 5);
		c.fill = GridBagConstraints.BOTH;
		JLabel listLabel = new JLabel("Graphics", JLabel.CENTER); //$NON-NLS-1$
		listPanel.add(listLabel, c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.insets = new Insets(1, 1, 1, 5);
		c.fill = GridBagConstraints.BOTH;
		listPanel.add(jp1, c);

		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.insets = new Insets(1, 1, 1, 1);
		listPanel.add(copyButton, c);
		c.gridx = 1;
		c.gridy = 2;
		listPanel.add(addButton, c);
		c.gridx = 2;
		c.gridy = 2;
		c.insets = new Insets(1, 1, 1, 5);
		listPanel.add(deleteButton, c);
		c.gridx = 3;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.insets = new Insets(1, 1, 1, 5);
		listPanel.add(jp2, c);
		c.gridx = 3;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 3;
		c.insets = new Insets(1, 1, 1, 5);
		JLabel subListLabel = new JLabel("Shapes", JLabel.CENTER); //$NON-NLS-1$
		listPanel.add(subListLabel, c);
		c.gridx = 3;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(1, 1, 1, 1);
		listPanel.add(upButton, c);
		c.gridx = 4;
		c.gridy = 2;
		listPanel.add(downButton, c);
		c.gridx = 5;
		c.gridy = 2;
		c.insets = new Insets(1, 1, 1, 5);
		listPanel.add(deleteShapeButton, c);
		c.gridx = 3;
		c.gridy = 3;
		c.gridwidth = 2;
		c.insets = new Insets(1, 1, 1, 5);
		listPanel.add(copySubShapeButton, c);
		c.gridx = 5;
		c.gridy = 3;
		c.gridwidth = 1;
		c.insets = new Insets(1, 1, 1, 5);
		listPanel.add(pasteSubShapeButton, c);
		setLayout(new BorderLayout());
		add(listPanel, BorderLayout.WEST);
		add(checkersPanel, BorderLayout.CENTER);
		add(shapesPanel, BorderLayout.EAST);
		add(buttonPanel, BorderLayout.SOUTH);
		add(generalInformationPanel, BorderLayout.NORTH);
		SwingUtilities.updateComponentTreeUI(this);
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private JPanel createGeneralInformationPanel() {
		JPanel toReturn = new JPanel();
		nameLabel = new JLabel("Name:"); //$NON-NLS-1$
		idLabel = new JLabel("Id:"); //$NON-NLS-1$
		nameField = new JTextField();
		nameField.addKeyListener(this);
		nameField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				currentRoomGraphic.setName(nameField.getText());
				int oid = currentRoomGraphic.getId();
				RoomGraphicsManager.getInstance().addShape(currentRoomGraphic);
				shapeList.setListData(RoomGraphicsManager.getInstance().getShapes());
				int i = 0;
				for (RoomGraphic rg : RoomGraphicsManager.getInstance().getShapes()) {
					if (rg.getId() == oid) {
						break;
					}
					i++;
				}
				shapeList.setSelectedIndex(i);
			}
		});
		idField = new JTextField();
		toReturn.setLayout(new GridLayout(2, 2));
		toReturn.add(idLabel);
		toReturn.add(idField);
		toReturn.add(nameLabel);
		toReturn.add(nameField);
		return toReturn;
	}

	private JPanel createShapesPanel() {
		JPanel toReturn = new JPanel();
		GridBagLayout gbl1 = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		toReturn.setLayout(gbl1);
		filledBox = new JCheckBox("Filled?"); //$NON-NLS-1$
		filledBox.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		lineButton = new JButton("Line"); //$NON-NLS-1$
		lineButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		lineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (lineButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.LINE;
					lineButton.setForeground(Color.GREEN);
					statusLabel.setText("Click on the square where you want the line to start."); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					lineButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		pointButton = new JButton("Point"); //$NON-NLS-1$
		pointButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		pointButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (pointButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.POINT;
					pointButton.setForeground(Color.GREEN);
					statusLabel.setText("Click on the square where you want the point to be."); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					pointButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		quadButton = new JButton("Quad"); //$NON-NLS-1$
		quadButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		quadButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (quadButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.QUAD;
					quadButton.setForeground(Color.GREEN);
					statusLabel.setText("Click on the square where you want the start of the curve."); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					quadButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		curveButton = new JButton("Curve"); //$NON-NLS-1$
		curveButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		curveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (curveButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.CCURVE;
					curveButton.setForeground(Color.GREEN);
					statusLabel.setText("Click on the square where you want the start of the curve."); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					curveButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		rectButton = new JButton("Rectangle"); //$NON-NLS-1$
		rectButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		rectButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (rectButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.RECTANGLE;
					rectButton.setForeground(Color.GREEN);
					statusLabel.setText("Click on the square where you want the upper left corner of the rectangle."); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					rectButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		ovalButton = new JButton("Oval"); //$NON-NLS-1$
		ovalButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		ovalButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ovalButton.getForeground().equals(Color.BLACK)) {
					resetButtons();
					currentShape = SHAPE.OVAL;
					ovalButton.setForeground(Color.GREEN);
					statusLabel.setText("Click where you want the \"upper left corner\" of the circle"); //$NON-NLS-1$
				} else {
					currentShape = SHAPE.NONE;
					ovalButton.setForeground(Color.BLACK);
					statusLabel.setText(""); //$NON-NLS-1$
				}
			}
		});
		colourButton = new JButton("Colour"); //$NON-NLS-1$
		colourButton.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		cardLayout = new CardLayout();
		propertiesPanel = new JPanel();
		propertiesPanel.setLayout(cardLayout);
		for (SHAPE shape : SHAPE.values()) {
			PropPanel pp = new PropPanel(shape, mapper, this);
			pp.addPropertyListener(this);
			propertiesPanel.add(pp, shape.toString());
		}
		cardLayout.show(propertiesPanel, SHAPE.NONE.toString());

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.BOTH;
		toReturn.add(filledBox, gbc);
		gbc.gridy = 1;
		toReturn.add(lineButton, gbc);
		gbc.gridy = 2;
		toReturn.add(rectButton, gbc);
		gbc.gridy = 3;
		toReturn.add(pointButton, gbc);
		gbc.gridy = 4;
		toReturn.add(ovalButton, gbc);
		gbc.gridy = 5;
		toReturn.add(curveButton, gbc);
		gbc.gridy = 6;
		toReturn.add(quadButton, gbc);
		gbc.gridy = 7;
		toReturn.add(colourButton, gbc);
		gbc.gridy = 8;
		toReturn.add(propertiesPanel, gbc);
		return toReturn;
	}

	private JPanel createCheckersPanel() {
		JPanel toReturn = new JPanel();
		checkers = new Square[side][side];
		int checkWidth = 700 / side;
		Dimension d = new Dimension(checkWidth, checkWidth);
		toReturn.setLayout(new GridBagLayout());
		GridBagConstraints con = new GridBagConstraints();
		con.fill = GridBagConstraints.BOTH;

		border = (int) ((side - roomDimInPixels.getHeight()) / 2);
		for (int r = 0; r < side; r++) {
			for (int c = 0; c < side; c++) {
				checkers[r][c] = new Square(r, c);
				if (r < border || r > side - border - 1 || c < border || c > side - border - 1) {
					checkers[r][c].setBackground(Color.GRAY);
				} else {
					checkers[r][c].setBackground(Color.WHITE);
				}
				checkers[r][c].setSize(d);
				checkers[r][c].setPreferredSize(d);
				checkers[r][c].setMinimumSize(d);
				checkers[r][c].setBorder(BorderFactory.createLineBorder(Color.BLACK));
				con.gridy = r;
				con.gridx = c;
				toReturn.add(checkers[r][c], con);
			}
		}

		toReturn.addMouseListener(this);
		toReturn.addMouseMotionListener(this);
		toReturn.setPreferredSize(new Dimension(700, 700));

		return toReturn;
	}

	public PropPanel getCurrentPanel() {
		PropPanel currentPanel = null;

		for (Component component : propertiesPanel.getComponents()) {
			if (component.isVisible()) {
				if (component instanceof PropPanel) {
					currentPanel = (PropPanel) component;
				}
			}
		}
		return currentPanel;
	}

	protected void updateSubShapeList() {
		if (currentRoomGraphic == null) return;
		MSShape[] shapes = new MSShape[currentRoomGraphic.getShapes().size()];
		shapes = currentRoomGraphic.getShapes().toArray(shapes);
		subShapeList.setListData(shapes);
		jp2.revalidate();
	}

	@Override
	public void mouseClicked(MouseEvent me) {
		if (currentRoomGraphic == null && currentShape != SHAPE.NONE)
																		JOptionPane.showMessageDialog(	this,
																										"You have to have an object choosen to be able to add something!", //$NON-NLS-1$
																										"Hey! Stop kissing my sister!", JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
		if (currentShape == null || currentRoomGraphic == null) return;
		Component c = checkersPanel.getComponentAt(me.getPoint());
		if (c instanceof Square) {
			Square pressed = (Square) c;
			switch (currentShape) {
				case POINT:
					MSPoint point = new MSPoint(pressed.getPoint().x - border, pressed.getPoint().y - border);
					point.setColour(currentColour);
					currentRoomGraphic.addShape(point.finish());
					statusLabel.setText("..."); //$NON-NLS-1$
					break;
				case CCURVE:
					break;
				case LINE:
					if (firstSet) {
						firstSet = false;
						MSLine line = new MSLine();
						line.setColour(currentColour);
						line.setX1(firstPoint.x - border);
						line.setY1(firstPoint.y - border);
						line.setX2(pressed.getPoint().x - border);
						line.setY2(pressed.getPoint().y - border);
						currentRoomGraphic.addShape(line.finish());
						statusLabel.setText(""); //$NON-NLS-1$
					} else {
						firstSet = true;
						firstPoint = pressed.getPoint();
						statusLabel.setText("Click on the square you want the line to end at."); //$NON-NLS-1$
					}
					break;
				case OVAL:
					if (firstSet) {
						firstSet = false;
						MSOval oval = new MSOval();
						oval.setColour(currentColour);
						oval.setFilled(filledBox.isSelected());
						if ((pressed.getPoint().x - firstPoint.x) < 0) {
							oval.setX(pressed.getPoint().x - border);
						} else {
							oval.setX(firstPoint.x - border);
						}
						if ((pressed.getPoint().y - firstPoint.y) < 0) {
							oval.setY(pressed.getPoint().y - border);
						} else {
							oval.setY(firstPoint.y - border);
						}
						oval.setWidth(Math.abs(pressed.getPoint().x - firstPoint.x));
						oval.setHeight(Math.abs(pressed.getPoint().y - firstPoint.y));
						currentRoomGraphic.addShape(oval.finish());
						statusLabel.setText(""); //$NON-NLS-1$
					} else {
						firstSet = true;
						firstPoint = pressed.getPoint();
						statusLabel.setText("Click on the lower right corner of your rectangle."); //$NON-NLS-1$
					}
					break;
				case QUAD:
					break;
				case RECTANGLE:
					if (firstSet) {
						firstSet = false;
						MSRectangle rect = new MSRectangle();
						rect.setColour(currentColour);
						rect.setFilled(filledBox.isSelected());
						if ((pressed.getPoint().x - firstPoint.x) < 0) {
							rect.setX(pressed.getPoint().x - border);
						} else {
							rect.setX(firstPoint.x - border);
						}
						if ((pressed.getPoint().y - firstPoint.y) < 0) {
							rect.setY(pressed.getPoint().y - border);
						} else {
							rect.setY(firstPoint.y - border);
						}
						rect.setWidth(Math.abs(pressed.getPoint().x - firstPoint.x + (filledBox.isSelected() ? 1 : 0)));
						rect.setHeight(Math.abs(pressed.getPoint().y - firstPoint.y + (filledBox.isSelected() ? 1 : 0)));
						currentRoomGraphic.addShape(rect.finish());
						statusLabel.setText(""); //$NON-NLS-1$
					} else {
						firstSet = true;
						firstPoint = pressed.getPoint();
						statusLabel.setText("Click on the lower right corner of your rectangle."); //$NON-NLS-1$
					}
					break;
				default:
					break;
			}
			updateSubShapeList();
			RoomGraphicsManager.getInstance().updateShape(currentRoomGraphic);
			draw(currentRoomGraphic);
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent me) {
		Component c = checkersPanel.getComponentAt(me.getPoint());
		if (c instanceof Square) {
			// Square pressed = (Square) c;
		}
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		Component c = checkersPanel.getComponentAt(me.getPoint());
		if (c instanceof Square) {
			// Square pressed = (Square) c;
		}
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		Component c = checkersPanel.getComponentAt(me.getPoint());
		if (c instanceof Square) {
			// Square pressed = (Square) c;
		}
	}

	@Override
	public void mouseMoved(MouseEvent me) {}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		currentRoomGraphic = shapeList.getSelectedValue();
		if (currentRoomGraphic == null) return;
		draw(currentRoomGraphic);
		MSShape[] shapes = new MSShape[currentRoomGraphic.getShapes().size()];
		shapes = currentRoomGraphic.getShapes().toArray(shapes);
		subShapeList.setListData(shapes);
		subShapeList.validate();
		jp2.validate();
		nameField.setText(currentRoomGraphic.getName());
		idField.setText(Integer.toString(currentRoomGraphic.getId()));
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		draw(currentRoomGraphic);
	}

	@Override
	public void keyPressed(KeyEvent e) {}

	@Override
	public void keyReleased(KeyEvent e) {
		if (currentRoomGraphic == null) return;
		currentRoomGraphic.setName(nameField.getText());
		int oid = currentRoomGraphic.getId();
		RoomGraphicsManager.getInstance().addShape(currentRoomGraphic);
		shapeList.setListData(RoomGraphicsManager.getInstance().getShapes());
		int i = 0;
		for (RoomGraphic rg : RoomGraphicsManager.getInstance().getShapes()) {
			if (rg.getId() == oid) {
				break;
			}
			i++;
		}
		shapeList.setSelectedIndex(i);
	}

	@Override
	public void keyTyped(KeyEvent e) {}
}

class PropPanel extends JPanel {
	/**
	 * 
	 */
	private static final long						serialVersionUID	= -6688165981627069982L;
	private MSShape									currentShape;
	private final ArrayList<PropertyChangeListener>	propertyListeners;
	private final Mapper							mapper;
	private final MSShapeEditorDialog				parent;

	public PropPanel(MSShapeEditorDialog.SHAPE shape, Mapper mapper, MSShapeEditorDialog parent) {
		this.mapper = mapper;
		this.parent = parent;

		propertyListeners = new ArrayList<PropertyChangeListener>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		switch (shape) {
			case CCURVE:
				add(createCCurveComponent());
				break;
			case LINE:
				add(createLineComponent());
				break;
			case OVAL:
				add(createOvalComponent());
				break;
			case POINT:
				add(createPointComponent());
				break;
			case QUAD:
				add(createQuadComponent());
				break;
			case RECTANGLE:
				add(createRectangleComponent());
				break;
			case NONE:
				add(new JLabel("Select shape.")); //$NON-NLS-1$
				break;
			default:
				break;
		}
	}

	public void addPropertyListener(PropertyChangeListener pcl) {
		propertyListeners.add(pcl);
	}

	private void properyChanged() {
		for (PropertyChangeListener pcl : propertyListeners) {
			pcl.propertyChange(null);
		}
	}

	JLabel		rect_xLabel, rect_yLabel, rect_widthLabel, rect_heightLabel, rect_colourLabel, rect_showColourLabel;
	JCheckBox	rect_filledBox;
	JSpinner	rect_xSpinner, rect_ySpinner, rect_widthSpinner, rect_heightSpinner;

	private JPanel createRectangleComponent() {
		JPanel toReturn = new JPanel();
		SpinnerNumberModel xmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel ymodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel wmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel hmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		rect_xLabel = new JLabel("X:"); //$NON-NLS-1$
		rect_xSpinner = new JSpinner(xmodel);
		rect_xSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		rect_xSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSRectangle rect = (MSRectangle) currentShape;
				rect.setX(((Integer) rect_xSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		rect_yLabel = new JLabel("Y:"); //$NON-NLS-1$
		rect_ySpinner = new JSpinner(ymodel);
		rect_ySpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		rect_ySpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSRectangle rect = (MSRectangle) currentShape;
				rect.setY(((Integer) rect_ySpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		rect_widthLabel = new JLabel("Width:"); //$NON-NLS-1$
		rect_widthSpinner = new JSpinner(wmodel);
		rect_widthSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		rect_widthSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSRectangle rect = (MSRectangle) currentShape;
				rect.setWidth(((Integer) rect_widthSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		rect_heightLabel = new JLabel("Height:"); //$NON-NLS-1$
		rect_heightSpinner = new JSpinner(hmodel);
		rect_heightSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		rect_heightSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSRectangle rect = (MSRectangle) currentShape;
				rect.setHeight(((Integer) rect_heightSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});

		rect_colourLabel = new JLabel("Colour:"); //$NON-NLS-1$
		rect_showColourLabel = new JLabel(" "); //$NON-NLS-1$
		rect_showColourLabel.setOpaque(true);
		rect_showColourLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(rect_showColourLabel.getBackground(), parent);
				Color currentColour = Color.BLACK;
				Color theNewColor = mapper.getCurrentColour();
				if (theNewColor != null) {
					currentColour = theNewColor;
				} else {
					currentColour = Color.BLACK;
				}

				rect_showColourLabel.setBackground(currentColour);
				currentShape.setColour(currentColour);
				properyChanged();
			}
		});
		rect_filledBox = new JCheckBox("Filled:"); //$NON-NLS-1$
		rect_filledBox.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		rect_filledBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				MSRectangle rect = (MSRectangle) currentShape;
				rect.setFilled(rect_filledBox.isSelected());
				properyChanged();
			}
		});
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		toReturn.setLayout(gbl);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		toReturn.add(rect_xLabel, c);
		c.gridx = 1;
		c.gridy = 0;
		toReturn.add(rect_xSpinner, c);
		c.gridx = 0;
		c.gridy = 1;
		toReturn.add(rect_yLabel, c);
		c.gridx = 1;
		c.gridy = 1;
		toReturn.add(rect_ySpinner, c);
		c.gridx = 0;
		c.gridy = 2;
		toReturn.add(rect_widthLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		toReturn.add(rect_widthSpinner, c);
		c.gridx = 0;
		c.gridy = 3;
		toReturn.add(rect_heightLabel, c);
		c.gridx = 1;
		c.gridy = 3;
		toReturn.add(rect_heightSpinner, c);
		c.gridx = 0;
		c.gridy = 4;
		toReturn.add(rect_colourLabel, c);
		c.gridx = 1;
		c.gridy = 4;
		toReturn.add(rect_showColourLabel, c);
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 2;
		toReturn.add(rect_filledBox, c);
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Rectangle")); //$NON-NLS-1$

		return toReturn;
	}

	private JPanel createQuadComponent() {
		JPanel toReturn = new JPanel();
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Quad Curve")); //$NON-NLS-1$
		return toReturn;
	}

	JLabel	point_xLabel, point_yLabel, point_colourLabel, point_showColourLabel;
	JSpinner	point_xSpinner, point_ySpinner;

	private JPanel createPointComponent() {
		JPanel toReturn = new JPanel();
		SpinnerNumberModel xmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel ymodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		point_xLabel = new JLabel("X:"); //$NON-NLS-1$
		point_xSpinner = new JSpinner(xmodel);
		point_xSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		point_xSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSPoint oval = (MSPoint) currentShape;
				oval.setX(((Integer) point_xSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		point_yLabel = new JLabel("Y:"); //$NON-NLS-1$
		point_ySpinner = new JSpinner(ymodel);
		point_ySpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		point_ySpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSPoint oval = (MSPoint) currentShape;
				oval.setY(((Integer) point_ySpinner.getValue()).doubleValue());
				properyChanged();
			}

		});

		point_colourLabel = new JLabel("Colour:"); //$NON-NLS-1$
		point_showColourLabel = new JLabel(" "); //$NON-NLS-1$
		point_showColourLabel.setOpaque(true);
		point_showColourLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(point_showColourLabel.getBackground(), parent);
				Color currentColour = Color.BLACK;
				Color theNewColor = mapper.getCurrentColour();
				if (theNewColor != null) {
					currentColour = theNewColor;
				} else {
					currentColour = Color.BLACK;
				}

				point_showColourLabel.setBackground(currentColour);
				currentShape.setColour(currentColour);
				properyChanged();

			}
		});
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		toReturn.setLayout(gbl);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		toReturn.add(point_xLabel, c);
		c.gridx = 1;
		c.gridy = 0;
		toReturn.add(point_xSpinner, c);
		c.gridx = 0;
		c.gridy = 1;
		toReturn.add(point_yLabel, c);
		c.gridx = 1;
		c.gridy = 1;
		toReturn.add(point_ySpinner, c);
		c.gridx = 0;
		c.gridy = 2;
		toReturn.add(point_colourLabel, c);
		c.gridx = 1;
		toReturn.add(point_showColourLabel, c);
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Point")); //$NON-NLS-1$
		return toReturn;
	}

	JLabel		oval_xLabel, oval_yLabel, oval_widthLabel, oval_heightLabel, oval_colourLabel, oval_showColourLabel;
	JCheckBox	oval_filledBox;
	JSpinner	oval_xSpinner, oval_ySpinner, oval_widthSpinner, oval_heightSpinner;

	private JPanel createOvalComponent() {
		JPanel toReturn = new JPanel();
		SpinnerNumberModel xmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel ymodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel wmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel hmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		oval_xLabel = new JLabel("X:"); //$NON-NLS-1$
		oval_xSpinner = new JSpinner(xmodel);
		oval_xSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		oval_xSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSOval oval = (MSOval) currentShape;
				oval.setX(((Integer) oval_xSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		oval_yLabel = new JLabel("Y:"); //$NON-NLS-1$
		oval_ySpinner = new JSpinner(ymodel);
		oval_ySpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		oval_ySpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSOval oval = (MSOval) currentShape;
				oval.setY(((Integer) oval_ySpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		oval_widthLabel = new JLabel("Width:"); //$NON-NLS-1$
		oval_widthSpinner = new JSpinner(wmodel);
		oval_widthSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		oval_widthSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSOval oval = (MSOval) currentShape;
				oval.setWidth(((Integer) oval_widthSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		oval_heightLabel = new JLabel("Height:"); //$NON-NLS-1$
		oval_heightSpinner = new JSpinner(hmodel);
		oval_heightSpinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		oval_heightSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSOval oval = (MSOval) currentShape;
				oval.setHeight(((Integer) oval_heightSpinner.getValue()).doubleValue());
				properyChanged();
			}

		});

		oval_colourLabel = new JLabel("Colour:"); //$NON-NLS-1$
		oval_showColourLabel = new JLabel(" "); //$NON-NLS-1$
		oval_showColourLabel.setOpaque(true);
		oval_showColourLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(oval_showColourLabel.getBackground(), parent);
				Color currentColour = Color.BLACK;
				Color theNewColor = mapper.getCurrentColour();
				if (theNewColor != null) {
					currentColour = theNewColor;
				} else {
					currentColour = Color.BLACK;
				}

				oval_showColourLabel.setBackground(currentColour);
				currentShape.setColour(currentColour);
				properyChanged();

			}
		});
		oval_filledBox = new JCheckBox("Filled:"); //$NON-NLS-1$
		oval_filledBox.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		oval_filledBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				MSOval oval = (MSOval) currentShape;
				oval.setFilled(oval_filledBox.isSelected());
				properyChanged();
			}
		});
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		toReturn.setLayout(gbl);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		toReturn.add(oval_xLabel, c);
		c.gridx = 1;
		c.gridy = 0;
		toReturn.add(oval_xSpinner, c);
		c.gridx = 0;
		c.gridy = 1;
		toReturn.add(oval_yLabel, c);
		c.gridx = 1;
		c.gridy = 1;
		toReturn.add(oval_ySpinner, c);
		c.gridx = 0;
		c.gridy = 2;
		toReturn.add(oval_widthLabel, c);
		c.gridx = 1;
		c.gridy = 2;
		toReturn.add(oval_widthSpinner, c);
		c.gridx = 0;
		c.gridy = 3;
		toReturn.add(oval_heightLabel, c);
		c.gridx = 1;
		c.gridy = 3;
		toReturn.add(oval_heightSpinner, c);
		c.gridx = 0;
		c.gridy = 4;
		toReturn.add(oval_colourLabel, c);
		c.gridx = 1;
		c.gridy = 4;
		toReturn.add(oval_showColourLabel, c);
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 2;
		toReturn.add(oval_filledBox, c);
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Oval")); //$NON-NLS-1$
		return toReturn;
	}

	JLabel	line_x1Label, line_y1Label, line_x2Label, line_y2Label, line_colourLabel, line_showColourLabel;
	JSpinner	line_x1Spinner, line_y1Spinner, line_x2Spinner, line_y2Spinner;

	private JPanel createLineComponent() {
		JPanel toReturn = new JPanel();
		SpinnerNumberModel xmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel ymodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel wmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		SpinnerNumberModel hmodel = new SpinnerNumberModel(1, -1000, 1000, 1);
		line_x1Label = new JLabel("Start X:"); //$NON-NLS-1$
		line_x1Spinner = new JSpinner(xmodel);
		line_x1Spinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		line_x1Spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSLine oval = (MSLine) currentShape;
				oval.setX1(((Integer) line_x1Spinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		line_y1Label = new JLabel("Start Y:"); //$NON-NLS-1$
		line_y1Spinner = new JSpinner(ymodel);
		line_y1Spinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		line_y1Spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSLine oval = (MSLine) currentShape;
				oval.setY1(((Integer) line_y1Spinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		line_x2Label = new JLabel("End X:"); //$NON-NLS-1$
		line_x2Spinner = new JSpinner(wmodel);
		line_x2Spinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		line_x2Spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSLine oval = (MSLine) currentShape;
				oval.setX2(((Integer) line_x2Spinner.getValue()).doubleValue());
				properyChanged();
			}

		});
		line_y2Label = new JLabel("End Y:"); //$NON-NLS-1$
		line_y2Spinner = new JSpinner(hmodel);
		line_y2Spinner.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$

		line_y2Spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				MSLine oval = (MSLine) currentShape;
				oval.setY2(((Integer) line_y2Spinner.getValue()).doubleValue());
				properyChanged();
			}

		});

		line_colourLabel = new JLabel("Colour:"); //$NON-NLS-1$
		line_showColourLabel = new JLabel(" "); //$NON-NLS-1$
		line_showColourLabel.setOpaque(true);
		line_showColourLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(line_showColourLabel.getBackground(), parent);
				Color currentColour = Color.BLACK;
				Color theNewColor = mapper.getCurrentColour();
				if (theNewColor != null) {
					currentColour = theNewColor;
				} else {
					currentColour = Color.BLACK;
				}

				line_showColourLabel.setBackground(currentColour);
				currentShape.setColour(currentColour);
				properyChanged();
			}
		});
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		toReturn.setLayout(gbl);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		toReturn.add(line_x1Label, c);
		c.gridx = 1;
		c.gridy = 0;
		toReturn.add(line_x1Spinner, c);
		c.gridx = 0;
		c.gridy = 1;
		toReturn.add(line_y1Label, c);
		c.gridx = 1;
		c.gridy = 1;
		toReturn.add(line_y1Spinner, c);
		c.gridx = 0;
		c.gridy = 2;
		toReturn.add(line_x2Label, c);
		c.gridx = 1;
		c.gridy = 2;
		toReturn.add(line_x2Spinner, c);
		c.gridx = 0;
		c.gridy = 3;
		toReturn.add(line_y2Label, c);
		c.gridx = 1;
		c.gridy = 3;
		toReturn.add(line_y2Spinner, c);
		c.gridx = 0;
		c.gridy = 4;
		toReturn.add(line_colourLabel, c);
		c.gridx = 1;
		c.gridy = 4;
		toReturn.add(line_showColourLabel, c);
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Line")); //$NON-NLS-1$
		return toReturn;
	}

	private JPanel createCCurveComponent() {
		JPanel toReturn = new JPanel();
		toReturn.putClientProperty("JComponent.sizeVariant", "mini"); //$NON-NLS-1$ //$NON-NLS-2$
		toReturn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Curve")); //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * @return the currentShape
	 */
	public MSShape getCurrentShape() {
		return currentShape;
	}

	/**
	 * @param currentShape
	 *            the currentShape to set
	 */
	public void setCurrentShape(MSShape currentShape) {
		this.currentShape = currentShape;
		updateGraphics();
	}

	private void updateGraphics() {
		if (currentShape instanceof MSRectangle) {
			MSRectangle rectangle = (MSRectangle) currentShape;
			rect_xSpinner.setValue(new Integer((int) rectangle.getX()));
			rect_ySpinner.setValue(new Integer((int) rectangle.getY()));
			rect_widthSpinner.setValue(new Integer((int) rectangle.getWidth()));
			rect_heightSpinner.setValue(new Integer((int) rectangle.getHeight()));
			rect_filledBox.setSelected(rectangle.isFilled());
			rect_showColourLabel.setBackground(rectangle.getColour());
		} else if (currentShape instanceof MSOval) {
			MSOval oval = (MSOval) currentShape;
			oval_xSpinner.setValue(new Integer((int) oval.getX()));
			oval_ySpinner.setValue(new Integer((int) oval.getY()));
			oval_widthSpinner.setValue(new Integer((int) oval.getWidth()));
			oval_heightSpinner.setValue(new Integer((int) oval.getHeight()));
			oval_filledBox.setSelected(oval.isFilled());
			oval_showColourLabel.setBackground(oval.getColour());
		} else if (currentShape instanceof MSLine) {
			MSLine line = (MSLine) currentShape;
			line_x1Spinner.setValue(new Integer((int) line.getX1()));
			line_y1Spinner.setValue(new Integer((int) line.getY1()));
			line_x2Spinner.setValue(new Integer((int) line.getX2()));
			line_y2Spinner.setValue(new Integer((int) line.getY2()));
			line_showColourLabel.setBackground(line.getColour());
		} else if (currentShape instanceof MSPoint) {
			MSPoint line = (MSPoint) currentShape;
			point_xSpinner.setValue(new Integer((int) line.getX1()));
			point_ySpinner.setValue(new Integer((int) line.getY1()));
			point_showColourLabel.setBackground(line.getColour());
		}
	}

	@Override
	public String toString() {
		return currentShape.toString();
	}
}

class Square extends JPanel {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1906638712176436483L;
	int							x, y;
	private boolean				selected;

	public Square(int y, int x) {
		super();
		this.x = x;
		this.y = y;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getXpos() {
		return x;
	}

	public int getYpos() {
		return y;
	}

	public Point getPoint() {
		return new Point(x, y);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (selected) {
			g.setColor(getBackground());
			g.fillRect(1, 1, getWidth() - 2, getHeight() - 2);
			g.setColor(getContrastColor(getBackground()));
			g.fillRect(getWidth() / 2 - 1, getHeight() / 2 - 1, 2, 2);
		}
	}

	private Color getContrastColor(Color color) {
		double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
		return y >= 128 ? Color.black : Color.white;
	}

}
