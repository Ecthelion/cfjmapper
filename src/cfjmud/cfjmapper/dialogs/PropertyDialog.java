/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.LabeledTextField;
import cfjmud.cfjmapper.custom.RoomRep;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;
import cfjmud.cfjmapper.panels.MapPanel;

/**
 * 
 * @author Ecthelion
 * 
 */
class MyTableModel extends DefaultTableModel {
	/**
     * 
     */
	private static final long	serialVersionUID	= -4869819960801760566L;

	public MyTableModel(String[][] array, String[] tabs) {
		super(array, tabs);
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		return col != 0;

	}
}

public class PropertyDialog extends JDialog implements PropertyChangeListener {

	private final static String		SIMPLE				= "Simple";				//$NON-NLS-1$
	private final static String		ADVANCED			= "Advanced";				//$NON-NLS-1$
	/**
     * 
     */
	private static final long		serialVersionUID	= -3781800987634247415L;
	private JButton					advancedButton;
	private final JPanel			advancedPropertyEditPanel;
	private JButton					applyButton;
	private final JPanel			buttonPanel;
	private JButton					cancelButton;
	private final Database			database;
	private final Dimension			mainViewSize		= new Dimension(750, 350);
	private final Mapper			mapper;
	private JButton					okButton;
	private final MapperProperties	properties;
	private final JPanel			propertyPanel;
	private boolean					simple				= true;
	private final JPanel			simpleProprtyEditPanel;
	private JTable					table;
	private final Properties		tmpProps;

	public PropertyDialog(Mapper mapper, MapperProperties properties) {
		super(mapper, true);
		this.mapper = mapper;
		tmpProps = new Properties();
		database = Database.getInstance();
		this.properties = properties;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		advancedPropertyEditPanel = createAdvancedPanel();
		buttonPanel = createButtonPanel();
		simpleProprtyEditPanel = createSimplePropertyPanel();
		propertyPanel = new JPanel(new CardLayout());
		propertyPanel.add(advancedPropertyEditPanel, ADVANCED);
		propertyPanel.add(simpleProprtyEditPanel, SIMPLE);

		CardLayout cl = (CardLayout) (propertyPanel.getLayout());
		cl.show(propertyPanel, SIMPLE);
		advancedButton.setText("Advanced"); //$NON-NLS-1$

		add(propertyPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setTitle("Preferences"); //$NON-NLS-1$
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	protected void advancedButtonActionPerformed() {
		CardLayout cl = (CardLayout) (propertyPanel.getLayout());
		if (simple) {
			cl.show(propertyPanel, ADVANCED);
			advancedButton.setText("Simple"); //$NON-NLS-1$
			simple = false;
		} else {
			cl.show(propertyPanel, SIMPLE);
			advancedButton.setText("Advanced"); //$NON-NLS-1$
			simple = true;
		}
		repaint();
	}

	protected void applyButtonActionPerformed() {
		properties.putAll(tmpProps);
		applyButton.setEnabled(false);
		tmpProps.clear();
		mapper.resetColoursInMainMap();
	}

	private JPanel createAdvancedPanel() {
		JPanel panelToReturn = new JPanel();

		Enumeration<?> elements = properties.propertyNames();
		int size = properties.size();
		String[][] array = new String[size][2];
		int y = 0;
		while (elements.hasMoreElements()) {
			String name = (String) elements.nextElement();
			String prop = properties.getProperty(name);
			array[y][0] = name;
			array[y][1] = prop;
			y++;
		}

		String[] tabs = { "Key", "Value" }; //$NON-NLS-1$ //$NON-NLS-2$
		TableModel model = new MyTableModel(array, tabs);
		model.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent tme) {
				tableChangedOccured(tme);
			}
		});
		table = new JTable(model);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPanel = new JScrollPane(table);
		scrollPanel.setSize(mainViewSize);
		scrollPanel.setPreferredSize(mainViewSize);
		panelToReturn.add(scrollPanel, BorderLayout.CENTER);
		return panelToReturn;
	}

	private JPanel createButtonPanel() {
		JPanel toReturn = new JPanel();
		advancedButton = new JButton("Advanced"); //$NON-NLS-1$
		advancedButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				advancedButtonActionPerformed();
			}
		});
		applyButton = new JButton("Apply"); //$NON-NLS-1$
		applyButton.setEnabled(false);
		applyButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				applyButtonActionPerformed();
			}

		});
		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonActionPerformed();

			}
		});
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		toReturn.setLayout(new FlowLayout(FlowLayout.RIGHT));
		toReturn.add(okButton);
		toReturn.add(applyButton);
		toReturn.add(advancedButton);
		toReturn.add(cancelButton);
		return toReturn;
	}

	private JPanel createSimplePropertyPanel() {
		JPanel toReturn = new JPanel();
		JPanel genericPanel = createGenericPanel();
		JPanel htmlPanel = createHTMLPanel();
		JTabbedPane tabPanel = new JTabbedPane();
		tabPanel.setSize(mainViewSize);
		tabPanel.setPreferredSize(mainViewSize);
		tabPanel.add("General", genericPanel); //$NON-NLS-1$
		tabPanel.add("Image Export", new JScrollPane(htmlPanel)); //$NON-NLS-1$
		toReturn.add(tabPanel, BorderLayout.CENTER);
		return toReturn;
	}

	private JPanel createHTMLPanel() {
		JPanel toReturn = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		toReturn.setLayout(layout);
		LabeledTextField htmlStartField = new LabeledTextField(MapperProperties.HTML_START, "HTML Start:", //$NON-NLS-1$
																properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT), true);
		LabeledTextField htmlEndField = new LabeledTextField(MapperProperties.HTML_END, "HTML End:", //$NON-NLS-1$
																properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT), true);
		LabeledTextField htmlCaptionStartField = new LabeledTextField(MapperProperties.HTML_CAPTION_START, "Caption Start:", //$NON-NLS-1$
																		properties.getProperty(	MapperProperties.HTML_CAPTION_START,
																								MapperProperties.HTML_CAPTION_START_DEFAULT), true);
		LabeledTextField htmlCaptionEndField = new LabeledTextField(MapperProperties.HTML_CAPTION_END, "Caption End:", //$NON-NLS-1$
																	properties.getProperty(	MapperProperties.HTML_CAPTION_END,
																							MapperProperties.HTML_CAPTION_END_DEFAULT), true);
		LabeledTextField htmlTableStartField = new LabeledTextField(MapperProperties.HTML_TABLE_START, "Table Start:", //$NON-NLS-1$
																	properties.getProperty(	MapperProperties.HTML_TABLE_START,
																							MapperProperties.HTML_TABLE_START_DEFAULT), true);
		LabeledTextField htmlTableEndField = new LabeledTextField(MapperProperties.HTML_TABLE_END, "Table End:", //$NON-NLS-1$
																	properties.getProperty(	MapperProperties.HTML_TABLE_END,
																							MapperProperties.HTML_TABLE_END_DEFAULT), true);
		LabeledTextField htmlTableRowStartField = new LabeledTextField(MapperProperties.HTML_TABLE_TR_START, "Row Start:", //$NON-NLS-1$
																		properties.getProperty(	MapperProperties.HTML_TABLE_TR_START,
																								MapperProperties.HTML_TABLE_TR_START_DEFAULT), true);
		LabeledTextField htmlTableRowEndField = new LabeledTextField(MapperProperties.HTML_TABLE_TR_END, "Row End:", //$NON-NLS-1$
																		properties.getProperty(	MapperProperties.HTML_TABLE_TR_END,
																								MapperProperties.HTML_TABLE_TR_END_DEFAULT), true);
		LabeledTextField htmlTableTDStartField = new LabeledTextField(MapperProperties.HTML_TABLE_TD_START, "Tabledata Start:", //$NON-NLS-1$
																		properties.getProperty(	MapperProperties.HTML_TABLE_TD_START,
																								MapperProperties.HTML_TABLE_TD_START_DEFAULT), true);
		LabeledTextField htmlTableTDEndField = new LabeledTextField(MapperProperties.HTML_TABLE_TD_END, "Tabledata End:", //$NON-NLS-1$
																	properties.getProperty(	MapperProperties.HTML_TABLE_TD_END,
																							MapperProperties.HTML_TABLE_TD_END_DEFAULT), true);

		htmlStartField.addPropertiesChangedListener(this);
		htmlEndField.addPropertiesChangedListener(this);
		htmlCaptionStartField.addPropertiesChangedListener(this);
		htmlCaptionEndField.addPropertiesChangedListener(this);
		htmlTableStartField.addPropertiesChangedListener(this);
		htmlTableEndField.addPropertiesChangedListener(this);
		htmlTableRowStartField.addPropertiesChangedListener(this);
		htmlTableRowEndField.addPropertiesChangedListener(this);
		htmlTableTDStartField.addPropertiesChangedListener(this);
		htmlTableTDEndField.addPropertiesChangedListener(this);

		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		toReturn.add(htmlStartField, c);
		c.gridy = 2;
		toReturn.add(htmlEndField, c);
		c.gridy = 3;
		toReturn.add(htmlCaptionStartField, c);
		c.gridy = 4;
		toReturn.add(htmlCaptionEndField, c);
		c.gridy = 5;
		toReturn.add(htmlTableTDStartField, c);
		c.gridy = 1;
		c.gridx = 1;
		toReturn.add(htmlTableStartField, c);
		c.gridy = 2;
		toReturn.add(htmlTableEndField, c);
		c.gridy = 3;
		toReturn.add(htmlTableRowStartField, c);
		c.gridy = 4;
		toReturn.add(htmlTableRowEndField, c);
		c.gridy = 5;
		toReturn.add(htmlTableTDEndField, c);
		return toReturn;
	}

	public JPanel createGenericPanel() {
		JPanel toReturn = new JPanel();
		GridLayout gl = new GridLayout(0, 2, 5, 5);
		toReturn.setLayout(gl);
		String[] areas = new String[database.getAreas().size()];
		int index = 0;
		for (Area area : database.getAreas()) {
			areas[index++] = area.getName();
		}
		int startingAreaId = Integer.parseInt(properties.getProperty(MapperProperties.STARTINGAREAID, MapperProperties.STARTINGAREAID_DEFAULT));
		Area startArea = database.getArea(startingAreaId);
		if (startArea == null) {
			startArea = database.getAnyArea();
		}
		String startingAreaStr = startArea.getName();
		LabeledTextField windowWidth = new LabeledTextField(MapperProperties.WINDOWWIDTH,
															"Window Width:", new Integer(properties.getProperty(MapperProperties.WINDOWWIDTH))); //$NON-NLS-1$ 
		LabeledTextField windowHeight = new LabeledTextField(MapperProperties.WINDOWHEIGHT,
																"Window Height:", new Integer(properties.getProperty(MapperProperties.WINDOWHEIGHT))); //$NON-NLS-1$ 
		LabeledTextField windowXPos = new LabeledTextField(MapperProperties.WINDOWSTARTX,
															"Horizontal Position:", new Integer(properties.getProperty(MapperProperties.WINDOWSTARTX))); //$NON-NLS-1$ 
		LabeledTextField windowYPos = new LabeledTextField(MapperProperties.WINDOWSTARTY,
															"Vertical Position:", new Integer(properties.getProperty(MapperProperties.WINDOWSTARTY))); //$NON-NLS-1$ 
		LabeledTextField startingArea = new LabeledTextField(MapperProperties.STARTINGAREAID, "Starting Area:", areas, startingAreaStr); //$NON-NLS-1$ 
		LabeledTextField normalExitsField = new LabeledTextField(MapperProperties.NORMALEXITCOLOUR,
																	"Colour of \"Normal\" Exits:", mapper.getNormalExitColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField swimExitsField = new LabeledTextField(MapperProperties.SWIMEXITCOLOUR, "Colour of \"Swim\" Exits:", mapper.getSwimExitColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField climbExitsField = new LabeledTextField(MapperProperties.CLIMBEXITCOLOUR,
																"Colour of \"Climb\" Exits:", mapper.getClimbExitColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField roomColourField = new LabeledTextField(MapperProperties.DEFAULTROOMCOLOUR,
																"Default Room Colour: ", Room.getDefaultRoomColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField currentRoomColourField = new LabeledTextField(MapperProperties.CURRENTROOMCOLOUR,
																		"Current Room Colour: ", Room.getCurrentRoomColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField defaultBackgroundColourField = new LabeledTextField(MapperProperties.DEFAULTBACKGROUNDCOLOUR,
																				"Background Colour of Map: ", MapPanel.getDefaultBackgroundColour(), mapper); //$NON-NLS-1$ 
		LabeledTextField defaultSpacingBetweenRooms = new LabeledTextField(
																			MapperProperties.SPACEBETWEENROOMS,
																			"Default Spacing between Rooms:", new Integer(	properties.getProperty(MapperProperties.SPACEBETWEENROOMS))); //$NON-NLS-1$ 

		LabeledTextField sizeOfImages = new LabeledTextField(MapperProperties.RADIUS, "The default Side of the exported image", //$NON-NLS-1$
																new Integer(properties.getProperty(MapperProperties.RADIUS, MapperProperties.RADIUS_DEFAULT)));

		String rp = properties.getProperty(MapperProperties.ROOMREPRESENTATION, MapperProperties.ROOMREPRESENTATION_DEFAULT);

		LabeledTextField roomReps = new LabeledTextField(MapperProperties.ROOMREPRESENTATION, "Room Graphics", RoomRep.getTypes(), //$NON-NLS-1$
															Integer.parseInt(rp), new RoomRep(RoomRep.TYPE.THREEDEESQUARE), mapper);

		LabeledTextField graphicEditSideField = new LabeledTextField(MapperProperties.GRAPHICEDITORSIDE, "The side for the graphics editor", //$NON-NLS-1$
																		new Integer(properties.getProperty(	MapperProperties.GRAPHICEDITORSIDE,
																											MapperProperties.GRAPHICEDITORSIDE_DEFAULT)));
		LabeledTextField defaultShadowColourField = new LabeledTextField(MapperProperties.CURRENTSHADOWCOLOUR,
																			"The Colour of the Shadow", Room.getCurrentShadowColour(), mapper); //$NON-NLS-1$ 

		String enterStallrp = properties.getProperty(MapperProperties.ENTERSTALLREPRESENTATION, MapperProperties.ENTERSTALLREPRESENTATIONDEFAULT);
		String enterTentrp = properties.getProperty(MapperProperties.ENTERTENTREPRESENTATION, MapperProperties.ENTERTENTREPRESENTATIONDEFAULT);
		LabeledTextField enterStallRep = new LabeledTextField(
																MapperProperties.ENTERSTALLREPRESENTATION,
																"Enter Stall Graphics", RoomGraphicsManager.getInstance().getShapes(), //$NON-NLS-1$
																RoomGraphicsManager.getInstance().getShape(Integer.parseInt(enterStallrp)), new RoomGraphic(),
																mapper);
		LabeledTextField enterTentRep = new LabeledTextField(
																MapperProperties.ENTERTENTREPRESENTATION,
																"Enter Tent Graphics", RoomGraphicsManager.getInstance().getShapes(), //$NON-NLS-1$
																RoomGraphicsManager.getInstance().getShape(Integer.parseInt(enterTentrp)), new RoomGraphic(),
																mapper);
		LabeledTextField alphaValue = new LabeledTextField(
															MapperProperties.ALPAVALUE,
															"Opacity of Cellar", new Float(	properties.getProperty(	MapperProperties.ALPAVALUE, MapperProperties.ALPAVALUEDEFAULT))); //$NON-NLS-1$ 

		LabeledTextField widthOfAreaLinkSize = new LabeledTextField(
																	MapperProperties.HTML_WIDTH_OF_LINK_SIZE,
																	"Width of Area To SubLink", new Integer(properties.getProperty(	MapperProperties.HTML_WIDTH_OF_LINK_SIZE, MapperProperties.HTML_WIDTH_OF_LINK_SIZE_DEFAULT))); //$NON-NLS-1$ 

		windowHeight.addPropertiesChangedListener(this);
		windowWidth.addPropertiesChangedListener(this);
		windowXPos.addPropertiesChangedListener(this);
		windowYPos.addPropertiesChangedListener(this);
		startingArea.addPropertiesChangedListener(this);
		normalExitsField.addPropertiesChangedListener(this);
		swimExitsField.addPropertiesChangedListener(this);
		climbExitsField.addPropertiesChangedListener(this);
		roomColourField.addPropertiesChangedListener(this);
		currentRoomColourField.addPropertiesChangedListener(this);
		defaultBackgroundColourField.addPropertiesChangedListener(this);
		defaultSpacingBetweenRooms.addPropertiesChangedListener(this);
		sizeOfImages.addPropertiesChangedListener(this);
		roomReps.addPropertiesChangedListener(this);
		graphicEditSideField.addPropertiesChangedListener(this);
		defaultShadowColourField.addPropertiesChangedListener(this);
		enterStallRep.addPropertiesChangedListener(this);
		enterTentRep.addPropertiesChangedListener(this);
		alphaValue.addPropertiesChangedListener(this);
		widthOfAreaLinkSize.addPropertiesChangedListener(this);

		toReturn.add(windowWidth);
		toReturn.add(windowHeight);
		toReturn.add(windowXPos);
		toReturn.add(windowYPos);
		toReturn.add(normalExitsField);
		toReturn.add(swimExitsField);
		toReturn.add(climbExitsField);
		toReturn.add(defaultSpacingBetweenRooms);
		toReturn.add(startingArea);
		toReturn.add(roomColourField);
		toReturn.add(currentRoomColourField);
		toReturn.add(defaultBackgroundColourField);
		toReturn.add(sizeOfImages);
		toReturn.add(roomReps);
		toReturn.add(graphicEditSideField);
		toReturn.add(defaultShadowColourField);
		toReturn.add(enterStallRep);
		toReturn.add(enterTentRep);
		toReturn.add(alphaValue);
		toReturn.add(widthOfAreaLinkSize);
		return toReturn;
	}

	protected void okButtonActionPerformed() {
		properties.putAll(tmpProps);
		mapper.resetColoursInMainMap();
		setVisible(false);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String toSet = ""; //$NON-NLS-1$
		String key = evt.getPropertyName();
		if (evt.getNewValue() instanceof Color) {
			Color colour = (Color) evt.getNewValue();
			toSet = Integer.toString(colour.getRGB());
		} else if (evt.getNewValue() instanceof Integer) {
			toSet = ((Integer) evt.getNewValue()).toString();
		} else if (evt.getNewValue() instanceof Float) {
			toSet = ((Float) evt.getNewValue()).toString();
		} else if (evt.getNewValue() instanceof Double) {
			toSet = ((Double) evt.getNewValue()).toString();
		} else if (evt.getNewValue() instanceof Boolean) {
			toSet = ((Boolean) evt.getNewValue()).toString();
		} else if (evt.getNewValue() instanceof String) {
			toSet = ((String) evt.getNewValue()).toString();
		} else if (evt.getNewValue() instanceof RoomGraphic) {
			toSet = Integer.toString(((RoomGraphic) evt.getNewValue()).getId());
		}
		applyButton.setEnabled(true);
		tmpProps.put(key, toSet);
	}

	protected void tableChangedOccured(TableModelEvent tme) {
		int row = tme.getFirstRow();
		String key = table.getValueAt(row, 0).toString();
		String value = table.getValueAt(row, 1).toString();
		tmpProps.setProperty(key, value);
	}
}
