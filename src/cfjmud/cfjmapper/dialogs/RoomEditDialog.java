/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;

/**
 * 
 * @author Ecthelion
 * 
 */
public class RoomEditDialog extends JDialog {
	/** 
     * 
     */
	private static final long		serialVersionUID	= 5540378757503337274L;
	private JPanel					buttonPanel, editPanel, positionPanel;
	private GridBagConstraints		c;
	private static String			VARIOUS				= "<various>";			//$NON-NLS-1$

	private JButton					cancelButton, okButton, editExitButton, deleteExitButton, copyPositionButton, pastePositionButton;

	private final Database			database;
	private JTextArea				descriptionTA;
	private final ArrayList<Exit>	exits;
	private final List<Room>		roomsToModify;
	private JList<Exit>				exitsList;
	private GridBagLayout			gbl;
	private JLabel					idLabel, nameLabel, shortLabel, descriptionLabel, exitsLabel, colourLabel, showColorLabel, xPosLabel, yPosLabel,
			graphicsLabel;
	private JTextField				idTF, nameTF, shortTF;
	private final Mapper			mapper;
	private boolean					ok					= false;
	private JFormattedTextField		xPosTF, yPosTF;
	private JComboBox<RoomGraphic>	graphicsComboBox;

	/**
	 * @author Ecthelion
	 * @param theRoom
	 */
	public RoomEditDialog(Mapper mapper, List<Room> list) {
		super(mapper, true);
		this.mapper = mapper;
		setIconImage(Mapper.getIcon().getImage());
		this.roomsToModify = list;
		database = Database.getInstance();
		exits = new ArrayList<Exit>();
		for (Room room : list) {
			exits.addAll(database.getExitsForRoom(room));
		}
		String title = Messages.getString("RoomEditDialog.0") + (list.size() == 1 ? list.get(0).getId() : VARIOUS) + " - " //$NON-NLS-1$ //$NON-NLS-2$ 
						+ (list.size() == 1 ? list.get(0).getName() : VARIOUS);

		setTitle(title);
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	public boolean getResult() {
		return ok;
	}

	private void initiateComponents() {
		editPanel = new JPanel();
		gbl = new GridBagLayout();
		c = new GridBagConstraints();
		editPanel.setLayout(gbl);
		editPanel.setOpaque(false);

		graphicsComboBox = new JComboBox<RoomGraphic>(RoomGraphicsManager.getInstance().getShapes());
		graphicsComboBox.setRenderer(new RoomGraphic());
		if (roomsToModify.size() == 1) graphicsComboBox.setSelectedItem(RoomGraphicsManager.getInstance().getShape(roomsToModify.get(0).getType()));

		graphicsComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (Room room : roomsToModify) {
					room.setType(((RoomGraphic) graphicsComboBox.getSelectedItem()).getId());
				}
			}
		});

		idLabel = new JLabel(Messages.getString("RoomEditDialog.2")); //$NON-NLS-1$
		nameLabel = new JLabel(Messages.getString("RoomEditDialog.3")); //$NON-NLS-1$
		shortLabel = new JLabel(Messages.getString("RoomEditDialog.4")); //$NON-NLS-1$
		descriptionLabel = new JLabel(Messages.getString("RoomEditDialog.5")); //$NON-NLS-1$
		exitsLabel = new JLabel(Messages.getString("RoomEditDialog.6")); //$NON-NLS-1$
		colourLabel = new JLabel(Messages.getString("RoomEditDialog.7")); //$NON-NLS-1$
		showColorLabel = new JLabel(Messages.getString("RoomEditDialog.8")); //$NON-NLS-1$
		graphicsLabel = new JLabel("Graphics: "); //$NON-NLS-1$
		positionPanel = new JPanel();
		xPosLabel = new JLabel("x: "); //$NON-NLS-1$
		yPosLabel = new JLabel("y: "); //$NON-NLS-1$
		Integer xPosString = (roomsToModify.size() == 1 ? new Integer((int) roomsToModify.get(0).getPosition().getX()) : new Integer(-1));
		Integer yPosString = (roomsToModify.size() == 1 ? new Integer((int) roomsToModify.get(0).getPosition().getY()) : new Integer(-1));
		xPosTF = new JFormattedTextField(xPosString);
		xPosTF.setColumns(20);
		xPosTF.setEditable(false);
		yPosTF = new JFormattedTextField(yPosString);
		yPosTF.setColumns(20);
		yPosTF.setEditable(false);
		copyPositionButton = new JButton("Copy"); //$NON-NLS-1$
		copyPositionButton.setEnabled(roomsToModify.size() == 1);
		copyPositionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.setCopyPosition(roomsToModify.get(0).getPosition());
				pastePositionButton.setEnabled(true);
			}
		});

		pastePositionButton = new JButton("Paste"); //$NON-NLS-1$
		if (mapper.getCopyPosition() == null) pastePositionButton.setEnabled(false);
		pastePositionButton.setEnabled(roomsToModify.size() == 1);
		pastePositionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				roomsToModify.get(0).setPosition(mapper.getCopyPosition());
				xPosTF.setValue(new Integer((int) roomsToModify.get(0).getPosition().getX()));
				yPosTF.setValue(new Integer((int) roomsToModify.get(0).getPosition().getY()));
			}
		});

		showColorLabel.setOpaque(true);
		Color colourToShow = (roomsToModify.size() == 1 ? roomsToModify.get(0).getColour() : Room.getDefaultRoomColour());
		showColorLabel.setBackground(colourToShow);
		showColorLabel.setForeground(colourToShow);

		idTF = new JTextField(20);
		idTF.setEditable(false);
		idTF.setText("" + (roomsToModify.size() == 1 ? "" + roomsToModify.get(0).getId() : VARIOUS)); //$NON-NLS-1$ //$NON-NLS-2$
		nameTF = new JTextField(20);
		nameTF.setText((roomsToModify.size() == 1 ? roomsToModify.get(0).getName() : VARIOUS));
		shortTF = new JTextField(20);
		shortTF.setText((roomsToModify.size() == 1 ? roomsToModify.get(0).getAlias() : VARIOUS));
		descriptionTA = new JTextArea(10, 20);
		descriptionTA.setLineWrap(true);
		JScrollPane descScrollPane = new JScrollPane(descriptionTA);
		descriptionTA.setText((roomsToModify.size() == 1 ? roomsToModify.get(0).getDescription() : VARIOUS));
		Exit[] exitsArray = new Exit[exits.size()];
		exitsArray = exits.toArray(exitsArray);
		exitsList = new JList<Exit>(exitsArray);
		JScrollPane scrollPane = new JScrollPane(exitsList);
		scrollPane.setSize(new Dimension(100, 100));
		scrollPane.setPreferredSize(new Dimension(100, 100));
		editExitButton = new JButton(Messages.getString("RoomEditDialog.9")); //$NON-NLS-1$
		editExitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (exitsList.getSelectedIndex() != -1) new ExitDialog(mapper, exitsList.getSelectedValue());
			}
		});
		deleteExitButton = new JButton(Messages.getString("RoomEditDialog.10")); //$NON-NLS-1$
		deleteExitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (exitsList.getSelectedIndex() != -1) {
					if (JOptionPane.showConfirmDialog(	RoomEditDialog.this,
														Messages.getString("RoomEditDialog.11"), Messages.getString("RoomEditDialog.12"), JOptionPane.YES_NO_OPTION) == 0) { //$NON-NLS-1$ //$NON-NLS-2$
						Exit exit = exitsList.getSelectedValue();
						database.deleteExit(exit);
						exits.clear();
						for (Room room : roomsToModify) {
							exits.addAll(database.getExitsForRoom(room));
						}
						Exit[] exitsArray = new Exit[exits.size()];
						exitsArray = exits.toArray(exitsArray);
						exitsList.setListData(exitsArray);
					}
				}

			}
		});

		showColorLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ae) {
				mapper.getColourChooser(showColorLabel.getBackground(), RoomEditDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				//Color theNewColor = JColorChooser.showDialog(RoomEditDialog.this, Messages.getString("RoomEditDialog.13"), theRoom.getColour()); //$NON-NLS-1$

				if (theNewColor != null) {
					for (Room room : roomsToModify) {
						room.setColour(theNewColor);
					}
					showColorLabel.setBackground(theNewColor);
				} else {
					for (Room room : roomsToModify) {
						room.setColour(Room.getDefaultRoomColour());
					}
					showColorLabel.setBackground(Room.getDefaultRoomColour());
				}

			}
		});

		c.insets = new Insets(3, 3, 3, 3);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		editPanel.add(idLabel, c);
		c.gridx = 1;
		c.gridy = 0;
		editPanel.add(idTF, c);

		c.gridx = 2;
		c.gridy = 0;
		editPanel.add(colourLabel, c);

		c.gridx = 3;
		c.gridy = 0;
		c.gridwidth = 1;
		editPanel.add(showColorLabel, c);
		c.gridx = 4;
		c.gridy = 0;
		c.gridwidth = 1;
		editPanel.add(graphicsLabel, c);
		c.gridx = 5;
		c.gridy = 0;
		c.gridwidth = 1;
		editPanel.add(graphicsComboBox, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		editPanel.add(nameLabel, c);

		c.gridx = 1;
		editPanel.add(nameTF, c);

		c.gridx = 2;
		editPanel.add(shortLabel, c);

		c.gridx = 3;
		c.gridwidth = 3;
		editPanel.add(shortTF, c);

		positionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		positionPanel.add(xPosLabel);
		positionPanel.add(xPosTF);
		positionPanel.add(yPosLabel);
		positionPanel.add(yPosTF);
		positionPanel.add(copyPositionButton);
		positionPanel.add(pastePositionButton);
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 6;
		editPanel.add(positionPanel, c);
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		editPanel.add(descriptionLabel, c);

		c.gridx = 2;
		editPanel.add(exitsLabel, c);

		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 2;
		editPanel.add(descScrollPane, c);

		c.gridx = 2;
		c.gridwidth = 4;
		editPanel.add(scrollPane, c);

		c.gridwidth = 1;
		c.gridx = 4;
		c.gridy = 5;
		editPanel.add(editExitButton, c);
		c.gridx = 5;
		editPanel.add(deleteExitButton, c);

		buttonPanel = new JPanel();
		cancelButton = new JButton(Messages.getString("RoomEditDialog.14")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				ok = false;
				setVisible(false);
				dispose();
			}
		});
		okButton = new JButton(Messages.getString("RoomEditDialog.15")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				ok = true;
				if (!nameTF.getText().equals(VARIOUS)) {
					for (Room room : roomsToModify) {
						room.setName(nameTF.getText());
					}
				}
				if (!descriptionTA.getText().equals(VARIOUS)) {
					for (Room room : roomsToModify) {
						room.setDescription(descriptionTA.getText());
					}
				}

				if (!shortTF.getText().equals(VARIOUS)) {
					for (Room room : roomsToModify) {
						room.setAlias(shortTF.getText());
					}
				}
				setVisible(false);
			}
		});
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		setLayout(new BorderLayout());
		add(buttonPanel, BorderLayout.SOUTH);
		add(editPanel, BorderLayout.CENTER);
		pack();
	}
}
