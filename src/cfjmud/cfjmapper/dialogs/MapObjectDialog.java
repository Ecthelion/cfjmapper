/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.MapObject;
import cfjmud.cfjmapper.panels.MapPanel;

/**
 * @author Ecthelion
 * 
 */
public class MapObjectDialog extends JDialog implements ActionListener {
	/**
     * 
     */
	private static final long	serialVersionUID	= -5719991364140417686L;

	private JLabel				fontLabel;
	private JLabel				fontTextLabel;
	private JTextField			labelField, widthField, heightField, xPosField, yPosField;
	private JFormattedTextField	angleField;
	private JLabel				labelLabel, iconLabel, widthLabel, heightLabel, xPosLabel, yPosLabel, angleLabel, textColourLabel;
	private JPanel				labelPositionPanel;
	private final Mapper		mapper;

	private JButton				nLabel, neLabel, eLabel, seLabel, sLabel, swLabel, wLabel, nwLabel, fillerLabel, textColourButton;
	private JCheckBox			lockedCheckBox;

	private MapObject			object;
	private JButton				okButton, cancelButton, backgroundColourButton;

	/**
	 * Constructor for editing an old Object.
	 * 
	 * @param object
	 */
	public MapObjectDialog(Mapper mapper, MapObject object) {
		super(mapper, true);
		this.mapper = mapper;
		this.object = object;
		initiateComponents();
	}

	/**
	 * Constructor for a new object.
	 * 
	 * @param p
	 */
	public MapObjectDialog(Mapper mapper, Point point, int currentAreaId, int level) {
		super(mapper, true);
		this.mapper = mapper;
		object = new MapObject(point, currentAreaId, level);
		initiateComponents();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton pressedButton = (JButton) e.getSource();
		String text = pressedButton.getText();
		int pos = getPositionFromString(text);
		setCurrentPosition(pos);
		object.setLabelPosition(pos);
	}

	int getPositionFromString(String arg) {
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.28"))) return 1; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.29"))) return 2; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.30"))) return 3; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.31"))) return 4; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.32"))) return 5; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.33"))) return 6; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.34"))) return 7; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.35"))) return 8; //$NON-NLS-1$
		if (arg.equalsIgnoreCase(Messages.getString("MapObjectDialog.36"))) return 0; //$NON-NLS-1$
		return 0;
	}

	public MapObject getResult() {

		return object;
	}

	private void initiateComponents() {
		labelLabel = new JLabel(Messages.getString("MapObjectDialog.0")); //$NON-NLS-1$
		widthLabel = new JLabel(Messages.getString("MapObjectDialog.1")); //$NON-NLS-1$
		heightLabel = new JLabel(Messages.getString("MapObjectDialog.2")); //$NON-NLS-1$
		xPosLabel = new JLabel(Messages.getString("MapObjectDialog.3")); //$NON-NLS-1$
		yPosLabel = new JLabel(Messages.getString("MapObjectDialog.4")); //$NON-NLS-1$
		fontLabel = new JLabel(Messages.getString("MapObjectDialog.5")); //$NON-NLS-1$
		angleLabel = new JLabel("Angle of Text (0 == horizonal): "); //$NON-NLS-1$
		textColourLabel = new JLabel("Colour of Text: "); //$NON-NLS-1$

		angleField = new JFormattedTextField(new Integer(object.getTextAngle()));

		fontTextLabel = new JLabel(Messages.getString("MapObjectDialog.6")); //$NON-NLS-1$

		textColourButton = new JButton("The Text Colour"); //$NON-NLS-1$
		textColourButton.setForeground(object.getTextColour());
		textColourButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapper.getColourChooser(iconLabel.getBackground(), MapObjectDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				//Color theNewColor = JColorChooser.showDialog(MapObjectDialog.this, Messages.getString("MapObjectDialog.10"), Mapper.oldColor); //$NON-NLS-1$
				Mapper.oldColor = theNewColor;
				if (theNewColor != null) {
					textColourButton.setForeground(theNewColor);
					object.setTextColour(theNewColor);
				} else {
					textColourButton.setForeground(Color.BLACK);
					object.setTextColour(Color.BLACK);
				}
			}
		});

		fontTextLabel.setText(object.getFont().getName());
		fontTextLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				FontDialog fd = new FontDialog(mapper, object.getLabel(), object.getFont());
				if (fd.getResult() != null) {
					Font f = fd.getResult();
					object.setFont(f);
					object.setFontSize(f.getSize());
					object.setFontStyle(f.getStyle());
					Database.getInstance().updateObject(object);
				}

			}
		});
		cancelButton = new JButton(Messages.getString("MapObjectDialog.7")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				object = null;
				setVisible(false);
			}
		});
		okButton = new JButton(Messages.getString("MapObjectDialog.8")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				okButtonActionPerformed();
			}

		});

		backgroundColourButton = new JButton(Messages.getString("MapObjectDialog.9")); //$NON-NLS-1$
		backgroundColourButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mapper.getColourChooser(iconLabel.getBackground(), MapObjectDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				//Color theNewColor = JColorChooser.showDialog(MapObjectDialog.this, Messages.getString("MapObjectDialog.10"), Mapper.oldColor); //$NON-NLS-1$
				Mapper.oldColor = theNewColor;
				if (theNewColor != null) {
					iconLabel.setBackground(theNewColor);
					object.setBackground(theNewColor);
				} else {
					iconLabel.setBackground(MapPanel.getDefaultBackgroundColour());
					object.setBackground(theNewColor);
				}
			}
		});

		iconLabel = new JLabel(Messages.getString("MapObjectDialog.11")); //$NON-NLS-1$
		iconLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				IconManager mi = new IconManager(mapper, true);
				if (mi.getResult()) {
					iconLabel.setIcon(mi.getIcon().getIconImage());
					iconLabel.setText(Messages.getString("MapObjectDialog.12")); //$NON-NLS-1$
					if (object != null) object.setIconId(mi.getIcon().getId());
				}
			}
		});
		iconLabel.setOpaque(true);
		iconLabel.setBackground(object.getBackground());
		labelField = new JTextField(25);
		widthField = new JTextField(25);
		heightField = new JTextField(25);
		xPosField = new JTextField(25);
		yPosField = new JTextField(25);
		lockedCheckBox = new JCheckBox("Locked", object.isLocked()); //$NON-NLS-1$
		lockedCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				object.setLocked(lockedCheckBox.isSelected());
			}
		});

		nLabel = new JButton(Messages.getString("MapObjectDialog.13")); //$NON-NLS-1$
		neLabel = new JButton(Messages.getString("MapObjectDialog.14")); //$NON-NLS-1$
		eLabel = new JButton(Messages.getString("MapObjectDialog.15")); //$NON-NLS-1$
		seLabel = new JButton(Messages.getString("MapObjectDialog.16")); //$NON-NLS-1$
		sLabel = new JButton(Messages.getString("MapObjectDialog.17")); //$NON-NLS-1$
		swLabel = new JButton(Messages.getString("MapObjectDialog.18")); //$NON-NLS-1$
		wLabel = new JButton(Messages.getString("MapObjectDialog.19")); //$NON-NLS-1$
		nwLabel = new JButton(Messages.getString("MapObjectDialog.20")); //$NON-NLS-1$
		fillerLabel = new JButton(Messages.getString("MapObjectDialog.21")); //$NON-NLS-1$

		nLabel.addActionListener(this);
		neLabel.addActionListener(this);
		eLabel.addActionListener(this);
		seLabel.addActionListener(this);
		sLabel.addActionListener(this);
		swLabel.addActionListener(this);
		wLabel.addActionListener(this);
		nwLabel.addActionListener(this);
		fillerLabel.addActionListener(this);

		labelPositionPanel = new JPanel();
		labelPositionPanel.setLayout(new GridLayout(3, 3, 10, 10));
		labelPositionPanel.add(nwLabel);
		labelPositionPanel.add(nLabel);
		labelPositionPanel.add(neLabel);
		labelPositionPanel.add(wLabel);
		labelPositionPanel.add(fillerLabel);
		labelPositionPanel.add(eLabel);
		labelPositionPanel.add(swLabel);
		labelPositionPanel.add(sLabel);
		labelPositionPanel.add(seLabel);

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 5, 5, 5);
		setLayout(gbl);
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		add(labelLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(labelField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		add(widthLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(widthField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 2;
		add(heightLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(heightField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 3;
		add(xPosLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(xPosField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 4;
		add(yPosLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(yPosField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 5;
		add(fontLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(fontTextLabel, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 6;
		add(angleLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(angleField, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 7;
		add(textColourLabel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(textColourButton, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 8;
		add(lockedCheckBox, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 9;
		add(labelPositionPanel, c);
		c.gridx = 1;
		c.gridwidth = 2;
		add(iconLabel, c);
		c.gridwidth = 1;

		c.gridx = 1;
		c.gridy = 10;
		add(backgroundColourButton, c);
		c.gridx = 2;
		add(okButton, c);
		c.gridx = 3;
		add(cancelButton, c);
		setValues();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private void okButtonActionPerformed() {
		if (object == null) return;

		object.setLabel(labelField.getText());
		object.setSize(new Dimension(Integer.parseInt(widthField.getText()), Integer.parseInt(heightField.getText())));
		object.setxPos(Integer.parseInt(xPosField.getText()));
		object.setyPos(Integer.parseInt(yPosField.getText()));
		object.setTextAngle(Integer.parseInt(angleField.getText()));
		object.setTextColour(textColourButton.getForeground());
		setVisible(false);
	}

	void setCurrentPosition(int pos) {
		nLabel.setForeground(Color.BLACK);
		neLabel.setForeground(Color.BLACK);
		eLabel.setForeground(Color.BLACK);
		seLabel.setForeground(Color.BLACK);
		sLabel.setForeground(Color.BLACK);
		swLabel.setForeground(Color.BLACK);
		wLabel.setForeground(Color.BLACK);
		nwLabel.setForeground(Color.BLACK);
		fillerLabel.setForeground(Color.BLACK);
		switch (pos) {
			case 1:
				nLabel.setForeground(Color.green);
				break;
			case 2:
				neLabel.setForeground(Color.green);
				break;
			case 3:
				eLabel.setForeground(Color.green);
				break;
			case 4:
				seLabel.setForeground(Color.green);
				break;
			case 5:
				sLabel.setForeground(Color.green);
				break;
			case 6:
				swLabel.setForeground(Color.green);
				break;
			case 7:
				wLabel.setForeground(Color.green);
				break;
			case 8:
				nwLabel.setForeground(Color.green);
				break;
			case 0:
			default:
				fillerLabel.setForeground(Color.green);
				break;
		}

	}

	private void setValues() {
		if (object == null) return;

		labelField.setText(Messages.getString("MapObjectDialog.22") + object.getLabel()); //$NON-NLS-1$
		widthField.setText(Messages.getString("MapObjectDialog.23") + (int) object.getSize().getWidth()); //$NON-NLS-1$
		heightField.setText(Messages.getString("MapObjectDialog.24") + (int) object.getSize().getHeight()); //$NON-NLS-1$
		xPosField.setText(Messages.getString("MapObjectDialog.25") + object.getxPos()); //$NON-NLS-1$
		yPosField.setText(Messages.getString("MapObjectDialog.26") + object.getyPos()); //$NON-NLS-1$
		setCurrentPosition(object.getLabelPosition());
		if (object.getIconId() != 0) {
			iconLabel.setIcon(Database.getInstance().getIcon(object.getIconId()).getIconImage());
			iconLabel.setText(Messages.getString("MapObjectDialog.27")); //$NON-NLS-1$
		}
	}

	/**
	 * @return the object
	 */
	MapObject getObject() {
		return object;
	}

	/**
	 * @param object
	 *            the object to set
	 */
	void setObject(MapObject object) {
		this.object = object;
	}
}
