/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.objects.Exit;
import cfjmud.cfjmapper.objects.Room;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;

/**
 * 
 * @author Ecthelion
 * 
 */
public class ExitDialog extends JDialog implements MapperConstants {

	/**
     * 
     */
	private static final long	serialVersionUID	= 3513615697380871828L;
	private final Database		database;
	private Exit				exit				= null;
	private final Mapper		mapper;
	private JButton				okButton, cancelButton, sourceChooseGraphicsButton, targetChooseGraphicsButton;
	private JPanel				repPanel;
	private JCheckBox			showStubCheckBox, oneWayCheckBox, isInvisibleCheckBox;
	private JTextField			startRoomField, endRoomField, startDirectionField, endDirectionField, sourceWalkCommandField, targetWalkCommandField;
	private JLabel				startRoomLabel, endRoomLabel, sourceWalkCommandLabel, targetWalkCommandLabel, sourceAreaRepTypeLabel, targetAreaRepTypeLabel;
	private JComboBox<String>	startTypeComboBox, endTypeComboBox, sourceAreaRepTypeComboBox, targetAreaRepTypeComboBox;
	private int					sourceGraphicsIconId, targetGraphicsIconId;

	public ExitDialog(Mapper mapper, Exit exit) {
		super(mapper, true);
		this.mapper = mapper;
		setIconImage(Mapper.getIcon().getImage());
		database = Database.getInstance();
		this.exit = exit;
		initiateComponents();
		setTitle("Id: " + exit.getId()); //$NON-NLS-1$
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private String getDir(int dir) {
		switch (dir) {
			case Exit.NORTH:
				return "North"; //$NON-NLS-1$
			case Exit.NORTHEAST:
				return "North-East"; //$NON-NLS-1$
			case Exit.EAST:
				return "East"; //$NON-NLS-1$
			case Exit.SOUTHEAST:
				return "South-East"; //$NON-NLS-1$
			case Exit.SOUTH:
				return "South"; //$NON-NLS-1$
			case Exit.SOUTHWEST:
				return "South-West"; //$NON-NLS-1$
			case Exit.WEST:
				return "West"; //$NON-NLS-1$
			case Exit.NORTHWEST:
				return "North-West"; //$NON-NLS-1$
			case Exit.UP:
				return "Up"; //$NON-NLS-1$
			case Exit.DOWN:
				return "Down"; //$NON-NLS-1$
			case Exit.ENTERTENT:
				return "Enter Tent"; //$NON-NLS-1$
			case Exit.ENTERSTALL:
				return "Enter Stall"; //$NON-NLS-1$
			case Exit.LEAVE:
				return "Leave"; //$NON-NLS-1$
			default:
				return ""; //$NON-NLS-1$
		}
	}

	private String getDirType(int type) {
		switch (type) {
			case 0:
				return ""; //$NON-NLS-1$
			case 1:
				return "swim"; //$NON-NLS-1$
			case 2:
				return "climb"; //$NON-NLS-1$
		}
		return ""; //$NON-NLS-1$
	}

	private void initiateComponents() {
		boolean betweenAreas = exit.getTargetArea() != exit.getSourceArea();
		startRoomLabel = new JLabel("Start Room"); //$NON-NLS-1$
		endRoomLabel = new JLabel("End Room"); //$NON-NLS-1$
		showStubCheckBox = new JCheckBox("Show Only Stub"); //$NON-NLS-1$
		oneWayCheckBox = new JCheckBox("One Way"); //$NON-NLS-1$
		isInvisibleCheckBox = new JCheckBox("Visible"); //$NON-NLS-1$
		sourceWalkCommandLabel = new JLabel("Command:"); //$NON-NLS-1$
		targetWalkCommandLabel = new JLabel("Command:"); //$NON-NLS-1$
		sourceWalkCommandField = new JTextField(15);
		targetWalkCommandField = new JTextField(15);
		sourceWalkCommandField.setText(exit.getSourceWalkCommand());
		targetWalkCommandField.setText(exit.getTargetWalkCommand());
		repPanel = new JPanel();
		sourceAreaRepTypeLabel = new JLabel("Source Area Representation: "); //$NON-NLS-1$
		targetAreaRepTypeLabel = new JLabel("Target Area Representation: "); //$NON-NLS-1$
		sourceAreaRepTypeComboBox = new JComboBox<String>(areaRepresentationTypes);
		targetAreaRepTypeComboBox = new JComboBox<String>(areaRepresentationTypes);
		sourceAreaRepTypeComboBox.setSelectedIndex(exit.getSourceAreaRep());
		targetAreaRepTypeComboBox.setSelectedIndex(exit.getTargetAreaRep());
		sourceAreaRepTypeComboBox.setEnabled(betweenAreas);
		targetAreaRepTypeComboBox.setEnabled(betweenAreas);
		sourceChooseGraphicsButton = new JButton("Source Graphics"); //$NON-NLS-1$
		sourceChooseGraphicsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (sourceAreaRepTypeComboBox.getSelectedIndex() <= 3) return;
				if (sourceAreaRepTypeComboBox.getSelectedIndex() == 4) {
					// TODO Show graphics selector and set the graphics id.
					sourceGraphicsIconId = RoomGraphicsManager.showChooser(mapper);
				} else {
					IconManager im = new IconManager(mapper, true);
					if (im.getResult()) {
						sourceGraphicsIconId = im.getIcon().getId();
					}
					im.setVisible(false);
				}
			}
		});
		sourceChooseGraphicsButton.setEnabled(betweenAreas && exit.getSourceAreaRep() >= 4);
		targetChooseGraphicsButton = new JButton("Target Graphics"); //$NON-NLS-1$
		targetChooseGraphicsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (targetAreaRepTypeComboBox.getSelectedIndex() <= 3) return;
				if (targetAreaRepTypeComboBox.getSelectedIndex() == 4) {
					// TODO Show graphics selector and set the graphics id.
					targetGraphicsIconId = RoomGraphicsManager.showChooser(mapper);

				} else {
					IconManager im = new IconManager(mapper, true);
					if (im.getResult()) {
						targetGraphicsIconId = im.getIcon().getId();
					}
					im.setVisible(false);
				}
			}
		});
		targetChooseGraphicsButton.setEnabled(betweenAreas && exit.getTargetAreaRep() >= 4);
		sourceAreaRepTypeComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (sourceAreaRepTypeComboBox.getSelectedIndex()) {
					case 4:
					case 5:
						sourceChooseGraphicsButton.setEnabled(true);
						break;
					default:
						sourceChooseGraphicsButton.setEnabled(false);
						sourceGraphicsIconId = 0;
						break;

				}
			}
		});

		targetAreaRepTypeComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (targetAreaRepTypeComboBox.getSelectedIndex()) {
					case 4:
					case 5:
						targetChooseGraphicsButton.setEnabled(true);
						break;
					default:
						targetChooseGraphicsButton.setEnabled(false);
						sourceGraphicsIconId = 0;
						break;

				}
			}
		});

		showStubCheckBox.setSelected(exit.getShowStub());
		oneWayCheckBox.setSelected(exit.isOneWay());
		isInvisibleCheckBox.setSelected(!exit.isInvisible());
		startTypeComboBox = new JComboBox<String>(Exit.movementTypes);
		endTypeComboBox = new JComboBox<String>(Exit.movementTypes);
		startTypeComboBox.setSelectedIndex(exit.getSourceType());
		endTypeComboBox.setSelectedIndex(exit.getTargetType());
		startRoomField = new JTextField();
		startRoomField.setEditable(false);
		endRoomField = new JTextField();
		endRoomField.setEditable(false);
		startDirectionField = new JTextField();
		startDirectionField.setEditable(false);
		startRoomField.setMaximumSize(new Dimension(400, 30));
		endDirectionField = new JTextField();
		endRoomField.setMaximumSize(new Dimension(400, 30));

		endDirectionField.setEditable(false);
		Room tmpRoom = database.getRoom(exit.getSourceArea(), exit.getSourceRoomId());
		if (tmpRoom != null) startRoomField.setText(tmpRoom.getId() + " - " + tmpRoom.getName()); //$NON-NLS-1$
		tmpRoom = database.getRoom(exit.getTargetArea(), exit.getTargetRoomId());
		if (tmpRoom != null) endRoomField.setText(tmpRoom.getId() + " - " + tmpRoom.getName()); //$NON-NLS-1$
		if (exit.getType() > 1) {
			String tmp = getDirType(exit.getSourceType()) + " " + getDir(exit.getType() / Exit.BASE); //$NON-NLS-1$
			startDirectionField.setText(tmp.trim());
			tmp = getDirType(exit.getSourceType()) + " " + getDir(exit.getType() % Exit.BASE); //$NON-NLS-1$
			endDirectionField.setText(tmp.trim());
		}
		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				exit.setShowStub(showStubCheckBox.isSelected());
				exit.setTargetType(endTypeComboBox.getSelectedIndex());
				exit.setSourceType(startTypeComboBox.getSelectedIndex());
				exit.setSourceWalkCommand(sourceWalkCommandField.getText());
				exit.setTargetWalkCommand(targetWalkCommandField.getText());
				exit.setSourceAreaRep(sourceAreaRepTypeComboBox.getSelectedIndex());
				exit.setTargetAreaRep(targetAreaRepTypeComboBox.getSelectedIndex());
				exit.setOneWay(oneWayCheckBox.isSelected());
				exit.setInvisible(!isInvisibleCheckBox.isSelected());
				exit.setSourceGraphicsIconId(sourceGraphicsIconId);
				exit.setTargetGraphicsIconId(targetGraphicsIconId);
				Database.getInstance().updateExit(null, exit);
				setVisible(false);
				dispose();
			}
		});
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setVisible(false);
				dispose();
			}
		});
		repPanel.add(sourceAreaRepTypeLabel);
		repPanel.add(sourceAreaRepTypeComboBox);
		repPanel.add(sourceChooseGraphicsButton);
		repPanel.add(targetAreaRepTypeLabel);
		repPanel.add(targetAreaRepTypeComboBox);
		repPanel.add(targetChooseGraphicsButton);
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5, 2, 5, 0);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.fill = GridBagConstraints.BOTH;
		add(startRoomLabel, c);
		c.gridx = 1;
		add(startRoomField, c);
		c.gridx = 2;
		add(startDirectionField, c);
		c.gridx = 3;
		add(startTypeComboBox, c);
		c.gridx = 4;
		add(sourceWalkCommandLabel, c);
		c.gridx = 5;
		add(sourceWalkCommandField, c);
		c.gridx = 0;
		c.gridy = 1;
		add(endRoomLabel, c);
		c.gridx = 1;
		add(endRoomField, c);
		c.gridx = 2;
		add(endDirectionField, c);
		c.gridx = 3;
		add(endTypeComboBox, c);
		c.gridx = 4;
		add(targetWalkCommandLabel, c);
		c.gridx = 5;
		add(targetWalkCommandField, c);
		c.gridwidth = 1;
		c.gridy = 2;
		c.gridx = 0;
		add(showStubCheckBox, c);
		c.gridy = 2;
		c.gridx = 1;
		add(oneWayCheckBox, c);
		c.gridy = 2;
		c.gridx = 2;
		add(isInvisibleCheckBox, c);

		c.gridwidth = 7;
		c.gridy = 3;
		c.gridx = 0;
		if (exit.getSourceArea() != exit.getTargetArea()) add(repPanel, c);

		c.gridwidth = 1;
		c.gridy = 4;
		c.gridx = 1;
		c.insets = new Insets(5, 5, 5, 5);
		add(okButton, c);
		c.gridx = 2;
		add(cancelButton, c);
		pack();
	}
}
