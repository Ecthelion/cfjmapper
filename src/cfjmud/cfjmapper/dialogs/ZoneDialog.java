/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import say.swing.JFontChooser;
import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.custom.SortedListModel;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Zone;
import cfjmud.cfjmapper.objects.ZoneRoom;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;

/**
 * @author Ecthelion
 * 
 */
public class ZoneDialog extends JDialog {

	/**
	 * 
	 */
	static final long			serialVersionUID	= 3491365264335167705L;
	Mapper						mapper;
	private JList<Zone>			zones;
	private JList<ZoneRoom>		roomsInZone;
	private JPanel				buttonPanel, prefPanel;
	private JButton				saveButton, deleteButton, addButton, closeButton, referenceButton, moveZRUp, moveZRDown, deleteRoomInZoneButton, borderButton,
			labelButton;
	private JLabel				colourLabel, colourChooserLabel, nameLabel, descriptionLabel, borderLabel;
	private JTextField			nameField;
	private JFormattedTextField	borderField;
	private JTextArea			descArea;
	private JComboBox<String>	types, fillStyle;
	private Zone				currentZone;
	private int					reference;
	private final Database		database;
	private JCheckBox			borderBox, labelBox;

	public enum BORDER_TYPE {
		NONE, LINE, DOTTED_LINE, WIDE_DOTTED_LINE, WIDER_DOTTED_LINE
	}

	static float[]	dottedLineType		= { 3f, 3f, 3f, 3f };
	static float[]	wideDottedLineType	= { 5f, 5f, 5f, 5f };
	static float[]	widerDottedLineType	= { 10f, 4f, 10f, 4f };

	public ZoneDialog(Mapper mapper) {
		super(mapper, true);
		database = Database.getInstance();
		setIconImage(mapper.getIconImage());
		this.mapper = mapper;
		initiateComponents();
		setTitle("Edit Zones..."); //$NON-NLS-1$
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	public static Stroke getBorderStroke(BORDER_TYPE type, float thickness) {
		switch (type) {
			case NONE:
				return null;
			case DOTTED_LINE:
				return new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, dottedLineType, 1);
			case WIDE_DOTTED_LINE:
				return new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, wideDottedLineType, 1);
			case WIDER_DOTTED_LINE:
				return new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, widerDottedLineType, 1);
			default:
				return new BasicStroke(thickness);
		}
	}

	private void initiateComponents() {
		zones = new JList<Zone>();
		SortedListModel<Zone> sortModel = new SortedListModel<Zone>();
		Zone[] zoneArray = database.getZonesForAreaForJList(mapper.getCurrentAreaId());
		for (Zone zone : zoneArray) {
			zone.cleanUpZoneRooms();
		}
		sortModel.addAll(zoneArray);
		zones.setModel(sortModel);
		zones.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (saveButton.isEnabled()) {

					int reply = JOptionPane.showConfirmDialog(	ZoneDialog.this,
																"The Current Zone is changed but not save. Want to save?", "Unsaved!", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
					if (reply == JOptionPane.YES_OPTION) {
						updateZoneObject();
					}
				} else {
					updatePrefPanel();
				}
				saveButton.setEnabled(false);
				updateRoomList(zones.getSelectedValue());
			}
		});

		// *********** Buttons ***************//
		saveButton = new JButton("Save"); //$NON-NLS-1$
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (zones.getSelectedValue() == null) return;
				updateZoneObject();
				saveButton.setEnabled(false);
				mapper.getMapView().getMainMap().updateZones();
			}
		});
		saveButton.setEnabled(false);
		deleteButton = new JButton("Delete"); //$NON-NLS-1$
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Zone selectedItem = zones.getSelectedValue();
				if (selectedItem != null) {
					int res = JOptionPane.showConfirmDialog(ZoneDialog.this, "Really delete Zone?", "Zone Deletion", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
					if (res == JOptionPane.YES_OPTION) {
						database.deleteZone(selectedItem);
						mapper.getMapView().getMainMap().updateZones();
						updateZoneList();
					}
				}

			}
		});
		addButton = new JButton("Add"); //$NON-NLS-1$
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = JOptionPane.showInputDialog(ZoneDialog.this, "Enter the name of the new Zone:"); //$NON-NLS-1$
				if (name != null) {
					Zone zone = new Zone(name, mapper.getCurrentArea().getId());
					int newZoneId = database.addZone(zone);
					zone.setZone_id(newZoneId);
					mapper.getMapView().getMainMap().updateZones();
					updateZoneList();
					int indexToSelect = 0;
					for (int i = 0; i < zones.getModel().getSize(); i++) {
						if (zones.getModel().getElementAt(i).getZone_id() == newZoneId) {
							indexToSelect = i;
							break;
						}
					}
					zones.setSelectedIndex(indexToSelect);
					zones.ensureIndexIsVisible(zones.getSelectedIndex());
					updatePrefPanel();
				}
			}

		});
		closeButton = new JButton("Close"); //$NON-NLS-1$
		closeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (saveButton.isEnabled()) {

					int reply = JOptionPane.showConfirmDialog(	ZoneDialog.this,
																"The Current Zone is changed but not save. Want to save?", "Unsaved!", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
					if (reply == JOptionPane.YES_OPTION) {
						updateZoneObject();
					}
				}
				setVisible(false);
			}
		});

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(saveButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(addButton);
		buttonPanel.add(closeButton);
		// ********* End Buttons *************//

		// ********* Pref. Panel *************//
		prefPanel = new JPanel();

		SortedListModel<ZoneRoom> model = new SortedListModel<ZoneRoom>();
		roomsInZone = new JList<ZoneRoom>(model);
		Insets margin = new Insets(3, 3, 3, 3);
		moveZRUp = new JButton("+"); //$NON-NLS-1$
		moveZRUp.setMargin(margin);
		moveZRUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				moveZRUpActionPerformed();
			}
		});
		moveZRDown = new JButton("-"); //$NON-NLS-1$
		moveZRDown.setMargin(margin);
		moveZRDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				moveZRDownActionPerformed();
			}
		});

		deleteRoomInZoneButton = new JButton("del"); //$NON-NLS-1$
		deleteRoomInZoneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				deleteZoneRoomButtonActionPerformed();
			}
		});

		JScrollPane roomsInZoneSP = new JScrollPane(roomsInZone);
		Dimension d = new Dimension(200, 150);
		roomsInZoneSP.setSize(d);
		roomsInZoneSP.setPreferredSize(d);
		types = new JComboBox<String>(Zone.types);
		types.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveButton.setEnabled(true);
				if (types.getSelectedIndex() == 0) {
					referenceButton.setEnabled(false);
					reference = 0;
				} else {
					referenceButton.setEnabled(true);
				}
			}
		});
		fillStyle = new JComboBox<String>(Zone.fillStyles);
		fillStyle.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				saveButton.setEnabled(true);
			}
		});
		nameLabel = new JLabel("Name:"); //$NON-NLS-1$
		nameField = new JTextField();
		nameField.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent arg0) {
				saveButton.setEnabled(true);
			}
		});
		descriptionLabel = new JLabel("Description"); //$NON-NLS-1$
		descArea = new JTextArea();
		descArea.setSize(d);
		descArea.setPreferredSize(d);
		descArea.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent arg0) {
				saveButton.setEnabled(true);
			}
		});
		borderLabel = new JLabel("Width:"); //$NON-NLS-1$
		borderField = new JFormattedTextField(new Integer(0));
		borderField.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent arg0) {
				saveButton.setEnabled(true);
			}
		});
		colourLabel = new JLabel("Colour:"); //$NON-NLS-1$
		colourChooserLabel = new JLabel("Colour", JLabel.CENTER); //$NON-NLS-1$
		colourChooserLabel.setOpaque(true);
		colourChooserLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(colourChooserLabel.getBackground(), ZoneDialog.this);
				Mapper.oldColor = mapper.getCurrentColour();
				if (mapper.getCurrentColour() != null) {
					colourChooserLabel.setBackground(mapper.getCurrentColour());
					saveButton.setEnabled(true);
				}

			}
		});
		referenceButton = new JButton("Brush"); //$NON-NLS-1$
		referenceButton.setEnabled(false);
		referenceButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (types.getSelectedIndex() == 0) return;
				if (types.getSelectedIndex() == 1) {
					reference = RoomGraphicsManager.showChooser(mapper);
				} else {
					IconManager im = new IconManager(mapper, true);
					if (im.getResult()) {
						reference = im.getIcon().getId();
					}
					im.setVisible(false);
				}
			}
		});

		borderButton = new JButton("Border"); //$NON-NLS-1$
		borderButton.setEnabled(false);
		borderButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (currentZone == null) return;
				ZoneBorderDialog zbd = new ZoneBorderDialog(ZoneDialog.this, currentZone, mapper);
				if (zbd.isResult()) saveButton.setEnabled(true);
			}
		});
		labelButton = new JButton("Label"); //$NON-NLS-1$
		if (currentZone != null) labelButton.setFont(currentZone.getLabelfont());
		labelButton.setEnabled(false);
		labelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ZoneLabelDialog zld = new ZoneLabelDialog(ZoneDialog.this, currentZone);
				if (zld.isResult()) saveButton.setEnabled(true);
				if (currentZone != null) labelButton.setFont(currentZone.getLabelfont());
				labelBox.setSelected(currentZone.getLabelalignment() != 0);
				labelButton.setEnabled(currentZone.getLabelalignment() != 0);
				repaint();
			}
		});
		labelBox = new JCheckBox();
		labelBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				labelButton.setEnabled(labelBox.isSelected());
				saveButton.setEnabled(true);
				if (labelBox.isSelected()) {
					if (currentZone != null) currentZone.setLabelalignment(1);
				} else {
					if (currentZone != null) currentZone.setLabelalignment(0);
				}
			}
		});
		borderBox = new JCheckBox();
		borderBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				borderButton.setEnabled(borderBox.isSelected());
				saveButton.setEnabled(true);
				if (borderBox.isSelected()) {
					if (currentZone != null) currentZone.setBordertype(1);
				} else {
					if (currentZone != null) currentZone.setBordertype(0);
				}
			}
		});
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		prefPanel.setLayout(gbl);
		c.insets = new Insets(3, 3, 3, 3);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.gridheight = 3;
		c.anchor = GridBagConstraints.CENTER;
		prefPanel.add(roomsInZoneSP, c);
		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		prefPanel.add(moveZRUp, c);
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 1;
		c.gridheight = 1;
		prefPanel.add(moveZRDown, c);
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		prefPanel.add(deleteRoomInZoneButton, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		prefPanel.add(nameLabel, c);
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 2;
		prefPanel.add(nameField, c);
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 2;
		prefPanel.add(descriptionLabel, c);
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 3;
		prefPanel.add(descArea, c);
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 1;
		prefPanel.add(colourLabel, c);
		c.gridx = 1;
		c.gridy = 6;
		c.gridwidth = 1;
		prefPanel.add(colourChooserLabel, c);
		c.gridx = 2;
		c.gridy = 6;
		c.gridwidth = 1;
		prefPanel.add(referenceButton, c);
		c.gridx = 0;
		c.gridy = 7;
		c.gridwidth = 1;
		prefPanel.add(borderLabel, c);
		c.gridx = 1;
		c.gridy = 7;
		c.gridwidth = 2;
		prefPanel.add(borderField, c);
		c.gridx = 0;
		c.gridy = 8;
		c.gridwidth = 1;
		prefPanel.add(borderBox, c);
		c.gridx = 1;
		c.gridy = 8;
		c.gridwidth = 2;
		prefPanel.add(borderButton, c);
		c.gridx = 0;
		c.gridy = 9;
		c.gridwidth = 1;
		prefPanel.add(labelBox, c);
		c.gridx = 1;
		c.gridy = 9;
		c.gridwidth = 2;
		prefPanel.add(labelButton, c);

		c.gridx = 0;
		c.gridy = 10;
		c.gridwidth = 3;
		prefPanel.add(types, c);
		c.gridx = 0;
		c.gridy = 11;
		c.gridwidth = 3;
		prefPanel.add(fillStyle, c);

		// ********* End Pref. *************//
		JScrollPane zonesSP = new JScrollPane(zones);
		Dimension dim = new Dimension(200, 400);
		zonesSP.setSize(dim);
		zonesSP.setPreferredSize(dim);
		add(zonesSP, BorderLayout.WEST);
		add(buttonPanel, BorderLayout.SOUTH);
		add(prefPanel, BorderLayout.CENTER);
		pack();
	}

	protected void moveZRDownActionPerformed() {
		ZoneRoom zr = roomsInZone.getSelectedValue();
		int index = roomsInZone.getSelectedIndex();
		if (zr == null) return;
		if (index >= (roomsInZone.getModel().getSize() - 1)) return;
		ZoneRoom toSwitchWith = roomsInZone.getModel().getElementAt(index + 1);
		int index_1 = zr.getIndex();
		int index_2 = toSwitchWith.getIndex();
		zr.setIndex(index_2);
		toSwitchWith.setIndex(index_1);
		database.updateZoneRoomIndex(zr);
		database.updateZoneRoomIndex(toSwitchWith);
		roomsInZone.setSelectionInterval(index_2, index_2);
		updateRoomList(currentZone);
	}

	protected void moveZRUpActionPerformed() {
		ZoneRoom zr = roomsInZone.getSelectedValue();
		int index = roomsInZone.getSelectedIndex();
		if (zr == null) return;
		if (index == 0) return;
		ZoneRoom toSwitchWith = roomsInZone.getModel().getElementAt(index - 1);
		int index_1 = zr.getIndex();
		int index_2 = toSwitchWith.getIndex();
		zr.setIndex(index_2);
		toSwitchWith.setIndex(index_1);
		database.updateZoneRoomIndex(zr);
		database.updateZoneRoomIndex(toSwitchWith);
		roomsInZone.setSelectionInterval(index_2, index_2);
		updateRoomList(currentZone);
	}

	protected void updateZoneList() {
		SortedListModel<Zone> sortModel = new SortedListModel<Zone>();
		sortModel.addAll(database.getZonesForAreaForJList(mapper.getCurrentAreaId()));
		zones.setModel(sortModel);
		zones.revalidate();
		repaint();
	}

	private void updatePrefPanel() {
		currentZone = zones.getSelectedValue();
		if (currentZone == null) return;
		nameField.setText(currentZone.getName());
		descArea.setText(currentZone.getDescription());
		roomsInZone.setListData(database.getZoneRoomsForZone(currentZone));
		types.setSelectedIndex(currentZone.getType());
		borderField.setValue(new Integer(currentZone.getBorder()));
		colourChooserLabel.setBackground(currentZone.getBackGroundColor());
		types.setSelectedIndex(currentZone.getType());
		fillStyle.setSelectedIndex(currentZone.getFillStyle());
		reference = currentZone.getBackGroundReference();
		labelButton.setEnabled(currentZone.getLabelalignment() != 0);
		labelButton.setFont(currentZone.getLabelfont());
		labelBox.setSelected(currentZone.getLabelalignment() != 0);
		borderBox.setSelected(currentZone.getBordertype() != 0);
		borderButton.setEnabled(currentZone.getBordertype() != 0);
		revalidate();
		repaint();
	}

	private void deleteZoneRoomButtonActionPerformed() {
		for (ZoneRoom zr : roomsInZone.getSelectedValuesList()) {
			if (zr == null) continue;
			database.deleteZoneRoom(zr, currentZone);
			updateRoomList(currentZone);
		}
	}

	void updateZoneObject() {
		currentZone.setName(nameField.getText());
		currentZone.setDescription(descArea.getText());
		currentZone.setType(types.getSelectedIndex());
		currentZone.setFillStyle(fillStyle.getSelectedIndex());
		currentZone.setBorder(Integer.parseInt(borderField.getText()));
		currentZone.setBackGroundColor(colourChooserLabel.getBackground());
		currentZone.setBackGroundReference(reference);
		database.updateZone(currentZone);
		mapper.getMapView().getMainMap().updateZones();
	}

	public void updateRoomList(Zone zone) {
		if (zone == null) return;

		roomsInZone.removeAll();
		SortedListModel<ZoneRoom> model = new SortedListModel<ZoneRoom>();
		model.addAll(database.getZoneRoomsForZone(currentZone));
		roomsInZone.setModel(model);
		roomsInZone.validate();
		roomsInZone.repaint();
	}

	private static Zone			toReturn	= null;
	private static JList<Zone>	zonesForZoneChooser;

	public static Zone showZoneChooser(Mapper mapper) {
		final JDialog dialog = new JDialog(mapper, "Choose Zone"); //$NON-NLS-1$
		zonesForZoneChooser = new JList<Zone>(Database.getInstance().getZonesForAreaForJList(mapper.getCurrentAreaId()));
		Dimension d = new Dimension(200, 400);
		JScrollPane scroll = new JScrollPane(zonesForZoneChooser);
		scroll.setSize(d);
		scroll.setPreferredSize(d);
		JButton okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				toReturn = zonesForZoneChooser.getSelectedValue();
				dialog.setVisible(false);
			}
		});
		JButton cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				toReturn = null;
				dialog.setVisible(false);
			}
		});

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		dialog.setLayout(gbl);
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		dialog.add(scroll, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		dialog.add(okButton, c);
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		dialog.add(cancelButton, c);
		dialog.pack();
		dialog.setLocationRelativeTo(mapper);
		dialog.setModal(true);
		dialog.setVisible(true);

		return toReturn;
	}

}

class ZoneBorderDialog extends JDialog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -111558973708234611L;

	private final int			borderWidth;
	private final Color			borderColour;

	private JLabel				borderWidthLabel, borderColourLabel;
	private JFormattedTextField	borderWidthTextField;
	private JComboBox<LineRep>	borderTypeComboBox;
	private final Mapper		mapper;
	private JButton				okButton, cancelButton;
	private JPanel				buttonPanel;
	private boolean				result;
	private final Zone			zone;

	public ZoneBorderDialog(ZoneDialog parent, Zone zone, Mapper mapper) {
		super(parent, true);
		this.mapper = mapper;
		this.zone = zone;
		borderWidth = zone.getBorderthickness();
		borderColour = zone.getBordercolour();
		initiateComponents();
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	private void initiateComponents() {
		LineRep[] reps = new LineRep[ZoneDialog.BORDER_TYPE.values().length];
		int i = 0;
		for (ZoneDialog.BORDER_TYPE type : ZoneDialog.BORDER_TYPE.values()) {
			reps[i++] = new LineRep(type);
		}
		borderTypeComboBox = new JComboBox<LineRep>(reps);
		borderTypeComboBox.setRenderer(reps[0]);
		borderTypeComboBox.setSelectedIndex(zone.getBordertype());
		borderWidthLabel = new JLabel("With of Border: "); //$NON-NLS-1$
		borderWidthTextField = new JFormattedTextField(new Integer(borderWidth));
		borderColourLabel = new JLabel("Colour"); //$NON-NLS-1$
		borderColourLabel.setBackground(borderColour);
		borderColourLabel.setOpaque(true);
		borderColourLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				mapper.getColourChooser(borderColourLabel.getBackground(), ZoneBorderDialog.this);
				Mapper.oldColor = mapper.getCurrentColour();
				if (mapper.getCurrentColour() != null) {
					borderColourLabel.setBackground(mapper.getCurrentColour());
				}
			}
		});

		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				result = true;
				zone.setBordercolour(borderColourLabel.getBackground());
				zone.setBorderthickness(((Integer) borderWidthTextField.getValue()).intValue());
				zone.setBordertype(borderTypeComboBox.getSelectedIndex());
				setVisible(false);
			}
		});

		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				result = false;
				setVisible(false);
			}
		});
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.BOTH;
		add(borderTypeComboBox, c);
		c.gridy = 1;
		c.gridwidth = 1;
		add(borderWidthLabel, c);
		c.gridx = 1;
		add(borderWidthTextField, c);
		c.gridy = 2;
		c.gridx = 0;
		c.gridwidth = 2;
		add(borderColourLabel, c);
		c.gridy = 3;
		add(buttonPanel, c);
	}

	/**
	 * @return the result
	 */
	public boolean isResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(boolean result) {
		this.result = result;
	}

	class LineRep extends JComponent implements ListCellRenderer<LineRep> {

		private final ZoneDialog.BORDER_TYPE	type;
		/**
		 * 
		 */
		private static final long				serialVersionUID	= 349231470482996785L;

		LineRep(ZoneDialog.BORDER_TYPE type) {
			this.type = type;
			Dimension dim = new Dimension(100, 30);
			setSize(dim);
			setPreferredSize(dim);
			setMinimumSize(dim);
			setMaximumSize(dim);
		}

		@Override
		public Component getListCellRendererComponent(JList<? extends LineRep> list, LineRep value, int index, boolean isSelected, boolean cellHasFocus) {
			return value;
		}

		@Override
		public void paint(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			BasicStroke stroke = null;
			g2.drawRect(0, 0, getWidth(), getHeight());
			g2.setPaint(Color.BLACK);
			switch (type) {
				case NONE:
					break;
				case LINE:
				default:
					stroke = new BasicStroke(borderWidth);
					g2.setStroke(stroke);
					g2.drawLine(10, 15, getWidth() - 10, 15);
					break;
				case DOTTED_LINE:
					g2.setStroke(ZoneDialog.getBorderStroke(type, 1));
					g2.drawLine(10, 15, getWidth() - 10, 15);
					break;
				case WIDE_DOTTED_LINE:
					g2.setStroke(ZoneDialog.getBorderStroke(type, 1));
					g2.drawLine(10, 15, getWidth() - 10, 15);
					break;
				case WIDER_DOTTED_LINE:
					g2.setStroke(ZoneDialog.getBorderStroke(type, 1));
					g2.drawLine(10, 15, getWidth() - 10, 15);
					break;
			}
		}
	}

}

class ZoneLabelDialog extends JDialog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -111558973708234612L;
	private final String[]		alignments			= { "No Label", "Center", "North", "North-East", "East", "South-East", "South", "South-West", "West", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
			"North-West"							};						//$NON-NLS-1$
	private int					labelPosition;
	private Color				labelColour;
	private String				labelText;
	private Font				labelFont;

	private JTextField			text;
	private JLabel				colour;
	private JButton				font;
	private JComboBox<String>	alignmentBox;
	private final Zone			zone;

	private JButton				okButton, cancelButton;
	private JPanel				buttonPanel;
	private boolean				result;
	private final Font			oldFont;
	private JSpinner			numberOfRepeatsSpinner;
	private int					numberOfRepeats;
	private JLabel				numberOfRepeatsLabel;

	public ZoneLabelDialog(ZoneDialog parent, Zone zone) {
		super(parent, true);
		oldFont = zone.getLabelfont();
		this.zone = zone;
		setValues(zone);
		initiateComponents();
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	private void setValues(Zone zone) {
		if (zone == null) return;
		labelPosition = zone.getLabelalignment();
		labelColour = zone.getLabelcolour();
		labelText = zone.getLabeltext();
		labelFont = zone.getLabelfont();
		numberOfRepeats = zone.getNumberOfRepeats();

	}

	private void initiateComponents() {
		text = new JTextField(labelText);
		text.setToolTipText("If left empty the name of the Zone will be used."); //$NON-NLS-1$
		colour = new JLabel("Colour"); //$NON-NLS-1$
		colour.setBackground(labelColour);
		colour.setOpaque(true);
		colour.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				Color c = JColorChooser.showDialog(ZoneLabelDialog.this, "Colour of Label", Mapper.oldColor); //$NON-NLS-1$
				colour.setBackground(c);
				zone.setLabelcolour(c);
			}
		});
		font = new JButton("Font: " + labelFont.getFamily()); //$NON-NLS-1$
		font.setFont(labelFont);
		font.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFontChooser fjc = new JFontChooser();
				fjc.setSelectedFontFamily(zone.getLabelfont().getFamily());
				fjc.setSelectedFontSize(zone.getLabelfont().getSize());
				fjc.setSelectedFontStyle(zone.getLabelfont().getStyle());
				fjc.showDialog(ZoneLabelDialog.this);
				Font res = fjc.getSelectedFont();
				if (res != null) {
					labelFont = res;
					font.setFont(labelFont);
					repaint();
				}
			}
		});
		alignmentBox = new JComboBox<String>(alignments);
		alignmentBox.setSelectedIndex(zone.getLabelalignment());
		buttonPanel = new JPanel();
		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				zone.setLabelalignment(alignmentBox.getSelectedIndex());
				zone.setLabelcolour(colour.getBackground());
				zone.setLabelfont(labelFont.getFamily());
				zone.setLabelfontsize(labelFont.getSize());
				zone.setLabelFontStyle(labelFont.getStyle());
				zone.setNumberOfRepeats(numberOfRepeats);
				if (!text.getText().trim().isEmpty()) {
					zone.setLabeltext(text.getText());
				}
				setResult(true);
				setVisible(false);
				dispose();
			}
		});
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				zone.setLabelfont(oldFont.getFamily());
				zone.setLabelfontsize(oldFont.getSize());
				zone.setLabelFontStyle(oldFont.getStyle());
				setVisible(false);
				setResult(false);
			}
		});

		SpinnerNumberModel snm = new SpinnerNumberModel(numberOfRepeats, 1, 100, 1);
		numberOfRepeatsSpinner = new JSpinner(snm);
		numberOfRepeatsSpinner.getEditor().setEnabled(false);
		numberOfRepeatsSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				Integer i = (Integer) numberOfRepeatsSpinner.getValue();
				numberOfRepeats = i.intValue();
			}
		});
		numberOfRepeatsLabel = new JLabel("No Repeats:"); //$NON-NLS-1$

		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 5));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		JPanel p = new JPanel();
		GridBagConstraints c = new GridBagConstraints();
		p.setLayout(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 2;
		p.add(text, c);
		c.gridy = 1;
		p.add(colour, c);
		c.gridy = 2;
		p.add(alignmentBox, c);
		c.gridy = 3;
		p.add(font, c);
		c.gridy = 4;
		c.gridx = 0;
		c.gridwidth = 1;
		p.add(numberOfRepeatsLabel, c);
		c.gridx = 1;
		p.add(numberOfRepeatsSpinner, c);
		setLayout(new BorderLayout(5, 5));
		add(p, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	/**
	 * @return the labelPosition
	 */
	public int getLabelPosition() {
		return labelPosition;
	}

	/**
	 * @param labelPosition
	 *            the labelPosition to set
	 */
	public void setLabelPosition(int labelPosition) {
		this.labelPosition = labelPosition;
	}

	/**
	 * @return the labelColour
	 */
	public Color getLabelColour() {
		return labelColour;
	}

	/**
	 * @param labelColour
	 *            the labelColour to set
	 */
	public void setLabelColour(Color labelColour) {
		this.labelColour = labelColour;
	}

	/**
	 * @return the labelText
	 */
	public String getLabelText() {
		return labelText;
	}

	/**
	 * @param labelText
	 *            the labelText to set
	 */
	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	/**
	 * @return the labelFont
	 */
	public Font getLabelFont() {
		return labelFont;
	}

	/**
	 * @param labelFont
	 *            the labelFont to set
	 */
	public void setLabelFont(Font labelFont) {
		this.labelFont = labelFont;
	}

	/**
	 * @return the result
	 */
	public boolean isResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(boolean result) {
		this.result = result;
	}
}
