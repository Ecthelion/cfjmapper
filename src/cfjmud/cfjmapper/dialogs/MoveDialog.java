/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.admin.MapperProperties;

public class MoveDialog extends JDialog implements ActionListener {
	/**
     * 
     */
	private static final long	serialVersionUID	= -4666380423798767254L;
	JPanel						directionPanel, resultPanel, buttonPanel, topPanel;
	boolean						inPixels			= false;
	JButton						northButton, northEastButton, eastButton, southEastButton, southButton, southWestButton, westButton, northWestButton, okButton,
			cancelButton, emptyButton, upButton, downButton;
	JCheckBox					pixelCheckBox;
	boolean						result				= false;
	JLabel						resultLabel;
	int							xPos, yPos, zPos;

	public MoveDialog(Mapper mapper) {
		super(mapper, true);
		xPos = yPos = zPos = 0;
		initiateComponents();
		updateLabels();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() instanceof JButton) {
			String text = ((JButton) ae.getSource()).getText();
			int multipel = Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.SPACEBETWEENROOMS));
			if (text.equals(Messages.getString("MoveDialog.15"))) { //$NON-NLS-1$
				yPos -= (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.16"))) { //$NON-NLS-1$
				xPos += (inPixels ? multipel : 1);
				yPos -= (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.17"))) { //$NON-NLS-1$
				xPos += (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.18"))) { //$NON-NLS-1$
				yPos += (inPixels ? multipel : 1);
				xPos += (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.19"))) { //$NON-NLS-1$
				yPos += (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.20"))) { //$NON-NLS-1$
				yPos += (inPixels ? multipel : 1);
				xPos -= (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.21"))) { //$NON-NLS-1$
				xPos -= (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.22"))) { //$NON-NLS-1$
				xPos -= (inPixels ? multipel : 1);
				yPos -= (inPixels ? multipel : 1);
			}
			if (text.equals(Messages.getString("MoveDialog.23"))) { //$NON-NLS-1$
				xPos = 0;
				yPos = 0;
			}

			if (text.equals("Up")) { //$NON-NLS-1$
				zPos += 1;
			}

			if (text.equals("Down")) { //$NON-NLS-1$
				zPos -= 1;
			}

			updateLabels();
		}
	}

	public int getLevelMod() {
		return zPos;
	}

	public Point getPointMod() {
		return new Point(xPos, yPos);
	}

	public boolean getResult() {
		return result;
	}

	private void initiateComponents() {
		directionPanel = new JPanel();
		resultPanel = new JPanel();
		buttonPanel = new JPanel();
		topPanel = new JPanel();
		resultLabel = new JLabel(Messages.getString("MoveDialog.0")); //$NON-NLS-1$
		northButton = new JButton(Messages.getString("MoveDialog.1")); //$NON-NLS-1$
		northButton.addActionListener(this);
		northEastButton = new JButton(Messages.getString("MoveDialog.2")); //$NON-NLS-1$
		northEastButton.addActionListener(this);
		eastButton = new JButton(Messages.getString("MoveDialog.3")); //$NON-NLS-1$
		eastButton.addActionListener(this);
		southEastButton = new JButton(Messages.getString("MoveDialog.4")); //$NON-NLS-1$
		southEastButton.addActionListener(this);
		southButton = new JButton(Messages.getString("MoveDialog.5")); //$NON-NLS-1$
		southButton.addActionListener(this);
		southWestButton = new JButton(Messages.getString("MoveDialog.6")); //$NON-NLS-1$
		southWestButton.addActionListener(this);
		westButton = new JButton(Messages.getString("MoveDialog.7")); //$NON-NLS-1$
		westButton.addActionListener(this);
		northWestButton = new JButton(Messages.getString("MoveDialog.8")); //$NON-NLS-1$
		northWestButton.addActionListener(this);
		emptyButton = new JButton(Messages.getString("MoveDialog.9")); //$NON-NLS-1$
		emptyButton.addActionListener(this);
		upButton = new JButton("Up"); //$NON-NLS-1$
		upButton.addActionListener(this);
		downButton = new JButton("Down"); //$NON-NLS-1$
		downButton.addActionListener(this);
		// emptyButton.setEnabled(false);
		okButton = new JButton(Messages.getString("MoveDialog.10")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = true;
				setVisible(false);
			}
		});
		cancelButton = new JButton(Messages.getString("MoveDialog.11")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = false;
				setVisible(false);
			}
		});
		pixelCheckBox = new JCheckBox(Messages.getString("MoveDialog.12")); //$NON-NLS-1$
		pixelCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent ie) {
				inPixels = !inPixels;
			}
		});
		topPanel.setLayout(new BorderLayout());
		topPanel.add(pixelCheckBox, BorderLayout.WEST);
		topPanel.add(resultLabel, BorderLayout.EAST);
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(3, 3, 3, 3);
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;

		directionPanel.setLayout(gbl);
		directionPanel.setBorder(new CompoundBorder(new EmptyBorder(30, 30, 30, 30), new TitledBorder(new LineBorder(Color.BLACK, 1, true),
																										Messages.getString("MoveDialog.13")))); //$NON-NLS-1$

		c.gridx = 1;
		c.gridy = 1;
		directionPanel.add(northWestButton, c);
		c.gridx = 2;
		directionPanel.add(northButton, c);
		c.gridx = 3;
		directionPanel.add(northEastButton, c);
		c.gridx = 1;
		c.gridy = 2;
		directionPanel.add(westButton, c);
		c.gridx = 2;
		directionPanel.add(emptyButton, c);
		c.gridx = 3;
		directionPanel.add(eastButton, c);
		c.gridx = 3;
		c.gridy = 3;
		directionPanel.add(southEastButton, c);
		c.gridx = 2;
		directionPanel.add(southButton, c);
		c.gridx = 1;
		directionPanel.add(southWestButton, c);

		c.gridx = 4;
		c.gridy = 1;
		directionPanel.add(upButton, c);

		c.gridx = 4;
		c.gridy = 3;
		directionPanel.add(downButton, c);

		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		buttonPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
		setLayout(new BorderLayout());
		add(topPanel, BorderLayout.NORTH);
		add(directionPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	protected void updateLabels() {
		resultLabel.setText("x: " + xPos + " y: " + yPos + " z: " + zPos); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
