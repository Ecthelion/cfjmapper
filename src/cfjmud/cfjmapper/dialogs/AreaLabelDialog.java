/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.objects.Area;

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaLabelDialog extends JDialog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3057922509892125619L;
	private final Area			area;
	private JPanel				componentsPanel, buttonPanel;
	private JLabel				fontLabel;
	private JLabel				fontTextLabel;
	private final Mapper		mapper;
	private JButton				okButton, cancelButton;
	private JComboBox<String>	positionComboBox;
	private final String[]		positions			= {
			Messages.getString("AreaLabelDialog.0"), Messages.getString("AreaLabelDialog.1"), Messages.getString("AreaLabelDialog.2"), Messages.getString("AreaLabelDialog.3"), Messages.getString("AreaLabelDialog.4"), Messages.getString("AreaLabelDialog.5"), Messages.getString("AreaLabelDialog.6"), Messages.getString("AreaLabelDialog.7"), Messages.getString("AreaLabelDialog.8"), Messages.getString("AreaLabelDialog.9") }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$
	private JTextField			textField;
	private JLabel				textLabel, positionLabel, colourLabel, colourBox;

	public AreaLabelDialog(Mapper mapper, Area area) {
		super(mapper, true);
		this.mapper = mapper;
		setModal(true);
		this.area = area;
		setIconImage(Mapper.getIcon().getImage());
		// setLocation((int) mapper.getLocation().getX(), (int)
		// mapper.getLocation().getY());
		setTitle(Messages.getString("AreaLabelDialog.10")); //$NON-NLS-1$
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private void initiateComponents() {
		textLabel = new JLabel(Messages.getString("AreaLabelDialog.11")); //$NON-NLS-1$
		textField = new JTextField(20);
		textField.setText(area.getLabel());
		positionLabel = new JLabel(Messages.getString("AreaLabelDialog.12")); //$NON-NLS-1$
		positionComboBox = new JComboBox<String>(positions);
		positionComboBox.setSelectedIndex(area.getLabelPosition());
		fontLabel = new JLabel(Messages.getString("AreaLabelDialog.14")); //$NON-NLS-1$
		fontTextLabel = new JLabel(Messages.getString("AreaLabelDialog.15")); //$NON-NLS-1$
		fontTextLabel.setText(area.getFont().getName() + Messages.getString("AreaLabelDialog.16") + area.getFont().getSize()); //$NON-NLS-1$
		fontTextLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				String label = area.getLabel();
				if (label.trim().isEmpty()) label = area.getName();
				FontDialog fd = new FontDialog(mapper, label, area.getFont());
				if (fd.getResult() != null) {
					Font f = fd.getResult();
					area.setFont(f);
					area.setLabelFontSize(f.getSize());
					area.setLabelFontStyle(f.getStyle());
				}
			}
		});
		okButton = new JButton(Messages.getString("AreaLabelDialog.17")); //$NON-NLS-1$
		cancelButton = new JButton(Messages.getString("AreaLabelDialog.18")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setVisible(false);
				dispose();
			}
		});
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				area.setLabel(textField.getText());
				area.setLabelPosition(positionComboBox.getSelectedIndex());
				setVisible(false);
				dispose();
			}
		});

		colourLabel = new JLabel(Messages.getString("AreaLabelDialog.19")); //$NON-NLS-1$
		colourBox = new JLabel();
		colourBox.setOpaque(true);
		colourBox.setBackground(area.getLabelColour());
		colourBox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ae) {
				mapper.getColourChooser(colourBox.getBackground(), AreaLabelDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				//Color theNewColor = JColorChooser.showDialog(AreaLabelDialog.this, Messages.getString("AreaLabelDialog.20"), area.getLabelColour()); //$NON-NLS-1$
				Mapper.oldColor = theNewColor;
				if (theNewColor != null) {
					colourBox.setBackground(theNewColor);
					area.setLabelColour(theNewColor);
				} else {
					colourBox.setBackground(Color.BLACK);
				}
			}
		});
		componentsPanel = new JPanel();
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		setLayout(new BorderLayout());
		componentsPanel.setLayout(new GridLayout(0, 2));
		componentsPanel.add(textLabel);
		componentsPanel.add(textField);
		componentsPanel.add(positionLabel);
		componentsPanel.add(positionComboBox);
		componentsPanel.add(colourLabel);
		componentsPanel.add(colourBox);
		componentsPanel.add(fontLabel);
		componentsPanel.add(fontTextLabel);
		add(componentsPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}
}
