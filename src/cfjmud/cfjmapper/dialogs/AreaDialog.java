/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.RoomRep;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.objects.MapperIcon;
import cfjmud.cfjmapper.panels.MapPanel;

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaDialog extends JDialog {
	/**
     * 
     */
	private static final long	serialVersionUID	= -8627151891489019171L;
	private Area				area				= null;
	private JCheckBox			borderCheckBox, labelCheckBox;
	// private boolean borderRaised;
	// private Color shadowColour = Color.GRAY, borderColour = Color.BLACK;
	private int					borderType			= 0, labelType = 0;		// ,
																				// borderThickness
																				// =
																				// 0
	private int					labelPosition		= -1;
	private final Database		database;
	private final int			height				= 16, width = 16;
	private JPanel				mainPanel, buttonPanel;
	private final Mapper		mapper;
	private JButton				okButton, cancelButton, editBorderButton, editLabelButton, removeDescriptionsButton;
	private int					parent, xPos, yPos, iconId;
	private JComboBox<Area>		parentAreaComboBox;
	private boolean				result				= false;
	// private Point areaPosition;
	private final Color			theBackgroundColour	= Color.RED;
	private JComboBox<String>	typeComboBox;
	private JLabel				xPosLabel, yPosLabel, nameLabel, heightLabel, widthLabel, colourLabel, colourBoxLabel, typeLabel, iconLabel, showIconLabel,
			preferredRoomDistanceLabel, parentAreaLabel, roomRepLabel;
	private JComboBox<RoomRep>	roomRepsComboBox;
	private JFormattedTextField	xPosTF, yPosTF, nameTF, heightTF, widthTF, preferredRoomDistanceField;
	private int					oldRoomRep, level;

	public AreaDialog(Mapper mapper, Area area) {
		super(mapper, true);
		this.mapper = mapper;
		setIconImage(Mapper.getIcon().getImage());
		this.area = area;
		database = Database.getInstance();
		initiateComponents();
		setValues();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	public AreaDialog(Mapper mapper, Point position, int parent, int level) {
		super(mapper, true);
		this.mapper = mapper;
		setIconImage(Mapper.getIcon().getImage());
		// this.areaPosition = position;
		xPos = (int) position.getX() - 8;
		yPos = (int) position.getY() - 8;
		this.level = level;
		this.parent = parent;
		database = Database.getInstance();
		area = createNewArea();
		initiateComponents();
		setValues();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private Area createNewArea() {
		Area toReturn = new Area(
									0,
									Messages.getString("AreaDialog.17"), parent, Area.OVAL, xPos, yPos, height, width, 0, Color.RED, 0, Color.BLACK, 0, Color.GRAY, false, false, Messages.getString("AreaDialog.18"), 0, 10, //$NON-NLS-1$ //$NON-NLS-2$
									Color.BLACK, Font.PLAIN, level, Integer.parseInt(MapperProperties.getInstance()
																										.getProperty(MapperProperties.SPACEBETWEENROOMS)));
		return toReturn;
	}

	public Area getArea() {
		return area;
	}

	public boolean getResult() {
		return result;
	}

	private void initiateComponents() {
		oldRoomRep = area.getRoomRep();
		okButton = new JButton(Messages.getString("AreaDialog.0")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (area != null) {
					updateOldArea();
				}
				result = true;
				setVisible(false);
			}
		});
		cancelButton = new JButton(Messages.getString("AreaDialog.1")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = false;
				setVisible(false);
				dispose();
			}
		});
		typeComboBox = new JComboBox<String>(Area.TYPES);
		typeComboBox.setSelectedIndex(3);
		typeComboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (typeComboBox.getSelectedIndex() == 6) {
					if (area != null) {
						if (area.getType() != 6) {
							// createThumbnail();

						}
					}
				}
			}
		});

		removeDescriptionsButton = new JButton("Delete Descriptions"); //$NON-NLS-1$
		removeDescriptionsButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int resp = JOptionPane.showConfirmDialog(	mapper,
															"<html>This will delete All Descriptions<br>in all rooms within the area!<br><br>Do you want to continue?</html>"); //$NON-NLS-1$
				if (resp == JOptionPane.YES_OPTION) {
					Database.getInstance().removeDescriptionsFromArea(area);
				}
			}
		});

		roomRepLabel = new JLabel("Room Representation"); //$NON-NLS-1$
		roomRepsComboBox = new JComboBox<RoomRep>(RoomRep.getTypesForArea());
		roomRepsComboBox.setRenderer(new RoomRep(RoomRep.TYPE.THREEDEESQUARE));
		roomRepsComboBox.setSelectedIndex((oldRoomRep == -1 ? Integer.parseInt(MapperProperties.getInstance()
																								.getProperty(MapperProperties.ROOMREPRESENTATION,
																												MapperProperties.ROOMREPRESENTATION_DEFAULT)) : oldRoomRep));

		xPosLabel = new JLabel(Messages.getString("AreaDialog.2")); //$NON-NLS-1$
		yPosLabel = new JLabel(Messages.getString("AreaDialog.3")); //$NON-NLS-1$
		nameLabel = new JLabel(Messages.getString("AreaDialog.4")); //$NON-NLS-1$
		heightLabel = new JLabel(Messages.getString("AreaDialog.5")); //$NON-NLS-1$
		widthLabel = new JLabel(Messages.getString("AreaDialog.6")); //$NON-NLS-1$
		preferredRoomDistanceLabel = new JLabel("Preferred Room Distance: "); //$NON-NLS-1$
		parentAreaLabel = new JLabel("Parent Area: "); //$NON-NLS-1$

		colourLabel = new JLabel(Messages.getString("AreaDialog.7")); //$NON-NLS-1$
		colourBoxLabel = new JLabel(Messages.getString("AreaDialog.8")); //$NON-NLS-1$
		colourBoxLabel.setBackground(theBackgroundColour);
		colourBoxLabel.setSize(colourLabel.getSize());
		colourBoxLabel.setOpaque(true);
		colourBoxLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ae) {
				mapper.getColourChooser(colourBoxLabel.getBackground(), AreaDialog.this);
				Color theNewColor = mapper.getCurrentColour();
				//Color theNewColor = JColorChooser.showDialog(AreaDialog.this, Messages.getString("AreaDialog.9"), Mapper.oldColor); //$NON-NLS-1$
				Mapper.oldColor = theNewColor;
				if (theNewColor != null) {
					colourBoxLabel.setBackground(theNewColor);
				} else {
					colourBoxLabel.setBackground(MapPanel.getDefaultBackgroundColour());
				}
			}
		});
		ArrayList<Area> areas = database.getAreas();
		Area a = new Area();
		a.setId(-1);
		a.setName("<none>"); //$NON-NLS-1$
		areas.add(0, a);
		Area[] areaArray = new Area[areas.size()];
		areaArray = areas.toArray(areaArray);

		parentAreaComboBox = new JComboBox<Area>(areaArray);

		int index = 0;
		for (int i = 0; i < database.getAreas().size(); i++) {
			if (database.getAreas().get(i).getId() == area.getParent()) {
				database.getAreas().get(i).toString();
				index = i + 1;
			}
		}
		parentAreaComboBox.setSelectedIndex(index);
		parentAreaComboBox.setEditable(false);

		typeLabel = new JLabel(Messages.getString("AreaDialog.10")); //$NON-NLS-1$
		iconLabel = new JLabel(Messages.getString("AreaDialog.11")); //$NON-NLS-1$
		showIconLabel = new JLabel((Icon) null);
		showIconLabel.setSize(new Dimension(50, 50));
		showIconLabel.setPreferredSize(new Dimension(50, 50));
		showIconLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				IconManager cim = new IconManager(mapper, true);
				if (cim.getResult()) {
					MapperIcon icon = cim.getIcon();
					if (icon != null) {
						iconId = icon.getId();
						showIconLabel.setIcon(icon.getIconImage());
						typeComboBox.setSelectedIndex(5);
					}
				}

			}
		});
		xPosTF = new JFormattedTextField(new Integer(xPos));
		yPosTF = new JFormattedTextField(new Integer(yPos));
		nameTF = new JFormattedTextField(new String(Messages.getString("AreaDialog.12"))); //$NON-NLS-1$
		heightTF = new JFormattedTextField(new Integer(16));
		widthTF = new JFormattedTextField(new Integer(16));
		preferredRoomDistanceField = new JFormattedTextField(new Integer(area.getPreferredRoomDistance()));

		editBorderButton = new JButton(Messages.getString("AreaDialog.13")); //$NON-NLS-1$
		editBorderButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				new AreaBorderDialog(mapper, area);
			}
		});
		if (area != null) {
			editBorderButton.setEnabled((area.getBorderType() != 0));
		} else {
			editBorderButton.setEnabled(false);
		}
		borderCheckBox = new JCheckBox(Messages.getString("AreaDialog.14")); //$NON-NLS-1$
		if (area != null) {
			borderCheckBox.setSelected((area.getBorderType() != 0));
		} else {
			borderCheckBox.setSelected(false);
		}
		borderCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				editBorderButton.setEnabled(borderCheckBox.isSelected());
				if (!borderCheckBox.isSelected()) {
					borderType = area.getBorderType();
					area.setBorderType(0);
				} else {
					area.setBorderType(borderType);
				}
			}
		});

		editLabelButton = new JButton(Messages.getString("AreaDialog.15")); //$NON-NLS-1$
		editLabelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				new AreaLabelDialog(mapper, area);
				editLabelButton.setEnabled((area.getLabelPosition() != 0));
				labelCheckBox.setSelected((area.getLabelPosition() != 0));
			}
		});
		labelCheckBox = new JCheckBox(Messages.getString("AreaDialog.16")); //$NON-NLS-1$
		labelCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent ae) {
				editLabelButton.setEnabled(labelCheckBox.isSelected());
				if (!labelCheckBox.isSelected()) {
					labelType = area.getLabelPosition();
					labelPosition = area.getLabelPosition();
					area.setLabelPosition(0);
				} else {
					area.setBorderType(labelType);
					area.setLabelPosition((labelPosition == -1 ? 9 : labelPosition));
				}
			}
		});

		if (area != null) {
			editLabelButton.setEnabled((area.getLabelPosition() != 0));
		} else {
			editLabelButton.setEnabled(false);
		}

		if (area != null) {
			labelCheckBox.setSelected((area.getLabelPosition() != 0));
		} else {
			labelCheckBox.setSelected(false);
		}

		xPosLabel.setLabelFor(xPosTF);
		yPosLabel.setLabelFor(yPosTF);
		nameLabel.setLabelFor(nameTF);
		heightLabel.setLabelFor(heightTF);
		widthLabel.setLabelFor(widthTF);
		colourLabel.setLabelFor(colourBoxLabel);
		typeLabel.setLabelFor(typeComboBox);
		iconLabel.setLabelFor(showIconLabel);
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(0, 4));
		mainPanel.add(xPosLabel);
		mainPanel.add(xPosTF);
		mainPanel.add(yPosLabel);
		mainPanel.add(yPosTF);
		mainPanel.add(nameLabel);
		mainPanel.add(nameTF);
		mainPanel.add(colourLabel);
		mainPanel.add(colourBoxLabel);
		mainPanel.add(typeLabel);
		mainPanel.add(typeComboBox);
		mainPanel.add(iconLabel);
		mainPanel.add(showIconLabel);
		mainPanel.add(heightLabel);
		mainPanel.add(heightTF);
		mainPanel.add(widthLabel);
		mainPanel.add(widthTF);
		mainPanel.add(borderCheckBox);
		mainPanel.add(editBorderButton);
		mainPanel.add(labelCheckBox);
		mainPanel.add(editLabelButton);
		mainPanel.add(parentAreaLabel);
		mainPanel.add(parentAreaComboBox);
		mainPanel.add(preferredRoomDistanceLabel);
		mainPanel.add(preferredRoomDistanceField);
		mainPanel.add(roomRepLabel);
		mainPanel.add(roomRepsComboBox);
		buttonPanel = new JPanel();
		JPanel confirmPanel = new JPanel();
		JPanel removePanel = new JPanel();
		removePanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		removePanel.add(removeDescriptionsButton);
		confirmPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		confirmPanel.add(okButton);
		confirmPanel.add(cancelButton);
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.add(removePanel, BorderLayout.WEST);
		buttonPanel.add(confirmPanel, BorderLayout.EAST);
		setLayout(new BorderLayout());
		add(mainPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		setTitle("This Area Has: " + database.getTotalRoomsInArea(area.getId()) + " Rooms!"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private void setValues() {
		// setValues on components from an old Area...
		if (area != null) {
			nameTF.setText(area.getName());
			parent = area.getParent();
			typeComboBox.setSelectedIndex(area.getType());
			xPosTF.setValue(new Integer(area.getXPos()));
			yPosTF.setValue(new Integer(area.getYPos()));
			widthTF.setValue(new Integer(area.getWidth()));
			heightTF.setValue(new Integer(area.getHeight()));
			colourBoxLabel.setBackground(area.getColour());
			iconId = 0;
			oldRoomRep = area.getRoomRep();
			roomRepsComboBox.setSelectedIndex(oldRoomRep != -1 ? oldRoomRep : 0);
			if (area.getType() == Area.ICON || area.getType() == Area.THUMBNAIL) {
				iconId = area.getIcon();
				if (database.getIcon(area.getIcon()) == null) {
					area.setType(Area.OVAL);
					area.setIcon(0);
				} else {
					showIconLabel.setIcon(database.getIcon(area.getIcon()).getIconImage());
				}
			}
			if (!borderCheckBox.isSelected()) area.setBorderType(0);
		}
	}

	private void updateOldArea() {
		area.setName(nameTF.getText());
		area.setType(typeComboBox.getSelectedIndex());
		area.setXPos(((Integer) xPosTF.getValue()).intValue());
		area.setYPos(((Integer) yPosTF.getValue()).intValue());
		area.setWidth(((Integer) widthTF.getValue()).intValue());
		area.setHeight(((Integer) heightTF.getValue()).intValue());
		area.setColour(colourBoxLabel.getBackground());
		area.setIcon(iconId);
		area.setPreferredRoomDistance(((Integer) preferredRoomDistanceField.getValue()).intValue());
		area.setRoomRep(roomRepsComboBox.getSelectedIndex());
		// update the area...
	}

}

class SwimStroke implements Stroke {

	static class AddSegmentControl {
		GeneralPath	s;
		boolean		start, downwards, startOverride, isClose;
		float		x, y, dx, dy, mx, my, startX, startY;
	}

	private static final float		DEFAULT_WAVE_AMPLITUDE	= 4;
	private static final float		DEFAULT_WAVE_LENGTH		= 8;
	private static final float		DEFAULT_WIDTH			= 1;

	private final BasicStroke		basicStroke;
	private final float				negativePhasePoints[]	= { 1, -2, 3, -2, 5, -2, 6, 0, 7, 2, 9, 2, 11, 2, 12, 0 };

	private final float				positivePhasePoints[]	= { 1, 2, 3, 2, 5, 2, 6, 0, 7, -2, 9, -2, 11, -2, 12, 0 };
	private final AffineTransform	t						= new AffineTransform();
	private float					waveAmplitude			= DEFAULT_WAVE_AMPLITUDE;

	private float					waveLength				= DEFAULT_WAVE_LENGTH;

	private float					width					= DEFAULT_WIDTH;

	public SwimStroke(float width) {
		this(width, DEFAULT_WAVE_LENGTH, DEFAULT_WAVE_AMPLITUDE);
	}

	public SwimStroke(float width, float waveLength, float waveAmplitude) {
		if (waveLength <= 0 || waveAmplitude <= 0 || width <= 0) throw new IllegalArgumentException("Arguments should be greater than zero"); //$NON-NLS-1$

		basicStroke = new BasicStroke(width, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);

		AffineTransform scale = new AffineTransform();
		scale.setToScale(waveLength / DEFAULT_WAVE_LENGTH, waveAmplitude / DEFAULT_WAVE_AMPLITUDE);
		scale.transform(positivePhasePoints, 0, positivePhasePoints, 0, positivePhasePoints.length / 2);
		scale.transform(negativePhasePoints, 0, negativePhasePoints, 0, negativePhasePoints.length / 2);

		this.waveLength = waveLength;
		this.waveAmplitude = waveAmplitude;
		this.width = width;
	}

	private void addSegment(AddSegmentControl ctl) {
		float x = ctl.x, y = ctl.y, dx = ctl.dx, dy = ctl.dy;
		GeneralPath s = ctl.s;

		if (!ctl.start && ctl.startOverride) {
			x = ctl.startX;
			y = ctl.startY;
		}

		if (ctl.start) ctl.s.moveTo(x, y);

		float wavePoints[] = positivePhasePoints;
		if (ctl.downwards) wavePoints = negativePhasePoints;

		if (x == dx && y == dy) return;

		float d = (float) Point2D.distance(x, y, dx, dy);

		double theta = Math.atan2((dy - y), (dx - x));

		float ratio = d / waveLength;
		int n = (int) Math.floor(ratio);

		t.setToScale(1, 1);

		t.translate(x, y);

		float working[] = new float[wavePoints.length];
		t.rotate(theta);

		if (n > 1 && (ratio - n) < 0.5) {
			float scale = ratio / n;
			t.scale(scale, 1);
			ratio = n;
		}

		t.transform(wavePoints, 0, working, 0, wavePoints.length / 2);

		float xShift = (dx - x) / ratio;
		float yShift = (dy - y) / ratio;

		int nQuads = working.length / 4;
		t.setToTranslation(xShift, yShift);

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < nQuads; j++)
				s.quadTo(working[4 * j], working[4 * j + 1], working[4 * j + 2], working[4 * j + 3]);
			t.transform(working, 0, working, 0, working.length / 2);
		}

		if (ctl.isClose) {
			if (ratio - n > 0.75) {
				s.quadTo(working[0], working[1], working[2], working[3]);
				s.quadTo(working[4], working[5], working[6], working[7]);
				s.quadTo(working[8], working[9], working[10], working[11]);
				s.quadTo(working[12], working[13], dx, dy);
				ctl.startOverride = false;
			} else if (ratio - n > 0.25) {
				s.quadTo(working[0], working[1], working[2], working[3]);
				s.quadTo(working[4], working[5], dx, dy);
				ctl.downwards = !ctl.downwards;
				ctl.startOverride = false;
			} else {
				s.lineTo(dx, dy);
				ctl.startOverride = false;
			}
		} else {
			if (ratio - n >= 0.5) {
				s.quadTo(working[0], working[1], working[2], working[3]);
				s.quadTo(working[4], working[5], working[6], working[7]);
				ctl.downwards = !ctl.downwards;
				ctl.startOverride = true;
				ctl.startX = x + (n + 0.5f) * xShift;
				ctl.startY = y + (n + 0.5f) * yShift;
			} else {
				ctl.startOverride = true;
				ctl.startX = x + n * xShift;
				ctl.startY = y + n * yShift;
			}
		}
	}

	@Override
	public Shape createStrokedShape(Shape shape) {
		return basicStroke.createStrokedShape(createWavyOutline(shape));
	}

	public Shape createWavyOutline(Shape shape) {
		PathIterator pathIterator = new FlatteningPathIterator(shape.getPathIterator(new AffineTransform()), 5.0f);// shapeMuncher.transform(shape).getPathIterator(null,
		// FLATNESS);

		float seg[] = new float[6];
		int segType = 0;
		AddSegmentControl addSegmentControl = new AddSegmentControl();
		GeneralPath shapeToReturn = new GeneralPath();
		addSegmentControl.s = shapeToReturn;

		while (!pathIterator.isDone()) {
			segType = pathIterator.currentSegment(seg);
			switch (segType) {
				case PathIterator.SEG_MOVETO:
					addSegmentControl.x = seg[0];
					addSegmentControl.y = seg[1];
					addSegmentControl.mx = addSegmentControl.x;
					addSegmentControl.my = addSegmentControl.y;
					addSegmentControl.start = true;
					break;
				case PathIterator.SEG_LINETO:
					addSegmentControl.dx = seg[0];
					addSegmentControl.dy = seg[1];
					addSegmentControl.isClose = (addSegmentControl.dx == addSegmentControl.mx && addSegmentControl.dy == addSegmentControl.my);
					addSegment(addSegmentControl);
					addSegmentControl.start = false;
					addSegmentControl.x = addSegmentControl.dx;
					addSegmentControl.y = addSegmentControl.dy;
					break;
				case PathIterator.SEG_CLOSE:
					addSegmentControl.isClose = true;
					addSegmentControl.dx = addSegmentControl.mx;
					addSegmentControl.dy = addSegmentControl.my;
					addSegment(addSegmentControl);
					addSegmentControl.start = false;
					addSegmentControl.x = addSegmentControl.mx;
					addSegmentControl.y = addSegmentControl.my;
					break;

				case PathIterator.SEG_QUADTO:
				case PathIterator.SEG_CUBICTO:
				default:
					throw new Error("Illegal seg type : " + segType); //$NON-NLS-1$
			}
			pathIterator.next();
		}
		return shapeToReturn;
	}

	public float getWaveAmplitude() {
		return waveAmplitude;
	}

	public float getWaveLength() {
		return waveLength;
	}

	public float getWidth() {
		return width;
	}
}
