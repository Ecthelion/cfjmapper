/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.custom.LabeledTextField;

/**
 * @author Ecthelion
 * 
 */
public class ThumbnailSizeDialog extends JDialog implements PropertyChangeListener, ChangeListener {

	/**
     * 
     */
	private static final long	serialVersionUID	= 5042511148792651067L;
	// private Mapper mapper;
	LabeledTextField			width, height;
	double						widthAspect, heightAspect;
	private Rectangle			rectangle;
	private final Rectangle		originalRect;
	private final String		WIDTH				= "WIDTH", HEIGHT = "HEIGHT";	//$NON-NLS-1$ //$NON-NLS-2$

	private JCheckBox			keepProportionsCheckBox;
	private JButton				okButton, cancelButton;
	private JSlider				slider;

	public ThumbnailSizeDialog(Mapper mapper, Rectangle rect) {
		super(mapper, true);
		// this.mapper = mapper;
		this.rectangle = new Rectangle(rect);
		this.originalRect = rect;
		widthAspect = rectangle.getWidth() / rectangle.getHeight();
		heightAspect = rectangle.getHeight() / rectangle.getWidth();
		rectangle.setSize((int) (rectangle.getWidth() * 0.10), (int) (rectangle.getHeight() * 0.10));
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private void initiateComponents() {
		width = new LabeledTextField(WIDTH, "Width:", new Integer((int) rectangle.getWidth())); //$NON-NLS-1$ 
		height = new LabeledTextField(HEIGHT, "Height: ", new Integer((int) rectangle.getHeight())); //$NON-NLS-1$ 
		width.addPropertiesChangedListener(this);
		height.addPropertiesChangedListener(this);
		keepProportionsCheckBox = new JCheckBox("Maintain Aspect Ratio", true); //$NON-NLS-1$
		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				rectangle = null;
				setVisible(false);
			}
		});
		slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 10);
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(5);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(this);
		Font font = new Font("Serif", Font.ITALIC, 15); //$NON-NLS-1$
		slider.setFont(font);
		GridBagConstraints c = new GridBagConstraints();
		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(2, 2, 2, 2);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		add(width, c);
		c.gridy = 1;
		add(height, c);
		c.gridy = 2;
		add(keepProportionsCheckBox, c);
		c.gridx = 0;
		c.gridy = 3;
		add(slider, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 4;
		add(okButton, c);
		c.gridx = 1;
		add(cancelButton, c);
	}

	@Override
	public void propertyChange(PropertyChangeEvent pc) {
		int theight = (int) rectangle.getHeight();
		int twidth = (int) rectangle.getWidth();
		if (pc.getPropertyName().equals(WIDTH)) {
			twidth = ((Integer) pc.getNewValue()).intValue();
			if (keepProportionsCheckBox.isSelected()) {
				theight = (int) (twidth * heightAspect);
				height.setValue(new Integer(theight));
			}
		} else if (pc.getPropertyName().equals(HEIGHT)) {
			theight = ((Integer) pc.getNewValue()).intValue();
			if (keepProportionsCheckBox.isSelected()) {
				twidth = (int) (theight * widthAspect);
				width.setValue(new Integer(twidth));
			}
		}
		rectangle.setSize(theight, twidth);
	}

	public static Rectangle getThumbnailSize(Mapper mapper, Rectangle rect) {
		ThumbnailSizeDialog tsd = new ThumbnailSizeDialog(mapper, rect);
		return tsd.getResult();
	}

	private Rectangle getResult() {
		return rectangle;
	}

	@Override
	public void stateChanged(ChangeEvent ce) {
		double proc = ((double) slider.getValue()) / 100;
		int twidth = (int) (originalRect.getWidth() * proc);
		int theight = (int) (originalRect.getHeight() * proc);
		width.setValue(new Integer(twidth));
		height.setValue(new Integer(theight));
		rectangle.setSize(twidth, theight);
	}
}
