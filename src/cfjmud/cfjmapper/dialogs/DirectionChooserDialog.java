/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.interfaces.MapperConstants;

/**
 * 
 * @author Ecthelion
 * 
 */
public class DirectionChooserDialog extends JDialog implements ActionListener, MapperConstants {
	/**
     * 
     */
	private static final long	serialVersionUID	= -333175749460534652L;
	JButton						cancelButton, northButton, northEastButton, eastButton, southEastButton, southButton, southWestButton, westButton,
			northWestButton, emptyButton, upButton, downButton;
	int							direction			= 0;
	JPanel						directionPanel, buttonPanel;
	boolean						result				= false;

	public DirectionChooserDialog(Mapper parent) {
		super(parent, true);
		setUndecorated(true);
		initiateComponents();
		pack();
		setLocationRelativeTo(parent);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() instanceof JButton) {
			String text = ((JButton) ae.getSource()).getText();
			if (text.equals("n")) { //$NON-NLS-1$
				direction = NORTH;
			}
			if (text.equals("ne")) { //$NON-NLS-1$
				direction = NORTHEAST;
			}
			if (text.equals("e")) { //$NON-NLS-1$
				direction = EAST;
			}
			if (text.equals("se")) { //$NON-NLS-1$
				direction = SOUTHEAST;
			}
			if (text.equals("s")) { //$NON-NLS-1$
				direction = SOUTH;
			}
			if (text.equals("sw")) { //$NON-NLS-1$
				direction = SOUTHWEST;
			}
			if (text.equals("w")) { //$NON-NLS-1$
				direction = WEST;
			}
			if (text.equals("nw")) { //$NON-NLS-1$
				direction = NORTHWEST;
			}
			if (text.equals("up")) { //$NON-NLS-1$
				direction = UP;
			}
			if (text.equals("down")) { //$NON-NLS-1$
				direction = DOWN;
			}
			result = true;
			setVisible(false);
		}
	}

	public int getDirection() {
		return direction;
	}

	public boolean getResult() {
		return result;
	}

	private void initiateComponents() {
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = false;
				setVisible(false);
				dispose();
			}
		});
		northButton = new JButton("n"); //$NON-NLS-1$
		northButton.addActionListener(this);
		northEastButton = new JButton("ne"); //$NON-NLS-1$
		northEastButton.addActionListener(this);
		eastButton = new JButton("e"); //$NON-NLS-1$
		eastButton.addActionListener(this);
		southEastButton = new JButton("se"); //$NON-NLS-1$
		southEastButton.addActionListener(this);
		southButton = new JButton("s"); //$NON-NLS-1$
		southButton.addActionListener(this);
		southWestButton = new JButton("sw"); //$NON-NLS-1$
		southWestButton.addActionListener(this);
		westButton = new JButton("w"); //$NON-NLS-1$
		westButton.addActionListener(this);
		northWestButton = new JButton("nw"); //$NON-NLS-1$
		northWestButton.addActionListener(this);
		emptyButton = new JButton("  "); //$NON-NLS-1$
		emptyButton.setEnabled(false);
		upButton = new JButton("up"); //$NON-NLS-1$
		upButton.addActionListener(this);
		downButton = new JButton("down"); //$NON-NLS-1$
		downButton.addActionListener(this);
		directionPanel = new JPanel();

		directionPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 2;
		c.weighty = 2;
		c.gridheight = 2;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.ipadx = 1;
		c.ipady = 1;
		// c.insets = new Insets(1, 1, 1, 1);
		directionPanel.add(northWestButton, c);
		c.gridx = 3;
		c.gridy = 0;
		directionPanel.add(northButton, c);
		c.gridx = 6;
		c.gridy = 0;
		directionPanel.add(northEastButton, c);
		c.gridx = 0;
		c.gridy = 3;
		directionPanel.add(westButton, c);
		c.gridx = 3;
		c.gridy = 3;
		directionPanel.add(emptyButton, c);
		c.gridx = 6;
		c.gridy = 3;
		directionPanel.add(eastButton, c);
		c.gridx = 0;
		c.gridy = 6;
		directionPanel.add(southWestButton, c);
		c.gridx = 3;
		c.gridy = 6;
		directionPanel.add(southButton, c);
		c.gridx = 6;
		c.gridy = 6;
		directionPanel.add(southEastButton, c);
		c.weightx = 2;
		c.weighty = 3;
		c.gridheight = 3;
		c.gridwidth = 2;
		c.gridx = 9;
		c.gridy = 0;
		directionPanel.add(upButton, c);
		c.gridx = 9;
		c.gridy = 5;
		directionPanel.add(downButton, c);
		setLayout(new BorderLayout());
		add(directionPanel, BorderLayout.CENTER);
		add(cancelButton, BorderLayout.SOUTH);
	}
}
