/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.objects.Area;

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaBorderDialog extends JDialog {
	public static int				BEVEL				= 1;
	public static int				EMPTY				= 2;
	public static int				ETCHED				= 3;
	public static int				LINE				= 4;
	/**
     * 
     */
	private static final long		serialVersionUID	= 6302803479138715564L;
	public static int				TITLED				= 5;
	public static final String[]	types				= {
			Messages.getString("AreaBorderDialog.0"), Messages.getString("AreaBorderDialog.1"), Messages.getString("AreaBorderDialog.2"), Messages.getString("AreaBorderDialog.3"), Messages.getString("AreaBorderDialog.4") }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	// public static final String[] movementTypes = {"None",
	// "Bevel","Empty","Etched","Line"};
	public static Border createBorder(Area area) {
		Border toReturn = null;
		switch (area.getBorderType()) {
			case 0:
				return toReturn;
			case 1:
				int bevelType = (area.getBorderRaised() ? BevelBorder.RAISED : BevelBorder.LOWERED);
				toReturn = BorderFactory.createBevelBorder(bevelType, area.getBorderColour(), area.getShadowColour());
				if (area.getBorderTitle()) {
					return BorderFactory.createTitledBorder(toReturn, area.getName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP);
				} else {
					return toReturn;
				}
			case 2:
				toReturn = BorderFactory.createEmptyBorder(	area.getBorderThickness(), area.getBorderThickness(), area.getBorderThickness(),
															area.getBorderThickness());
				if (area.getBorderTitle()) {
					return BorderFactory.createTitledBorder(toReturn, area.getName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP);
				} else {
					return toReturn;
				}
			case 3:
				int type = (area.getBorderRaised() ? EtchedBorder.RAISED : EtchedBorder.LOWERED);
				toReturn = BorderFactory.createEtchedBorder(type, area.getBorderColour(), area.getShadowColour());
				if (area.getBorderTitle()) {
					return BorderFactory.createTitledBorder(toReturn, area.getName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP);
				} else {
					return toReturn;
				}
			case 4:
				toReturn = BorderFactory.createLineBorder(area.getBorderColour(), area.getBorderThickness());
				if (area.getBorderTitle()) {
					return BorderFactory.createTitledBorder(toReturn, area.getName(), TitledBorder.LEFT, TitledBorder.BELOW_TOP);
				} else {
					return toReturn;
				}
			default:
				return null;
		}
	}

	private final Area			area;
	private JComboBox<String>	borderComboBox;
	private JLabel				colourLabel, shadowColourLabel, colourBox, shadowColourBox, thicknessLabel, typeLabel;
	private JPanel				componentsPanel, buttonPanel;
	private final Color			mainColour, shadowColour;
	private JButton				okButton, cancelButton;
	private boolean				raised	= false, title = false;
	private int					thickness, type;
	private JSpinner			thicknessSpinner;

	private JCheckBox			titleCheckBox, raisedCheckBox;

	public AreaBorderDialog(Mapper mapper, Area area) {
		super(mapper, true);
		setIconImage(Mapper.getIcon().getImage());

		this.area = area;
		this.title = area.getBorderTitle();
		this.raised = area.getBorderRaised();
		this.mainColour = area.getBorderColour();
		this.shadowColour = area.getShadowColour();
		this.thickness = area.getBorderThickness();
		this.type = area.getBorderType();
		initiateComponents();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private void initiateComponents() {
		colourLabel = new JLabel(Messages.getString("AreaBorderDialog.5")); //$NON-NLS-1$
		shadowColourLabel = new JLabel(Messages.getString("AreaBorderDialog.6")); //$NON-NLS-1$
		colourBox = new JLabel();
		colourBox.setOpaque(true);
		colourBox.setBackground(mainColour);
		shadowColourBox = new JLabel();
		shadowColourBox.setOpaque(true);
		shadowColourBox.setBackground(shadowColour);
		thicknessLabel = new JLabel(Messages.getString("AreaBorderDialog.7")); //$NON-NLS-1$
		thicknessSpinner = new JSpinner(new SpinnerNumberModel(thickness, 0, 10, 1));
		thicknessSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent ce) {
				thickness = ((Integer) thicknessSpinner.getValue()).intValue();
			}
		});

		titleCheckBox = new JCheckBox(Messages.getString("AreaBorderDialog.8")); //$NON-NLS-1$
		titleCheckBox.setSelected(title);
		titleCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				title = titleCheckBox.isSelected();
			}
		});
		raisedCheckBox = new JCheckBox(Messages.getString("AreaBorderDialog.9")); //$NON-NLS-1$
		raisedCheckBox.setSelected(raised);
		raisedCheckBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				raised = raisedCheckBox.isSelected();
			}
		});
		okButton = new JButton(Messages.getString("AreaBorderDialog.10")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				okButtonActionPerformed();
			}
		});
		cancelButton = new JButton(Messages.getString("AreaBorderDialog.11")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setVisible(false);
				dispose();
			}
		});
		typeLabel = new JLabel(Messages.getString("AreaBorderDialog.12")); //$NON-NLS-1$
		borderComboBox = new JComboBox<String>(types);
		borderComboBox.setSelectedIndex(type);
		borderComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				type = borderComboBox.getSelectedIndex();
			}
		});
		componentsPanel = new JPanel();
		componentsPanel.setLayout(new GridLayout(0, 2));
		componentsPanel.add(typeLabel);
		componentsPanel.add(borderComboBox);
		componentsPanel.add(colourLabel);
		componentsPanel.add(colourBox);
		componentsPanel.add(shadowColourLabel);
		componentsPanel.add(shadowColourBox);
		componentsPanel.add(thicknessLabel);
		componentsPanel.add(thicknessSpinner);
		componentsPanel.add(titleCheckBox);
		componentsPanel.add(raisedCheckBox);
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		setLayout(new BorderLayout());
		add(componentsPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
	}

	private void okButtonActionPerformed() {
		area.setBorderTitle(title);
		area.setBorderRaised(raised);
		area.setBorderColour(mainColour);
		area.setShadowColour(shadowColour);
		area.setBorderThickness(thickness);
		area.setBorderType(type);
		setVisible(false);
		dispose();
	}

}
