/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.CheckList;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.panels.MapPanel;
import cfjmud.cfjmapper.panels.MapView;

/**
 * @author Ecthelion
 * 
 */
public class ExportAsImageDialog extends JDialog {

	private final Area				areaToExport;
	private final Mapper			mapper;
	private JCheckBox				htmlCheckBox, subAreasCheckBox, oldStyleCheckBox;
	private SizeChooserPanel		sizeChooserPanel;
	private final Database			database;
	private final MapperProperties	properties;

	private JButton					exportButton, cancelButton;
	private JPanel					buttonPanel, mainPanel;
	private final Rectangle			areaDimension;
	private int						sideHeight			= 5000;
	private int						sideWidth			= 5000;
	private JLabel					areaSizeLabel;
	// private final String separator = "/";

	private CheckList<Area>			areaCheckList;

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 7676542766059119290L;

	/**
	 * 
	 */
	public ExportAsImageDialog(Mapper mapper, Area areaToExport) {
		super(mapper, "Export as Image...", true); //$NON-NLS-1$
		this.mapper = mapper;
		this.areaToExport = areaToExport;
		database = Database.getInstance();
		properties = MapperProperties.getInstance();
		areaDimension = database.getAreaDimension(areaToExport);
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	private void initiateComponents() {
		htmlCheckBox = new JCheckBox("Add HTML."); //$NON-NLS-1$
		htmlCheckBox.setSelected(true);
		subAreasCheckBox = new JCheckBox("Add Sub Areas."); //$NON-NLS-1$
		subAreasCheckBox.setSelected(true);
		subAreasCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				areaCheckList.setEnabled(subAreasCheckBox.isSelected());
			}
		});
		oldStyleCheckBox = new JCheckBox("Use Old Style"); //$NON-NLS-1$
		oldStyleCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				htmlCheckBox.setSelected(!oldStyleCheckBox.isSelected());
				htmlCheckBox.setEnabled(!oldStyleCheckBox.isSelected());
				subAreasCheckBox.setSelected(!oldStyleCheckBox.isSelected());
				subAreasCheckBox.setEnabled(!oldStyleCheckBox.isSelected());
				areaCheckList.setEnabled(subAreasCheckBox.isSelected());
			}
		});
		sizeChooserPanel = new SizeChooserPanel();
		exportButton = new JButton("Export"); //$NON-NLS-1$
		exportButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				cancelButton.setEnabled(false);
				exportButton.setEnabled(false);
				htmlCheckBox.setEnabled(false);
				subAreasCheckBox.setEnabled(false);
				areaCheckList.setEnabled(false);
				sizeChooserPanel.setEnabled(false);
				oldStyleCheckBox.setEnabled(false);
				exportAreaAsImage();
				setVisible(false);
				dispose();
			}
		});
		exportButton.setMargin(new Insets(3, 3, 3, 3));
		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.setMargin(new Insets(3, 3, 3, 3));
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				dispose();
			}
		});

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 3, 3));
		buttonPanel.add(cancelButton);
		buttonPanel.add(exportButton);
		areaSizeLabel = new JLabel("<html>Size of area Width: " + (int) areaDimension.getWidth() + " Height: " + (int) areaDimension.getHeight() + "</html>"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		mainPanel = new JPanel();
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setBorder(new LineBorder(Color.BLACK, 2, true));
		checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.Y_AXIS));
		checkBoxPanel.add(oldStyleCheckBox);
		checkBoxPanel.add(htmlCheckBox);
		checkBoxPanel.add(subAreasCheckBox);
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		areaCheckList = new CheckList<Area>(true);
		areaCheckList.setData(database.getAreasInArea(areaToExport));
		mainPanel.setLayout(gbl);

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(10, 5, 5, 5);
		mainPanel.add(areaSizeLabel, c);
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		mainPanel.add(checkBoxPanel, c);
		c.gridx = 1;
		mainPanel.add(sizeChooserPanel, c);
		c.gridx = 2;
		c.gridy = 0;
		c.gridheight = 2;
		mainPanel.add(areaCheckList, c);
		setLayout(new BorderLayout(5, 5));
		add(buttonPanel, BorderLayout.SOUTH);
		add(mainPanel, BorderLayout.CENTER);
	}

	protected void exportAreaAsImage() {
		boolean debug = false;
		if (oldStyleCheckBox.isSelected()) {
			areaToExport.saveAreaAsPicture(mapper);
			return;
		}
		sideWidth = sizeChooserPanel.getSideWidth();
		sideHeight = sizeChooserPanel.getSideHeight();
		if (debug) return;
		JFileChooser chooser = new JFileChooser("."); //$NON-NLS-1$
		int res = chooser.showSaveDialog(mapper);
		if (JFileChooser.APPROVE_OPTION != res) return;
		File file = chooser.getSelectedFile();
		chooser.setVisible(false);
		if (file == null) return;
		exportAreaAsImage(file, areaToExport, null);
		System.out.println("Exporting as Image: " + areaToExport.getName()); //$NON-NLS-1$
	}

	private void exportAreaAsImage(File path, Area areaToExport, String parentName) {
		ArrayList<Area> subAreas = database.getSubAreasForArea(areaToExport.getId(), 0);
		if (areaToExport.getId() == this.areaToExport.getId()) {
			subAreas.clear();
			subAreas.addAll(areaCheckList.getSelectedItems());
		}
		Rectangle areaDimension = database.getAreaDimension(areaToExport);
		saveAsImage(calculateRectangles(areaDimension), path, areaToExport, (parentName == null ? "" : parentName)); //$NON-NLS-1$
		if (!subAreasCheckBox.isSelected()) return;
		for (Area area : subAreas) {
			File newFile = new File("" + path.getAbsoluteFile() + File.separator + area.getName()); //$NON-NLS-1$
			exportAreaAsImage(newFile, area, areaToExport.getName());
		}
	}

	private ArrayList<RectHolder> calculateRectangles(Rectangle size) {
		ArrayList<RectHolder> toReturn = new ArrayList<RectHolder>();
		if (size.getWidth() > sideWidth || size.getHeight() > sideHeight) {
			int x = (int) size.getX();
			int y = (int) size.getY();
			int columns = size.width / sideWidth + 1;
			int rows = size.height / sideHeight + 1;
			int newWidth = size.width / columns;
			int newHeight = size.height / rows;
			for (int r = 0; r < rows; r++) {
				for (int c = 0; c < columns; c++) {
					int pX = x + c * newWidth;
					int pY = y + r * newHeight;
					toReturn.add(new RectHolder(new Rectangle(pX, pY, newWidth, newHeight), r, c));
				}
			}

		} else {
			toReturn.add(new RectHolder(size, 0, 0));
		}
		return toReturn;
	}

	/**
	 * Saving the area as image(s)
	 * 
	 * @param rectangles
	 * @param path
	 * @param area
	 */
	private void saveAsImage(ArrayList<RectHolder> rectangles, File path, Area area, String parentName) {
		boolean debug = false;
		if (!path.exists()) path.mkdir();
		if (htmlCheckBox.isSelected()) createIndexFileForArea(area, rectangles, path, parentName);
		if (debug) return;
		for (RectHolder rh : rectangles) {
			double diff = rh.rectangle.getHeight() / rh.rectangle.getWidth();
			BufferedImage image = createImageFromArea(area, rh.rectangle);
			BufferedImage thumbnail = createThumbnailFromImage(image, 100, 100, diff);
			try {
				if (image != null) {
					String newThumbFileName = path.getAbsolutePath() + File.separator + area.getName() + "_" + rh.c + "_" + rh.r + "_thumbnail.png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ 
					String fileName = path.getAbsolutePath() + File.separator + area.getName() + "_" + rh.c + "_" + rh.r + ".png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ 
					File thumbFile = new File(newThumbFileName);
					File file = new File(fileName);
					ImageIO.write(image, "png", file); //$NON-NLS-1$
					ImageIO.write(thumbnail, "png", thumbFile); //$NON-NLS-1$
				}
			} catch (IOException e) {
				System.out.println("Humpf! Couldn't not write to file in saveAreaAsPicture(Area): " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
			System.runFinalization();
			System.gc();
		}

	}

	private BufferedImage createThumbnailFromImage(BufferedImage image, int w, int h, double diff) {
		Image tmpnail = image.getScaledInstance(w, (int) (h * diff), Image.SCALE_SMOOTH);
		BufferedImage thumbnail = new BufferedImage(tmpnail.getWidth(null), tmpnail.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D biG2 = (Graphics2D) thumbnail.getGraphics();
		biG2.drawImage(tmpnail, 0, 0, null);
		biG2.dispose();
		return thumbnail;
	}

	private BufferedImage createImageFromArea(Area area, Rectangle rectangle) {
		MapView tmpMapView = new MapView(mapper);
		MapPanel tmpMap = new MapPanel(tmpMapView);
		tmpMap.setSize(rectangle.getSize());
		tmpMap.setMinimumSize(rectangle.getSize());
		tmpMap.setPreferredSize(rectangle.getSize());
		tmpMap.setBounds(rectangle);
		tmpMap.setCurrentLevel(0);
		tmpMap.setScale(1);
		tmpMap.setCurrentArea(area);
		tmpMap.setBackground(MapPanel.getDefaultBackgroundColour());
		tmpMap.setViewableRectangle(rectangle);
		tmpMap.updateObjects();
		BufferedImage image = new BufferedImage(rectangle.width, rectangle.height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = image.createGraphics();

		g2d.setColor(MapPanel.getDefaultBackgroundColour());
		g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
		tmpMap.drawZones(g2d);
		g2d.translate(-rectangle.x, -rectangle.y);
		tmpMap.drawStuff(g2d);
		g2d.translate(rectangle.x, rectangle.y);
		tmpMap.drawZoneLabels(g2d);

		/*
		 * tmpMap.drawObjects(g2d); tmpMap.drawAreas(g2d);
		 * tmpMap.drawExits(g2d); tmpMap.drawRooms(g2d);
		 */
		g2d.dispose();
		return image;
	}

	private void createIndexFileForArea(Area area, ArrayList<RectHolder> rectangles, File path, String parentName) {
		boolean multi = (rectangles.size() > 1);
		Rectangle areaSize = database.getAreaDimension(area);
		PrintStream ps = null;
		try {
			ps = new PrintStream(path.getAbsolutePath() + File.separator + "index.html"); //$NON-NLS-1$
			ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
			ps.println(properties.getProperty(MapperProperties.HTML_BODY_START, MapperProperties.HTML_BODY_START_DEFAULT));
			if (multi) {
				ps.println(properties.getProperty(MapperProperties.HTML_TABLE_START, MapperProperties.HTML_TABLE_START_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_CAPTION_START, MapperProperties.HTML_CAPTION_START_DEFAULT) + area.getName()
							+ properties.getProperty(MapperProperties.HTML_CAPTION_END, MapperProperties.HTML_CAPTION_END_DEFAULT));
				RectHolder[][] holders = new RectHolder[getMaxRows(rectangles) + 1][getMaxCols(rectangles) + 1];

				for (RectHolder rh : rectangles) {
					holders[rh.r][rh.c] = rh;
				}
				for (int r = 0; r < holders.length; r++) {
					ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TR_START, MapperProperties.HTML_TABLE_TR_START_DEFAULT));
					for (int c = 0; c < holders[0].length; c++) {
						String newFileName = path.getAbsolutePath() + File.separatorChar + area.getName() + "_" + c + "_" + r + ".png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						File newFile = new File(newFileName);
						String newHTMLFileName = path.getAbsolutePath() + File.separatorChar + area.getName() + "_" + c + "_" + r + ".html"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						File newHTMLFile = new File(newHTMLFileName);
						String newThumbFileName = path.getAbsolutePath() + File.separatorChar + area.getName() + "_" + c + "_" + r + "_thumbnail.png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						File thumbFile = new File(newThumbFileName);
						createHtmlForAreaPart(area, holders[r][c], path, newFile);
						ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TD_START, MapperProperties.HTML_TABLE_TD_START_DEFAULT));
						ps.println(properties.getProperty(MapperProperties.HTML_IMAGE_LINK_START, MapperProperties.HTML_IMAGE_LINK_START_DEFAULT)
									+ newHTMLFile.getName()
									+ properties.getProperty(MapperProperties.HTML_IMAGE_LINK_END, MapperProperties.HTML_IMAGE_LINK_END_DEFAULT));
						ps.println("<img src=\"" //$NON-NLS-1$
									+ thumbFile.getName()
									+ "\" alt=\"Part: " + newFile.getName() + " of the map\" title=\"Click to view\" border=\"0\" width=\"150\" height=\"150\" hspace=\"0\" /></a>"); //$NON-NLS-1$ //$NON-NLS-2$

						ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TD_END, MapperProperties.HTML_TABLE_TD_END_DEFAULT));

					}
					ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TR_END, MapperProperties.HTML_TABLE_TR_END_DEFAULT));
				}
				ps.println(properties.getProperty(MapperProperties.HTML_BODY_END, MapperProperties.HTML_BODY_END_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT));
			} else {
				int width = Integer.parseInt(properties.getProperty(MapperProperties.HTML_WIDTH_OF_LINK_SIZE, MapperProperties.HTML_WIDTH_OF_LINK_SIZE_DEFAULT));
				boolean bigPicture = areaSize.getWidth() > width;
				String newFileName = area.getName() + "_" + rectangles.get(0).c + "_" + rectangles.get(0).r + ".html"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				String imageFileName = area.getName() + "_" + rectangles.get(0).c + "_" + rectangles.get(0).r + ".png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_BODY_START, MapperProperties.HTML_BODY_START_DEFAULT));
				if (bigPicture) {
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_LINK_START, MapperProperties.HTML_IMAGE_LINK_START_DEFAULT));
					ps.print(newFileName);
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_LINK_END, MapperProperties.HTML_IMAGE_LINK_END_DEFAULT));
					ps.println("<img src=\"" //$NON-NLS-1$
								+ imageFileName
								+ "\" alt=\"Part: " + imageFileName + " of the map\" title=\"Click to view\" border=\"0\" width=\"100%\" hspace=\"0\" /></a>"); //$NON-NLS-1$ //$NON-NLS-2$
					ps.print("\n"); //$NON-NLS-1$
				} else {
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_START, MapperProperties.HTML_IMAGE_START_DEFAULT));
					ps.print(imageFileName);
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE, MapperProperties.HTML_IMAGE_MIDDLE_DEFAULT));
					ps.print(area.getName());
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE_2, MapperProperties.HTML_IMAGE_MIDDLE_2_DEFAULT));
					ps.print(area.getName());
					ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_END, MapperProperties.HTML_IMAGE_END_DEAFULT));
					ps.print("\n"); //$NON-NLS-1$
					if (subAreasCheckBox.isSelected()) {
						ps.print(properties.getProperty(MapperProperties.HTML_MAP_START, MapperProperties.HTML_MAP_START_DEFAULT));
						ps.print(area.getName());
						ps.print(properties.getProperty(MapperProperties.HTML_MAP_END, MapperProperties.HTML_MAP_END_DEFAULT));
						ps.print("\n"); //$NON-NLS-1$
						ArrayList<Area> subAreas = database.getAreasInArea(area);
						if (area.getId() == this.areaToExport.getId()) {
							subAreas.clear();
							subAreas.addAll(areaCheckList.getSelectedItems());
						}

						for (Area subArea : subAreas) {
							ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_START, MapperProperties.HTML_MAP_AREA_START_DEFAULT));
							ps.print((subArea.getType() == Area.OVAL ? "circle" : "rect")); //$NON-NLS-1$ //$NON-NLS-2$
							ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_SECOND, MapperProperties.HTML_MAP_AREA_SECOND_DEFAULT));
							ps.print(translateCoords(subArea, rectangles.get(0).rectangle));
							ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_THIRD, MapperProperties.HTML_MAP_AREA_THIRD_DEFAULT));
							ps.print(createSubAreaFileName(path, subArea));
							ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_FOURTH, MapperProperties.HTML_MAP_AREA_FOUTH_DEFAULT));
							ps.print(subArea.getName());
							ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_END, MapperProperties.HTML_MAP_AREA_END_DEFAULT));
							ps.print("\n"); //$NON-NLS-1$
						}
						ps.println(properties.getProperty(MapperProperties.HTML_MAP_CLOSE_TAG, MapperProperties.HTML_MAP_CLOSE_TAG_DEFAULT));
					}

				}
				// <a href="url">Link text</a>
				if (!parentName.trim().isEmpty()) ps.println("<a href=\"../index.html\"><br>Back to: " + parentName + "</a>"); //$NON-NLS-1$ //$NON-NLS-2$

				ps.println(properties.getProperty(MapperProperties.HTML_BODY_END, MapperProperties.HTML_BODY_END_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT));
				ps.flush();
				ps.close();
				if (bigPicture) createImageHTML(newFileName, imageFileName, area, path, rectangles.get(0).rectangle);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Failed to create main index file for: " + area.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	private void createImageHTML(String newFileName, String image, Area area, File path, Rectangle rectangle) throws FileNotFoundException {
		PrintStream ps = new PrintStream(path.getAbsolutePath() + File.separator + newFileName);
		ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_BODY_START, MapperProperties.HTML_BODY_START_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_BODY_START, MapperProperties.HTML_BODY_START_DEFAULT));
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_START, MapperProperties.HTML_IMAGE_START_DEFAULT));
		ps.print(image);
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE, MapperProperties.HTML_IMAGE_MIDDLE_DEFAULT));
		ps.print(area.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE_2, MapperProperties.HTML_IMAGE_MIDDLE_2_DEFAULT));
		ps.print(area.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_END, MapperProperties.HTML_IMAGE_END_DEAFULT));
		ps.print("\n"); //$NON-NLS-1$
		if (subAreasCheckBox.isSelected()) {
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_START, MapperProperties.HTML_MAP_START_DEFAULT));
			ps.print(area.getName());
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_END, MapperProperties.HTML_MAP_END_DEFAULT));
			ps.print("\n"); //$NON-NLS-1$
			ArrayList<Area> subAreas = database.getAreasInArea(area);
			if (area.getId() == this.areaToExport.getId()) {
				subAreas.clear();
				subAreas.addAll(areaCheckList.getSelectedItems());
			}

			for (Area subArea : subAreas) {
				ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_START, MapperProperties.HTML_MAP_AREA_START_DEFAULT));
				ps.print((subArea.getType() == Area.OVAL ? "circle" : "rect")); //$NON-NLS-1$ //$NON-NLS-2$
				ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_SECOND, MapperProperties.HTML_MAP_AREA_SECOND_DEFAULT));
				ps.print(translateCoords(subArea, rectangle));
				ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_THIRD, MapperProperties.HTML_MAP_AREA_THIRD_DEFAULT));
				ps.print(createSubAreaFileName(path, subArea));
				ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_FOURTH, MapperProperties.HTML_MAP_AREA_FOUTH_DEFAULT));
				ps.print(subArea.getName());
				ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_END, MapperProperties.HTML_MAP_AREA_END_DEFAULT));
				ps.print("\n"); //$NON-NLS-1$
			}
			ps.println(properties.getProperty(MapperProperties.HTML_MAP_CLOSE_TAG, MapperProperties.HTML_MAP_CLOSE_TAG_DEFAULT));
		}
		ps.println(properties.getProperty(MapperProperties.HTML_BODY_END, MapperProperties.HTML_BODY_END_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT));
		ps.flush();
		ps.close();
	}

	private int getMaxCols(ArrayList<RectHolder> rectangles) {
		int toReturn = 0;
		for (RectHolder rh : rectangles) {
			if (rh.c > toReturn) toReturn = rh.c;
		}
		return toReturn;
	}

	private int getMaxRows(ArrayList<RectHolder> rectangles) {
		int toReturn = 0;
		for (RectHolder rh : rectangles) {
			if (rh.r > toReturn) toReturn = rh.r;
		}
		return toReturn;
	}

	private String createSubAreaFileName(File path, Area subArea) {
		String toReturn = ""; //$NON-NLS-1$
		toReturn = subArea.getName() + "/" + "index.html"; //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn;
	}

	private void createHtmlForAreaPart(Area area, RectHolder rh, File path, File imageToLink) throws FileNotFoundException {
		String fileName = path.getAbsolutePath() + File.separatorChar + area.getName() + "_" + rh.c + "_" + rh.r + ".html"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		PrintStream ps = new PrintStream(fileName);
		ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_BODY_START, MapperProperties.HTML_BODY_START_DEFAULT));
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_START, MapperProperties.HTML_IMAGE_START_DEFAULT));
		ps.print(imageToLink.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE, MapperProperties.HTML_IMAGE_MIDDLE_DEFAULT));
		ps.print(area.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_MIDDLE_2, MapperProperties.HTML_IMAGE_MIDDLE_2_DEFAULT));
		ps.print(area.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_IMAGE_END, MapperProperties.HTML_IMAGE_END_DEAFULT));
		ps.print("\n"); //$NON-NLS-1$
		/* ******************* */
		ps.print(properties.getProperty(MapperProperties.HTML_MAP_START, MapperProperties.HTML_MAP_START_DEFAULT));
		ps.print(area.getName());
		ps.print(properties.getProperty(MapperProperties.HTML_MAP_END, MapperProperties.HTML_MAP_END_DEFAULT));
		ps.print("\n"); //$NON-NLS-1$
		List<Area> subAreas = database.getAreasForRectangle(area.getId(), rh.rectangle, 0);
		for (Area subArea : subAreas) {
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_START, MapperProperties.HTML_MAP_AREA_START_DEFAULT));
			ps.print((subArea.getType() == Area.OVAL ? "circle" : "rect")); //$NON-NLS-1$ //$NON-NLS-2$
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_SECOND, MapperProperties.HTML_MAP_AREA_SECOND_DEFAULT));
			ps.print(translateCoords(subArea, rh.rectangle));
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_THIRD, MapperProperties.HTML_MAP_AREA_THIRD_DEFAULT));
			ps.print(createSubAreaFileName(path, subArea));
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_FOURTH, MapperProperties.HTML_MAP_AREA_FOUTH_DEFAULT));
			ps.print(subArea.getName());
			ps.print(properties.getProperty(MapperProperties.HTML_MAP_AREA_END, MapperProperties.HTML_MAP_AREA_END_DEFAULT));
			ps.print("\n"); //$NON-NLS-1$
		}
		ps.println(properties.getProperty(MapperProperties.HTML_MAP_CLOSE_TAG, MapperProperties.HTML_MAP_CLOSE_TAG_DEFAULT));
		// sub links...
		// <area shape="rect" coords="0,0,82,126" href="sun.htm" alt="Sun">

		/* ******************* */
		ps.println(properties.getProperty(MapperProperties.HTML_BODY_END, MapperProperties.HTML_BODY_END_DEFAULT));
		ps.println(properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT));
		ps.flush();
		ps.close();
		// <img src="url" alt="some_text">
	}

	private String translateCoords(Area subArea, Rectangle rectangle) {
		String toReturn = ""; //$NON-NLS-1$
		int sx = subArea.getPosition().x - rectangle.x;
		int sy = subArea.getPosition().y - rectangle.y;
		int width = subArea.getWidth();
		int heigth = subArea.getHeight();
		int ex = sx + width;
		int ey = sy + heigth;
		int mx = sx + (width / 2);
		int my = sy + (heigth / 2);
		if (subArea.getType() == Area.OVAL) {
			toReturn += mx + "," + my + "," + width / 2; //$NON-NLS-1$ //$NON-NLS-2$
		} else {
			toReturn += sx + "," + sy + "," + ex + "," + ey; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return toReturn;
	}
}

class SizeChooserPanel extends JPanel {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 747946918289941740L;
	private JSlider				horizontalSlider, verticalSlider;
	private final int			side, maxSide, minSide;
	private Graph				graph;
	private JFormattedTextField	verticalTextField, horizontalTextField;

	public SizeChooserPanel() {
		MapperProperties prop = MapperProperties.getInstance();
		Dimension dim = new Dimension(150, 150);
		side = Integer.parseInt(prop.getProperty(MapperProperties.RADIUS, MapperProperties.RADIUS_DEFAULT));
		minSide = Integer.parseInt(prop.getProperty(MapperProperties.MINSIDE, MapperProperties.MINSIDEDEFAULT));
		maxSide = Integer.parseInt(prop.getProperty(MapperProperties.MAXSIDE, MapperProperties.MAXSIDEDEFAULT));
		setSize(dim);
		setPreferredSize(dim);
		initiateComponents();
	}

	public int getSideHeight() {
		return ((Integer) verticalTextField.getValue()).intValue();
	}

	public int getSideWidth() {
		return ((Integer) horizontalTextField.getValue()).intValue();
	}

	private void initiateComponents() {
		horizontalSlider = new JSlider(JSlider.HORIZONTAL, minSide, maxSide, side);
		verticalSlider = new JSlider(JSlider.VERTICAL, minSide, maxSide, side);
		horizontalSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				graph.setWidthOfArea((float) source.getValue() / (float) maxSide);
				verticalTextField.setValue(new Integer(source.getValue()));
			}
		});
		verticalSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				graph.setHeightOfArea((float) (source.getValue()) / (float) maxSide);
				horizontalTextField.setValue(new Integer(source.getValue()));
			}
		});

		horizontalTextField = new JFormattedTextField(new Integer(side));
		horizontalTextField.setMaximumSize(new Dimension(30, 8));
		verticalTextField = new JFormattedTextField(new Integer(side));

		graph = new Graph(maxSide);
		JPanel main = new JPanel();
		main.setLayout(new BorderLayout(1, 1));

		main.add(horizontalSlider, BorderLayout.SOUTH);
		main.add(verticalSlider, BorderLayout.WEST);
		main.add(graph, BorderLayout.CENTER);
		setLayout(new BorderLayout(1, 1));
		add(verticalTextField, BorderLayout.SOUTH);
		add(horizontalTextField, BorderLayout.WEST);
		add(main, BorderLayout.CENTER);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		horizontalSlider.setEnabled(enabled);
		verticalSlider.setEnabled(enabled);
		horizontalTextField.setEnabled(enabled);
		verticalTextField.setEnabled(enabled);
		graph.setEnabled(false);
		repaint();
	}

	/**
	 * @return the dimensionOfSquare
	 */
	public Dimension getDimensionOfSquare() {
		return new Dimension((int) (graph.getHeightOfArea() * maxSide), (int) (graph.getWidthOfArea() * maxSide));
	}

	class Graph extends JComponent {
		private float				areaWidth, areaHeight;
		float[]						dashes				= { 5.0f, 5.0f, 5.0f, 5.0f };

		int							topMinHeight, topMaxHeight, leftMinWidth, leftMaxWidth, rightMinWidth, rightMaxWidth, bottomMinHeight, bottomMaxHeight,
				topHorizontal, leftVertical, bottomHorizontal, rightVertical;

		/**
		 * 
		 */
		private static final long	serialVersionUID	= -9059676669241652165L;

		public Graph(int maxSide) {
			areaWidth = 0.5f;
			areaHeight = 0.5f;
			calculateLines();
			Dimension dim = new Dimension(150, 150);
			setSize(dim);
			setPreferredSize(dim);
			setMinimumSize(dim);
			setMaximumSize(dim);
		}

		private void calculateLines() {
			topMinHeight = (int) (getHeight() * 0.05f);
			topMaxHeight = (int) (getHeight() / 2f * 0.95f);
			leftMinWidth = (int) (getWidth() * 0.05f);
			leftMaxWidth = (int) (getWidth() / 2f * 0.95f);
			rightMinWidth = getWidth() - leftMinWidth;
			rightMaxWidth = getWidth() - leftMaxWidth;
			bottomMinHeight = getHeight() - topMinHeight;
			bottomMaxHeight = getHeight() - topMaxHeight;
			topHorizontal = (int) ((topMaxHeight - topMinHeight) * areaHeight) + topMinHeight;
			bottomHorizontal = getHeight() - topHorizontal;
			leftVertical = (int) ((leftMaxWidth - leftMinWidth) * areaWidth + leftMinWidth);
			rightVertical = getWidth() - leftVertical;
		}

		public void setWidthOfArea(float areaWidth) {
			if (areaWidth < 0f || areaWidth > 1.0f) return;
			this.areaWidth = 1.0f - areaWidth;
			repaint();
		}

		public void setHeightOfArea(float areaHeight) {
			if (areaHeight < 0f || areaHeight > 1.0f) return;
			this.areaHeight = 1.0f - areaHeight;
			repaint();
		}

		public float getWidthOfArea() {
			return 1.0f - areaWidth;
		}

		public float getHeightOfArea() {
			return 1.0f - areaHeight;
		}

		@Override
		public void paint(Graphics g1) {
			if (isEnabled()) {
				calculateLines();
				Graphics2D g = (Graphics2D) g1;
				g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dashes, 0f));
				g.drawLine(0, topHorizontal, getWidth(), topHorizontal);
				g.drawLine(0, bottomHorizontal, getWidth(), bottomHorizontal);
				g.drawLine(leftVertical, 0, leftVertical, getHeight());
				g.drawLine(rightVertical, 0, rightVertical, getHeight());
			} else {
				g1.setColor(Color.BLACK.brighter().brighter());
				g1.fill3DRect(0, getHeight() / 2 - 15, getWidth(), 20, true);
				g1.setColor(Color.YELLOW);
				g1.drawString("Exporting...", 20, getHeight() / 2); //$NON-NLS-1$
			}
		}
	}

}

class RectHolder {
	public Rectangle	rectangle;
	public int			r, c;

	public RectHolder(Rectangle rect, int r, int c) {
		this.rectangle = rect;
		this.r = r;
		this.c = c;
	}
}
