/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.objects.MapperIcon;

/**
 * 
 * @author Ecthelion
 * 
 */
public class IconManager extends JDialog {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8669809928799518667L;
	private JButton				addButton, deleteButton, closeButton, okButton, cancelButton, updateButton;
	private boolean				chooser				= false, result = false;
	private final Database		database;
	private JLabel				iconLabel;
	private final Mapper		mapper;
	private JList<MapperIcon>	pictureList;
	private JPanel				picturePanel, buttonPanel;
	private MapperIcon			theChoosenIcon		= null;

	public IconManager(Mapper mapper, boolean chooser) {
		super(mapper, true);
		this.mapper = mapper;
		if (chooser) {
			setTitle(Messages.getString("IconManager.0")); //$NON-NLS-1$
		} else {
			setTitle(Messages.getString("IconManager.1")); //$NON-NLS-1$
		}

		setIconImage(Mapper.getIcon().getImage());
		this.chooser = chooser;
		database = Database.getInstance();
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	/**
	 * Return the selected icon for the chooser.
	 * 
	 * @return the Icon choosen.
	 */
	public MapperIcon getIcon() {
		return theChoosenIcon;
	}

	public boolean getResult() {
		return result;
	}

	private void initiateComponents() {
		if (chooser) {
			initiateComponentsForChooser();
		} else {
			initiateComponentsForManager();
		}

		MapperIcon[] icons = new MapperIcon[database.getIcons().size()];
		icons = database.getIcons().toArray(icons);
		pictureList = new JList<MapperIcon>(icons);
		pictureList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pictureList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent le) {
				theChoosenIcon = pictureList.getSelectedValue();
				if (theChoosenIcon != null) {
					iconLabel.setIcon(theChoosenIcon.getIconImage());
					iconLabel.setSize(theChoosenIcon.getIconImage().getIconWidth(), theChoosenIcon.getIconImage().getIconHeight());
					iconLabel.repaint();
				}
			}
		});
		JScrollPane scrollPane = new JScrollPane(pictureList);
		/*
		 * pictureList.setSize(new Dimension(200,300));
		 * pictureList.setPreferredSize(new Dimension(200,300));
		 */
		scrollPane.setSize(new Dimension(150, 200));
		scrollPane.setPreferredSize(new Dimension(150, 200));
		picturePanel = new JPanel();

		iconLabel = new JLabel();
		picturePanel.setLayout(new BorderLayout());
		picturePanel.add(iconLabel, BorderLayout.CENTER);
		iconLabel.setOpaque(true);
		picturePanel.setSize(new Dimension(300, 300));
		picturePanel.setPreferredSize(new Dimension(300, 300));
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.WEST);
		add(picturePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	private void initiateComponentsForChooser() {
		okButton = new JButton(Messages.getString("IconManager.2")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (pictureList.getSelectedIndex() != -1) {
					result = true;
					setVisible(false);
				}
			}
		});
		cancelButton = new JButton(Messages.getString("IconManager.3")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = false;
				setVisible(false);
				dispose();
			}
		});
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
	}

	private void initiateComponentsForManager() {
		addButton = new JButton(Messages.getString("IconManager.4")); //$NON-NLS-1$
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				JFileChooser fileChooser = new JFileChooser();
				if (!mapper.getLastPath().trim().isEmpty()) {
					fileChooser.setSelectedFile(new File(mapper.getLastPath()));
				}
				int i = fileChooser.showOpenDialog(IconManager.this);
				if (i == JFileChooser.APPROVE_OPTION) {
					String inputValue = JOptionPane.showInputDialog(Messages.getString("IconManager.5")); //$NON-NLS-1$
					database.addIcon(fileChooser.getSelectedFile(), inputValue);
					pictureList.setSelectedIndex(-1);
					MapperIcon[] icons = new MapperIcon[database.getIcons().size()];
					icons = database.getIcons().toArray(icons);
					pictureList.setListData(icons);
					pictureList.validate();
					mapper.setLastPath(fileChooser.getSelectedFile().getPath());
				}
			}
		});
		deleteButton = new JButton(Messages.getString("IconManager.7")); //$NON-NLS-1$
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MapperIcon selectedIcon = pictureList.getSelectedValue();
				if (selectedIcon == null) return;
				database.deleteIcon(selectedIcon);
				pictureList.setSelectedIndex(-1);
				MapperIcon[] icons = new MapperIcon[database.getIcons().size()];
				icons = database.getIcons().toArray(icons);
				pictureList.removeAll();
				pictureList.setListData(icons);
				pictureList.validate();
			}
		});
		updateButton = new JButton("Update"); //$NON-NLS-1$
		updateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				if (!mapper.getLastPath().trim().isEmpty()) {
					fileChooser.setSelectedFile(new File(mapper.getLastPath()));
				}
				int i = fileChooser.showOpenDialog(IconManager.this);
				if (i == JFileChooser.APPROVE_OPTION) {
					database.updateIcon(pictureList.getSelectedValue().getId(), fileChooser.getSelectedFile());
					int index = pictureList.getSelectedIndex();
					MapperIcon[] icons = new MapperIcon[database.getIcons().size()];
					icons = database.getIcons().toArray(icons);
					pictureList.setListData(icons);
					pictureList.validate();
					pictureList.setSelectedIndex(index);
					mapper.setLastPath(fileChooser.getSelectedFile().getPath());
				}
			}
		});
		closeButton = new JButton(Messages.getString("IconManager.8")); //$NON-NLS-1$
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				setVisible(false);
				dispose();
			}
		});
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(addButton);
		buttonPanel.add(updateButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(closeButton);
	}
}
