/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cfjmud.cfjmapper.Mapper;

/**
 * @author Ecthelion
 * 
 */
public class FontDialog extends JDialog {

	/**
     * 
     */
	private static final long	serialVersionUID	= -3615677095554024898L;

	boolean						bold				= false;
	JCheckBox					boldCheckBox, italicsCheckBox;
	float						currentSize;
	int							currentStyle;
	JComboBox<String>			fontComboBox;
	private String				fontName;
	boolean						italics				= false;
	private final String		label;
	private final Mapper		mapper;

	private Font				myFont;
	JButton						okButton, cancelButton;

	JSpinner					sizeSpinner;
	JLabel						testLabel;

	public FontDialog(Mapper mapper) {
		this(mapper, "This is a test string..."); //$NON-NLS-1$
	}

	/**
     * 
     */
	public FontDialog(Mapper mapper, String label) {
		super(mapper, true);
		this.mapper = mapper;
		this.label = label;
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	public FontDialog(Mapper mapper, String label, Font oldFont) {
		super(mapper, true);
		this.mapper = mapper;
		this.label = label;
		this.myFont = oldFont;
		this.fontName = oldFont.getName();
		this.currentSize = oldFont.getSize2D();
		this.bold = oldFont.isBold();
		this.italics = oldFont.isItalic();
		setLocationRelativeTo(this.mapper);
		initiateComponents();
		pack();
		setVisible(true);

	}

	public Font getResult() {
		return myFont;
	}

	private void initiateComponents() {
		GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
		String[] fonts = e.getAvailableFontFamilyNames();
		testLabel = new JLabel(label, JLabel.CENTER);
		Dimension dim = new Dimension(300, 150);
		testLabel.setSize(dim);
		testLabel.setMinimumSize(dim);
		testLabel.setMaximumSize(dim);
		testLabel.setPreferredSize(dim);
		fontComboBox = new JComboBox<String>(fonts);
		fontComboBox.setSelectedItem(fontName);
		fontComboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				fontName = (String) fontComboBox.getSelectedItem();
				updateLabel();
			}
		});

		okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				myFont = null;
				setVisible(false);
			}
		});
		boldCheckBox = new JCheckBox("Bold"); //$NON-NLS-1$
		boldCheckBox.setSelected(bold);
		boldCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				bold = boldCheckBox.isSelected();
				updateLabel();
			}
		});

		italicsCheckBox = new JCheckBox("Italics"); //$NON-NLS-1$
		italicsCheckBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				italics = italicsCheckBox.isSelected();
				updateLabel();
			}
		});
		italicsCheckBox.setSelected(italics);
		Float value = new Float(currentSize);
		Float min = new Float(4);
		Float max = new Float(50);
		Float step = new Float(1.0f);
		SpinnerNumberModel model = new SpinnerNumberModel(value, min, max, step);
		sizeSpinner = new JSpinner(model);
		sizeSpinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				updateLabel();
			}
		});
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(15, 15, 15, 15);
		c.gridwidth = 2;
		c.fill = GridBagConstraints.BOTH;
		add(testLabel, c);
		c.gridwidth = 1;
		c.insets = new Insets(5, 5, 5, 5);
		c.gridy = 1;
		add(fontComboBox, c);
		c.gridx = 1;
		add(sizeSpinner, c);
		c.gridy = 2;
		c.gridx = 0;
		add(boldCheckBox, c);
		c.gridx = 1;
		add(italicsCheckBox, c);
		c.gridy = 3;
		c.gridx = 0;
		add(okButton, c);
		c.gridx = 1;
		add(cancelButton, c);
		updateLabel();
	}

	protected void updateLabel() {
		currentSize = ((Float) sizeSpinner.getValue()).floatValue();
		currentStyle = Font.PLAIN;
		if (bold) currentStyle += Font.BOLD;
		if (italics) currentStyle += Font.ITALIC;
		Font aFont = Font.decode((String) fontComboBox.getSelectedItem());
		aFont = aFont.deriveFont(currentStyle, currentSize);
		testLabel.setFont(aFont);
		myFont = aFont;
		repaint();
	}
}
