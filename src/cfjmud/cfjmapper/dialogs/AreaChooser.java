/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.objects.Area;
import cfjmud.cfjmapper.panels.AreaTreeViewer;

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaChooser extends JDialog {
	/**
     * 
     */
	private static final long		serialVersionUID	= -1149789959615361780L;
	private JPanel					buttonPanel;
	private JButton					okButton, cancelButton;
	private Area					result				= null;
	private final AreaTreeViewer	theViewer;

	public AreaChooser(Mapper mapper, ArrayList<Area> areas) {
		super(mapper, true);
		theViewer = new AreaTreeViewer(areas, mapper);
		initiateComponents();
		pack();
		setLocationRelativeTo(mapper);
		setVisible(true);
	}

	public Area getResult() {
		return result;
	}

	private void initiateComponents() {
		okButton = new JButton(Messages.getString("AreaChooser.0")); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = theViewer.getSelectedArea();
				setVisible(false);
			}
		});
		cancelButton = new JButton(Messages.getString("AreaChooser.1")); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				result = null;
				setVisible(false);
			}
		});
		buttonPanel = new JPanel();
		buttonPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		setLayout(new BorderLayout());
		JScrollPane treePane = new JScrollPane(theViewer);
		add(treePane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

}
