/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JComponent;

/**
 * @author Ecthelion
 * 
 */
public class AreaRepresentation extends JComponent {

	/**
     * 
     */
	private static final long	serialVersionUID	= -3494946537989022588L;
	private int					areaId;
	private int					roomId;

	/**
     * 
     */
	public AreaRepresentation() {}

	/**
	 * @param arg0
	 */
	public AreaRepresentation(Dimension arg0) {
		super();
		setSize(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public AreaRepresentation(int arg0, int arg1) {
		super();
		setSize(arg0, arg1);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public AreaRepresentation(int x, int y, int width, int height) {
		super();
		setLocation(x, y);
		setSize(width, height);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param toolTip
	 */
	public AreaRepresentation(int x, int y, int width, int height, String toolTip) {
		this(x, y, width, height);
		setToolTipText(toolTip);
	}

	/**
	 * @param arg0
	 */
	public AreaRepresentation(Point arg0) {
		super();
		setLocation(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public AreaRepresentation(Point arg0, Dimension arg1) {
		super();
		setLocation(arg0);
		setSize(arg1);
	}

	/**
	 * @param arg0
	 */
	public AreaRepresentation(Rectangle arg0) {
		super();
		setBounds(arg0);
	}

	/**
	 * @return the areaId
	 */
	public int getAreaId() {
		return areaId;
	}

	/**
	 * @return the roomId
	 */
	public int getRoomId() {
		return roomId;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	/**
	 * @param roomId
	 *            the roomId to set
	 */
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
}
