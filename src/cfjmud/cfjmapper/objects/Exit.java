/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cfjmud.cfjmapper.ExitsManager;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.interfaces.MapperConstants;

/**
 * 
 * @author Ecthelion
 * 
 */
public class Exit implements MapperConstants {

	private static Color	normalColour	= null;
	private static Color	swimColour		= null;
	private static Color	climbColour		= null;

	/**
	 * @return the normalColour
	 */
	public static Color getNormalColour() {
		return getColour(NORMAL_TYPE);
	}

	/**
	 * @param normalColour
	 *            the normalColour to set
	 */
	public static void setNormalColour(Color normalColour) {
		Exit.normalColour = normalColour;
	}

	/**
	 * @return the swimColour
	 */
	public static Color getSwimColour() {
		return getColour(SWIM_TYPE);
	}

	/**
	 * @param swimColour
	 *            the swimColour to set
	 */
	public static void setSwimColour(Color swimColour) {
		Exit.swimColour = swimColour;
	}

	/**
	 * @return the climbColour
	 */
	public static Color getClimbColour() {
		return getColour(CLIMB_TYPE);
	}

	/**
	 * @param climbColour
	 *            the climbColour to set
	 */
	public static void setClimbColour(Color climbColour) {
		Exit.climbColour = climbColour;
	}

	public static Color getColour(int type) {
		String colour = null;
		MapperProperties mp = MapperProperties.getInstance();
		if (normalColour == null) {
			colour = mp.getProperty(MapperProperties.NORMALEXITCOLOUR);
			if (colour == null) {
				normalColour = Color.BLACK;
				mp.setProperty(MapperProperties.NORMALEXITCOLOUR, Integer.toString(Color.BLACK.getRGB()));
			} else {
				normalColour = new Color(Integer.parseInt(colour));
			}
		}
		if (swimColour == null) {
			colour = mp.getProperty(MapperProperties.SWIMEXITCOLOUR);
			if (colour == null) {
				swimColour = Color.BLUE;
				mp.setProperty(MapperProperties.SWIMEXITCOLOUR, Integer.toString(Color.BLUE.getRGB()));
			} else {
				swimColour = new Color(Integer.parseInt(colour));
			}
		}
		if (climbColour == null) {
			colour = mp.getProperty(MapperProperties.CLIMBEXITCOLOUR);
			if (colour == null) {
				climbColour = Color.MAGENTA;
				mp.setProperty(MapperProperties.CLIMBEXITCOLOUR, Integer.toString(Color.MAGENTA.getRGB()));
			} else {
				climbColour = new Color(Integer.parseInt(colour));
			}
		}

		switch (type) {
			case NORMAL_TYPE:
				return normalColour;
			case SWIM_TYPE:
				return swimColour;
			case CLIMB_TYPE:
				return climbColour;
			default:
				return Color.BLACK;
		}
	}

	public static String getLongForDir(int dir) {
		String toReturn = ""; //$NON-NLS-1$
		switch (dir) {
			case NORTH:
				toReturn = "north"; //$NON-NLS-1$
				break;
			case NORTHEAST:
				toReturn = "northeast"; //$NON-NLS-1$
				break;
			case EAST:
				toReturn = "east"; //$NON-NLS-1$
				break;
			case SOUTHEAST:
				toReturn = "southeast"; //$NON-NLS-1$
				break;
			case SOUTH:
				toReturn = "south"; //$NON-NLS-1$
				break;
			case SOUTHWEST:
				toReturn = "southwest"; //$NON-NLS-1$
				break;
			case WEST:
				toReturn = "west"; //$NON-NLS-1$
				break;
			case NORTHWEST:
				toReturn = "northwest"; //$NON-NLS-1$
				break;
			case UP:
				toReturn = "up"; //$NON-NLS-1$
				break;
			case DOWN:
				toReturn = "down"; //$NON-NLS-1$
				break;
			default:
				break;
		}
		return toReturn;

	}

	public static String getShortForDir(int dir) {
		String toReturn = "?"; //$NON-NLS-1$
		switch (dir) {
			case NORTH:
				toReturn = "n"; //$NON-NLS-1$
				break;
			case NORTHEAST:
				toReturn = "ne"; //$NON-NLS-1$
				break;
			case EAST:
				toReturn = "e"; //$NON-NLS-1$
				break;
			case SOUTHEAST:
				toReturn = "se"; //$NON-NLS-1$
				break;
			case SOUTH:
				toReturn = "s"; //$NON-NLS-1$
				break;
			case SOUTHWEST:
				toReturn = "sw"; //$NON-NLS-1$
				break;
			case WEST:
				toReturn = "w"; //$NON-NLS-1$
				break;
			case NORTHWEST:
				toReturn = "nw"; //$NON-NLS-1$
				break;
			case UP:
				toReturn = "u"; //$NON-NLS-1$
				break;
			case DOWN:
				toReturn = "d"; //$NON-NLS-1$
				break;
			default:
				break;
		}
		return toReturn;
	}

	String					dtab			= "\t\t";	//$NON-NLS-1$
	private int				endX			= 0;
	private int				endY			= 0;
	private final String	eol				= "\n";	//$NON-NLS-1$
	private boolean			highlighted		= false;
	private int				id				= 0;
	private int				level			= 0;
	private Line2D			line;
	private boolean			oneWay;
	private boolean			showStub;
	private int				sourceAreaRep	= 0;
	private int				sourceLevel		= Integer.MAX_VALUE, targetLevel = Integer.MAX_VALUE;

	private int				sourceRoom, targetRoom;
	private int				sourceType		= 0, targetType = 0;									// 0==normal,
																									// 1==swim,
	// 2==climb...
	private int				startX			= 0;
	private int				startY			= 0;
	String					tab				= "\t";												//$NON-NLS-1$
	private int				targetArea		= 0, sourceArea = 0;

	private int				targetAreaRep	= 0;

	private boolean			targetLock		= false, sourceLock = false;

	private String			targetWalkCommand	= "", sourceWalkCommand = "";	//$NON-NLS-1$ //$NON-NLS-2$

	private int				type				= 0;
	private boolean			invisible;
	private int				sourceGraphicsIconId, targetGraphicsIconId;

	/**
	 * Only to use when Importing XML
	 */
	public Exit() {}

	public Exit(int startX, int startY, int endX, int endY, int areaName, int level, int type, int sourceRoom, int targetRoom, int sourceLevel,
				int targetLevel, int sourceArea, int targetArea, int sourceType, int targetType, boolean showStub) {
		this.startX = startX;
		this.startY = startY;
		this.endX = endX;
		this.endY = endY;
		this.level = level;
		// this.areaName = areaName;
		this.type = type;
		this.sourceRoom = sourceRoom;
		this.targetRoom = targetRoom;
		this.sourceLevel = sourceLevel;
		this.targetLevel = targetLevel;
		this.sourceArea = sourceArea;
		this.targetArea = targetArea;
		this.sourceType = sourceType;
		this.targetType = targetType;
		this.showStub = showStub;
	}

	public Exit(int id, int startX, int startY, int endX, int endY, int areaName, int level, int type, int sourceRoom, int targetRoom, int sourceLevel,
				int targetLevel, int sourceArea, int targetArea, int sourceType, int targetType, boolean showStub, boolean sourceLock, boolean targetLock,
				String sourceWalkCommand, String targetWalkCommand) {
		this(startX, startY, endX, endY, areaName, level, type, sourceRoom, targetRoom, sourceLevel, targetLevel, sourceArea, targetArea, sourceType,
				targetType, showStub);
		this.id = id;
	}

	// e_id, startX, startY, endX, endY, oneWay, Level, Type, sourceRoom,
	// targetRoom, sourceLevel, targetLevel, sourceArea, targetArea, sourceType,
	// targetType, ShowStub, sourceLock, targetLock, sourceWalkCommand,
	// targetWalkCommand
	public Exit(ResultSet rs) throws SQLException {
		this.id = rs.getInt("e_id"); //$NON-NLS-1$
		this.startX = rs.getInt("startX"); //$NON-NLS-1$
		this.startY = rs.getInt("startY"); //$NON-NLS-1$
		this.endX = rs.getInt("endX"); //$NON-NLS-1$
		this.endY = rs.getInt("endY"); //$NON-NLS-1$
		this.level = rs.getInt("Level"); //$NON-NLS-1$
		this.type = rs.getInt("Type"); //$NON-NLS-1$
		this.sourceRoom = rs.getInt("sourceRoom"); //$NON-NLS-1$
		this.targetRoom = rs.getInt("targetRoom"); //$NON-NLS-1$
		this.sourceLevel = rs.getInt("sourceLevel"); //$NON-NLS-1$
		this.targetLevel = rs.getInt("targetLevel"); //$NON-NLS-1$
		this.sourceArea = rs.getInt("sourceArea"); //$NON-NLS-1$
		this.targetArea = rs.getInt("targetArea"); //$NON-NLS-1$
		this.sourceType = rs.getInt("sourceType"); //$NON-NLS-1$
		this.targetType = rs.getInt("targetType"); //$NON-NLS-1$
		this.showStub = (rs.getInt("showStub") == 1); //$NON-NLS-1$
		String tmp = rs.getString("sourceWalkCommand"); //$NON-NLS-1$
		this.sourceWalkCommand = (tmp == null ? "" : tmp); //$NON-NLS-1$
		tmp = rs.getString("targetWalkCommand"); //$NON-NLS-1$
		this.targetWalkCommand = (tmp == null ? "" : tmp); //$NON-NLS-1$
		this.sourceAreaRep = rs.getInt("sourceAreaRep") % BASE; //$NON-NLS-1$
		this.targetAreaRep = rs.getInt("targetAreaRep") % BASE; //$NON-NLS-1$
		this.setSourceGraphicsIconId(rs.getInt("sourceAreaRep") / BASE); //$NON-NLS-1$
		this.setTargetGraphicsIconId(rs.getInt("targetAreaRep") / BASE); //$NON-NLS-1$
		int oneWayInt = rs.getInt("oneway"); //$NON-NLS-1$
		this.setOneWay(((oneWayInt & 1) == 1));
		this.setInvisible(((oneWayInt & 2) == 2));
	}

	public void setInvisible(boolean b) {
		invisible = b;
	}

	public boolean isInvisible() {
		return invisible;
	}

	public void calculateLine() {
		line = createStubLine();
	}

	@Override
	public Exit clone() {
		Exit toReturn = new Exit();
		toReturn.setId(getId());
		toReturn.setStartX(getStartX());
		toReturn.setStartY(getStartY());
		toReturn.setEndX(getEndX());
		toReturn.setEndY(getEndY());
		toReturn.setOneWay(isOneWay());
		toReturn.setLevel(getLevel());
		toReturn.setType(getType());
		toReturn.setSourceRoomId(getSourceRoomId());
		toReturn.setTargetRoomId(getTargetRoomId());
		toReturn.setSourceLevel(getSourceLevel());
		toReturn.setTargetLevel(getTargetLevel());
		toReturn.setSourceType(getSourceType());
		toReturn.setTargetType(getTargetType());
		toReturn.setShowStub(getShowStub());
		toReturn.setSourceLock(sourceLock);
		toReturn.setTargetLock(targetLock);
		toReturn.setSourceWalkCommand(getSourceWalkCommand());
		toReturn.setTargetWalkCommand(getTargetWalkCommand());
		toReturn.setSourceAreaRep(getSourceAreaRep());
		toReturn.setTargetAreaRep(getTargetAreaRep());
		toReturn.setTargetArea(getTargetArea());
		toReturn.setSourceArea(getSourceArea());
		toReturn.setInvisible(isInvisible());
		return toReturn;
	}

	public PreparedStatement createInsertStatement(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getStartX());
		ps.setInt(2, getStartY());
		ps.setInt(3, getEndX());
		ps.setInt(4, getEndY());
		ps.setInt(5, createOneWayInt());
		ps.setInt(6, getLevel());
		ps.setInt(7, getType());
		ps.setInt(8, getSourceRoomId());
		ps.setInt(9, getTargetRoomId());
		ps.setInt(10, getSourceArea());
		ps.setInt(11, getTargetArea());
		ps.setInt(12, getSourceLevel());
		ps.setInt(13, getTargetLevel());
		ps.setInt(14, getSourceType());
		ps.setInt(15, getTargetType());
		ps.setInt(16, (getShowStub() ? 1 : 0));
		ps.setInt(17, (hasSourceLock() ? 1 : 0));
		ps.setInt(18, (hasTargetLock() ? 1 : 0));
		ps.setString(19, getSourceWalkCommand());
		ps.setString(20, getTargetWalkCommand());
		int sourceAreaRep = (getSourceGraphicsIconId() * BASE) + getSourceAreaRep();
		int targetAreaRep = (getTargetGraphicsIconId() * BASE) + getTargetAreaRep();
		ps.setInt(21, sourceAreaRep);
		ps.setInt(22, targetAreaRep);
		return ps;
	}

	private Line2D createStubLine() {
		Line2D toReturn = new Line2D.Double();
		if (startX == 0 || startY == 0) {
			Point mod = getStub(false);
			toReturn = new Line2D.Double(endX + mod.getX(), endY + mod.getY(), endX, endY);
		} else if (endX == 0 || endY == 0) {
			Point mod = getStub(true);
			toReturn = new Line2D.Double(startX, startY, startX + mod.getX(), startY + mod.getY());
		} else {
			toReturn = new Line2D.Double(startX, startY, endX, endY);
		}
		return toReturn;
	}

	public PreparedStatement createUpdateExitPrepareStatement(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getStartX());
		ps.setInt(2, getStartY());
		ps.setInt(3, getEndX());
		ps.setInt(4, getEndY());
		ps.setInt(5, createOneWayInt());
		ps.setInt(6, getLevel());
		ps.setInt(7, getType());
		ps.setInt(8, getSourceRoomId());
		ps.setInt(9, getTargetRoomId());
		ps.setInt(10, getSourceArea());
		ps.setInt(11, getTargetArea());
		ps.setInt(12, getSourceType());
		ps.setInt(13, getTargetType());
		ps.setInt(14, (getShowStub() ? 1 : 0));
		ps.setInt(15, (hasSourceLock() ? 1 : 0));
		ps.setInt(16, (hasTargetLock() ? 1 : 0));
		ps.setString(17, getSourceWalkCommand());
		ps.setString(18, getTargetWalkCommand());
		ps.setInt(19, getSourceLevel());
		ps.setInt(20, getTargetLevel());
		int sourceAreaRep = (getSourceGraphicsIconId() * BASE) + getSourceAreaRep();
		int targetAreaRep = (getTargetGraphicsIconId() * BASE) + getTargetAreaRep();
		ps.setInt(21, sourceAreaRep);
		ps.setInt(22, targetAreaRep);
		ps.setInt(23, getId());

		return ps;
	}

	private int createOneWayInt() {
		int toReturn = (isOneWay() ? 1 : 0) + (isInvisible() ? 2 : 0);
		return toReturn;
	}

	public int getEndX() {
		return endX;
	}

	public int getEndY() {
		return endY;
	}

	public int getId() {
		return id;
	}

	public int getLevel() {
		return level;
	}

	public boolean getShowStub() {
		return showStub;
	}

	public int getSourceArea() {
		return sourceArea;
	}

	/**
	 * @return the sourceAreaRep
	 */
	public int getSourceAreaRep() {
		return sourceAreaRep;
	}

	public int getSourceLevel() {
		return sourceLevel;
	}

	public int getSourceRoomId() {
		return sourceRoom;
	}

	/**
	 * Get the Exit type for the source room.
	 * 
	 * @return NORMAL_TYPE, SWIM_TYPE, or CLIMB_TYPE for the source room.
	 */
	public int getSourceType() {

		return sourceType;
	}

	public String getSourceWalkCommand() {
		if (sourceWalkCommand == null) return ""; //$NON-NLS-1$
		return sourceWalkCommand;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	private Point getStub(boolean start) {
		Point toReturn = new Point();
		int t = 0;
		if (start) {
			t = type / BASE;
		} else {
			t = type % BASE;
		}
		switch (t) {
			case NORTH:
				toReturn.setLocation(0, -5);
				break;
			case NORTH_EAST:
				toReturn.setLocation(5, -5);
				break;
			case EAST:
				toReturn.setLocation(5, 0);
				break;
			case SOUTH_EAST:
				toReturn.setLocation(5, 5);
				break;
			case SOUTH:
				toReturn.setLocation(0, 5);
				break;
			case SOUTH_WEST:
				toReturn.setLocation(-5, 5);
				break;
			case WEST:
				toReturn.setLocation(-5, 0);
				break;
			case NORTH_WEST:
				toReturn.setLocation(-5, -5);
				break;
			default:
				break;
		}
		return toReturn;
	}

	public int getTargetArea() {
		return targetArea;
	}

	/**
	 * @return the targetAreaRep
	 */
	public int getTargetAreaRep() {
		return targetAreaRep;
	}

	public int getTargetLevel() {
		return targetLevel;
	}

	public int getTargetRoomId() {
		return targetRoom;
	}

	/**
	 * Get the Exit type for the target room.
	 * 
	 * @return NORMAL_TYPE, SWIM_TYPE, or CLIMB_TYPE for the target room.
	 */
	public int getTargetType() {
		return targetType;
	}

	public String getTargetWalkCommand() {
		if (targetWalkCommand == null) return ""; //$NON-NLS-1$
		return targetWalkCommand;
	}

	public int getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	public boolean hasSourceLock() {
		return sourceLock;
	}

	public boolean hasTargetLock() {
		return targetLock;
	}

	public boolean isHighlighted() {
		return highlighted;
	}

	/**
	 * @return the oneWay
	 */
	public boolean isOneWay() {
		return oneWay;
	}

	public boolean onLine(Point point) {
		return line.intersects(point.getX() - 2, point.getY() - 2, 4, 4);
	}

	/**
	 * e_id, startX, startY, endX, endY, oneWay, Level, Type, sourceRoom,
	 * targetRoom, sourceArea, targetArea, sourceLevel, targetLevel, sourceType,
	 * targetType, ShowStub, sourceLock, targetLock, sourceWalkCommand,
	 * targetWalkCommand, SourceAreaRep, TargetAreaRep
	 * 
	 * @param ps
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatementForRestore(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setInt(2, getStartX());
		ps.setInt(3, getStartY());
		ps.setInt(4, getEndX());
		ps.setInt(5, getEndY());
		ps.setInt(6, createOneWayInt());
		ps.setInt(7, getLevel());
		ps.setInt(8, getType());
		ps.setInt(9, getSourceRoomId());
		ps.setInt(10, getTargetRoomId());
		ps.setInt(11, getSourceArea());
		ps.setInt(12, getTargetArea());
		ps.setInt(13, getSourceLevel());
		ps.setInt(14, getTargetLevel());
		ps.setInt(15, getSourceType());
		ps.setInt(16, getTargetType());
		ps.setInt(17, (getShowStub() ? 1 : 0));
		ps.setInt(18, (hasSourceLock() ? 1 : 0));
		ps.setInt(19, (hasTargetLock() ? 1 : 0));
		ps.setString(20, getSourceWalkCommand());
		ps.setString(21, getTargetWalkCommand());
		ps.setInt(22, getSourceAreaRep());
		ps.setInt(23, getTargetAreaRep());
		return ps;
	}

	public void setEndX(int endX) {
		this.endX = endX;
	}

	public void setEndY(int endY) {
		this.endY = endY;
	}

	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}

	private void setId(int id) {
		this.id = id;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @param oneWay
	 *            the oneWay to set
	 */
	public void setOneWay(boolean oneWay) {
		this.oneWay = oneWay;
	}

	public void setShowStub(boolean showStub) {
		this.showStub = showStub;
	}

	public void setSourceArea(int sourceArea) {
		this.sourceArea = sourceArea;
	}

	/**
	 * @param sourceAreaRep
	 *            the sourceAreaRep to set
	 */
	public void setSourceAreaRep(int sourceAreaRep) {
		this.sourceAreaRep = sourceAreaRep;
	}

	public void setSourceLevel(int sourceLevel) {
		this.sourceLevel = sourceLevel;
	}

	public void setSourceLock(boolean sourceLock) {
		this.sourceLock = sourceLock;
	}

	public void setSourcePoint(Point p) {
		this.startX = (int) p.getX();
		this.startY = (int) p.getY();
	}

	public void setSourceRoom(Room room, int direction) {
		if (room == null) return;
		setSourceRoomId(room.getId());
		setSourcePoint(ExitsManager.translateCoordinates(direction, room));
		setSourceLevel(room.getLevel());
		setSourceArea(room.getAreaId());
		if (targetArea == 0) setTargetArea(room.getAreaId());
		setType(direction * BASE + type % BASE);
	}

	public void setSourceRoomId(int sourceRoom) {
		this.sourceRoom = sourceRoom;
	}

	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}

	public void setSourceWalkCommand(String sourceWalkCommand) {
		this.sourceWalkCommand = sourceWalkCommand;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public void setTargetArea(int targetArea) {
		this.targetArea = targetArea;
	}

	/**
	 * @param targetAreaRep
	 *            the targetAreaRep to set
	 */
	public void setTargetAreaRep(int targetAreaRep) {
		this.targetAreaRep = targetAreaRep;
	}

	public void setTargetLevel(int targetLevel) {
		this.targetLevel = targetLevel;
	}

	public void setTargetLock(boolean targetLock) {
		this.targetLock = targetLock;
	}

	public void setTargetPoint(Point p) {
		this.endX = (int) p.getX();
		this.endY = (int) p.getY();
	}

	public void setTargetRoom(Room room, int direction) {
		if (room == null) return;
		setTargetRoomId(room.getId());
		setTargetPoint(ExitsManager.translateCoordinates(direction, room));
		setTargetLevel(room.getLevel());
		setTargetArea(room.getAreaId());
		if (sourceArea == 0) setSourceArea(room.getAreaId());
		setType((type / BASE) * Exit.BASE + direction);
	}

	public void setTargetRoomId(int targetRoom) {
		this.targetRoom = targetRoom;
	}

	public void setTargetType(int targetType) {
		this.targetType = targetType;
	}

	public void setTargetWalkCommand(String targetWalkCommand) {
		this.targetWalkCommand = targetWalkCommand;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean showStub() {
		return showStub;
	}

	@Override
	public String toString() {
		Database database = Database.getInstance();
		String sourceRoomName = ""; //$NON-NLS-1$
		if (sourceRoom != 0) {
			Room tmpRoom = database.getRoom(getSourceArea(), sourceRoom);
			if (tmpRoom != null) {
				sourceRoomName = tmpRoom.getName();
			} else {
				sourceRoomName = "Oh Dear, the exits points to a non existing room, BUG!"; //$NON-NLS-1$
			}
		}
		String targetRoomName = ""; //$NON-NLS-1$
		if (targetRoom != 0) {
			Room tmpRoom = database.getRoom(getTargetArea(), targetRoom);
			if (tmpRoom != null) {
				targetRoomName = tmpRoom.getName();
			} else {
				targetRoomName = "Oh Dear, the exits points to a non existing room, BUG!"; //$NON-NLS-1$
			}
		}
		String typeString = "(" + getShortForDir(type / Exit.BASE) + "-" + getShortForDir(type % Exit.BASE) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (targetArea == sourceArea) {
			return sourceRoom + " : " + sourceRoomName + " <---> " + targetRoom + " : " + targetRoomName + typeString; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} else {
			Area sourceAreaObject = database.getArea(sourceArea);
			Area targetAreaObject = database.getArea(targetArea);
			String sourceAreaName = (sourceAreaObject == null ? "null" : sourceAreaObject.getName()); //$NON-NLS-1$
			String targetAreaName = (targetAreaObject == null ? "null" : targetAreaObject.getName()); //$NON-NLS-1$
			return sourceRoom + " : " + sourceRoomName + "(" + sourceAreaName + ")  <---> " + targetRoom + " : " + targetRoomName + " (" + targetAreaName //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					+ ") :" + typeString; //$NON-NLS-1$
		}
	}

	/**
	 * Creates xml from the instance.
	 * 
	 * @return xml version of the instance of an Exit.
	 */
	public String toXml() {

		String toReturn = tab + "<exit>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<exittartx>" + startX + "</exittartx>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitstarty>" + startY + "</exitstarty>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitendx>" + endX + "</exitendx>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitendy>" + endY + "</exitendy>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitlevel>" + level + "</exitlevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittype>" + type + "</exittype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourceroom>" + sourceRoom + "</exitsourceroom>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetroom>" + targetRoom + "</exittargetroom>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourcelevel>" + sourceLevel + "</exitsourcelevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetlevel>" + targetLevel + "</exittargetlevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourcearea>" + sourceArea + "</exitsourcearea>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetarea>" + targetArea + "</exittargetarea>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourcetype>" + sourceType + "</exitsourcetype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetype>" + targetType + "</exittargetype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitshowstub>" + showStub + "</exitshowstub>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourcewalkcommand>" + sourceWalkCommand + "</exitsourcewalkcommand>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetwalkcommand>" + targetWalkCommand + "</exittargetwalkcommand>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exittargetAreaRep>" + targetAreaRep + "</exittargetAreaRep>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitsourceAreaRep>" + sourceAreaRep + "</exitsourceAreaRep>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitisinvisible>" + isInvisible() + "</exitisinvisible>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<exitisoneway>" + isOneWay() + "</exitisoneway>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "</exit>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	public String translateSourceTypeToText() {
		String toReturn = ""; //$NON-NLS-1$
		if (sourceType == SWIM_TYPE) toReturn = "swim"; //$NON-NLS-1$
		if (sourceType == CLIMB_TYPE) toReturn = "climb"; //$NON-NLS-1$
		return toReturn;
	}

	public String translateTargetTypeToText() {
		String toReturn = ""; //$NON-NLS-1$
		if (targetType == SWIM_TYPE) toReturn = "swim"; //$NON-NLS-1$
		if (targetType == CLIMB_TYPE) toReturn = "climb"; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * @return the sourceGraphicsIconId
	 */
	public int getSourceGraphicsIconId() {
		return sourceGraphicsIconId;
	}

	/**
	 * @param sourceGraphicsIconId
	 *            the sourceGraphicsIconId to set
	 */
	public void setSourceGraphicsIconId(int sourceGraphicsIconId) {
		this.sourceGraphicsIconId = sourceGraphicsIconId;
	}

	/**
	 * @return the targetGraphicsIconId
	 */
	public int getTargetGraphicsIconId() {
		return targetGraphicsIconId;
	}

	/**
	 * @param targetGraphicsIconId
	 *            the targetGraphicsIconId to set
	 */
	public void setTargetGraphicsIconId(int targetGraphicsIconId) {
		this.targetGraphicsIconId = targetGraphicsIconId;
	}
}
