/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import cfjmud.cfjmapper.database.Database;

/**
 * 
 * @author Ecthelion
 * 
 */
public class MapperIcon {
	String					dtab		= "\t\t";	//$NON-NLS-1$
	private final String	eol			= "\n";	//$NON-NLS-1$
	private String			fileName	= "";		//$NON-NLS-1$
	private int				id;
	private String			name		= "";		//$NON-NLS-1$
	String					tab			= "\t";	//$NON-NLS-1$
	private ImageIcon		theImage	= null;

	/**
	 * To use for xml purposes only
	 * 
	 * @return
	 */
	public MapperIcon() {}

	public MapperIcon(int id, String name, byte[] bytes) {
		theImage = new ImageIcon(bytes);
		this.id = id;
		this.name = name;
	}

	public MapperIcon(ResultSet rs) throws IOException, SQLException {
		int dbType = Database.getInstance().getDatabaseType();
		InputStream inputStream;
		if (dbType == Database.POSTG) {
			inputStream = rs.getBinaryStream("Icon"); //$NON-NLS-1$
			BufferedInputStream bis = new BufferedInputStream(inputStream);
			ArrayList<Integer> tmpV = new ArrayList<Integer>();
			int val = bis.read();
			while (val != -1) {
				tmpV.add(new Integer(val));
				val = bis.read();
			}
			Object[] arr = tmpV.toArray();
			byte[] img = new byte[arr.length];
			int c = 0;
			for (Object i : arr) {
				img[c++] = ((Integer) i).byteValue();
			}
			setName(rs.getString("Name")); //$NON-NLS-1$
			setId(rs.getInt("ti_id")); //$NON-NLS-1$
			theImage = new ImageIcon(img);
		} else {
			Blob img = null;
			img = rs.getBlob("Icon"); //$NON-NLS-1$
			setName(rs.getString("Name")); //$NON-NLS-1$
			setId(rs.getInt("ti_id")); //$NON-NLS-1$
			theImage = new ImageIcon(img.getBytes(1, (int) img.length()));
		}
	}

	public ImageIcon getIconImage() {
		return theImage;
	}

	public int getId() {
		return id;
	}

	public Image getImage() {
		return theImage.getImage();
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setImageIcon(ImageIcon theImage) {
		this.theImage = theImage;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toFileName() {
		if (fileName.isEmpty()) {
			fileName = getName() + ".png"; //$NON-NLS-1$
		}
		return fileName;
	}

	@Override
	public String toString() {
		return id + " - " + name;} //$NON-NLS-1$

	public String toXml() {
		String toReturn = tab + "<mappericon>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<mappericonname>" + getName() + "</mappericonname>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mappericonid>" + getId() + "</mappericonid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mappericonfilename>" + toFileName() + "</mappericonfilename>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "</mappericon>" + eol; //$NON-NLS-1$
		return toReturn;
	}

}
