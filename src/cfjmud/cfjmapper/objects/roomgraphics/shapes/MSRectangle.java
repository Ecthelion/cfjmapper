/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects.roomgraphics.shapes;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

/**
 * @author Ecthelion
 * 
 */
public class MSRectangle extends Rectangle2D.Double implements MSShape {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1696690247212428962L;
	private Color				colour				= Color.BLACK;
	private boolean				filled				= false;

	public MSRectangle() {
		super();
	}

	@Override
	public boolean isFilled() {
		return filled;
	}

	@Override
	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	@Override
	public Color getColour() {
		return colour;
	}

	@Override
	public void setColour(Color colour) {
		this.colour = colour;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public MSShape finish() {
		setRect(x, y, width, height);
		return this;
	}

	@Override
	public String toString() {
		return "Rectangle (" + (int) x + "," + (int) y + "," + (int) width + "," + (int) height + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
	}

	@Override
	public String toXml() {
		String tab = "\t\t"; //$NON-NLS-1$
		String t = "\t"; //$NON-NLS-1$
		String eol = "\n"; //$NON-NLS-1$
		String toReturn = t + "<rect>" + eol; //$NON-NLS-1$
		toReturn += tab + "<x>" + getX() + "</x>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<y>" + getY() + "</y>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<width>" + getWidth() + "</width>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<height>" + getHeight() + "</height>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<colour>" + Integer.toString(getColour().getRGB(), 16) + "</colour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<filled>" + Boolean.toString(isFilled()) + "</filled>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += t + "</rect>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	@Override
	public int compareTo(MSShape o) {
		return toString().compareTo(o.toString());
	}
}
