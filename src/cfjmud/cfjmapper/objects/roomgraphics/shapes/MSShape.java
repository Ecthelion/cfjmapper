/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects.roomgraphics.shapes;

import java.awt.Color;
import java.awt.Shape;

/**
 * 
 * @author Ecthelion
 * 
 */
public interface MSShape extends Shape, Comparable<MSShape> {

	/**
	 * @return the filled
	 */
	public abstract boolean isFilled();

	/**
	 * @param filled
	 *            should the rectangle be filled.
	 */
	public abstract void setFilled(boolean filled);

	/**
	 * @return the colour
	 */
	public abstract Color getColour();

	/**
	 * @param colour
	 *            the colour to set
	 */
	public abstract void setColour(Color colour);

	/**
	 * updates the parent...
	 * 
	 * @return an instance of the object finished.
	 */
	public abstract MSShape finish();

	public String toXml();

	@Override
	public String toString();

}
