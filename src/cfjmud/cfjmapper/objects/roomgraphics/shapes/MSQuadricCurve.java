/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects.roomgraphics.shapes;

import java.awt.Color;
import java.awt.geom.QuadCurve2D;

/**
 * @author Ecthelion
 * 
 */
public class MSQuadricCurve extends QuadCurve2D.Double implements MSShape {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1017935761674963332L;
	private Color				colour				= Color.BLACK;
	private boolean				filled				= false;

	public MSQuadricCurve() {
		super();
	}

	/**
	 * @return the colour
	 */
	@Override
	public Color getColour() {
		return colour;
	}

	/**
	 * @param colour
	 *            the colour to set
	 */
	@Override
	public void setColour(Color colour) {
		this.colour = colour;
	}

	/**
	 * @return the filled
	 */
	@Override
	public boolean isFilled() {
		return filled;
	}

	/**
	 * @param filled
	 *            the filled to set
	 */
	@Override
	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public void setCtrlX(double ctrlx) {
		this.ctrlx = ctrlx;
	}

	public void setCtrlY(double ctrly) {
		this.ctrly = ctrly;
	}

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public void setX2(double x2) {
		this.x2 = x2;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public void setY2(double y2) {
		this.y2 = y2;
	}

	@Override
	public MSShape finish() {
		setCurve(x1, y1, ctrlx, ctrly, x2, y2);
		return this;
	}

	@Override
	public String toXml() {
		String tab = "\t\t"; //$NON-NLS-1$
		String t = "\t"; //$NON-NLS-1$
		String eol = "\n"; //$NON-NLS-1$
		String toReturn = t + "<curve>" + eol; //$NON-NLS-1$
		toReturn += tab + "<x1>" + getX1() + "</x1>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<y1>" + getY1() + "</y1>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<ctrlx>" + getCtrlX() + "</ctrlx>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<ctrly>" + getCtrlY() + "</ctrly>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<x2>" + getX2() + "</x2>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<y2>" + getY2() + "</y2>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<colour>" + Integer.toString(getColour().getRGB(), 16) + "</colour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += t + "</curve>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	@Override
	public int compareTo(MSShape o) {
		return toString().compareTo(o.toString());
	}
}
