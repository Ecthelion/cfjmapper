/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects.roomgraphics.shapes;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Line2D;

/**
 * @author Ecthelion
 * 
 */
public class MSPoint extends Line2D.Double implements MSShape {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8687662258395675385L;
	private Color				colour				= Color.BLACK;
	@SuppressWarnings("unused")
	private boolean				filled				= false;

	public MSPoint(double x, double y) {
		super(x, y, x, y);
	}

	public MSPoint() {
		super();
	}

	public void setLocation(Point p) {
		x1 = x2 = p.x;
		y1 = y2 = p.y;
	}

	public void setX(double x) {
		x1 = x2 = x;
	}

	public void setY(double y) {
		y1 = y2 = y;
	}

	public Point getLocation() {
		return new Point((int) x1, (int) y1);
	}

	/**
	 * @return the colour
	 */
	@Override
	public Color getColour() {
		return colour;
	}

	/**
	 * @param colour
	 *            the colour to set
	 */
	@Override
	public void setColour(Color colour) {
		this.colour = colour;
	}

	/**
	 * @return the filled
	 */
	@Override
	public boolean isFilled() {
		return false;
	}

	/**
	 * @param filled
	 *            the filled to set
	 */
	@Override
	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	@Override
	public MSShape finish() {
		super.setLine(x1, y1, x2, y2);
		return this;
	}

	@Override
	public String toString() {
		return "Point (" + (int) x1 + "," + (int) y1 + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	@Override
	public String toXml() {
		String tab = "\t\t"; //$NON-NLS-1$
		String t = "\t"; //$NON-NLS-1$
		String eol = "\n"; //$NON-NLS-1$
		String toReturn = t + "<point>" + eol; //$NON-NLS-1$
		toReturn += tab + "<x>" + getX1() + "</x>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<y>" + getY1() + "</y>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<colour>" + Integer.toString(getColour().getRGB(), 16) + "</colour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += t + "</point>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	@Override
	public int compareTo(MSShape o) {
		return toString().compareTo(o.toString());
	}
}
