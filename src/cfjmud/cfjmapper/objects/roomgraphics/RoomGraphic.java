/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects.roomgraphics;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSShape;

/**
 * 
 * @author Ecthelion
 * 
 */
public class RoomGraphic implements ListCellRenderer<RoomGraphic>, Comparable<RoomGraphic> {
	private int						id;
	private String					name;
	private final Vector<MSShape>	shapes;

	public RoomGraphic() {
		id = 0;
		name = ""; //$NON-NLS-1$
		shapes = new Vector<MSShape>();
	}

	/**
	 * Set id...
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Get Id.
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Adds a Shape to the list
	 * 
	 * @return
	 */
	public void addShape(MSShape shape) {
		shapes.add(shape.finish());
	}

	/**
	 * 
	 * @return the shapes in this RoomGraphic
	 */
	public Vector<MSShape> getShapes() {
		return shapes;
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends RoomGraphic> arg0, RoomGraphic rep, int arg2, boolean arg3, boolean arg4) {
		if (rep == null) return new JLabel();
		// Dimension d = rep.getBounds();// new Dimension(16, 16);
		int side = (int) rep.getBounds().getHeight();
		if (side == 0) side = 16;
		//Integer.parseInt(MapperProperties.getInstance().getProperty(MapperProperties.GRAPHICEDITORSIDE, "16")); //$NON-NLS-1$
		// side = side * 4;
		BufferedImage bi = new BufferedImage(side, side, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D) bi.getGraphics();
		int border = (side - 16) / 2;

		g.fillRect(0, 0, side, side);
		g.translate(border, border);
		g.setColor(Color.WHITE);
		for (MSShape shape : rep.getShapes()) {
			if (shape.isFilled()) {
				g.setColor(shape.getColour());
				g.fill(shape);
			} else {
				g.setColor(shape.getColour());
				g.draw(shape);
			}
		}
		Image image = bi.getScaledInstance(16, 16, BufferedImage.SCALE_SMOOTH);
		JPanel toReturn = new JPanel();
		JLabel label = new JLabel(new ImageIcon(image));
		JLabel name = new JLabel((rep == null ? "" : " - ") + rep.getName()); //$NON-NLS-1$ //$NON-NLS-2$
		toReturn.setLayout(new FlowLayout(FlowLayout.LEFT));
		toReturn.add(label);
		toReturn.add(name);
		return toReturn;
	}

	public Rectangle2D getBounds() {
		java.awt.geom.Area area = new Area();
		for (MSShape shape : shapes) {
			area.add(new Area(shape));
		}
		return area.getBounds2D();
	}

	@Override
	public String toString() {
		return name;
	}

	public String toXml() {
		String tab = "\t"; //$NON-NLS-1$
		String eol = "\n"; //$NON-NLS-1$
		String toReturn = "<graphic>" + eol; //$NON-NLS-1$
		toReturn += tab + "<id>" + getId() + "</id>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "<name>" + getName() + "</name>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		for (MSShape shape : shapes) {
			toReturn += shape.toXml();
		}
		toReturn += "</graphic>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	@Override
	public int compareTo(RoomGraphic o) {
		return getName().compareTo(o.getName());
	}

	public int moveItemUp(int selectedIndex) {
		if (selectedIndex <= 0) return 0;
		MSShape shape = shapes.get(selectedIndex);
		shapes.remove(selectedIndex);
		shapes.add(selectedIndex - 1, shape);
		return selectedIndex - 1;
	}

	public int moveItemDown(int selectedIndex) {
		if (selectedIndex >= shapes.size() || selectedIndex < 0) return 0;
		MSShape shape = shapes.get(selectedIndex);
		shapes.remove(selectedIndex);
		shapes.add(selectedIndex + 1, shape);
		return selectedIndex + 1;
	}

	public void deleteShape(int selectedIndex) {
		if (selectedIndex == -1) return;
		shapes.remove(selectedIndex);
	}

	public RoomGraphic copy() {
		RoomGraphic toReturn = new RoomGraphic();
		for (MSShape shape : getShapes()) {
			toReturn.addShape(shape);
		}
		toReturn.setName("Copy of " + getName()); //$NON-NLS-1$
		toReturn.setId(RoomGraphicsManager.getInstance().getNewId());
		return toReturn;
	}
}
