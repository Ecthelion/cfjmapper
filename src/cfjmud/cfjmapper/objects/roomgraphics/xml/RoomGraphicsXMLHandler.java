/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects.roomgraphics.xml;

import java.awt.Color;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSCubicCurve;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSLine;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSOval;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSPoint;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSQuadricCurve;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSRectangle;

/**
 * 
 * @author Ecthelion
 * 
 */
public class RoomGraphicsXMLHandler extends DefaultHandler {
	StringBuffer			textBuffer;
	private final boolean	debugga				= false;
	String					currentElement		= "";		//$NON-NLS-1$
	String					currentMainElement	= "";		//$NON-NLS-1$
	RoomGraphic				currentRoomShape;
	ArrayList<RoomGraphic>	shapeList;
	private MSLine			currentMSLine;
	private MSPoint			currentMSPoint;
	private MSQuadricCurve	currentMSQuadraticCurve;
	private MSCubicCurve	currentMSCubicCurve;
	private MSOval			currentMSOval;
	private MSRectangle		currentMSRectangle;
	private String			currentElementName;

	public RoomGraphicsXMLHandler() {
		shapeList = new ArrayList<RoomGraphic>();
	}

	@Override
	public void characters(char buf[], int offset, int len) throws SAXException {
		String s = new String(buf, offset, len);
		if (textBuffer == null) {
			textBuffer = new StringBuffer(s);
		} else {
			textBuffer.append(s);
		}
	}

	private void debug(String s) throws SAXException {
		if (debugga) {
			System.out.print(s);
			System.out.flush();
		}
	}

	private void echoText() throws SAXException {
		if (textBuffer == null) return;
		nl();
		debug("CHARS:   "); //$NON-NLS-1$ 
		String s = "" + textBuffer; //$NON-NLS-1$
		debug("The Current Element: " + currentElement + " - " + s + " "); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		if (!s.trim().equals("")) { //$NON-NLS-1$
			debug("The Current Element: " + currentElement + s); //$NON-NLS-1$
			/* ********************* RoomGraphic ********************* */
			/* ********************** General ********************** */
			if (currentElement.equalsIgnoreCase("id")) currentRoomShape.setId(Integer.parseInt(s)); //$NON-NLS-1$
			if (currentElement.equalsIgnoreCase("name")) currentRoomShape.setName(s); //$NON-NLS-1$
			/* ********************* End General ******************* */
			if (currentElementName != null) {
				if (currentElementName.equalsIgnoreCase("line")) { //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x1")) currentMSLine.setX1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x2")) currentMSLine.setX2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y1")) currentMSLine.setY1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y2")) currentMSLine.setY2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("colour")) currentMSLine.setColour(new Color(Integer.parseInt(s, 16))); //$NON-NLS-1$
				} else if (currentElementName.equalsIgnoreCase("point")) { //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x")) currentMSPoint.setX(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y")) currentMSPoint.setY(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("colour")) currentMSPoint.setColour(new Color(Integer.parseInt(s, 16))); //$NON-NLS-1$
				} else if (currentElementName.equalsIgnoreCase("quad")) { //$NON-NLS-1$

				} else if (currentElementName.equalsIgnoreCase("curve")) { //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x1")) currentMSCubicCurve.setX1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x2")) currentMSCubicCurve.setX2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y1")) currentMSCubicCurve.setY1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y2")) currentMSCubicCurve.setY2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("ctrlx1")) currentMSCubicCurve.setCtrlX1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("ctrlx2")) currentMSCubicCurve.setCtrlX2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("ctrly1")) currentMSCubicCurve.setCtrlY1(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("ctrly2")) currentMSCubicCurve.setCtrlY2(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("colour")) currentMSCubicCurve.setColour(new Color(Integer.parseInt(s, 16))); //$NON-NLS-1$

				} else if (currentElementName.equalsIgnoreCase("oval")) { //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x")) currentMSOval.setX(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y")) currentMSOval.setY(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("width")) currentMSOval.setWidth(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("height")) currentMSOval.setHeight(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("filled")) currentMSOval.setFilled(Boolean.parseBoolean(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("colour")) currentMSOval.setColour(new Color(Integer.parseInt(s, 16))); //$NON-NLS-1$

				} else if (currentElementName.equalsIgnoreCase("rect")) { //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("x")) currentMSRectangle.setX(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("y")) currentMSRectangle.setY(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("width")) currentMSRectangle.setWidth(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("height")) currentMSRectangle.setHeight(Double.parseDouble(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("filled")) currentMSRectangle.setFilled(Boolean.parseBoolean(s)); //$NON-NLS-1$
					if (currentElement.equalsIgnoreCase("colour")) currentMSRectangle.setColour(new Color(Integer.parseInt(s, 16))); //$NON-NLS-1$
				}
			}
			/* ******************* End RoomGraphic ******************* */
			debug("|"); //$NON-NLS-1$
		}

		textBuffer = null;
	}

	@Override
	public void endDocument() throws SAXException {
		nl();
		debug("END DOCUMENT"); //$NON-NLS-1$

	}

	@Override
	public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
		echoText();
		nl();
		debug("END_ELM: "); //$NON-NLS-1$

		String eName = sName; // element name
		if ("".equals(eName)) eName = qName; // not name space-aware //$NON-NLS-1$

		if (eName.equalsIgnoreCase("graphic")) { //$NON-NLS-1$
			debug("Adding RoomShape to Vector"); //$NON-NLS-1$
			shapeList.add(currentRoomShape);
		}

		if (eName.equalsIgnoreCase("line")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSLine);
		}

		if (eName.equalsIgnoreCase("point")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSPoint);
		}

		if (eName.equalsIgnoreCase("quad")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSQuadraticCurve);
		}

		if (eName.equalsIgnoreCase("curve")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSCubicCurve);
		}

		if (eName.equalsIgnoreCase("oval")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSOval);
		}

		if (eName.equalsIgnoreCase("rect")) { //$NON-NLS-1$
			currentRoomShape.addShape(currentMSRectangle);
		}

		debug("</" + eName + ">"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Override
	public void startDocument() throws SAXException {
		debug("START DOCUMENT"); //$NON-NLS-1$
		debug("<?xml version='1.0' encoding='UTF-8'?>"); //$NON-NLS-1$
	}

	private void nl() throws SAXException {
		String lineEnd = System.getProperty("line.separator"); //$NON-NLS-1$
		debug(lineEnd);
	}

	@Override
	public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
		echoText();
		nl();
		debug("ELEMENT: "); //$NON-NLS-1$
		String eName = sName; // element name
		if ("".equals(eName)) //$NON-NLS-1$
								eName = qName; // not name space-aware

		if (eName.equalsIgnoreCase("graphic")) { //$NON-NLS-1$
			debug("Creating New RoomGraphic."); //$NON-NLS-1$
			currentRoomShape = new RoomGraphic();
		}

		if (eName.equalsIgnoreCase("line")) { //$NON-NLS-1$
			currentMSLine = new MSLine();
			currentElementName = eName;
		}

		if (eName.equalsIgnoreCase("point")) { //$NON-NLS-1$
			currentMSPoint = new MSPoint();
			currentElementName = eName;
		}

		if (eName.equalsIgnoreCase("quad")) { //$NON-NLS-1$
			currentMSQuadraticCurve = new MSQuadricCurve();
			currentElementName = eName;
		}

		if (eName.equalsIgnoreCase("curve")) { //$NON-NLS-1$
			currentMSCubicCurve = new MSCubicCurve();
			currentElementName = eName;
		}

		if (eName.equalsIgnoreCase("oval")) { //$NON-NLS-1$
			currentMSOval = new MSOval();
			currentElementName = eName;
		}

		if (eName.equalsIgnoreCase("rect")) { //$NON-NLS-1$
			currentMSRectangle = new MSRectangle();
			currentElementName = eName;
		}

		currentElement = eName;

		debug("<" + eName); //$NON-NLS-1$
		debug(">"); //$NON-NLS-1$
	}

	public ArrayList<RoomGraphic> getShapeList() {
		return shapeList;
	}

}
