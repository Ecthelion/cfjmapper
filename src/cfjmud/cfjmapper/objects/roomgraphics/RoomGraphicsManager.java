/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects.roomgraphics;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.objects.roomgraphics.xml.RoomGraphicsXMLHandler;

/**
 * @author Ecthelion
 * 
 */
public class RoomGraphicsManager {
	private final Hashtable<Integer, RoomGraphic>	shapes;
	private final String							fileName	= "customGraphics.xml"; //$NON-NLS-1$

	private static RoomGraphicsManager				instance	= null;

	private RoomGraphicsManager() {
		shapes = new Hashtable<Integer, RoomGraphic>();
		File file = new File(fileName);
		if (!file.exists()) createDefaultFile(file);
	}

	public RoomGraphic getShape(int i) {
		RoomGraphic shape = shapes.get(new Integer(i));
		if (shape == null) {
			loadShapes();
		} else {
			return shape;
		}
		shape = shapes.get(new Integer(i));
		return shape;
	}

	public void addShape(RoomGraphic rg) {
		shapes.put(new Integer(rg.getId()), rg);
		saveShapes();
	}

	public void saveShapes() {
		File file = new File(fileName);
		PrintStream ps;
		try {
			ps = new PrintStream(file);
			ps.println("<?xml version='1.0' encoding='UTF-8'?>"); //$NON-NLS-1$
			ps.println("<root>"); //$NON-NLS-1$
			for (RoomGraphic rg : shapes.values()) {
				ps.print(rg.toXml());
			}
			ps.println("</root>"); //$NON-NLS-1$
			ps.flush();
			ps.close();
		} catch (FileNotFoundException e) {
			System.out.println("Oh Dear, couldn't save the RoomGraphics: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	public void loadShapes() {
		File file = new File(fileName);
		if (!file.exists()) createDefaultFile(file);

		try {
			RoomGraphicsXMLHandler handler = new RoomGraphicsXMLHandler();
			SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
			saxParser.parse(file, handler);
			for (RoomGraphic rg : handler.getShapeList()) {
				shapes.put(new Integer(rg.getId()), rg);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			System.out.println("Failed to import shapes: " + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}

	}

	private void createDefaultFile(File file) {
		try {
			String t = "\t"; //$NON-NLS-1$
			String dt = t + t;
			String tt = t + t + t;
			PrintStream ps = new PrintStream(file);
			ps.println("<?xml version='1.0' encoding='UTF-8'?>"); //$NON-NLS-1$
			ps.println("<root>"); //$NON-NLS-1$
			ps.println(t + "<graphic>"); //$NON-NLS-1$
			ps.println(tt + "<id>1</id>"); //$NON-NLS-1$
			ps.println(tt + "<name>hammer and anvil</name>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>0</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>0</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>16</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>16</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(tt + "<colour>FFCC00</colour>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>1</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>2</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>4</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>4</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>2</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>1</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>2</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>6</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>5</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>3</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>10</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>2</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<point>"); //$NON-NLS-1$
			ps.println(tt + "<x>1</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>8</y>"); //$NON-NLS-1$
			ps.println(dt + "</point>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>2</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>8</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>2</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>2</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>4</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>8</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>2</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>3</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>6</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>8</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>7</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>4</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<rect>"); //$NON-NLS-1$
			ps.println(tt + "<x>13</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>8</y>"); //$NON-NLS-1$
			ps.println(tt + "<width>2</width>"); //$NON-NLS-1$
			ps.println(tt + "<height>2</height>"); //$NON-NLS-1$
			ps.println(tt + "<filled>true</filled>"); //$NON-NLS-1$
			ps.println(dt + "</rect>"); //$NON-NLS-1$

			ps.println(dt + "<point>"); //$NON-NLS-1$
			ps.println(tt + "<x>13</x>"); //$NON-NLS-1$
			ps.println(tt + "<y>10</y>"); //$NON-NLS-1$
			ps.println(dt + "</point>"); //$NON-NLS-1$

			ps.println(dt + "<line>"); //$NON-NLS-1$
			ps.println(tt + "<x1>7</x1>"); //$NON-NLS-1$
			ps.println(tt + "<y1>12</y1>"); //$NON-NLS-1$
			ps.println(tt + "<x2>9</x2>"); //$NON-NLS-1$
			ps.println(tt + "<y2>12</y2>"); //$NON-NLS-1$
			ps.println(dt + "</line>"); //$NON-NLS-1$

			ps.println(dt + "<line>"); //$NON-NLS-1$
			ps.println(tt + "<x1>2</x1>"); //$NON-NLS-1$
			ps.println(tt + "<y1>13</y1>"); //$NON-NLS-1$
			ps.println(tt + "<x2>13</x2>"); //$NON-NLS-1$
			ps.println(tt + "<y2>13</y2>"); //$NON-NLS-1$
			ps.println(dt + "</line>"); //$NON-NLS-1$

			ps.println(dt + "<line>"); //$NON-NLS-1$
			ps.println(tt + "<x1>1</x1>"); //$NON-NLS-1$
			ps.println(tt + "<y1>14</y1>"); //$NON-NLS-1$
			ps.println(tt + "<x2>14</x2>"); //$NON-NLS-1$
			ps.println(tt + "<y2>14</y2>"); //$NON-NLS-1$
			ps.println(dt + "</line>"); //$NON-NLS-1$

			ps.println(t + "</graphic>"); //$NON-NLS-1$
			ps.println("</root>\n"); //$NON-NLS-1$
			ps.flush();
			ps.close();
		} catch (IOException e) {
			System.out.println("Failed to create default graphics file." + e.toString()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * @return the instance
	 */
	public static RoomGraphicsManager getInstance() {
		if (instance == null) instance = new RoomGraphicsManager();
		return instance;
	}

	public RoomGraphic[] getShapes() {
		loadShapes();
		RoomGraphic[] gs = new RoomGraphic[shapes.size()];
		gs = shapes.values().toArray(gs);
		RoomGraphic[] toReturn = new RoomGraphic[shapes.size() + 1];
		toReturn[0] = new RoomGraphic();
		for (int i = 1; i <= shapes.size(); i++) {
			toReturn[i] = gs[i - 1];
		}
		Arrays.sort(toReturn);
		return toReturn;
	}

	public int getHighestId() {
		int toReturn = 0;
		for (Integer key : shapes.keySet()) {
			if (key.intValue() > toReturn) toReturn = key.intValue();
		}
		return toReturn;
	}

	public int getNewId() {
		return getHighestId() + 1;
	}

	public void deleteGraphics(RoomGraphic selectedValue) {
		shapes.remove(new Integer(selectedValue.getId()));
	}

	public void updateShape(RoomGraphic rg) {
		shapes.put(new Integer(rg.getId()), rg);
	}

	private static int		toReturn	= 0;
	private static JDialog	chooserdialog;

	/**
	 * creates and show a chooser and returns the id of the graphics choosen.
	 * 
	 * @return RoomGraphics Id.
	 */
	public static int showChooser(Mapper mapper) {
		RoomGraphicsManager manager = RoomGraphicsManager.getInstance();
		final JList<RoomGraphic> graphicsList = new JList<RoomGraphic>(manager.getShapes());
		// graphicsList.setCellRenderer(manager.getShapes()[0]);
		JScrollPane pane = new JScrollPane(graphicsList);
		Dimension dim = new Dimension(200, 300);
		pane.setSize(dim);
		pane.setPreferredSize(dim);

		JButton okButton = new JButton("Ok"); //$NON-NLS-1$
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				toReturn = graphicsList.getSelectedValue().getId();
				chooserdialog.setVisible(false);
			}
		});
		JButton cancelButton = new JButton("Cancel"); //$NON-NLS-1$
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooserdialog.setVisible(false);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);
		chooserdialog = new JDialog(mapper, true);
		chooserdialog.setLayout(new BorderLayout(2, 5));
		chooserdialog.add(pane, BorderLayout.CENTER);
		chooserdialog.add(buttonPanel, BorderLayout.SOUTH);
		chooserdialog.pack();
		chooserdialog.setLocationRelativeTo(mapper);
		chooserdialog.setVisible(true);
		return toReturn;
	}
}
