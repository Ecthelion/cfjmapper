/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;

import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.interfaces.MapperConstants;

/**
 * 
 * A wrap for the ZoneRoom Table...
 * 
 * @author Ecthelion
 * 
 */
public class ZoneRoom implements Comparable<ZoneRoom>, Comparator<ZoneRoom>, MapperConstants {
	private Room	room;
	private int		index;
	private int		id;
	private int		a_id;
	private int		r_id;
	private int		z_id;

	public ZoneRoom(ResultSet rs, int currentArea) throws SQLException {
		r_id = rs.getInt("r_id"); //$NON-NLS-1$
		id = rs.getInt("zr_id"); //$NON-NLS-1$
		a_id = currentArea;
		setZ_id(rs.getInt("z_id")); //$NON-NLS-1$
		room = null;
		setIndex(rs.getInt("index")); //$NON-NLS-1$
	}

	/**
	 * For use in the importer only.
	 */
	public ZoneRoom() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the room
	 */
	public Room getRoom() {
		if (room == null) room = Database.getInstance().getRoom(a_id, r_id);
		return room;
	}

	/**
	 * @param room
	 *            the room to set
	 */
	public void setRoom(Room room) {
		this.room = room;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		Room room = getRoom();
		return (room != null ? room.toString() : "null") + "(" + index + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	@Override
	public int compareTo(ZoneRoom o) {
		Integer q = new Integer(o.getIndex());
		Integer w = new Integer(getIndex());
		return w.compareTo(q);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public int getRoomId() {
		return r_id;
	}

	public int getAreaId() {
		return a_id;
	}

	@Override
	public int compare(ZoneRoom o1, ZoneRoom o2) {
		return Integer.compare(o1.getIndex(), o2.getIndex());
	}

	public String toXml() {
		String toReturn = tab + "<zoneroom>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<zoneroomid>" + getId() + "</zoneroomid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneroomroomid>" + getRoomId() + "</zoneroomroomid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneroomareaid>" + getAreaId() + "</zoneroomareaid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneroomzoneid>" + getZ_id() + "</zoneroomzoneid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneroomindex>" + getIndex() + "</zoneroomindex>" + eol; //$NON-NLS-1$ //$NON-NLS-2$

		toReturn += tab + "</zoneroom>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * @return the a_id
	 */
	public int getA_id() {
		return a_id;
	}

	/**
	 * @param a_id
	 *            the a_id to set
	 */
	public void setA_id(int a_id) {
		this.a_id = a_id;
	}

	/**
	 * @return the r_id
	 */
	public int getR_id() {
		return r_id;
	}

	/**
	 * @param r_id
	 *            the r_id to set
	 */
	public void setR_id(int r_id) {
		this.r_id = r_id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the z_id
	 */
	public int getZ_id() {
		return z_id;
	}

	/**
	 * @param z_id
	 *            the z_id to set
	 */
	public void setZ_id(int z_id) {
		this.z_id = z_id;
	}

	public PreparedStatement prepareInsertForZoneRoom(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getR_id());
		ps.setInt(2, getZ_id());
		ps.setInt(3, getIndex());
		return ps;
	}

}
