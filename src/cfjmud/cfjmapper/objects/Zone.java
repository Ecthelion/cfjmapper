/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.MovedOutlineStroke;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.ZoneDialog;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphic;
import cfjmud.cfjmapper.objects.roomgraphics.RoomGraphicsManager;
import cfjmud.cfjmapper.objects.roomgraphics.shapes.MSShape;
import cfjmud.cfjmapper.panels.MapPanel;

import com.jhlabs.awt.TextStroke;

/**
 * @author Ecthelion
 * 
 */
public class Zone implements MapperConstants, Comparable<Zone>, Comparator<Zone> {

	public static String		types[]			= { "Filled", "Graphics", "Icon" };				//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	public static String		fillStyles[]	= { "Local", "Line", "Expand", "Filled Outline" };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	public final static int		LOCAL_SHAPE		= 0;
	public final static int		LINE_SHAPE		= 1;
	public final static int		EXPAND_SHAPE	= 2;
	public final static int		FILLED_OUTLINE	= 3;
	public final static int		FILL_TYPE		= 0;
	public final static int		GRAPHIC_TYPE	= 1;
	public final static int		ICON_TYPE		= 2;
	private int					zone_id, area_id, backGroundStyle, backGroundReference, type, border, fillStyle;
	private ArrayList<Room>		roomsInZone;
	private String				name, description;
	private Color				backGroundColor;
	private final Database		database;
	private Shape				area;
	private TreeSet<ZoneRoom>	set				= null;
	private boolean				highlighted;

	private int					labelalignment	= 0;
	private String				labeltext		= "";																//$NON-NLS-1$
	private Color				labelcolour		= Color.BLACK;
	private String				labelfont		= Font.DIALOG;
	private int					borderthickness	= 0;
	private Color				bordercolour	= Color.BLACK;
	private int					bordertype		= 0;
	private float				labelfontsize	= 10;
	private int					labelfontstyle	= Font.PLAIN;
	private int					numberOfRepeats	= 1;

	public Zone() {
		roomsInZone = null;
		set = null;
		database = Database.getInstance();
		backGroundColor = Color.BLUE;
		name = ""; //$NON-NLS-1$
		description = ""; //$NON-NLS-1$
	}

	public Zone(ResultSet rs) throws SQLException {
		this();
		zone_id = rs.getInt("z_id"); //$NON-NLS-1$
		area_id = rs.getInt("a_id"); //$NON-NLS-1$
		backGroundStyle = rs.getInt("bgstyle"); //$NON-NLS-1$
		backGroundReference = rs.getInt("bgref"); //$NON-NLS-1$
		backGroundColor = new Color(rs.getInt("bgcolour")); //$NON-NLS-1$
		border = rs.getInt("border"); //$NON-NLS-1$
		type = rs.getInt("type"); //$NON-NLS-1$
		fillStyle = rs.getInt("paintstyle"); //$NON-NLS-1$
		description = rs.getString("description"); //$NON-NLS-1$
		name = rs.getString("name"); //$NON-NLS-1$
		labelalignment = rs.getInt("labelalignment") % Exit.BASE; //$NON-NLS-1$
		numberOfRepeats = rs.getInt("labelalignment") / Exit.BASE; //$NON-NLS-1$
		labeltext = rs.getString("labeltext"); //$NON-NLS-1$
		labelcolour = new Color(rs.getInt("labelcolour")); //$NON-NLS-1$
		labelfontsize = rs.getFloat("labelfontsize"); //$NON-NLS-1$
		borderthickness = rs.getInt("borderthickness"); //$NON-NLS-1$
		bordercolour = new Color(rs.getInt("bordercolour")); //$NON-NLS-1$
		bordertype = rs.getInt("bordertype"); //$NON-NLS-1$
		labelfont = rs.getString("labelfont"); //$NON-NLS-1$
		labelfontstyle = rs.getInt("labelfontstyle"); //$NON-NLS-1$
	}

	public Zone(String name, int area_id) {
		this();
		this.name = name;
		this.area_id = area_id;
	}

	public void deleteRoom(Room room) {
		if (roomsInZone == null) roomsInZone = database.getRoomsForZone(zone_id, area_id);
		roomsInZone.remove(room);
	}

	/**
	 * @return the zone_id
	 */
	public int getZone_id() {
		return zone_id;
	}

	/**
	 * @param zone_id
	 *            the zone_id to set
	 */
	public void setZone_id(int zone_id) {
		this.zone_id = zone_id;
	}

	/**
	 * @return the area_id
	 */
	public int getArea_id() {
		return area_id;
	}

	/**
	 * @param area_id
	 *            the area_id to set
	 */
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}

	/**
	 * @return the backGroundStyle
	 */
	public int getBackGroundStyle() {
		return backGroundStyle;
	}

	/**
	 * @param backGroundStyle
	 *            the backGroundStyle to set
	 */
	public void setBackGroundStyle(int backGroundStyle) {
		this.backGroundStyle = backGroundStyle;
	}

	/**
	 * @return the backGroundReference
	 */
	public int getBackGroundReference() {
		return backGroundReference;
	}

	/**
	 * @param backGroundReference
	 *            the backGroundReference to set
	 */
	public void setBackGroundReference(int backGroundReference) {
		this.backGroundReference = backGroundReference;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the roomsInZone
	 */
	public ArrayList<Room> getRoomsInZone() {
		if (roomsInZone == null) roomsInZone = database.getRoomsForZone(zone_id, area_id);

		return roomsInZone;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null) return ""; //$NON-NLS-1$
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		if (description == null) return ""; //$NON-NLS-1$
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the backGroundColor
	 */
	public Color getBackGroundColor() {
		return backGroundColor;
	}

	/**
	 * @param backGroundColor
	 *            the backGroundColor to set
	 */
	public void setBackGroundColor(Color backGroundColor) {
		this.backGroundColor = backGroundColor;
	}

	public Room[] getRoomsForList() {
		if (roomsInZone == null) roomsInZone = database.getRoomsForZone(zone_id, area_id);
		Room[] toReturn = new Room[roomsInZone.size()];
		toReturn = roomsInZone.toArray(toReturn);
		return toReturn;
	}

	/**
	 * @return the border
	 */
	public int getBorder() {
		return border;
	}

	/**
	 * @param border
	 *            the border to set
	 */
	public void setBorder(int border) {
		this.border = border;
	}

	/**
	 * @return the fillStyle
	 */
	public int getFillStyle() {
		return fillStyle;
	}

	/**
	 * @param fillStyle
	 *            the fillStyle to set
	 */
	public void setFillStyle(int fillStyle) {
		this.fillStyle = fillStyle;
	}

	@Override
	public String toString() {
		if (name == null) return "<null>"; //$NON-NLS-1$
		return name;
	}

	public TreeSet<ZoneRoom> getSortedZoneRooms() {
		if (set == null) set = database.getSortedZoneRoomsForZone(this);
		return set;
	}

	private Rectangle2D[]	localShape		= null;
	private Path2D			lineShape		= null;
	private Rectangle2D		rectangleShape	= null;

	public Rectangle2D[] getLocalShapeForZone() {
		if (localShape == null) {
			TreeSet<ZoneRoom> set = getSortedZoneRooms();
			if (set.isEmpty()) return null;
			localShape = new Rectangle2D[set.size()];
			int i = 0;
			for (ZoneRoom zr : set.descendingSet()) {
				Room room = zr.getRoom();
				double x = (double) room.getPosition().x - (double) border;
				double y = (double) room.getPosition().y - (double) border;
				double w = 16d + ((double) 2 * (double) border);
				double h = 16d + ((double) 2 * (double) border);
				localShape[i++] = new Rectangle2D.Double(x, y, w, h);
			}
		}
		return localShape;
	}

	public Path2D getLineShapeForZone() {
		if (lineShape == null) {
			TreeSet<ZoneRoom> set = getSortedZoneRooms();
			if (set == null) return null;
			if (set.isEmpty()) return null;
			lineShape = new Path2D.Double();
			lineShape.setWindingRule(Path2D.WIND_NON_ZERO);
			Room r = set.first().getRoom();

			if (r == null) return null;

			lineShape.moveTo(r.getPosition().x, r.getPosition().y);
			for (ZoneRoom ze : set) {
				Room room = ze.getRoom();
				if (room != null) lineShape.lineTo(room.getPosition().x, room.getPosition().y);
			}
		}
		return lineShape;
	}

	public Path2D getFilledOutlineShapeForZone() {
		lineShape = getLineShapeForZone();
		lineShape.closePath();
		return lineShape;
	}

	public Rectangle2D getExpandShapeForZone() {
		if (rectangleShape == null) {
			TreeSet<ZoneRoom> set = getSortedZoneRooms();
			if (set.isEmpty()) return null;

			int maxX = set.first().getRoom().getPosition().x;
			int maxY = set.first().getRoom().getPosition().y;
			int minX = set.first().getRoom().getPosition().x;
			int minY = set.first().getRoom().getPosition().y;

			for (ZoneRoom zr : getSortedZoneRooms()) {
				int x = zr.getRoom().getPosition().x;
				int y = zr.getRoom().getPosition().y;
				if (maxX < x) maxX = x;
				if (maxY < y) maxY = y;

				if (minX > x) minX = x;
				if (minY > y) minY = y;
			}

			int x = minX - border;
			int y = minY - border;
			int width = (maxX - minX) + (2 * border) + 15;
			int height = (maxY - minY) + (2 * border) + 15;

			rectangleShape = new Rectangle2D.Double(x, y, width, height);
		}
		return rectangleShape;
	}

	/**
	 * 
	 * @param ps
	 *            The PreparedStatement to prepare.
	 * @return The prepared PreparedStatement
	 * @throws SQLException
	 */
	public PreparedStatement prepareInsertForZoneInsert(PreparedStatement ps) throws SQLException {
		// "INSERT INTO Zone(a_id, bgcolour, bgstyle, bgref, border, name, type,
		// paintstyle, description,
		// labelalignment ,labeltext ,labelcolour ,labelfontsize
		// ,borderthickness ,bordercolour ,bordertype ,labelfont,
		// labelfontstyle)) VALUES
		// (?,?,?,?,?,?,?,?,?,?,?)"
		ps.setInt(1, getArea_id());
		ps.setInt(2, getBackGroundColor().getRGB());
		ps.setInt(3, getBackGroundStyle());
		ps.setInt(4, getBackGroundReference());
		ps.setInt(5, getBorder());
		ps.setString(6, (getName() == null ? "" : getName())); //$NON-NLS-1$
		ps.setInt(7, getType());
		ps.setInt(8, getFillStyle());
		ps.setString(9, getDescription());
		ps.setInt(10, (numberOfRepeats * Exit.BASE) + getLabelalignment());
		ps.setString(11, getLabeltext());
		ps.setInt(12, getLabelcolour().getRGB());
		ps.setFloat(13, getLabelfontsize());
		ps.setInt(14, getBorderthickness());
		ps.setInt(15, getBordercolour().getRGB());
		ps.setInt(16, getBordertype());
		ps.setString(17, (getLabelfont().getFamily() == null ? Font.DIALOG : getLabelfont().getFamily()));
		ps.setInt(18, getLabelfont().getStyle());
		return ps;
	}

	/**
	 * 
	 * @param ps
	 *            The PreparedStatement to prepare.
	 * @param room
	 *            The Room to use.
	 * @return The prepared PreparedStatement
	 * @throws SQLException
	 */
	public PreparedStatement prepareInsertForZoneRoomInsert(PreparedStatement ps, Room room) throws SQLException {
		ps.setInt(1, room.getId());
		ps.setInt(2, getZone_id());
		ps.setInt(3, database.getMaxIndexForZoneRoomsInZone(this) + 1);
		return ps;
	}

	/**
	 * 
	 * @param ps
	 *            The PreparedStatement to prepare.
	 * @return The prepared PreparedStatement
	 * @throws SQLException
	 */
	public PreparedStatement prepareUpdateForZone(PreparedStatement ps) throws SQLException {
		// "UPDATE Zones SET a_id=?, bgcolour=?, bgstyle=?, bgref=?, border=?,
		// name=?, type=?, paintstyle=?, description=?,
		// labelalignment=? ,labeltext=?, labelcolour=?, labelfontsize=?, borderthickness=?, bordercolour=?, bordertype=?, labelfont=?, labelfontstyle=? WHERE z_id=?" };																																																								//$NON-NLS-1$
		ps.setInt(1, getArea_id());
		ps.setInt(2, getBackGroundColor().getRGB());
		ps.setInt(3, getBackGroundStyle());
		ps.setInt(4, getBackGroundReference());
		ps.setInt(5, getBorder());
		ps.setString(6, (getName() == null ? "" : getName())); //$NON-NLS-1$
		ps.setInt(7, getType());
		ps.setInt(8, getFillStyle());
		ps.setString(9, getDescription());
		ps.setInt(10, (numberOfRepeats * Exit.BASE) + getLabelalignment());
		ps.setString(11, getLabeltext());
		ps.setInt(12, getLabelcolour().getRGB());
		ps.setFloat(13, getLabelfontsize());
		ps.setInt(14, getBorderthickness());
		ps.setInt(15, getBordercolour().getRGB());
		ps.setInt(16, getBordertype());
		ps.setString(17, (getLabelfont().getFamily() == null ? Font.DIALOG : getLabelfont().getFamily()));
		ps.setInt(18, getLabelfont().getStyle());
		ps.setInt(19, getZone_id());
		return ps;
	}

	public PreparedStatement prepareDeleteZone(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getZone_id());
		return ps;
	}

	public PreparedStatement prepareDeleteZoneRoomsForZone(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getZone_id());
		return ps;
	}

	// "deleteRoomInZoneRooms", "DELETE FROM ZoneRooms WHERE r_id=? AND z_id=?"
	public PreparedStatement prepareDeleteZoneRooms(PreparedStatement ps, Room room) throws SQLException {
		ps.setInt(1, getZone_id());
		return ps;
	}

	public boolean intersects(Rectangle theAreaToShow) {
		if (theAreaToShow == null) return false;
		if (area == null) {
			switch (getFillStyle()) {
				case LINE_SHAPE:
					area = getLineShapeForZone();
					break;
				case EXPAND_SHAPE:
					area = getExpandShapeForZone();
					break;
				case LOCAL_SHAPE:
					java.awt.geom.Area a = new java.awt.geom.Area();
					Rectangle2D[] rects = getLocalShapeForZone();
					if (rects == null) return false;
					for (Rectangle2D rect : rects) {
						a.add(new java.awt.geom.Area(rect));
					}
					area = a;
					break;
				case FILLED_OUTLINE:
					Path2D path = getFilledOutlineShapeForZone();
					if (path != null) area = path.getBounds2D();
					return false;
				default:
					return false;
			}
		}
		if (area == null) return false;
		return area.intersects(theAreaToShow);
	}

	public Point getPosition() {
		if (set == null) set = getSortedZoneRooms();
		return set.first().getRoom().getPosition();
	}

	/**
	 * @return the highlighted
	 */
	public boolean isHighlighted() {
		return highlighted;
	}

	/**
	 * @param highlighted
	 *            the highlighted to set
	 */
	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}

	public float getBorderF() {
		return getBorder();
	}

	public String toXml() {
		String toReturn = tab + "<zone>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<zoneid>" + getZone_id() + "</zoneid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonename>" + getName() + "</zonename>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonedescription>" + getDescription() + "</zonedescription>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonearea>" + getArea_id() + "</zonearea>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonebackgroundstyle>" + getBackGroundStyle() + "</zonebackgroundstyle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonebackgroundref>" + getBackGroundReference() + "</zonebackgroundref>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonebackgroundcolour>" + getBackGroundColor().getRGB() + "</zonebackgroundcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonetype>" + getType() + "</zonetype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneborder>" + getBorder() + "</zoneborder>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonefillstyle>" + getFillStyle() + "</zonefillstyle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabelalignment>" + getLabelalignment() + "</zonelabelalignment>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabeltext>" + getLabeltext() + "</zonelabeltext>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabelcolour>" + getLabelcolour().getRGB() + "</zonelabelcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabelfont>" + getLabelfont().getFontName() + "</zonelabelfont>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zoneborderthickness>" + getBorderthickness() + "</zoneborderthickness>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonebordercolour>" + getBordercolour().getRGB() + "</zonebordercolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonebordertype>" + getBordertype() + "</zonebordertype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabelfontsize>" + getLabelfontsize() + "</zonelabelfontsize>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<zonelabelfontstyle>" + getLabelfontsize() + "</zonelabelfontstyle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "</zone>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	@Override
	public int compareTo(Zone o) {
		Integer o1 = new Integer(getZone_id());
		Integer o2 = new Integer(o.getZone_id());
		return o1.compareTo(o2);
	}

	@Override
	public int compare(Zone o1, Zone o2) {
		return Integer.compare(o1.getZone_id(), o2.getZone_id());
	}

	public int getLevel() {
		if (set == null) set = getSortedZoneRooms();
		if (set.isEmpty()) return 0;
		Room r = set.first().getRoom();
		if (r == null) {
			cleanUpZoneRooms();
			set = getSortedZoneRooms();
			if (set.isEmpty()) return 0;
		}
		return set.first().getRoom().getLevel();
	}

	public void cleanUpZoneRooms() {
		if (set == null) return;
		for (ZoneRoom zr : set) {
			Room r = zr.getRoom();
			if (r == null) database.deleteZoneRoom(zr, this);
		}
		set = database.getSortedZoneRoomsForZone(this);
	}

	public void paint(Graphics2D g, Rectangle theAreaToShow, int currentLevel, MapPanel mapPanel) {
		boolean debug = false;
		if (debug) System.out.println("Zone to get drawn: " + getName() + " - " + getFillStyle()); //$NON-NLS-1$ //$NON-NLS-2$
		Color c = g.getColor();
		Stroke s = g.getStroke();
		Composite composite = g.getComposite();
		if (getLevel() == currentLevel || getLevel() == currentLevel - 1) {
			if (getLevel() == currentLevel - 1) {
				float alphaValue = Float.parseFloat(MapperProperties.getInstance().getProperty(MapperProperties.ALPAVALUE, MapperProperties.ALPAVALUEDEFAULT));
				g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alphaValue));
				g.translate(-5, 5);
			}
			g.setColor(getBackGroundColor());
			switch (getFillStyle()) {
				case Zone.LINE_SHAPE:
					if (debug) System.out.println("Line!"); //$NON-NLS-1$
					drawLineShapeForZone(getLineShapeForZone(), g, getBorder(), isHighlighted(), theAreaToShow, mapPanel, getBordertype() != 0);
					break;
				case Zone.EXPAND_SHAPE:
					if (debug) System.out.println("Expand!"); //$NON-NLS-1$
					drawExpandShapeForZone(getExpandShapeForZone(), g, isHighlighted(), theAreaToShow, mapPanel, getBordertype() != 0);
					break;
				case Zone.LOCAL_SHAPE:
					if (debug) System.out.println("Local!"); //$NON-NLS-1$
					drawLocalShapeForZone(getLocalShapeForZone(), g, isHighlighted(), theAreaToShow, mapPanel, getBordertype() != 0);
					break;
				case Zone.FILLED_OUTLINE:
					if (debug) System.out.println("Filled!"); //$NON-NLS-1$
					drawFilledOutlineShapeForZone(getFilledOutlineShapeForZone(), g, isHighlighted(), theAreaToShow, mapPanel, getBordertype() != 0);
					break;
				default:
					System.out.println("Error! Error! No such FillStyle!"); //$NON-NLS-1$
					break;
			}
			if (getLevel() == currentLevel - 1) {
				g.setComposite(composite);
				g.translate(5, -5);
			}
		}
		g.setStroke(s);
		g.setPaint(c);

	}

	private void drawFilledOutlineShapeForZone(Path2D path, Graphics2D g, boolean highlighted2, Rectangle theAreaToShow, MapPanel mapPanel, boolean haveBorder) {
		int roomwidth = Room.SIDE - 1;
		MovedOutlineStroke mos = new MovedOutlineStroke(border, roomwidth);
		AffineTransform at = g.getTransform();
		at.translate(-theAreaToShow.x, -theAreaToShow.y);
		Path2D toShape = (Path2D) path.clone();
		toShape.transform(at);
		toShape.closePath();
		Shape outline = mos.createStrokedShape(toShape);
		Paint p = createFillerForZone(getBackGroundColor(), getType(), getBackGroundReference(), mapPanel);
		g.setPaint((p == null ? getBackGroundColor() : p));
		g.fill(outline);
		if (haveBorder) {
			Stroke s = g.getStroke();
			Color c = g.getColor();
			g.setColor(getBordercolour());
			g.setStroke(ZoneDialog.getBorderStroke(ZoneDialog.BORDER_TYPE.values()[getBordertype()], getBorderthickness()));
			g.draw(toShape);
			g.setColor(c);
			g.setStroke(s);
		}
		if (highlighted) {
			g.setPaint(createFillerForHighlightZone(getBackGroundColor()));
			g.fill(toShape);
		}
	}

	private void drawLocalShapeForZone(Rectangle2D[] localShapeForZone, Graphics2D g, boolean highlighted, Rectangle theAreaToShow, MapPanel mapPanel,
			boolean haveBorder) {
		for (Rectangle2D rect : localShapeForZone) {
			g.translate(-1, -1);
			drawExpandShapeForZone(rect, g, highlighted, theAreaToShow, mapPanel, haveBorder);
			g.translate(1, 1);
		}
	}

	private void drawExpandShapeForZone(Rectangle2D rect, Graphics2D g, boolean highlighted, Rectangle theAreaToShow, MapPanel mapPanel, boolean haveBorder) {
		Rectangle2D tRec = new Rectangle2D.Double(rect.getX() - theAreaToShow.x, rect.getY() - theAreaToShow.y, rect.getWidth(), rect.getHeight());
		Paint p = createFillerForZone(getBackGroundColor(), getType(), getBackGroundReference(), mapPanel);
		g.translate(1, 1);
		g.setPaint((p == null ? getBackGroundColor() : p));
		g.fill(tRec);
		if (haveBorder) {
			Stroke s = g.getStroke();
			Color c = g.getColor();
			g.setColor(getBordercolour());
			g.setStroke(ZoneDialog.getBorderStroke(ZoneDialog.BORDER_TYPE.values()[getBordertype()], getBorderthickness()));
			g.draw(tRec);
			g.setColor(c);
			g.setStroke(s);
		}
		if (getLabelalignment() != 0) {
			drawLabelForSquare(g, tRec, mapPanel);
		}
		if (highlighted) {
			g.setPaint(createFillerForHighlightZone(getBackGroundColor()));
			g.fill(tRec);
			g.setPaint(getBackGroundColor());
		}
		g.translate(-1, -1);
	}

	private void drawLabelForSquare(Graphics2D g, Rectangle2D tRec, MapPanel mapPanel) {
		int x = (int) tRec.getX(), y = (int) tRec.getY();
		Font fontToWriteWith = new Font("Dialog", getLabelFontStyle(), (int) getLabelfontsize()); //$NON-NLS-1$
		if (getLabelfont() != null) fontToWriteWith = getLabelfont();
		FontMetrics metrics = g.getFontMetrics(fontToWriteWith);
		String toWrite = getLabeltext();
		if (toWrite == null) toWrite = getName();
		if (toWrite.trim().isEmpty()) toWrite = getName();
		int widthOfFont = metrics.stringWidth(toWrite);
		int heightOfFont = metrics.getHeight();
		g.setFont(fontToWriteWith);
		Color labelColor = getLabelcolour();
		if (labelColor != null) {
			g.setColor(labelColor);
		} else {
			g.setColor(Color.RED);
		}
		int margin = 4;
		switch (getLabelalignment()) {
			case 0:
				break;
			case 1: // CENTER
				y += (int) (tRec.getHeight() / 2 + heightOfFont / 4);
				x += (int) (tRec.getWidth() / 2 - widthOfFont / 2);
				break;
			case 2: // NORTH
				y += heightOfFont / 2 + margin;
				x += (int) (tRec.getWidth() / 2 - widthOfFont / 2);
				break;
			case 3:
				y += heightOfFont / 2 + margin;
				x += (int) (tRec.getWidth() - widthOfFont) - margin;
				break;
			case 4: // EAST
				y += (int) (tRec.getHeight() / 2 + heightOfFont / 4);
				x += (int) (tRec.getWidth() - widthOfFont) - margin;
				break;
			case 5: // SOUTHEAST
				y += (int) (tRec.getHeight() - margin);
				x += (int) (tRec.getWidth() - widthOfFont) - margin;
				break;
			case 6: // SOUTH
				y += (int) (tRec.getHeight() - margin);
				x += (int) (tRec.getWidth() / 2 - widthOfFont / 2);
				break;
			case 7: // SOUTHWEST
				y += (int) (tRec.getHeight() - margin);
				x += margin;
				break;
			case 8: // WEST
				y += (int) (tRec.getHeight() / 2 + heightOfFont / 4);
				x += margin;
				break;
			case 9: // NORTHWEST
				x += margin;
				y += heightOfFont / 2 + margin;
				break;
		}
		mapPanel.registerZoneSquareLabel(x, y, toWrite, g.getColor(), fontToWriteWith);
		// g.drawString(toWrite, x, y);
	}

	private void drawLineShapeForZone(Path2D path, Graphics2D g, int border, boolean highlighted, Rectangle theAreaToShow, MapPanel mapPanel, boolean haveBorder) {
		float roomwidth = Room.SIDE - 1f;
		float width = (border * 2f) + roomwidth;
		BasicStroke wideStroke = new BasicStroke(width, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		AffineTransform at = g.getTransform();
		at.translate(-theAreaToShow.x, -theAreaToShow.y);
		Path2D toShape = (Path2D) path.clone();
		toShape.transform(at);
		Shape outline = wideStroke.createStrokedShape(toShape);
		double tran = (double) roomwidth / 2 + 1;
		g.translate(tran, tran);
		Paint p = createFillerForZone(getBackGroundColor(), getType(), getBackGroundReference(), mapPanel);
		g.setPaint((p == null ? getBackGroundColor() : p));
		g.fill(outline);
		if (haveBorder) {
			Stroke s = g.getStroke();
			Color c = g.getColor();
			g.setColor(getBordercolour());
			g.setStroke(ZoneDialog.getBorderStroke(ZoneDialog.BORDER_TYPE.values()[getBordertype()], getBorderthickness()));
			g.draw(outline);
			g.setColor(c);
			g.setStroke(s);
		}

		if (getLabelalignment() != 0) {
			String label = getLabeltext();
			if (label == null) label = getName();
			if (label.trim().isEmpty()) label = getName();
			TextStroke ts = new TextStroke(label, getLabelfont(), false, false, true, getNumberOfRepeats());
			mapPanel.registerZoneLineLabel(ts, toShape, getLabelcolour());
		}
		if (highlighted) {
			g.setPaint(createFillerForHighlightZone(getBackGroundColor()));
			g.fill(outline);
		}
		g.translate(-tran, -tran);
	}

	private Paint createFillerForZone(Color backGroundColor, int type, int backGroundReference, MapPanel mapPanel) {
		switch (type) {
			case Zone.FILL_TYPE:
				return backGroundColor;
			case Zone.GRAPHIC_TYPE:
				if (backGroundReference == 0) return backGroundColor;
				RoomGraphic rg = RoomGraphicsManager.getInstance().getShape(backGroundReference);
				if (rg == null) return backGroundColor;
				if (rg.getBounds().getWidth() == 0 || rg.getBounds().getHeight() == 0) return backGroundColor;
				BufferedImage bi = new BufferedImage((int) rg.getBounds().getWidth(), (int) rg.getBounds().getHeight(), BufferedImage.TYPE_INT_RGB);
				Graphics2D g = bi.createGraphics();
				for (MSShape shape : rg.getShapes()) {
					g.setColor(shape.getColour());
					if (shape.isFilled()) {
						g.fill(shape);
					} else {
						g.draw(shape);
					}
				}
				return new TexturePaint(bi, rg.getBounds());
			case Zone.ICON_TYPE:
				if (backGroundReference == 0) return backGroundColor;
				MapperIcon mi = database.getIcon(backGroundReference);
				if (mi == null) return backGroundColor;
				BufferedImage image = new BufferedImage(mi.getImage().getWidth(mapPanel), mi.getImage().getHeight(mapPanel), BufferedImage.TYPE_INT_RGB);
				Graphics2D g2 = image.createGraphics();
				g2.drawImage(mi.getImage(), 0, 0, mapPanel);
				return new TexturePaint(image, new Rectangle2D.Double(0, 0, mi.getImage().getWidth(mapPanel), mi.getImage().getHeight(mapPanel)));
			default:
				return backGroundColor;
		}
	}

	TexturePaint createFillerForHighlightZone(Color background) {
		BufferedImage bi = new BufferedImage(20, 20, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		g.setPaint(background);
		g.fillRect(0, 0, bi.getWidth(), bi.getHeight());
		g.setPaint(Color.CYAN);
		g.setStroke(new BasicStroke(2.0f));
		g.drawLine(0, 0, bi.getWidth(), bi.getHeight());
		g.drawLine(0, bi.getHeight(), bi.getWidth(), 0);

		return new TexturePaint(bi, new Rectangle2D.Double(0, 0, 10, 10));
	}

	/**
	 * @return the labelalignment
	 */
	public int getLabelalignment() {
		return labelalignment;
	}

	/**
	 * @param labelalignment
	 *            the labelalignment to set
	 */
	public void setLabelalignment(int labelalignment) {
		this.labelalignment = labelalignment;
	}

	/**
	 * @return the labeltext
	 */
	public String getLabeltext() {
		if (labeltext == null) return ""; //$NON-NLS-1$
		return labeltext;
	}

	/**
	 * @param labeltext
	 *            the labeltext to set
	 */
	public void setLabeltext(String labeltext) {
		this.labeltext = labeltext;
	}

	/**
	 * @return the labelcolour
	 */
	public Color getLabelcolour() {
		return labelcolour;
	}

	/**
	 * @param labelcolour
	 *            the labelcolour to set
	 */
	public void setLabelcolour(Color labelcolour) {
		this.labelcolour = labelcolour;
	}

	/**
	 * @return the labelfont
	 */
	public Font getLabelfont() {
		if (labelfont == null) labelfont = Font.DIALOG;
		if (labelfont.trim().isEmpty()) labelfont = Font.DIALOG;
		Font toReturn = new Font(labelfont, labelfontstyle, (int) labelfontsize);
		return toReturn;
	}

	/**
	 * @param labelfont
	 *            the labelfont to set
	 */
	public void setLabelfont(String labelfont) {
		this.labelfont = labelfont;
	}

	/**
	 * @return the borderthickness
	 */
	public int getBorderthickness() {
		return borderthickness;
	}

	/**
	 * @param borderthickness
	 *            the borderthickness to set
	 */
	public void setBorderthickness(int borderthickness) {
		this.borderthickness = borderthickness;
	}

	/**
	 * @return the bordercolour
	 */
	public Color getBordercolour() {
		return bordercolour;
	}

	/**
	 * @param bordercolour
	 *            the bordercolour to set
	 */
	public void setBordercolour(Color bordercolour) {
		this.bordercolour = bordercolour;
	}

	/**
	 * @return the bordertype
	 */
	public int getBordertype() {
		return bordertype;
	}

	/**
	 * @param bordertype
	 *            the bordertype to set
	 */
	public void setBordertype(int bordertype) {
		this.bordertype = bordertype;
	}

	/**
	 * @return the labelfontsize
	 */
	public float getLabelfontsize() {
		return labelfontsize;
	}

	/**
	 * @param labelfontsize
	 *            the labelfontsize to set
	 */
	public void setLabelfontsize(float labelfontsize) {
		this.labelfontsize = labelfontsize;
	}

	public void setLabelFontStyle(int labelfontstyle) {
		this.labelfontstyle = labelfontstyle;
	}

	public int getLabelFontStyle() {
		return labelfontstyle;
	}

	/**
	 * @return the numberOfRepeats
	 */
	public int getNumberOfRepeats() {
		return numberOfRepeats + 1;
	}

	/**
	 * @param numberOfRepeats
	 *            the numberOfRepeats to set
	 */
	public void setNumberOfRepeats(int numberOfRepeats) {
		this.numberOfRepeats = numberOfRepeats - 1;
	}

	public Rectangle2D getShapeBoundariesForZone() {
		switch (getFillStyle()) {
			case EXPAND_SHAPE:
				return getExpandShapeForZone();
			case LINE_SHAPE:
				Path2D path1 = getLineShapeForZone();
				if (path1 != null) return path1.getBounds2D();
				return null;
			case LOCAL_SHAPE:
				Rectangle2D[] rects = getLocalShapeForZone();
				if (rects.length <= 0) return null;
				java.awt.geom.Area area = new java.awt.geom.Area();
				for (Rectangle2D rect : rects) {
					area.add(new java.awt.geom.Area(rect));
				}
				return area.getBounds2D();
			case FILLED_OUTLINE:
				Path2D path = getFilledOutlineShapeForZone();
				if (path != null) return path.getBounds2D();
				return null;
			default:
				return null;
		}
	}

}
