/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;

import javax.swing.JComponent;

import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.custom.RoomRep;

/**
 * 
 * @author Ecthelion
 * 
 */
public class Room extends JComponent implements Comparable<Room>, Comparator<Room> {// JComponent
	// {
	private static Color		defaultColour		= null;
	private static Color		currentRoomColour	= null;
	public static int			NONE				= 0;
	/**
     * 
     */
	private static final long	serialVersionUID	= 5190071374459912663L;
	public static int			STALL				= 2;

	public static int			TENT				= 1;
	private static Color		currentShadowColour	= null;
	private String				alias				= "";					//$NON-NLS-1$
	private int					areaId				= 0;
	private Color				colour				= Color.LIGHT_GRAY;
	private boolean				currentRoom			= true;
	String						dtab				= "\t\t";				//$NON-NLS-1$
	private final String		eol					= "\n";				//$NON-NLS-1$
	private boolean				highlighted			= false;
	private int					level				= 0;
	Point						location;
	private String				name				= "", description = ""; //$NON-NLS-1$ //$NON-NLS-2$
	private boolean				normalized			= false;
	private String				notes				= "";					//$NON-NLS-1$
	private String				notification		= "";					//$NON-NLS-1$
	private Point				position;

	private int					r_id				= 0;
	private boolean				shadowRoom			= true;

	String						tab					= "\t";				//$NON-NLS-1$

	private int					type				= 0;
	public final static int		SIDE				= 16;

	public Room() {
		super();
		position = new Point();
		setSize(SIDE, SIDE);
		setBackground(getDefaultRoomColour());
		setForeground(Color.WHITE);
		colour = getDefaultRoomColour();
	}

	public Room(int r_id, Point position, int level, String name, String description, int areaId, String alias) {
		this(position, level, name, description, areaId, alias);
		this.r_id = r_id;
	}

	public Room(Point position, int level, String name, String description, int areaId, String alias) {
		this();
		this.position = position;
		this.name = name;
		this.description = description;
		this.level = level;
		this.areaId = areaId;
		this.alias = alias;
		setToolTipText(getName());
	}

	/*
	 * r_id int NOT NULL GENERATED ALWAYS AS IDENTITY, Name varchar(200),
	 * Description varchar(1000), Colour int, Level int, xPos int, yPos int,
	 * Area int, Type int DEFAULT 0, Alias varchar(30)
	 */
	public Room(ResultSet rs) throws SQLException {
		this();
		this.r_id = rs.getInt("r_id"); //$NON-NLS-1$
		this.level = rs.getInt("Level"); //$NON-NLS-1$
		String n = rs.getString("Name"); //$NON-NLS-1$
		this.name = (n == null ? "" : n); //$NON-NLS-1$
		this.description = rs.getString("Description"); //$NON-NLS-1$
		this.colour = new Color(rs.getInt("Colour")); //$NON-NLS-1$
		this.position = new Point(rs.getInt("xPos"), rs.getInt("yPos")); //$NON-NLS-1$ //$NON-NLS-2$
		this.areaId = rs.getInt("Area"); //$NON-NLS-1$
		this.type = rs.getInt("Type"); //$NON-NLS-1$
		this.alias = rs.getString("Alias"); //$NON-NLS-1$
		this.notes = rs.getString("Notes"); //$NON-NLS-1$
		setToolTipText(getName());
	}

	public String getAlias() {
		return alias;
	}

	/**
	 * Getting current AreaId;
	 * 
	 * @return
	 */
	public int getAreaId() {
		return areaId;
	}

	public Color getColour() {
		return colour;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return r_id;
	}

	public int getLevel() {
		return level;
	}

	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @return the notification
	 */
	public String getNotification() {
		return notification;
	}

	public Point getPosition() {
		return position;
	}

	@Override
	public String getToolTipText() {
		String toReturn = "<html>Name: " + name; //$NON-NLS-1$
		if (!getAlias().trim().isEmpty()) {
			toReturn += "<br>Alias: " + getAlias() + "</br>"; //$NON-NLS-1$ //$NON-NLS-2$
		}

		if (!getNotification().trim().isEmpty()) {
			toReturn += "<br>Notification: " + getNotification() + "</br>"; //$NON-NLS-1$ //$NON-NLS-2$
		}
		toReturn.concat("</html>"); //$NON-NLS-1$

		return toReturn;
	}

	public int getType() {
		return type;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	public boolean isCurrentRoom() {
		return currentRoom;
	}

	public boolean isHighLighted() {
		return highlighted;
	}

	/**
	 * @return the normalized
	 */
	public boolean isNormalized() {
		return normalized;
	}

	public boolean isShadowRoom() {
		return shadowRoom;
	}

	@Override
	public void paint(Graphics graphics1) {
		Graphics2D graphics = (Graphics2D) graphics1;
		colour = getColour();
		if (getColour() == null) colour = Room.defaultColour;
		graphics.setColor(colour);
		RoomRep.paintRoomRep(this, graphics);
		RoomRep.paintRoomGraphics(this, graphics);
		if (isCurrentRoom()) {
			graphics.setColor(Room.getCurrentRoomColour());
			if (RoomRep.getCurrentType(getAreaId()) == RoomRep.TYPE.THREEDEESQUARE) {
				graphics.fillOval(3, 3, getWidth() - 5, getHeight() - 5);
			} else {
				graphics.fillOval(3, 3, getWidth() - 6, getHeight() - 6);
			}
		}
		if (isShadowRoom() && !isCurrentRoom()) {
			graphics.setColor(Room.getCurrentShadowColour());
			graphics.fillRect(4, 4, getWidth() - 6, getHeight() - 6);
		}

		if (!getNotification().isEmpty()) {
			graphics.setColor(Color.RED);
			graphics.drawString("#", getWidth() / 3, getHeight() - 2); //$NON-NLS-1$
		}
	}

	public PreparedStatement prepareGetExitsForRoom(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setInt(2, getId());
		ps.setInt(3, getAreaId());
		ps.setInt(4, getId());
		ps.setInt(5, getAreaId());
		ps.setInt(6, getId());
		return ps;
	}

	public PreparedStatement prepareInsertStatements(PreparedStatement ps) throws SQLException {
		ps.setString(1, getName());
		ps.setString(2, getDescription());
		ps.setInt(3, getColour().getRGB());
		ps.setInt(4, getLevel());
		int x = (int) getPosition().getX();
		int y = (int) getPosition().getY();
		ps.setInt(5, x);
		ps.setInt(6, y);
		ps.setInt(7, getAreaId());
		ps.setInt(8, getType());
		ps.setString(9, getAlias());
		ps.setString(10, getNotes());
		return ps;
	}

	public PreparedStatement prepareUpdateStatement(PreparedStatement ps) throws SQLException {
		ps.setString(1, getName());
		ps.setString(2, getDescription());
		ps.setInt(3, getColour().getRGB());
		ps.setInt(4, getLevel());
		int x = (int) getPosition().getX();
		int y = (int) getPosition().getY();

		ps.setInt(5, x);
		ps.setInt(6, y);
		ps.setInt(7, getAreaId());
		ps.setInt(8, getType());
		ps.setString(9, getAlias());
		ps.setString(10, getNotes());
		ps.setInt(11, getId());
		return ps;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Setting AreaId.
	 * 
	 * @param areaId
	 */
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	// public void paint(Graphics g) {
	// super.paint(g);
	/*
	 * g.setColor(colour); g.fill3DRect(1, 1, getWidth() - 1, getHeight() - 1,
	 * true); if (currentRoom) { g.setColor(Color.BLUE); g.fillOval(3, 3,
	 * getWidth() - 5, getHeight() - 5); } if (highlighted) {
	 * g.setColor(Color.CYAN); g.drawRect(0, 0, getWidth(), getHeight()); } if
	 * (shadowRoom && !currentRoom) { g.setColor(Color.GRAY); g.fillRect(4, 4,
	 * getWidth() - 6, getHeight() - 6); }
	 */
	// }

	public void setColour(Color colour) {
		this.colour = colour;
	}

	public void setCurrentRoom(boolean currentRoom) {
		this.currentRoom = currentRoom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}

	public void setId(int r_id) {
		this.r_id = r_id;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param normalized
	 *            the normalized to set
	 */
	public void setNormalized(boolean normalized) {
		this.normalized = normalized;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @param notification
	 *            the notification to set
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public void setShadowRoom(boolean shadowRoom) {
		this.shadowRoom = shadowRoom;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setXPos(double xPos) {
		getLocation().setLocation(xPos, getLocation().getY());
	}

	public void setXPosition(int xPos) {
		position = new Point(xPos, (int) position.getY());
	}

	public void setYPos(double yPos) {
		getLocation().setLocation(getLocation().getX(), yPos);
	}

	public void setYPosition(int yPos) {
		position = new Point((int) position.getX(), yPos);
	}

	@Override
	public String toString() {
		return r_id + " - " + name + " - " + description; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public String[] toTableRow() {
		String[] toReturn = { Integer.toString(getId()), getName(), getDescription() };
		return toReturn;
	}

	/**
	 * Creates xml from the instance.
	 * 
	 * @return xml version of the instance of an Area.
	 */
	public String toXml() {
		String toReturn = tab + "<room>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<roomid>" + getId() + "</roomid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomname>" + cleanOutSpecialCharacters(getName()) + "</roomname>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomdescription>" + cleanOutSpecialCharacters(getDescription()) + "</roomdescription>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomcolour>" + getColour().getRGB() + "</roomcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomlevel>" + getLevel() + "</roomlevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomxPos>" + (int) getPosition().getX() + "</roomxPos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomyPos>" + (int) getPosition().getY() + "</roomyPos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomareaforroom>" + getAreaId() + "</roomareaforroom>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomtype>" + getType() + "</roomtype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<roomalias>" + getAlias() + "</roomalias>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += tab + "</room>" + eol; //$NON-NLS-1$

		return toReturn;
	}

	private String cleanOutSpecialCharacters(String toChange) {
		toChange = toChange.replaceAll("&", "and"); //$NON-NLS-1$ //$NON-NLS-2$
		toChange = toChange.replaceAll("<|>", "*"); //$NON-NLS-1$ //$NON-NLS-2$
		return toChange;
	}

	/**
	 * r_id, Name,Description, Colour, Level, xPos, yPos, Area, type, alias,
	 * Notes
	 * 
	 * @param ps
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatementForRestore(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setString(2, getName());
		ps.setString(3, getDescription());
		ps.setInt(4, getColour().getRGB());
		ps.setInt(5, getLevel());
		int x = (int) getPosition().getX();
		int y = (int) getPosition().getY();

		ps.setInt(6, x);
		ps.setInt(7, y);
		ps.setInt(8, getAreaId());
		ps.setInt(9, getType());
		ps.setString(10, getAlias());
		ps.setString(11, getNotes());
		return ps;
	}

	public static void setDefaultRoomColour(Color colour) {
		defaultColour = colour;
	}

	public static Color getDefaultRoomColour() {
		String colour = MapperProperties.getInstance().getProperty(MapperProperties.DEFAULTROOMCOLOUR);
		if (colour == null) {
			defaultColour = Color.LIGHT_GRAY;
			MapperProperties.getInstance().setProperty(MapperProperties.DEFAULTROOMCOLOUR, Integer.toString(defaultColour.getRGB()));
		} else {
			defaultColour = new Color(Integer.parseInt(colour));
		}

		return defaultColour;
	}

	/**
	 * @param currentRoomColour
	 *            the currentRoomColour to set
	 */
	public static void setCurrentRoomColour(Color currentRoomColour) {
		Room.currentRoomColour = currentRoomColour;
	}

	public static Color getCurrentRoomColour() {
		String colour = MapperProperties.getInstance().getProperty(MapperProperties.CURRENTROOMCOLOUR);
		if (colour == null) {
			currentRoomColour = Color.BLUE;
			MapperProperties.getInstance().setProperty(MapperProperties.CURRENTROOMCOLOUR, Integer.toString(currentRoomColour.getRGB()));
		} else {
			currentRoomColour = new Color(Integer.parseInt(colour));
		}
		return currentRoomColour;
	}

	public static void setCurrentShadowColour(Color currentShadowColour) {
		Room.currentShadowColour = currentShadowColour;
	}

	public static Color getCurrentShadowColour() {
		String colour = MapperProperties.getInstance().getProperty(MapperProperties.CURRENTSHADOWCOLOUR);
		if (colour == null) {
			currentShadowColour = Color.GRAY;
			MapperProperties.getInstance().setProperty(MapperProperties.CURRENTSHADOWCOLOUR, Integer.toString(currentShadowColour.getRGB()));
		} else {
			currentShadowColour = new Color(Integer.parseInt(colour));
		}
		return currentShadowColour;
	}

	@Override
	public int compareTo(Room o2) {
		Integer i1 = new Integer(getId());
		Integer i2 = new Integer(o2.getId());
		return i1.compareTo(i2);
	}

	@Override
	public int compare(Room o1, Room o2) {
		Integer i1 = new Integer(getId());
		Integer i2 = new Integer(o2.getId());
		return Integer.compare(i1, i2);

	}

	@Override
	public boolean equals(Object o2) {
		if (o2 == null) return false;
		if (o2 instanceof Room) {
			return (getId() == ((Room) o2).getId());
		} else {
			return false;
		}
	}

}
