/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.AreaBorderDialog;

/**
 * 
 * @author Ecthelion
 * 
 */
public class AreaPanel extends JComponent {// JComponent {
	/**
     * 
     */
	private static final long	serialVersionUID	= -3035841079490952217L;
	Area						area;
	JLabel						iconLabel;
	ImageIcon					theIcon;
	Triangle					shape				= null;

	// private final Mapper mapper;

	/**
     * 
     */
	public AreaPanel(Mapper mapper, Area area) {
		this.area = area;
		// this.mapper = mapper;
		Dimension dimension = new Dimension(area.getWidth(), area.getHeight());
		setSize(dimension);
		setPreferredSize(dimension);
		setBorder();
		setDoubleBuffered(false);
		setToolTipText(area.getName());
		if (area.getType() == Area.ICON || area.getType() == Area.THUMBNAIL) {
			setBackground(area.getColour());
			iconLabel = new JLabel();
			iconLabel.setSize(new Dimension(area.getWidth(), area.getHeight()));
			iconLabel.setPreferredSize(new Dimension(area.getWidth(), area.getHeight()));
			iconLabel.setHorizontalAlignment(JLabel.CENTER);
			float multipel = (area.getBorderTitle() ? 0.75f : 1.0f);
			if (area.getIcon() != 0) {
				theIcon = new ImageIcon(Database.getInstance().getIcon(area.getIcon()).getImage().getScaledInstance((int) (area.getWidth() * multipel),
																													(int) (area.getHeight() * multipel),
																													Image.SCALE_SMOOTH));
				// iconLabel.setIcon(icon);
			}
			setLayout(new BorderLayout());
			// add(iconLabel, BorderLayout.CENTER);
		} else {
			if (area.getType() == Area.TRIANGLE) {
				shape = new Triangle(false, this);
			}

			if (area.getType() == Area.REVERSE_TRIANGLE) {
				shape = new Triangle(true, this);
			}
			setOpaque(false);
		}
	}

	public Area getArea() {
		return area;
	}

	/*
	 * public void paint(Graphics g) { super.paint(g); }
	 */

	/*
	 * public static final int NONE = 0; public static final int SQUARE = 1;
	 * public static final int ROUNDED_SQUARE = 2; public static final int OVAL
	 * = 3; public static final int TRIANGLE = 4; public static final int ICON =
	 * 5;
	 */
	@Override
	public void paint(Graphics g0) {
		Graphics2D g = (Graphics2D) g0;
		super.paintComponent(g);
		//System.out.println("Trying to paint AreaPanel: " + area.getName()); //$NON-NLS-1$
		if (getBorder() != null) {
			paintBorder(g);
		}
		if (area.getColour() != null) {
			g.setColor(area.getColour());
		} else {
			g.setColor(Color.RED);
		}
		switch (area.getType()) {
			case Area.NONE:
				break;
			case Area.SQUARE:
				g.fill3DRect(0, 0, getWidth(), getHeight(), true);
				break;
			case Area.ROUNDED_SQUARE:
				g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
				break;
			case Area.OVAL:
				g.fillOval(0, 0, getWidth(), getHeight());
				break;
			case Area.TRIANGLE:
			case Area.REVERSE_TRIANGLE:
				g.fillPolygon(shape);
				break;
			case Area.ICON:
				g.drawImage(theIcon.getImage(), 0, 0, (ImageObserver) null);
			case Area.THUMBNAIL:
				break;
			case Area.DIAMOND:
				double width = getWidth();
				double height = getHeight();
				g.rotate(Math.toRadians(45), width / 2, height / 2);
				g.fillRect(0, 0, (int) width, (int) height);
				g.rotate(Math.toRadians(-45), width / 2, height / 2);
				break;
		}

		if (area.getLabelPosition() != 0) {
			int x = 0, y = 0;
			Font fontToWriteWith = new Font("Dialog", area.getLabelFontStyle(), (int) area.getLabelFontSize()); //$NON-NLS-1$
			if (area.getFont() != null) fontToWriteWith = area.getFont();
			FontMetrics metrics = g.getFontMetrics(fontToWriteWith);
			String toWrite = area.getLabel();
			if (toWrite.trim().isEmpty()) toWrite = area.getName();
			int widthOfFont = metrics.stringWidth(toWrite);
			int heightOfFont = metrics.getHeight();
			g.setFont(fontToWriteWith);
			Color labelColor = area.getLabelColour();
			if (labelColor != null) {
				g.setColor(labelColor);
			} else {
				g.setColor(Color.RED);
			}
			switch (area.getLabelPosition()) {
				case 0:
					break;
				case Exit.NORTH: // NORTH
					y = heightOfFont / 2 + 2;
					x = getWidth() / 2 - widthOfFont / 2;
					break;
				case Exit.NORTHEAST:
					y = heightOfFont / 2 + 2;
					x = getWidth() - widthOfFont;
					break;
				case Exit.EAST: // EAST
					y = getHeight() / 2 + heightOfFont / 4;
					x = getWidth() - widthOfFont;
					break;
				case Exit.SOUTHEAST: // SOUTHEAST
					y = getHeight() - 2;
					x = getWidth() - widthOfFont;
					break;
				case Exit.SOUTH: // SOUTH
					y = getHeight() - 2;
					x = getWidth() / 2 - widthOfFont / 2;
					break;
				case Exit.SOUTHWEST: // SOUTHWEST
					y = getHeight() - 2;
					x = 2;
					break;
				case Exit.WEST: // WEST
					y = getHeight() / 2 + heightOfFont / 4;
					x = 2;
					break;
				case Exit.NORTHWEST: // NORTHWEST
					x = 2;
					y = heightOfFont / 2 + 2;
					break;
				case 9: // CENTER
					y = getHeight() / 2 + heightOfFont / 4;
					x = getWidth() / 2 - widthOfFont / 2;
					break;
			}
			g.drawString(toWrite, x, y);
		}
	}

	protected void setBorder() {
		setBorder(AreaBorderDialog.createBorder(area));
	}
}

class Triangle extends Polygon {
	/**
     * 
     */
	private static final long	serialVersionUID	= -7299535684389626582L;

	public Triangle(boolean reversed, AreaPanel rect) {
		if (reversed) {
			addPoint(0, 0);
			addPoint(rect.getWidth(), 0);
			addPoint(rect.getWidth() / 2, rect.getHeight());
			addPoint(0, 0);
		} else {
			addPoint(0, rect.getHeight());
			addPoint(rect.getWidth(), rect.getHeight());
			addPoint(rect.getWidth() / 2, 0);
			addPoint(0, rect.getHeight());
		}

	}
}
