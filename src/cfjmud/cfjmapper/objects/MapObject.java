/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
/**
 * 
 */
package cfjmud.cfjmapper.objects;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.panels.MapPanel;

/**
 * @author Ecthelion
 * 
 */
public class MapObject extends JComponent {

	/**
     * 
     */
	private static final long	serialVersionUID	= 7833837129365824857L;

	private int					areaId				= 0;
	private Database			database;
	String						dtab				= "\t\t";					//$NON-NLS-1$
	String						eol					= "\n";					//$NON-NLS-1$
	private float				fontSize;
	private int					fontStyle;

	private int					iconId				= 0;
	private int					id;
	private String				label				= Messages.getString("");	//$NON-NLS-1$

	private int					labelPosition		= 0;
	private int					level;
	private int					textAngle;
	private Color				textColour			= Color.BLACK;
	private boolean				normalized			= false;
	private Point				position;
	String						tab					= "\t";					//$NON-NLS-1$
	private Font				theFont;

	private ImageIcon			theIcon;

	private int					width, height;
	private boolean				locked				= false;

	/**
	 * To be used when importing XML
	 */
	public MapObject() {
		position = new Point();
	}

	public MapObject(int xPos, int yPos, int iconId, String label, int width, int height, int labelPosition, int areaId, int level) {
		database = Database.getInstance();
		this.position = new Point(xPos, yPos);
		this.iconId = iconId;
		this.areaId = areaId;
		this.width = width;
		this.height = height;
		MapperIcon icon = database.getIcon(iconId);
		if (icon != null) theIcon = icon.getIconImage();
		this.theFont = new Font("Dialog", Font.PLAIN, 12); //$NON-NLS-1$
		this.fontSize = 12.0f;
		this.fontStyle = Font.BOLD;
		this.label = label;
		this.level = level;
		this.setLocked(false);
		setSize(width, height);
		this.setLabelPosition(labelPosition);
	}

	public MapObject(Point point, int areaId, int level) {
		this((int) point.getX(), (int) point.getY(), 0, "", 100, 100, 0, areaId, level); //$NON-NLS-1$
	}

	/**
	 * o_id, Icon, Description, xPos, yPos, width, height, labelposition,
	 * background
	 * 
	 * @param rs
	 * @throws SQLException
	 */

	public MapObject(ResultSet rs) throws SQLException {
		if (database == null) database = Database.getInstance();
		this.position = new Point(rs.getInt("xPos"), rs.getInt("yPos")); //$NON-NLS-1$ //$NON-NLS-2$
		this.id = rs.getInt("o_id"); //$NON-NLS-1$
		this.iconId = rs.getInt("Icon"); //$NON-NLS-1$
		this.label = rs.getString("Description"); //$NON-NLS-1$
		this.labelPosition = rs.getInt("labelposition"); //$NON-NLS-1$
		this.setAreaId(rs.getInt("area")); //$NON-NLS-1$
		String fontName = rs.getString("font"); //$NON-NLS-1$
		this.fontSize = rs.getFloat("textsize"); //$NON-NLS-1$
		this.fontStyle = rs.getInt("textstyle"); //$NON-NLS-1$
		this.theFont = Font.decode(fontName);
		this.theFont = theFont.deriveFont(fontStyle, fontSize);
		int width = rs.getInt("width"); //$NON-NLS-1$
		int height = rs.getInt("height"); //$NON-NLS-1$
		this.setTextAngle(rs.getInt("textangle")); //$NON-NLS-1$
		this.setTextColour(new Color(rs.getInt("textcolour"))); //$NON-NLS-1$
		this.setLevel(rs.getInt("level")); //$NON-NLS-1$
		this.setLocked(rs.getInt("locked") != 0); //$NON-NLS-1$
		setSize(width, height);
		setBackground(new Color(rs.getInt("background"))); //$NON-NLS-1$
		MapperIcon icon = database.getIcon(iconId);
		if (icon != null) {
			theIcon = new ImageIcon(icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
		}
	}

	/**
	 * @return the areaId
	 */
	public int getAreaId() {
		return areaId;
	}

	@Override
	public Font getFont() {
		return theFont;
	}

	public float getFontSize() {
		return fontSize;
	}

	public int getFontStyle() {
		return fontStyle;
	}

	/**
	 * 
	 * @return
	 */
	public int getIconId() {
		return iconId;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the labelPosition
	 */
	public int getLabelPosition() {
		return labelPosition;
	}

	public int getLevel() {
		return level;
	}

	public Point getPosition() {
		return position;
	}

	/**
	 * @return the theFont
	 */
	public Font getTheFont() {
		return theFont;
	}

	/**
	 * 
	 * @return
	 */
	public int getxPos() {
		return (int) position.getX();
	}

	/**
	 * 
	 * @return
	 */
	public int getyPos() {
		return (int) position.getY();
	}

	@Override
	public int hashCode() {
		return getId();
	}

	/**
	 * @return the normalized
	 */
	public boolean isNormalized() {
		return normalized;
	}

	@Override
	public void paint(Graphics g1) {
		super.paint(g1);
		Graphics2D g = (Graphics2D) g1;
		int x = 0, y = 0;
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		Font fontToWriteWith = new Font("Dialog", Font.BOLD, 12); //$NON-NLS-1$
		if (theIcon != null) g.drawImage(theIcon.getImage(), 0, 0, this);
		if (theFont != null) fontToWriteWith = theFont;
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		AffineTransform fontAT = new AffineTransform();
		double theta = getTextAngle() * java.lang.Math.PI / 180;
		fontAT.rotate(theta);
		Font theDerivedFont = fontToWriteWith.deriveFont(fontAT);
		FontMetrics metrics = g.getFontMetrics(fontToWriteWith);
		String toWrite = getLabel();
		int widthOfFont = SwingUtilities.computeStringWidth(metrics, toWrite);
		int heightOfFont = metrics.getHeight();
		g.setFont(theDerivedFont);
		g.setColor(getTextColour());

		switch (getLabelPosition()) {
			case Exit.NORTH: // NORTH
				y = heightOfFont;
				x = getWidth() / 2 - widthOfFont / 2;
				break;
			case Exit.NORTHEAST:
				y = heightOfFont;
				x = getWidth() - widthOfFont;
				break;
			case Exit.EAST: // EAST
				y = getHeight() / 2;
				x = getWidth() - widthOfFont;
				break;
			case Exit.SOUTHEAST: // SOUTHEAST
				y = getHeight() - 1;
				x = getWidth() - widthOfFont;
				break;
			case Exit.SOUTH: // SOUTH
				y = getHeight() - 1;
				x = getWidth() / 2 - widthOfFont / 2;
				break;
			case Exit.SOUTHWEST: // SOUTHWEST
				y = getHeight() - 1;
				x = -1;
				break;
			case Exit.WEST: // WEST
				y = getHeight() / 2;
				x = 1;
				break;
			case Exit.NORTHWEST: // NORTHWEST
				x = -1;
				y = heightOfFont;
				break;
			case 0:
			default:
				y = getHeight() / 2 + heightOfFont / 4;
				x = getWidth() / 2 - widthOfFont / 2;
				break;
		}

		int angleus = getTextAngle();
		double halfWidth_1 = (((double) widthOfFont) / 2);
		double halfWidth_2 = ((widthOfFont) / 2);
		y = (int) (y - halfWidth_1 * Math.sin(Math.toRadians(angleus)));
		if (angleus >= 0 && angleus <= 90) {
			x += (int) (halfWidth_1 - (int) (halfWidth_2 * Math.sin(Math.toRadians(angleus - 270))));
		} else if (angleus > 90 && angleus <= 180) {
			x += (int) (halfWidth_1 + (int) (halfWidth_2 * Math.sin(Math.toRadians(angleus - 90))));
		} else if (angleus > 180 && angleus <= 270) {
			x += (int) (halfWidth_1 + (int) (halfWidth_2 * Math.sin(Math.toRadians(angleus - 90))));
		} else if (angleus > 270) {
			x += (int) (halfWidth_1 - (int) (halfWidth_2 * Math.sin(Math.toRadians(angleus - 270))));
		}
		g.drawString(toWrite, x, y);
	}

	public PreparedStatement prepareDeleteObject(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		return ps;
	}

	//"INSERT INTO Objects(Icon, Description, xPos, yPos, width, height, labelposition, background, area, level , textsize , font , textstyle, textangle, textcolour) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" }; //$NON-NLS-1$

	public PreparedStatement prepareInsertStatement(PreparedStatement ps) throws SQLException {
		Color backgroundColourOfObject = getBackground();
		String font = getFont().getName();
		if (font == null) font = "Dialog"; //$NON-NLS-1$
		if (backgroundColourOfObject == null) backgroundColourOfObject = MapPanel.getDefaultBackgroundColour();
		ps.setInt(1, getIconId());
		ps.setString(2, getLabel());
		ps.setInt(3, getxPos());
		ps.setInt(4, getyPos());
		ps.setInt(5, getWidth());
		ps.setInt(6, getHeight());
		ps.setInt(7, getLabelPosition());
		ps.setInt(8, backgroundColourOfObject.getRGB());
		ps.setInt(9, getAreaId());
		ps.setInt(10, getLevel());
		ps.setFloat(11, getFontSize());
		ps.setString(12, font);
		ps.setInt(13, getFontStyle());
		ps.setInt(14, getTextAngle());
		ps.setInt(15, getTextColour().getRGB());
		ps.setInt(16, (isLocked() ? 1 : 0));
		return ps;
	}

	//"UPDATE Objects SET Icon=?, Description=?, xPos=?, yPos=?, width=?, height=?, labelposition=?, background=?, area=?, level =?, textsize=?, font=?, textstyle=?, textangle=?, textcolour=? WHERE o_id=?" }; //$NON-NLS-1$
	public PreparedStatement prepareUpdateStatement(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getIconId());
		ps.setString(2, getLabel());
		ps.setInt(3, getxPos());
		ps.setInt(4, getyPos());
		ps.setInt(5, getWidth());
		ps.setInt(6, getHeight());
		ps.setInt(7, getLabelPosition());
		ps.setInt(8, (getBackground() != null ? getBackground().getRGB() : Color.GRAY.getRGB()));
		ps.setInt(9, getAreaId());
		ps.setInt(10, getLevel());
		ps.setFloat(11, getFontSize());
		ps.setString(12, getFont().getName());
		ps.setInt(13, getFontStyle());
		ps.setInt(14, getTextAngle());
		ps.setInt(15, getTextColour().getRGB());
		ps.setInt(16, (isLocked() ? 1 : 0));
		ps.setInt(17, getId());
		return ps;
	}

	/**
	 * @param areaId
	 *            the areaId to set
	 */
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	/**
	 * @param theFont
	 *            the theFont to set
	 */
	@Override
	public void setFont(Font theFont) {
		this.theFont = theFont;
	}

	public void setFontSize(float fontSize) {
		this.fontSize = fontSize;
	}

	public void setFontStyle(int fontStyle) {
		this.fontStyle = fontStyle;
	}

	public void setHeight(int height) {
		setSize(width, height);
	}

	/**
	 * 
	 * @param icon
	 */
	public void setIconId(int iconId) {
		this.iconId = iconId;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @param labelPosition
	 *            the labelPosition to set
	 */
	public void setLabelPosition(int labelPosition) {
		this.labelPosition = labelPosition;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @param normalized
	 *            the normalized to set
	 */
	public void setNormalized(boolean normalized) {
		this.normalized = normalized;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(Point position) {
		this.position = position;
	}

	@Override
	public void setPreferredSize(Dimension preferredSize) {
		super.setPreferredSize(preferredSize);
		this.width = (int) preferredSize.getWidth();
		this.height = (int) preferredSize.getHeight();
	}

	@Override
	public void setSize(int width, int height) {
		super.setSize(width, height);
		this.width = width;
		this.height = height;
	}

	public void setWidth(int width) {
		setSize(width, height);
	}

	/**
	 * 
	 * @param xPos
	 */
	public void setxPos(int xPos) {
		position.setLocation(xPos, position.getY());
	}

	/**
     * 
     */
	public void setyPos(int yPos) {
		position.setLocation(position.getX(), yPos);
	}

	public String toXml() {
		String toReturn = tab + "<mapobject>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<mapobjectlabel>" + getLabel() + "</mapobjectlabel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectlabelposition>" + getLabelPosition() + "</mapobjectlabelposition>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectlevel>" + getLevel() + "</mapobjectlevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectfont>" + getFont().getFontName() + "</mapobjectfont>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectfontsize>" + getFontSize() + "</mapobjectfontsize>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectfontstyle>" + getFontStyle() + "</mapobjectfontstyle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjecticonid>" + getIconId() + "</mapobjecticonid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectxpos>" + getxPos() + "</mapobjectxpos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectypos>" + getyPos() + "</mapobjectypos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectareaid>" + getAreaId() + "</mapobjectareaid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectid>" + getId() + "</mapobjectid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectwidth>" + width + "</mapobjectwidth>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectheight>" + height + "</mapobjectheight>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<mapobjectbackgroundcolour>" + getBackground().getRGB() + "</mapobjectbackgroundcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$

		toReturn += tab + "</mapobject>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * o_id, Icon, Description, xPos, yPos, width, height, labelposition,
	 * background, area, level , textsize , font , textstyle
	 * 
	 * @param ps
	 * @return ps
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatementForRestore(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setInt(2, getIconId());
		ps.setString(3, getLabel());
		ps.setInt(4, getxPos());
		ps.setInt(5, getyPos());
		ps.setInt(6, getWidth());
		ps.setInt(7, getHeight());
		ps.setInt(8, getLabelPosition());
		ps.setInt(9, getBackground().getRGB());
		ps.setInt(10, getAreaId());
		ps.setInt(11, getLevel());
		ps.setFloat(12, getFontSize());
		ps.setString(13, getFont().getName());
		ps.setInt(14, getFontStyle());
		return ps;
	}

	/**
	 * @return the textColour
	 */
	public Color getTextColour() {
		return textColour;
	}

	/**
	 * @param textColour
	 *            the textColour to set
	 */
	public void setTextColour(Color textColour) {
		this.textColour = textColour;
	}

	/**
	 * @return the textAngle
	 */
	public int getTextAngle() {
		return textAngle;
	}

	/**
	 * @param textAngle
	 *            the textAngle to set
	 */
	public void setTextAngle(int textAngle) {
		this.textAngle = textAngle;
	}

	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * @param locked
	 *            the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean intersects(Rectangle theAreaToShow) {
		return theAreaToShow.intersects(new Rectangle(getxPos(), getyPos(), width, height));
	}

}
