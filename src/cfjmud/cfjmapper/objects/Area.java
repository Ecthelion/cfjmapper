/**
 * CFJMapper - a stand-alone mapper made for mushClient
 * Copyright (C) 2013  Carl F Jansson a.k.a. Ecthelion
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.s
 */
package cfjmud.cfjmapper.objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import cfjmud.cfjmapper.Mapper;
import cfjmud.cfjmapper.Messages;
import cfjmud.cfjmapper.admin.MapperProperties;
import cfjmud.cfjmapper.database.Database;
import cfjmud.cfjmapper.dialogs.ThumbnailSizeDialog;
import cfjmud.cfjmapper.interfaces.MapperConstants;
import cfjmud.cfjmapper.interfaces.SQLConstants;
import cfjmud.cfjmapper.panels.MapPanel;
import cfjmud.cfjmapper.panels.MapView;

/**
 * 
 * @author Ecthelion
 * 
 */
public class Area implements MapperConstants, SQLConstants, Comparator<Area> {
	public static final String[]	TYPES			= {
			"None", "Square", "Rounded Square", "Oval/Circle", "Triangle", "Icon", "Thumbnail", "Triangle Pointing Down", "Diamond" };	//$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
	private boolean					alreadyPainted	= false;																			;
	private String					areaName;
	private Color					borderColour	= Color.BLACK;
	private boolean					borderRaised	= false;
	private int						borderThickness	= 1;
	private boolean					borderTitle		= false;
	private Color					colour			= Color.RED;
	String							dtab			= "\t\t";																			//$NON-NLS-1$
	private final String			eol				= "\n";																			//$NON-NLS-1$
	private Font					font;
	// private String alias;
	private int						id				= 0, parent = 0, theIcon = 0, type = 0, xPos, yPos, height, width, level, borderType = 0,
			labelPosition = 0, labelFontStyle = Font.PLAIN;
	String							label			= Messages.getString("Area.0");													//$NON-NLS-1$
	private Color					labelColour		= Color.BLACK;
	float							labelFontSize	= 10.0f;

	private int						preferredRoomDistance;
	private final Rectangle			rectangle;

	private Color					shadowColour	= Color.GRAY;

	private final String			tab				= "\t";																			//$NON-NLS-1$

	private int						roomRep;
	public static String			ROOMS			= "Rooms";																			//$NON-NLS-1$
	public static String			ROOMS_			= "Rooms_";																		//$NON-NLS-1$
	public static String			EXITS			= "Exits";																			//$NON-NLS-1$
	public static String			EXITS_			= "Exits_";																		//$NON-NLS-1$

	/**
	 * To Use when importing XML
	 */
	public Area() {
		rectangle = new Rectangle();
	}

	public Area(int id, String areaName, int parent, int type, int xPos, int yPos, int height, int width, int theIcon, Color colour, int borderType,
				Color borderColour, int borderThickness, Color shadowColour, boolean borderRaised, boolean borderTitle, String label, int labelPosition,
				int labelFontSize, Color labelColour, int labelFontStyle, int level, int preferredRoomDistance) {
		this.id = id;
		this.areaName = areaName;
		// this.alias = alias;
		this.parent = parent;
		this.type = type;
		this.xPos = xPos;
		this.yPos = yPos;
		this.height = height;
		this.width = width;
		this.theIcon = theIcon;
		this.borderType = borderType;
		this.borderThickness = borderThickness;
		this.rectangle = new Rectangle(xPos, yPos, width, height);
		this.borderRaised = borderRaised;
		this.borderTitle = borderTitle;
		if (borderColour != null) this.borderColour = borderColour;
		if (colour != null) this.colour = colour;
		if (shadowColour != null) this.shadowColour = shadowColour;
		this.label = label;
		this.labelPosition = labelPosition;
		this.labelFontSize = labelFontSize;
		this.labelFontStyle = labelFontStyle;
		this.labelColour = labelColour;
		this.font = new Font(Messages.getString("Area.8"), labelFontStyle, labelFontSize); //$NON-NLS-1$
		this.preferredRoomDistance = preferredRoomDistance;
		this.roomRep = 0;
	}

	/**
	 * a_id, Name, Parent, Type, xPos, yPos, Icon, Width, Height, Colour,
	 * BorderType, BorderColour, BorderThickness, BorderShadow , Raised,
	 * BorderTitle, LabelText, LabelPosition, LabelFontSize, LabelColour, Font
	 * 
	 * @param rs
	 */
	public Area(ResultSet rs) throws SQLException {
		this.id = rs.getInt("a_id"); //$NON-NLS-1$
		this.areaName = rs.getString("Name"); //$NON-NLS-1$
		this.parent = rs.getInt("Parent"); //$NON-NLS-1$
		this.type = rs.getInt("Type"); //$NON-NLS-1$
		this.xPos = rs.getInt("xPos"); //$NON-NLS-1$
		this.yPos = rs.getInt("yPos"); //$NON-NLS-1$
		this.theIcon = rs.getInt("Icon"); //$NON-NLS-1$
		this.width = rs.getInt("Width"); //$NON-NLS-1$
		this.height = rs.getInt("Height"); //$NON-NLS-1$
		this.colour = new Color(rs.getInt("Colour")); //$NON-NLS-1$
		this.borderType = rs.getInt("BorderType"); //$NON-NLS-1$
		this.borderColour = new Color(rs.getInt("BorderColour")); //$NON-NLS-1$
		this.borderThickness = rs.getInt("BorderThickness"); //$NON-NLS-1$
		this.shadowColour = new Color(rs.getInt("BorderShadow")); //$NON-NLS-1$
		this.borderRaised = (rs.getInt("Raised") == 1); //$NON-NLS-1$
		this.borderTitle = (rs.getInt("BorderTitle") == 1); //$NON-NLS-1$
		this.label = rs.getString("LabelText"); //$NON-NLS-1$
		this.labelPosition = rs.getInt("LabelPosition"); //$NON-NLS-1$
		this.labelFontSize = rs.getFloat("LabelFontSize"); //$NON-NLS-1$
		this.labelFontStyle = rs.getInt("LabelFontStyle"); //$NON-NLS-1$
		this.labelColour = new Color(rs.getInt("LabelColour")); //$NON-NLS-1$
		this.setFont(Font.decode(rs.getString("Font"))); //$NON-NLS-1$
		this.font = this.font.deriveFont(labelFontStyle, labelFontSize);
		this.rectangle = new Rectangle(xPos, yPos, width, height);
		this.setLevel(rs.getInt("Level")); //$NON-NLS-1$
		if (font == null) font = new Font("Dialog", Font.BOLD, 12); //$NON-NLS-1$
		this.preferredRoomDistance = rs.getInt("preferredRoomDistance"); //$NON-NLS-1$
		this.roomRep = rs.getInt("roomrep"); //$NON-NLS-1$
	}

	public Area(String name, int parent, int type, int xPos, int yPos) {
		this(0, name, parent, type, xPos, yPos, 100, 100, 0, Color.RED, 0, Color.BLACK, 0, Color.BLACK, false, false,
				Messages.getString("Area.9"), 0, 10, Color.BLACK, Font.PLAIN, 0, getPreferredDistanceFromProperties()); //$NON-NLS-1$
	}

	private static int getPreferredDistanceFromProperties() {
		int spaceBR = 35;
		String pdbr = MapperProperties.getInstance().getProperty(MapperProperties.SPACEBETWEENROOMS);
		if (pdbr == null) {
			MapperProperties.getInstance().setProperty(MapperProperties.SPACEBETWEENROOMS, "35"); //$NON-NLS-1$ 
		} else {
			spaceBR = Integer.parseInt(pdbr);
		}
		return spaceBR;

	}

	public Color getBorderColour() {
		return borderColour;
	}

	public boolean getBorderRaised() {
		return borderRaised;
	}

	public int getBorderThickness() {
		return borderThickness;
	}

	public boolean getBorderTitle() {
		return borderTitle;
	}

	public int getBorderType() {
		return borderType;
	}

	public Color getColour() {
		return colour;
	}

	/**
	 * @return the font
	 */
	public Font getFont() {
		return font;
	}

	public int getHeight() {
		return height;
	}

	public int getIcon() {
		return theIcon;
	}

	public int getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public Color getLabelColour() {
		return labelColour;
	}

	/*
	 * public String getAlias() { return alias; }
	 */

	public float getLabelFontSize() {
		return labelFontSize;
	}

	public int getLabelFontStyle() {
		return labelFontStyle;
	}

	public int getLabelPosition() {
		return labelPosition;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	public String getName() {
		return areaName;
	}

	public AreaPanel getPanel(Mapper mapper) {
		return new AreaPanel(mapper, this);
	}

	public int getParent() {
		return parent;
	}

	public Point getPosition() {
		return new Point(xPos, yPos);
	}

	/**
	 * @return the preferredRoomDistance
	 */
	public int getPreferredRoomDistance() {
		return preferredRoomDistance;
	}

	public Color getShadowColour() {
		return shadowColour;
	}

	public int getType() {
		return type;
	}

	public int getWidth() {
		return width;
	}

	public int getXPos() {
		return xPos;
	}

	public int getYPos() {
		return yPos;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	public boolean intersects(Rectangle rect) {
		return rect.intersects(rectangle);
	}

	public boolean isPainted() {
		return alreadyPainted;
	}

	public PreparedStatement preparedStatementForInsert(PreparedStatement ps) throws SQLException {
		return ps;
	}

	/*
	 * UPDATE Areas SET Name = ?, Parent = ?, Type = ?, xPos = ?, yPos = ?, Icon
	 * =?, " +
	 * "Width = ?, Height =?, Colour=?, BorderType=?, BorderColour=?, BorderThickness=?, "
	 * +
	 * "BorderShadow=?, Raised=? , BorderTitle=?, LabelText=?, LabelPosition=?, LabelFontSize=?, "
	 * + "LabelColour=?, Font=? WHERE a_id = ?"
	 */
	public PreparedStatement prepareStatementForUpdate(PreparedStatement ps) throws SQLException {
		ps.setString(1, getName());
		ps.setInt(2, getParent());
		ps.setInt(3, getType());
		ps.setInt(4, getXPos());
		ps.setInt(5, getYPos());
		ps.setInt(6, getIcon());
		ps.setInt(7, getWidth());
		ps.setInt(8, getHeight());
		ps.setInt(9, getColour().getRGB());
		ps.setInt(10, getBorderType());
		ps.setInt(11, getBorderColour().getRGB());
		ps.setInt(12, getBorderThickness());
		ps.setInt(13, getShadowColour().getRGB());
		ps.setInt(14, (getBorderRaised() ? 1 : 0));
		ps.setInt(15, (getBorderTitle() ? 1 : 0));
		ps.setString(16, getLabel());
		ps.setInt(17, getLabelPosition());
		ps.setFloat(18, getLabelFontSize());
		ps.setInt(19, getLabelColour().getRGB());
		ps.setString(20, font.getName());
		ps.setInt(21, font.getStyle());
		ps.setInt(22, getLevel());
		ps.setInt(23, getPreferredRoomDistance());
		ps.setInt(24, getRoomRep());
		ps.setInt(25, getId());
		return ps;
	}

	public void setBorderColour(Color borderColour) {
		this.borderColour = borderColour;
	}

	public void setBorderRaised(boolean borderRaised) {
		this.borderRaised = borderRaised;
	}

	public void setBorderThickness(int borderThickness) {
		this.borderThickness = borderThickness;
	}

	public void setBorderTitle(boolean borderTitle) {
		this.borderTitle = borderTitle;
	}

	/*
	 * public void setAlias(String alias) { this.alias = alias; }
	 */

	public void setBorderType(int borderType) {
		this.borderType = borderType;
	}

	public void setColour(Color colour) {
		this.colour = colour;
	}

	/**
	 * @param font
	 *            the font to set
	 */
	public void setFont(Font font) {
		this.font = font;
	}

	public void setHeight(int height) {
		this.height = height;
		rectangle.setSize(width, height);
	}

	public void setIcon(int theIcon) {
		this.theIcon = theIcon;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setLabelColour(Color labelColour) {
		this.labelColour = labelColour;
	}

	public void setLabelFontSize(int labelFontSize) {
		this.labelFontSize = labelFontSize;
	}

	public void setLabelFontStyle(int labelFontStyle) {
		this.labelFontStyle = labelFontStyle;
	}

	public void setLabelPosition(int labelPosition) {
		this.labelPosition = labelPosition;
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	public void setName(String areaName) {
		this.areaName = areaName;
	}

	public void setPainted(boolean alreadyPainted) {
		this.alreadyPainted = alreadyPainted;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public void setPosition(Point point) {
		xPos = (int) point.getX();
		yPos = (int) point.getY();
		rectangle.setLocation(xPos, yPos);
	}

	/**
	 * @param preferredRoomDistance
	 *            the preferredRoomDistance to set
	 */
	public void setPreferredRoomDistance(int preferredRoomDistance) {
		this.preferredRoomDistance = preferredRoomDistance;
	}

	public void setShadowColour(Color shadowColour) {
		this.shadowColour = shadowColour;
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
		rectangle.setSize(width, height);
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setWidth(int width) {
		this.width = width;
		rectangle.setSize(width, height);
	}

	public void setXPos(int xPos) {
		this.xPos = xPos;
		rectangle.setLocation(xPos, yPos);
	}

	public void setYPos(int yPos) {
		this.yPos = yPos;
		rectangle.setLocation(xPos, yPos);
	}

	@Override
	public String toString() {
		return areaName;
	}

	/**
	 * Creates xml from the instance.
	 * 
	 * @return xml version of the instance of an Area.
	 */
	public String toXml() {
		String toReturn = tab + "<area>" + eol; //$NON-NLS-1$
		toReturn += dtab + "<areaid>" + getId() + "</areaid>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaname>" + getName() + "</areaname>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areabordercolour>" + getBorderColour().getRGB() + "</areabordercolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaborderraised>" + getBorderRaised() + "</areaborderraised>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaborderthickness>" + getBorderThickness() + "</areaborderthickness>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areabordertitle>" + getBorderTitle() + "</areabordertitle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areabordertype>" + getBorderType() + "</areabordertype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areacolour>" + getColour().getRGB() + "</areacolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areafont>" + getFont().getFontName() + "</areafont>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaheight>" + getHeight() + "</areaheight>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areawidth>" + getWidth() + "</areawidth>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaicon>" + getIcon() + "</areaicon>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealabeltext>" + getLabel() + "</arealabeltext>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealabelcolour>" + getLabelColour().getRGB() + "</arealabelcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealabelfontsize>" + getLabelFontSize() + "</arealabelfontsize>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealabelfontstyle>" + getLabelFontStyle() + "</arealabelfontstyle>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealabelposition>" + getLabelPosition() + "</arealabelposition>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arealevel>" + getLevel() + "</arealevel>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaparent>" + getParent() + "</areaparent>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areashadowcolour>" + getShadowColour().getRGB() + "</areashadowcolour>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areatype>" + getType() + "</areatype>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaxpos>" + getXPos() + "</areaxpos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<areaypos>" + getYPos() + "</areaypos>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<preferredRoomDistance>" + getPreferredRoomDistance() + "</preferredRoomDistance>" + eol; //$NON-NLS-1$ //$NON-NLS-2$
		toReturn += dtab + "<arearoomrep>" + getRoomRep() + "</arearoomrep>" + eol; //$NON-NLS-1$ //$NON-NLS-2$

		toReturn += tab + "</area>" + eol; //$NON-NLS-1$
		return toReturn;
	}

	/**
	 * a_id, Name, Parent, Type, xPos, yPos, Icon, Width, Height, Colour,
	 * BorderType, BorderColour, BorderThickness, BorderShadow, Raised,
	 * BorderTitle, LabelText, LabelPosition, LabelFontSize, LabelColour, Font,
	 * LabelFontStyle, Level, preferredRoomDistance
	 * 
	 * @param ps
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatementForRestore(PreparedStatement ps) throws SQLException {
		ps.setInt(1, getId());
		ps.setString(2, getName());
		ps.setInt(3, getParent());
		ps.setInt(4, getType());
		ps.setInt(5, getXPos());
		ps.setInt(6, getYPos());
		ps.setInt(7, getIcon());
		ps.setInt(8, getWidth());
		ps.setInt(9, getHeight());
		ps.setInt(10, getColour().getRGB());
		ps.setInt(11, getBorderType());
		ps.setInt(12, getBorderColour().getRGB());
		ps.setInt(13, getBorderThickness());
		ps.setInt(14, getShadowColour().getRGB());
		ps.setInt(15, (getBorderRaised() ? 1 : 0));
		ps.setInt(16, (getBorderTitle() ? 1 : 0));
		ps.setString(17, getLabel());
		ps.setInt(18, getLabelPosition());
		ps.setFloat(19, getLabelFontSize());
		ps.setInt(20, getLabelColour().getRGB());
		ps.setString(21, font.getName());
		ps.setInt(22, font.getStyle());
		ps.setInt(23, getLevel());
		ps.setInt(24, getPreferredRoomDistance());
		ps.setInt(25, getRoomRep());
		return ps;
	}

	public String getCreateExitsTableStringForArea() {
		String toReturn = null;
		switch (Database.dbType) {
			case POSTG:
				toReturn = postgreSQLcreateExits.replaceAll(EXITS, EXITS_ + getId());
				break;
			case MYSQL:
				toReturn = mySQLcreateExits.replaceAll(EXITS, EXITS_ + getId());
				break;
			case DERBY:
				toReturn = javaDBcreateExits.replaceAll(EXITS, EXITS_ + getId());
				break;
		}
		return toReturn;
	}

	public String getCreateRoomsTableStringForArea() {
		String toReturn = null;
		switch (Database.dbType) {
			case POSTG:
				toReturn = postgreSQLcreateRooms.replaceAll(ROOMS, ROOMS_ + getId());
				break;
			case MYSQL:
				toReturn = mySQLcreateRooms.replaceAll(ROOMS, ROOMS_ + getId());
				break;
			case DERBY:
				toReturn = javaDBcreateRooms.replaceAll(ROOMS, ROOMS_ + getId());
				break;
		}
		return toReturn;
	}

	public String getCreateIndexOnExitsString() {
		String toReturn = postgreSQLcreateExitIndex.replaceAll("ExitIndex", "ExitIndex_" + getId()); //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn.replaceAll(EXITS, EXITS_ + getId());
	}

	public String getCreateIndexOnExitsOnRoomString() {
		String toReturn = postgreSQLcreateExitIndexOnRooms.replaceAll("ExitIndexOnRooms", "ExitIndexOnRooms_" + getId()); //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn.replaceAll(EXITS, EXITS_ + getId());
	}

	public String getCreateIndexOnRoomsString() {
		String toReturn = postgreSQLRoomIndex.replaceAll("RoomIndex", "RoomIndex_" + getId()); //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn.replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String getCreateIndexOnRoomNamesString() {
		String toReturn = postgreSQLRoomNameIndex.replaceAll("NameIndex", "NameIndex_" + getId()); //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn.replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createSelectExitsForAreaString() {
		return exitSelect2[1].replaceAll(EXITS, EXITS_ + getId()) + " UNION ALL " + exitSelect[1]; //$NON-NLS-1$ 
	}

	public String createSelectRoomsForAreaString() {
		return roomSelect[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createInsertExitsForAreaString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String createInsertRoomsForAreaString() {
		return roomInsert[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createUpdateExitForAreaString() {
		// TODO Auto-generated method stub
		return null;
	}

	public String createUpdateRoomForAreaString() {
		return roomUpdate[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createRoomFindForAreaString() {
		// TODO Auto-generated method stub
		return roomFind[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createOldestRoomFindString() {
		return oldestRoomInArea[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createDeleteRoomForAreaString() {
		return roomDelete[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createAreaDimensionString() {
		return areaDimension[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	// targetRoom=? OR sourceRoom=?
	public String createSelectExitsForRoomString() {
		return exitSelectForRoom[1].replaceAll(EXITS, EXITS_ + getId()) + " UNION ALL " //$NON-NLS-1$ 
				+ exitSelectForRoom[1].replaceAll("targetRoom=\\? OR sourceRoom=\\?", "targetArea=? AND targetRoom=? OR sourceArea=? AND sourceRoom=?") + ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	public String createInsertExitsString() {
		return exitInsert[1].replaceAll(EXITS, EXITS_ + getId());
	}

	public String createUpdateExitsString() {
		return exitUpdate[1].replaceAll(EXITS, EXITS_ + getId());
	}

	public String createDeleteExitsString() {
		return deleteExit[1].replaceAll(EXITS, EXITS_ + getId());
	}

	public String createTotalRoomsInAreaString() {
		return getTotalOfRoomsInArea[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createMaxRoomIdInAreaString() {
		return maxRoomId[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createGetRoomAtPointString() {
		return roomFindByCoordinates[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createDropRoomTableForAreaString() {
		return "DROP TABLE rooms_" + getId(); //$NON-NLS-1$
	}

	public String createDropExitsTableForAreaString() {
		return "DROP TABLE exits_" + getId(); //$NON-NLS-1$
	}

	public String createGetAllRoomsInAreaString() {
		return allRoomsForArea[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createGetAllExitsForAreaString() {
		String toReturn = allExitsForArea[1].replaceAll(EXITS, EXITS_ + getId());
		toReturn = toReturn.replaceAll(" WHERE sourceArea=\\? OR targetArea=\\?", ""); //$NON-NLS-1$ //$NON-NLS-2$
		return toReturn;
	}

	public String createRoomUpdateColourByNameString() {
		return roomUpdateColourByNameString[1].replaceAll(ROOMS, ROOMS_ + getId());
	}

	public String createZoneRoomsSelect() {
		return zoneRoomsSelect[1].replaceAll(" " + ROOMS, " " + ROOMS_ + getId()); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public String createDeleteDescriptionsInArea() {
		return deleteDescriptionsInArea[1].replaceAll(" " + ROOMS, " " + ROOMS_ + getId()); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public void saveAreaAsPicture(Mapper mapper) {
		Database database = Database.getInstance();
		MapperProperties properties = MapperProperties.getInstance();

		String radius = properties.getProperty(MapperProperties.RADIUS, MapperProperties.RADIUS_DEFAULT);

		int rad = Integer.parseInt(radius);
		FileFilter ff = new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if (pathname.isDirectory()) return true;
				if (pathname.getName().contains(".png")) return true; //$NON-NLS-1$
				return false;
			}

			@Override
			public String getDescription() {
				return "png files"; //$NON-NLS-1$
			}
		};
		JFileChooser chooser = new JFileChooser("."); //$NON-NLS-1$
		chooser.setFileFilter(ff);
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int res = chooser.showSaveDialog(mapper);
		if (JFileChooser.APPROVE_OPTION != res) return;
		File tmpIcon = chooser.getSelectedFile();
		if (tmpIcon == null) return;
		Rectangle rect = database.getAreaDimension(this);
		if (rect.width > rad || rect.height > rad) {

			int colums = rect.width / rad + 1;
			int rows = rect.height / rad + 1;
			int newWidth = rect.width / colums;
			int newHeight = rect.height / rows;
			MapView tmpMapView = new MapView(mapper);
			MapPanel tmpMap = new MapPanel(tmpMapView);
			tmpMap.setSize(rect.getSize());
			tmpMap.setMinimumSize(rect.getSize());
			tmpMap.setPreferredSize(rect.getSize());
			tmpMap.setBounds(rect);
			tmpMap.setCurrentLevel(0);
			tmpMap.setScale(1);
			tmpMap.setCurrentArea(this);
			tmpMap.setBackground(MapPanel.getDefaultBackgroundColour());
			Rectangle newRect = new Rectangle(rect.x, rect.y, newWidth, newHeight);
			tmpIcon.mkdir();
			String htmlFileName = tmpIcon.getAbsolutePath() + File.separatorChar + tmpIcon.getName() + ".html"; //$NON-NLS-1$
			File htmlFile = new File(htmlFileName);
			PrintStream ps = null;
			try {
				if (!htmlFile.exists()) htmlFile.createNewFile();
				ps = new PrintStream(htmlFile);
				ps.println(properties.getProperty(MapperProperties.HTML_START, MapperProperties.HTML_START_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_TABLE_START, MapperProperties.HTML_TABLE_START_DEFAULT));
				ps.println(properties.getProperty(MapperProperties.HTML_CAPTION_START, MapperProperties.HTML_CAPTION_START_DEFAULT) + getName()
							+ properties.getProperty(MapperProperties.HTML_CAPTION_END, MapperProperties.HTML_CAPTION_END_DEFAULT));
			} catch (IOException e1) {
				System.out.println("Oups! Failed to create the html-file in save as picture."); //$NON-NLS-1$
				e1.printStackTrace();
			}

			for (int r = 0; r < rows; r++) {
				newRect.setLocation(rect.x, rect.y + r * newHeight);
				ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TD_START, MapperProperties.HTML_TABLE_TR_START_DEFAULT));
				for (int c = 0; c < colums; c++) {
					newRect.setLocation(rect.x + c * newWidth, newRect.y);
					String newFileName = tmpIcon.getAbsolutePath() + File.separatorChar + tmpIcon.getName() + "_" + r + "_" + c + ".png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					String newThumbFileName = tmpIcon.getAbsolutePath() + File.separatorChar + tmpIcon.getName() + "_" + r + "_" + c + "_thumbnail.png"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					File newFile = new File(newFileName);
					tmpMap.setSize(newRect.getSize());
					tmpMap.setMinimumSize(newRect.getSize());
					tmpMap.setPreferredSize(newRect.getSize());
					tmpMap.setBounds(newRect);
					tmpMap.setViewableRectangle(newRect);
					tmpMap.updateObjects();
					BufferedImage image = new BufferedImage(newRect.width, newRect.height, BufferedImage.TYPE_INT_RGB);
					Graphics2D g2d = image.createGraphics();

					g2d.setColor(MapPanel.getDefaultBackgroundColour());
					g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
					tmpMap.drawZones(g2d);
					g2d.translate(-newRect.x, -newRect.y);
					tmpMap.drawStuff(g2d);
					g2d.translate(newRect.x, newRect.y);
					tmpMap.drawZoneLabels(g2d);
					/*
					 * tmpMap.drawObjects(g2d); tmpMap.drawAreas(g2d);
					 * tmpMap.drawExits(g2d); tmpMap.drawRooms(g2d);
					 */
					g2d.dispose();
					double diff = (double) newHeight / newWidth;
					Image tmpnail = image.getScaledInstance(100, (int) (100 * diff), Image.SCALE_SMOOTH);
					BufferedImage thumbnail = new BufferedImage(tmpnail.getWidth(null), tmpnail.getHeight(null), BufferedImage.TYPE_INT_RGB);
					Graphics2D biG2 = (Graphics2D) thumbnail.getGraphics();
					biG2.drawImage(tmpnail, 0, 0, null);
					biG2.dispose();
					try {
						if (image != null) {
							File thumbFile = new File(newThumbFileName);
							ImageIO.write(image, "png", newFile); //$NON-NLS-1$
							ImageIO.write(thumbnail, "png", thumbFile); //$NON-NLS-1$
							ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TD_START, MapperProperties.HTML_TABLE_TD_START_DEFAULT));
							ps.println("<a href=\"" + newFile.getName() + "\" target=\"_blank\">"); //$NON-NLS-1$ //$NON-NLS-2$
							ps.println("<img src=\"" //$NON-NLS-1$
										+ thumbFile.getName()
										+ "\" alt=\"Part: " + newFile.getName() + " of the map\" title=\"Click to view\" border=\"0\" width=\"150\" height=\"150\" hspace=\"0\" /></a>"); //$NON-NLS-1$ //$NON-NLS-2$
							ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TD_END, MapperProperties.HTML_TABLE_TD_END_DEFAULT));
						}
					} catch (IOException e) {
						System.out.println("Humpf! Couldn't not write to file in saveAreaAsPicture(Area): " + e.toString()); //$NON-NLS-1$
						e.printStackTrace();
					}
				}
				ps.println(properties.getProperty(MapperProperties.HTML_TABLE_TR_END, MapperProperties.HTML_TABLE_TR_END_DEFAULT));
			}
			ps.println(properties.getProperty(MapperProperties.HTML_END, MapperProperties.HTML_END_DEFAULT));
			ps.close();

		} else {
			if (tmpIcon.getName().split("\\.").length == 1) { //$NON-NLS-1$
				tmpIcon = new File(tmpIcon.getAbsolutePath() + ".png"); //$NON-NLS-1$
			}
			rect.setLocation(rect.x - getPreferredRoomDistance() * 2, rect.y - getPreferredRoomDistance() * 2);
			rect.width = rect.width + getPreferredRoomDistance() * 4;
			rect.height = rect.height + getPreferredRoomDistance() * 4;

			MapView tmpMapView = new MapView(mapper);
			MapPanel tmpMap = new MapPanel(tmpMapView);
			tmpMap.setSize(rect.getSize());
			tmpMap.setMinimumSize(rect.getSize());
			tmpMap.setPreferredSize(rect.getSize());
			tmpMap.setBounds(rect);
			tmpMap.setCurrentLevel(0);
			tmpMap.setScale(1);
			tmpMap.setCurrentArea(this);
			tmpMap.setViewableRectangle(rect);
			tmpMap.setBackground(MapPanel.getDefaultBackgroundColour());
			tmpMap.updateObjects();

			BufferedImage image = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = image.createGraphics();

			g2d.setColor(MapPanel.getDefaultBackgroundColour());
			g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
			tmpMap.drawZones(g2d);
			g2d.translate(-rect.x, -rect.y);
			tmpMap.drawStuff(g2d);
			g2d.translate(rect.x, rect.y);
			tmpMap.drawZoneLabels(g2d);
			/*
			 * tmpMap.drawObjects(g2d); tmpMap.drawAreas(g2d);
			 * tmpMap.drawExits(g2d); tmpMap.drawRooms(g2d);
			 */
			g2d.dispose();
			try {
				if (image != null) {
					ImageIO.write(image, "png", tmpIcon); //$NON-NLS-1$
				}
			} catch (Exception e) {
				System.out.println("Humpf! Couldn't write File in Area.java: " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
	}

	public void saveAreaAsThumbnail(Mapper mapper) {
		MapperProperties properties = MapperProperties.getInstance();
		String radius = properties.getProperty(MapperProperties.RADIUS, MapperProperties.RADIUS_DEFAULT);

		int rad = Integer.parseInt(radius);
		Database database = Database.getInstance();
		Rectangle rect = database.getAreaDimension(this);
		if (rect.width > rad || rect.height > rad) {
			JOptionPane.showMessageDialog(mapper, "For the moment one cannot save Too Large Areas as Thumbnail."); //$NON-NLS-1$
		} else {
			Rectangle newSize = ThumbnailSizeDialog.getThumbnailSize(mapper, rect);
			if (newSize == null) {
				System.out.println("No Thumbnail For You!"); //$NON-NLS-1$
				return;
			}
			System.out.println("Thumbnail Rectangle: " + newSize); //$NON-NLS-1$
			System.out.println("Original Rectangle: " + rect); //$NON-NLS-1$
			FileFilter ff = new FileFilter() {

				@Override
				public boolean accept(File pathname) {
					if (pathname.isDirectory()) return true;
					if (pathname.getName().contains(".png")) return true; //$NON-NLS-1$
					return false;
				}

				@Override
				public String getDescription() {
					return "png files"; //$NON-NLS-1$
				}
			};
			JFileChooser chooser = new JFileChooser("."); //$NON-NLS-1$
			chooser.setFileFilter(ff);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int res = chooser.showSaveDialog(mapper);
			if (JFileChooser.APPROVE_OPTION != res) return;
			File file = chooser.getSelectedFile();
			if (file == null) return;
			if (file.getName().split("\\.").length == 1) { //$NON-NLS-1$
				file = new File(file.getAbsolutePath() + ".png"); //$NON-NLS-1$
			}
			rect.setLocation(rect.x - getPreferredRoomDistance() * 2, rect.y - getPreferredRoomDistance() * 2);
			rect.width = rect.width + getPreferredRoomDistance() * 4;
			rect.height = rect.height + getPreferredRoomDistance() * 4;

			MapView tmpMapView = new MapView(mapper);
			MapPanel tmpMap = new MapPanel(tmpMapView);
			tmpMap.setSize(rect.getSize());
			tmpMap.setMinimumSize(rect.getSize());
			tmpMap.setPreferredSize(rect.getSize());
			tmpMap.setBounds(rect);
			tmpMap.setCurrentLevel(0);
			tmpMap.setScale(1);
			tmpMap.setCurrentArea(this);
			tmpMap.setViewableRectangle(rect);
			tmpMap.setBackground(MapPanel.getDefaultBackgroundColour());
			tmpMap.updateObjects();

			BufferedImage image = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = image.createGraphics();

			g2d.setColor(MapPanel.getDefaultBackgroundColour());
			g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
			tmpMap.drawZones(g2d);
			g2d.translate(-rect.x, -rect.y);
			tmpMap.drawStuff(g2d);
			g2d.translate(rect.x, rect.y);
			tmpMap.drawZoneLabels(g2d);
			/*
			 * tmpMap.drawObjects(g2d); tmpMap.drawAreas(g2d);
			 * tmpMap.drawExits(g2d); tmpMap.drawRooms(g2d);
			 */
			g2d.dispose();
			try {
				if (image != null) {
					Image i = image.getScaledInstance((int) newSize.getWidth(), (int) newSize.getHeight(), BufferedImage.SCALE_SMOOTH);
					BufferedImage bi = new BufferedImage(i.getWidth(null), i.getHeight(null), BufferedImage.TYPE_INT_RGB);
					Graphics g = bi.getGraphics();
					g.drawImage(i, 0, 0, null);
					ImageIO.write(bi, "png", file); //$NON-NLS-1$
				}
			} catch (Exception e) {
				System.out.println("Humpf! Couldn't write File in saveAreaAsThumbnail (Area.java): " + e.toString()); //$NON-NLS-1$
				e.printStackTrace();
			}
		}
	}

	public boolean contains(Room currentRoom) {
		return currentRoom.getAreaId() == getId();
	}

	/**
	 * @return the roomRep
	 */
	public int getRoomRep() {
		return roomRep;
	}

	/**
	 * @param roomRep
	 *            the roomRep to set
	 */
	public void setRoomRep(int roomRep) {
		this.roomRep = roomRep;
	}

	@Override
	public int compare(Area o1, Area o2) {
		return Integer.compare(o1.getId(), o2.getId());
	}
}
